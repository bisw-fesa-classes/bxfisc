/*
 * SpeedHandlerFactory.h
 *
 *  Created on: Mar 15, 2021
 *      Author: arlis
 */

#ifndef SRC_BXFISC_COMMON_SPEEDHANDLERFACTORY_H_
#define SRC_BXFISC_COMMON_SPEEDHANDLERFACTORY_H_

#include <memory>
#include <BXFISC/Common/ISpeedHandler.h>
#include <BXFISC/GeneratedCode/TypeDefinition.h>
#include <BXFISC/GeneratedCode/Device.h>

namespace NsSpeedConverter
{

class SpeedHandlerFactory
{
public:
    SpeedHandlerFactory() = default;
    virtual ~SpeedHandlerFactory() = default;

    std::unique_ptr<ISpeedHandler> get(const BXFISC::EVENTS::EVENTS ev, BXFISC::Device * d, const fesa::MultiplexingContext * c);


};

} /* namespace NsSpeedConverter */

#endif /* SRC_BXFISC_COMMON_SPEEDHANDLERFACTORY_H_ */
