#ifndef COMPO_REG_HPP
#define COMPO_REG_HPP

#include <memory>
#include <array>
#include <XFISC/XFISC.hpp>

namespace NsRegistry {

class ComponentRegistry {

public:
    using Card = NsXFISC::XFISC;
    using CardPtr = std::shared_ptr<Card>;


    static ComponentRegistry & instance();
    CardPtr get(const uint32_t driverNumber);

    ComponentRegistry(const ComponentRegistry & other) = delete;
    ComponentRegistry(ComponentRegistry && other) = delete;
    void operator=(const ComponentRegistry & other) = delete;
    void operator=(ComponentRegistry && other) = delete;
private:
    ComponentRegistry();
    ~ComponentRegistry() = default;

    std::array<CardPtr, 16> registry;
};


}

#endif
