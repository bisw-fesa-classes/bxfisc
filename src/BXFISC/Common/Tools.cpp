//#include <BXFISCDevice.h>
//#include <BXFISCGlobalStore.h>

#include <fesa-core/Exception/FesaException.h>
#include <BXFISC/RealTime/MonitorAction.h>

#include <fesa-core/RealTime/RTEvent.h>

#include <BXFISC/GeneratedCode/ServiceLocator.h>

#include <time.h>
#include <BXFISC/GeneratedCode/Device.h>
#include <BXFISC/GeneratedCode/GenRTActions.h>
#include <fesa-core/Exception/FesaException.h>
#include <BXFISC/Common/LowLevel.h>

#include "Tools.h"
#include <iostream>

#include <BXFISC/Common/ComponentRegistry.hpp>
#include <ADA08/PulseModes.hpp>

#include <cmw-log/Logger.h>
namespace
{

CMW::Log::Logger& logger = CMW::Log::LoggerFactory::getLogger("FESA.USR.BXFISC.RealTime.EeEventAction");

} // namespace

// INPUT fields: 
// OUTPUT fields: 

using namespace BXFISC;
//using namespace DEVICE_LOWLEVEL_ACTION ;

namespace BXFISC_TOOL
{
Tools::Tools()
{
}

Tools::~Tools()
{

}
HARDWARE_ERRORS::HARDWARE_ERRORS Tools::ToDefaults(Device *pWorkingDevice, const MultiplexingContext *pContext)
{
    HARDWARE_ERRORS::HARDWARE_ERRORS hardWareError = HARDWARE_ERRORS::NONE;
    hardWareError = Tools::VoltagePulseExtTo(pWorkingDevice->moduleLedPulse_off.get(pContext), pWorkingDevice,
            pContext);
    hardWareError = Tools::VoltagePedestalTo(pWorkingDevice->moduleLedPiedestal_off.get(pContext), pWorkingDevice,
            pContext);
    Tools::VoltageHighPm1To(pWorkingDevice->highVoltagePm1_default.get(pContext), pWorkingDevice, pContext);
    Tools::VoltageHighPm2To(pWorkingDevice->highVoltagePm2_default.get(pContext), pWorkingDevice, pContext);
    Tools::VoltageThresholdPm1To(pWorkingDevice->moduleThresholdPm1_default.get(pContext), pWorkingDevice, pContext);
    Tools::VoltageThresholdPm2To(pWorkingDevice->moduleThresholdPm2_default.get(pContext), pWorkingDevice, pContext);
    Tools::PositionSet(pWorkingDevice->motorPosition_default.get(pContext), true, pWorkingDevice, pContext);
    Tools::DelaySet(pWorkingDevice, pContext, 0, pWorkingDevice->delayPm1_default.get(pContext),
            pWorkingDevice->delayWidthPm1_default.get(pContext));
    Tools::DelaySet(pWorkingDevice, pContext, 1, pWorkingDevice->delayPm2_default.get(pContext),
            pWorkingDevice->delayWidthPm2_default.get(pContext));


//	Tools::EmcPhaRegSet(pWorkingDevice, pContext,(EMCREG::EMCREG)pWorkingDevice->emcPhaReg_default.get(pContext));
//	Tools::EmcGainSet(pWorkingDevice, pContext,pWorkingDevice->emcGain_default.get(pContext));
//	pWorkingDevice->delayPm1.set(pWorkingDevice->delayPm1_default.get(pContext));
//	pWorkingDevice->delayPm1_setting.set(pWorkingDevice->delayPm1_default.get(pContext));
//	pWorkingDevice->delayPm2.set(pWorkingDevice->delayPm2_default.get(pContext));
//	pWorkingDevice->delayPm2_setting.set(pWorkingDevice->delayPm2_default.get(pContext));
    return hardWareError;
}

HARDWARE_ERRORS::HARDWARE_ERRORS Tools::AAsGet(uint16_t *aaShort, float *aaFloat, Device *pWorkingDevice,
        const MultiplexingContext *pContext)
{
//	LowLevel::AAVerbose(pWorkingDevice->driverNumber.get(pContext));	
    LowLevel::AAGet(pWorkingDevice->driverNumber.get(pContext), aaShort, aaFloat);
//	LowLevel::AASilent(pWorkingDevice->driverNumber.get(pContext));
    pWorkingDevice->highVoltagePm1.set(aaFloat[0], pContext);
    pWorkingDevice->highVoltagePm2.set(aaFloat[1], pContext);

    pWorkingDevice->moduleThresholdPm1.set(aaFloat[2], pContext);
    pWorkingDevice->moduleThresholdPm2.set(aaFloat[3], pContext);

    pWorkingDevice->highVoltagePm1_current.set(aaFloat[4], pContext);
    pWorkingDevice->highVoltagePm2_current.set(aaFloat[5], pContext);
    pWorkingDevice->motorCurrent.set(aaFloat[6], pContext);
    pWorkingDevice->module_mpx.set(aaFloat[7], pContext);
    return HARDWARE_ERRORS::NONE;
}

HARDWARE_ERRORS::HARDWARE_ERRORS Tools::AAsGetMpx(uint16_t *aaShort, float *aaFloat, Device *pWorkingDevice,
        const MultiplexingContext *pContext)
{

    LowLevel::AAVerbose(pWorkingDevice->driverNumber.get(pContext));
    LowLevel::AAGetMpx(pWorkingDevice->driverNumber.get(pContext), aaShort, aaFloat);
    LowLevel::AASilent(pWorkingDevice->driverNumber.get(pContext));
    return HARDWARE_ERRORS::NONE;
}

void Tools::DacConvertToHexChars(char *dacChars, short dac)
{
    sprintf(dacChars, "%x", dac);
}

void Tools::AAConvertToHexChars(char *aaChars, short aa)
{
    sprintf(aaChars, "%x", aa);
}

HARDWARE_ERRORS::HARDWARE_ERRORS Tools::RegBaseGet(REGBASE::REGBASE *baseReg, Device *pWorkingDevice,
        const MultiplexingContext *pContext)
{
    uint16_t baseRegShort;
    int status = pWorkingDevice->deviceDetailedStatus.get(pContext);
    if (((int) status & DEVICE_DETAILED_STATUS::MODULE_ON) == (int) DEVICE_DETAILED_STATUS::MODULE_ON)
    {
        LowLevel::BaseRegGet(pWorkingDevice->driverNumber.get(pContext), &baseRegShort);
        *baseReg = (REGBASE::REGBASE) baseRegShort;
    }

    return HARDWARE_ERRORS::NONE;
}
HARDWARE_ERRORS::HARDWARE_ERRORS Tools::RegCoinGet(REGCOIN::REGCOIN *regCoin, Device *pWorkingDevice,
        const MultiplexingContext *pContext)
{
    uint16_t regShort;
    int status = pWorkingDevice->deviceDetailedStatus.get(pContext);
    if (((int) status & DEVICE_DETAILED_STATUS::MODULE_ON) == (int) DEVICE_DETAILED_STATUS::MODULE_ON)
    {
        LowLevel::RegCoinGet(pWorkingDevice->driverNumber.get(pContext), &regShort);
        *regCoin = (REGCOIN::REGCOIN) regShort;
    }

    return HARDWARE_ERRORS::NONE;
}

HARDWARE_ERRORS::HARDWARE_ERRORS Tools::RegMotorGet(REGMOTOR::REGMOTOR *reg, Device *pWorkingDevice,
        const MultiplexingContext *pContext)
{
    uint16_t regShort;
    int status = pWorkingDevice->deviceDetailedStatus.get(pContext);
    if (((int) status & DEVICE_DETAILED_STATUS::MODULE_ON) == (int) DEVICE_DETAILED_STATUS::MODULE_ON)
    {
        LowLevel::RegMotorGet(pWorkingDevice->driverNumber.get(pContext), &regShort);
        *reg = (REGMOTOR::REGMOTOR) regShort;
    }

    return HARDWARE_ERRORS::NONE;
}

HARDWARE_ERRORS::HARDWARE_ERRORS Tools::RegBaseSet(uint16_t reg, Device *pWorkingDevice,
        const MultiplexingContext *pContext, bool force)
{
    int status = pWorkingDevice->deviceDetailedStatus.get(pContext);
    if (force || (((int) status & DEVICE_DETAILED_STATUS::MODULE_ON) == (int) DEVICE_DETAILED_STATUS::MODULE_ON))
    {
        pWorkingDevice->moduleHwRegBase_setting.set(reg, pContext);
        LowLevel::RegBaseSet(pWorkingDevice->driverNumber.get(pContext), (uint16_t) reg);
    }
    return HARDWARE_ERRORS::NONE;
}

HARDWARE_ERRORS::HARDWARE_ERRORS Tools::RegCoinSet(REGCOIN::REGCOIN reg, Device *pWorkingDevice,
        const MultiplexingContext *pContext)
{
    int status = pWorkingDevice->deviceDetailedStatus.get(pContext);
    if (((int) status & DEVICE_DETAILED_STATUS::MODULE_ON) == (int) DEVICE_DETAILED_STATUS::MODULE_ON)
    {
        pWorkingDevice->moduleHwRegCoin_setting.set(reg, pContext);
        LowLevel::RegCoinSet(pWorkingDevice->driverNumber.get(pContext), (uint16_t) reg);
    }
    return HARDWARE_ERRORS::NONE;
}
HARDWARE_ERRORS::HARDWARE_ERRORS Tools::RegMotorSet(REGMOTOR::REGMOTOR reg, Device *pWorkingDevice,
        const MultiplexingContext *pContext)
{
    int status = pWorkingDevice->deviceDetailedStatus.get(pContext);
    pWorkingDevice->moduleHwRegMotor_setting.set(reg, pContext);
    if (((int) status & DEVICE_DETAILED_STATUS::MODULE_ON) == (int) DEVICE_DETAILED_STATUS::MODULE_ON)
    {
        LowLevel::RegMotorSet(pWorkingDevice->driverNumber.get(pContext), (uint16_t) reg);
    }
    return HARDWARE_ERRORS::NONE;
}

HARDWARE_ERRORS::HARDWARE_ERRORS Tools::CountsGet(long *counts, Device *pWorkingDevice,
        const MultiplexingContext *pContext)
{

    double countsD[4];

    HARDWARE_ERRORS::HARDWARE_ERRORS hardWareError = HARDWARE_ERRORS::NONE;
    LowLevel::Counters(pWorkingDevice->driverNumber.get(pContext), countsD);
    for (int i = 0; i < 4; i++)
    {
        counts[i] = (long) countsD[i];
    }
    //if ( ((int)currentHwStatus&DEVICE_HW_STATUS::DEBUG_LOW_OFF)!=(int)DEVICE_HW_STATUS::DEBUG_LOW_OFF )
    return HARDWARE_ERRORS::NONE;
}

HARDWARE_ERRORS::HARDWARE_ERRORS Tools::DacsGet(uint16_t *dacShorts, float *dacFloats, Device *pWorkingDevice,
        const MultiplexingContext *pContext)
{

    LowLevel::DacVerbose(pWorkingDevice->driverNumber.get(pContext));
    LowLevel::DacsGet(dacShorts, dacFloats, pWorkingDevice->driverNumber.get(pContext));
    LowLevel::DacSilent(pWorkingDevice->driverNumber.get(pContext));

    HARDWARE_ERRORS::HARDWARE_ERRORS flag = HARDWARE_ERRORS::NONE;
    return flag;
}
HARDWARE_ERRORS::HARDWARE_ERRORS Tools::DacsMpcGet(uint16_t *dacShorts, float *dacFloats, Device *pWorkingDevice,
        const MultiplexingContext *pContext)
{
    LowLevel::DacVerbose(pWorkingDevice->driverNumber.get(pContext));
    LowLevel::DacsMpcGet(dacShorts, dacFloats, pWorkingDevice->driverNumber.get(pContext));
    LowLevel::DacSilent(pWorkingDevice->driverNumber.get(pContext));
    HARDWARE_ERRORS::HARDWARE_ERRORS flag = HARDWARE_ERRORS::NONE;
    return flag;
}
HARDWARE_ERRORS::HARDWARE_ERRORS Tools::DacsBaseGet(uint16_t *dacShorts, float *dacFloats, Device *pWorkingDevice,
        const MultiplexingContext *pContext)
{
    LowLevel::DacVerbose(pWorkingDevice->driverNumber.get(pContext));
    LowLevel::DacsBaseGet(dacShorts, dacFloats, pWorkingDevice->driverNumber.get(pContext));
    LowLevel::DacSilent(pWorkingDevice->driverNumber.get(pContext));
    HARDWARE_ERRORS::HARDWARE_ERRORS flag = HARDWARE_ERRORS::NONE;
    return flag;
}

DEVICE_MODE::DEVICE_MODE Tools::DeviceMode(Device *pWorkingDevice, const MultiplexingContext *pContext)
{

// just copies

    DEVICE_MODE::DEVICE_MODE deviceMode = DEVICE_MODE::UNKNOWN;
    DEVICE_DETAILED_STATUS::DEVICE_DETAILED_STATUS detailState =
            (DEVICE_DETAILED_STATUS::DEVICE_DETAILED_STATUS) pWorkingDevice->deviceDetailedStatus.get(pContext);
    if ((int) (detailState & DEVICE_DETAILED_STATUS::ON) == (int) DEVICE_DETAILED_STATUS::ON) deviceMode =
            DEVICE_MODE::ON;
//	if  ( (int)(detailState&DEVICE_DETAILED_STATUS::VALUES_AT_DEFAULTS)		!=(int)DEVICE_DETAILED_STATUS::VALUES_AT_DEFAULTS ) 	deviceMode= DEVICE_MODE::UNKNOWN; 
//	if  ( (int)(detailState&DEVICE_DETAILED_STATUS::VALUES_AT_SETTINGS)		!=(int)DEVICE_DETAILED_STATUS::VALUES_AT_SETTINGS ) 	deviceMode= DEVICE_MODE::UNKNOWN; 
    if ((int) (detailState & DEVICE_DETAILED_STATUS::STANDBY_NO) != (int) DEVICE_DETAILED_STATUS::STANDBY_NO) deviceMode =
            DEVICE_MODE::STANDBY;
    if ((int) (detailState & DEVICE_DETAILED_STATUS::INITIALIZING_NO) != (int) DEVICE_DETAILED_STATUS::INITIALIZING_NO) deviceMode =
            DEVICE_MODE::INITIALIZING;
    if ((int) (detailState & DEVICE_DETAILED_STATUS::UNKNOWN_NO) != (int) DEVICE_DETAILED_STATUS::UNKNOWN_NO) deviceMode =
            DEVICE_MODE::UNKNOWN;
    if ((int) (detailState & DEVICE_DETAILED_STATUS::OFF_NO) != (int) DEVICE_DETAILED_STATUS::OFF_NO) deviceMode =
            DEVICE_MODE::OFF;
    return deviceMode;

}

DEVICE_CONTROL::DEVICE_CONTROL Tools::DeviceControl(Device *pWorkingDevice, const MultiplexingContext *pContext)
{
// just copies

    DEVICE_CONTROL::DEVICE_CONTROL deviceControl = DEVICE_CONTROL::LOCAL;
    DEVICE_DETAILED_STATUS::DEVICE_DETAILED_STATUS detailState =
            (DEVICE_DETAILED_STATUS::DEVICE_DETAILED_STATUS) pWorkingDevice->deviceDetailedStatus.get(pContext);

    if ((int) (detailState & DEVICE_DETAILED_STATUS::REMOTE) == (int) DEVICE_DETAILED_STATUS::REMOTE
            && (int) (detailState & DEVICE_DETAILED_STATUS::LOCAL_NO) == (int) DEVICE_DETAILED_STATUS::LOCAL_NO)
    {
        deviceControl = DEVICE_CONTROL::REMOTE;
    }
    return deviceControl;
}

DEVICE_STATUS::DEVICE_STATUS Tools::DeviceStatus(Device *pWorkingDevice, const MultiplexingContext *pContext)
{
// just copies
    DEVICE_STATUS::DEVICE_STATUS deviceStatus = DEVICE_STATUS::INFO;
    DEVICE_DETAILED_STATUS::DEVICE_DETAILED_STATUS detailState =
            (DEVICE_DETAILED_STATUS::DEVICE_DETAILED_STATUS) pWorkingDevice->deviceDetailedStatus.get(pContext);
    DEVICE_HW_STATUS::DEVICE_HW_STATUS hwState =
            (DEVICE_HW_STATUS::DEVICE_HW_STATUS) pWorkingDevice->deviceHwStatus.get(pContext);

    if ((int) (detailState & DEVICE_DETAILED_STATUS::OK) == (int) DEVICE_DETAILED_STATUS::OK) deviceStatus =
            DEVICE_STATUS::INFO;
    if ((int) (detailState & DEVICE_DETAILED_STATUS::UNKNOWN_NO) != (int) DEVICE_DETAILED_STATUS::UNKNOWN_NO) deviceStatus =
            DEVICE_STATUS::INFO;
    if ((int) (detailState & DEVICE_DETAILED_STATUS::WARNING_NO) != (int) DEVICE_DETAILED_STATUS::WARNING_NO) deviceStatus =
            DEVICE_STATUS::WARNING;
    if ((int) (detailState & DEVICE_DETAILED_STATUS::ERROR_NO) != (int) DEVICE_DETAILED_STATUS::ERROR_NO) deviceStatus =
            DEVICE_STATUS::ERROR;
    if ((int) (hwState && DEVICE_HW_STATUS::STATUS_NOT_FORCED_TO_OK) != (int) DEVICE_HW_STATUS::STATUS_NOT_FORCED_TO_OK) deviceStatus =
            DEVICE_STATUS::INFO;

    return deviceStatus;

}

HARDWARE_ERRORS::HARDWARE_ERRORS Tools::DelayInfo(Device *pWorkingDevice, const MultiplexingContext *pContext,
        double *delay, double *delay_setting, double *delayWidth, double *delayWidth_setting, double *delayRanges_max,
        double *delayRanges_min, double *delayWidthRanges_max, double *delayWidthRanges_min)
{
    using NsRegistry::ComponentRegistry;
    const auto phosRanges = ComponentRegistry::instance().get(pWorkingDevice->driverNumber.get(pContext))->getPHOSRanges();

    /* Replace with my components library (see it on cern github) */
    const auto rangeChannelOne = phosRanges[0][0];
    const auto rangeChannelTwo = phosRanges[1][0];

//    LowLevel::DelayInfo(pWorkingDevice->driverNumber.get(pContext), 0, delay, delay_setting, delayWidth,
//            delayWidth_setting, delayRanges_max, delayRanges_min, delayWidthRanges_max, delayWidthRanges_min);

    pWorkingDevice->delayPm1_max.set(rangeChannelOne.getDelayMax(), pContext);
    pWorkingDevice->delayPm1_min.set(rangeChannelOne.getDelayMin(), pContext);
    pWorkingDevice->delayWidthPm1_max.set(rangeChannelOne.getWidthMax(), pContext);
    pWorkingDevice->delayWidthPm1_min.set(rangeChannelOne.getWidthMin(), pContext);

//    LowLevel::DelayInfo(pWorkingDevice->driverNumber.get(pContext), 1, (delay + 1), (delay_setting + 1),
//            (delayWidth + 1), delayWidth_setting, (delayRanges_max + 1), (delayRanges_min + 1),
//            (delayWidthRanges_max + 1), (delayWidthRanges_min + 1));

    pWorkingDevice->delayPm2_max.set(rangeChannelTwo.getDelayMax(), pContext);
    pWorkingDevice->delayPm2_min.set(rangeChannelTwo.getDelayMin(), pContext);
    pWorkingDevice->delayWidthPm2_max.set(rangeChannelTwo.getWidthMax(), pContext);
    pWorkingDevice->delayWidthPm2_min.set(rangeChannelTwo.getWidthMin(), pContext);

    return HARDWARE_ERRORS::NONE;
}

HARDWARE_ERRORS::HARDWARE_ERRORS Tools::DelaySet(Device *pWorkingDevice, const MultiplexingContext *pContext,
        int channel, double delayValue, double widthValue)
{
    using NsRegistry::ComponentRegistry;

    try {
        ComponentRegistry::instance().get(pWorkingDevice->driverNumber.get(pContext))->setPHOS(channel,
                delayValue, widthValue);
    } catch(std::runtime_error & e) {
        LOG_ERROR_FORMAT_IF(logger, "Delay setting HW failed: %s \n", e.what());
        return HARDWARE_ERRORS::SETTING_NOT_ALLOWED;
    }

    if (channel == 0)
    {
        pWorkingDevice->delayPm1_setting.set(delayValue, pContext);
        pWorkingDevice->delayWidthPm1_setting.set(widthValue, pContext);

        pWorkingDevice->delayPm1.set(delayValue, pContext);
        pWorkingDevice->delayWidthPm1.set(widthValue, pContext);
    }
    else
    {
        pWorkingDevice->delayPm2_setting.set(delayValue, pContext);
        pWorkingDevice->delayWidthPm2_setting.set(widthValue, pContext);

        pWorkingDevice->delayPm2.set(delayValue, pContext);
        pWorkingDevice->delayWidthPm2.set(widthValue, pContext);
    }

    return HARDWARE_ERRORS::NONE;

}

HARDWARE_ERRORS::HARDWARE_ERRORS Tools::EmcPhaGet(Device *pWorkingDevice, const MultiplexingContext *pContext, int *pha)
{

    HARDWARE_ERRORS::HARDWARE_ERRORS hardWareError = HARDWARE_ERRORS::NONE;

    if (pWorkingDevice->phaIs.get(pContext) == YES_NO::YES)
    {
        LowLevel::EmcPhaGet(pWorkingDevice->driverNumber.get(pContext), pha);
    }
    return hardWareError;
}

HARDWARE_ERRORS::HARDWARE_ERRORS Tools::EmcGainSet(Device *pWorkingDevice, const MultiplexingContext *pContext,
        long int gain)
{
    HARDWARE_ERRORS::HARDWARE_ERRORS hardWareError = HARDWARE_ERRORS::NONE;
    int status = pWorkingDevice->deviceDetailedStatus.get(pContext);
    pWorkingDevice->phaGain.set((BXFISC::EMCGAIN::EMCGAIN) gain, pContext);
    if ( ((int) status & DEVICE_DETAILED_STATUS::ON) == (int) DEVICE_DETAILED_STATUS::ON &&
            (pWorkingDevice->phaIs.get(pContext) == YES_NO::YES))
    {
        LowLevel::EmcGainSet(pWorkingDevice->driverNumber.get(pContext), gain);
    }
    return hardWareError;
}

HARDWARE_ERRORS::HARDWARE_ERRORS Tools::EmcPhaDacGet(Device *pWorkingDevice, const MultiplexingContext *pContext,
        double *dacs)
{
    HARDWARE_ERRORS::HARDWARE_ERRORS hardWareError = HARDWARE_ERRORS::NONE;


    if ((int) pWorkingDevice->phaIs.get(pContext) == (int) YES_NO::YES)
    {
        LowLevel::EmcPhaDacsGet(pWorkingDevice->driverNumber.get(pContext), dacs);
    }
    //return hardWareError;

    return hardWareError;
}

HARDWARE_ERRORS::HARDWARE_ERRORS Tools::EmcPhaDacSet(Device *pWorkingDevice, const MultiplexingContext *pContext,
        EMCDACS::EMCDACS which, double dac)
{
    HARDWARE_ERRORS::HARDWARE_ERRORS hardWareError = HARDWARE_ERRORS::NONE;


    //int status=pWorkingDevice->deviceDetailedStatus.get(pContext);

    if (pWorkingDevice->phaIs.get(pContext) == YES_NO::YES)
    {
        LowLevel::EmcPhaDacSet(pWorkingDevice->driverNumber.get(pContext), which, dac);
    }
    return hardWareError;
}

//HARDWARE_ERRORS::HARDWARE_ERRORS Tools::EmcPhaRegGet(Device *pWorkingDevice,const MultiplexingContext* pContext,BXFISC::EMCREG::EMCREG *reg)
//{
//	uint16_t emcReg; /
//
//	HARDWARE_ERRORS::HARDWARE_ERRORS hardWareError=HARDWARE_ERRORS::NONE;
//	int status=pWorkingDevice->deviceDetailedStatus.get(pContext);
//	if (((int)status&DEVICE_DETAILED_STATUS::NOT_EMC)!=(int)DEVICE_DETAILED_STATUS::NOT_EMC )
//	{	
//		hardWareError = LowLevel::EmcPhaRegGet(pWorkingDevice->driverNumber.get(pContext),&emcReg);
//		*reg=(EMCREG::EMCREG)emcReg;
//	}else
//	{
//		*reg=(EMCREG::EMCREG)0;
//	}
//	return hardWareError;
//}

HARDWARE_ERRORS::HARDWARE_ERRORS Tools::EmcPhaRegSet(Device *pWorkingDevice, const MultiplexingContext *pContext,
        BXFISC::EMCREG::EMCREG regx)
{

    //int status=pWorkingDevice->deviceDetailedStatus.get(pContext);
    HARDWARE_ERRORS::HARDWARE_ERRORS hardWareError = HARDWARE_ERRORS::NONE;
//	printf("Tools::EmcPhaRehSet\n");return hardWareError;

    if (pWorkingDevice->phaIs.get(pContext) == YES_NO::YES)
    {
        LowLevel::EmcPhaRegSet(pWorkingDevice->driverNumber.get(pContext), regx);
    }
    return hardWareError;
}
HARDWARE_ERRORS::HARDWARE_ERRORS Tools::FlashLed(Device *pWorkingDevice, const MultiplexingContext *pContext)
{

    HARDWARE_ERRORS::HARDWARE_ERRORS hardWareError;
    hardWareError = HARDWARE_ERRORS::NONE;
    LowLevel::FlashLed(pWorkingDevice->driverNumber.get(pContext));

    char msg[MSG_SIZE];
    sprintf(msg, "%s flashled ", Tools::TimeString().c_str());
    pWorkingDevice->msgCommand.set(msg, pContext);

    return hardWareError;
}

std::string Tools::HardwareErrorMsg(HARDWARE_ERRORS::HARDWARE_ERRORS errorCode)
{
    std::string errorMsg;
    switch (errorCode)
    {
        case HARDWARE_ERRORS::NONE:
            errorMsg = "There was no error";
            break;
        case HARDWARE_ERRORS::MODULE_NOT_ON:
            errorMsg = "Module is not on";
            break;
        case HARDWARE_ERRORS::SETTING_NOT_ALLOWED:
            errorMsg = "Setting are not allowed";
            break;
        case HARDWARE_ERRORS::UNKNOWN_ACTION:
            errorMsg = "Unkown action";
            break;
        default:
            errorMsg = "Unkown error";
            break;
    }
    return errorMsg;
}

std::string Tools::TimeString()
{
    struct tm *t;
    time_t now;
    time(&now);
    t = localtime(&now);

    //LowLevel::sayHelloStatic();
    char timeNow[60];
    // year month and date include
    //sprintf(timeNow,"%2.2d/%2.2d/%4d %2.2d:%2.2d:%2.2d\n",
    //                    t->tm_mday,t->tm_mon+1,t->tm_year+1900,t->tm_hour,t->tm_min,t->tm_sec);
    sprintf(timeNow, "%2.2d:%2.2d:%2.2d ", t->tm_hour, t->tm_min, t->tm_sec);

    std::string msg = timeNow;
    return msg;
}

HARDWARE_ERRORS::HARDWARE_ERRORS Tools::PortsGet(uint16_t *ports, Device *pWorkingDevice,
        const MultiplexingContext *pContext)
{
    LowLevel::GetPort(pWorkingDevice->driverNumber.get(pContext), (int) HARDWARE_PORTS::PORTA, &ports[0]);
    LowLevel::GetPort(pWorkingDevice->driverNumber.get(pContext), (int) HARDWARE_PORTS::PORTB, &ports[1]);
    LowLevel::GetPort(pWorkingDevice->driverNumber.get(pContext), (int) HARDWARE_PORTS::PORTC, &ports[2]);
    return HARDWARE_ERRORS::NONE;

}

HARDWARE_ERRORS::HARDWARE_ERRORS Tools::PortsSet(REGBLANK::REGBLANK port, PORT_ACTION::PORT_ACTION which,
        Device *pWorkingDevice, const MultiplexingContext *pContext)
{
    Tools::ManualChangeOfPortsOrRegs(pWorkingDevice, pContext);
    if (which == PORT_ACTION::PORTA)
    {
        pWorkingDevice->moduleHwPortA_setting.set(port, pContext);
    }
    if (which == PORT_ACTION::PORTB)
    {
        pWorkingDevice->moduleHwPortB_setting.set(port, pContext);
    }
    if (which == PORT_ACTION::PORTC)
    {
        pWorkingDevice->moduleHwPortC_setting.set(port, pContext);
    }
    LowLevel::SetPort(pWorkingDevice->driverNumber.get(pContext), (int) which, (uint16_t) port);
    return HARDWARE_ERRORS::NONE;

}

HARDWARE_ERRORS::HARDWARE_ERRORS Tools::PositionGet(float *position, Device *pWorkingDevice,
        const MultiplexingContext *pContext)
{

    LowLevel::FifoPositionGet(position, pWorkingDevice->motorPosition_gain.getCell(GAIN::GET, pContext),
            pWorkingDevice->driverNumber.get(pContext));

    return HARDWARE_ERRORS::NONE;
}

HARDWARE_ERRORS::HARDWARE_ERRORS Tools::MonitorDefaults(Device *pWorkingDevice, const MultiplexingContext *pContext)
{
    HARDWARE_ERRORS::HARDWARE_ERRORS flag = HARDWARE_ERRORS::NONE;
    int hwStatus = pWorkingDevice->deviceHwStatus.get(pContext);
    int detailStatus = pWorkingDevice->deviceDetailedStatus.get(pContext);
    float delayPm1 = pWorkingDevice->delayPm1_default.get(pContext) / pWorkingDevice->delayPm1.get(pContext);
    float delayPm2 = pWorkingDevice->delayPm2_default.get(pContext) / pWorkingDevice->delayPm2.get(pContext);
//	float thresPm1 = pWorkingDevice->moduleThresholdPm1_default.get(pContext)/pWorkingDevice->moduleThresholdPm1.get(pContext);
//	float thresPm2 = pWorkingDevice->moduleThresholdPm2_default.get(pContext)/pWorkingDevice->moduleThresholdPm2.get(pContext);

    float hv1 = pWorkingDevice->highVoltagePm1_default.get(pContext) / pWorkingDevice->highVoltagePm1.get(pContext);
    float hv2 = pWorkingDevice->highVoltagePm2_default.get(pContext) / pWorkingDevice->highVoltagePm2.get(pContext);
    if (((int) hwStatus & DEVICE_HW_STATUS::HVS_AT_DEFAULTS) != (int) DEVICE_HW_STATUS::HVS_AT_DEFAULTS)
    {
        hwStatus = hwStatus + DEVICE_HW_STATUS::HVS_AT_DEFAULTS;
    }
    if (((int) hwStatus & DEVICE_HW_STATUS::THRESSS_AT_DEFAULTS) != (int) DEVICE_HW_STATUS::THRESSS_AT_DEFAULTS)
    {
        hwStatus = hwStatus + DEVICE_HW_STATUS::THRESSS_AT_DEFAULTS;
    }
    if (((int) hwStatus & DEVICE_HW_STATUS::DELAYS_AT_DEFAULTS) != (int) DEVICE_HW_STATUS::DELAYS_AT_DEFAULTS)
    {
        hwStatus = hwStatus + DEVICE_HW_STATUS::DELAYS_AT_DEFAULTS;
    }
    if (((int) detailStatus & DEVICE_DETAILED_STATUS::VALUES_AT_DEFAULTS)
            != (int) DEVICE_DETAILED_STATUS::VALUES_AT_DEFAULTS)
    {
        detailStatus = detailStatus + DEVICE_DETAILED_STATUS::VALUES_AT_DEFAULTS;
    }

    if (hv1 > 0.9 && hv1 < 1.1 && hv2 > 0.9 && hv2 < 1.1)
    {
    }
    else
    {
        hwStatus = hwStatus - DEVICE_HW_STATUS::HVS_AT_DEFAULTS;
    }
//	if ( thresPm1 < 1.10 && thresPm1 > 0.90 && thresPm2 < 1.10 && thresPm2 > 0.90 )
//	{
//	} else
//	{
//
//		if ( ((int)hwStatus&DEVICE_HW_STATUS::THRESSS_AT_DEFAULTS)==(int)DEVICE_HW_STATUS::THRESSS_AT_DEFAULTS )
//		{
//			hwStatus=hwStatus-DEVICE_HW_STATUS::THRESSS_AT_DEFAULTS;
//		}
//	}
    if (delayPm1 < 1.05 && delayPm1 > 0.90 && delayPm2 < 1.05 && delayPm2 > 0.90)
    {
    }
    else
    {
        hwStatus = hwStatus - DEVICE_HW_STATUS::DELAYS_AT_DEFAULTS;
    }
    if (((int) hwStatus & DEVICE_HW_STATUS::HVS_AT_DEFAULTS) == (int) DEVICE_HW_STATUS::HVS_AT_DEFAULTS &&
    //		((int)hwStatus&DEVICE_HW_STATUS::THRESSS_AT_DEFAULTS)==(int)DEVICE_HW_STATUS::THRESSS_AT_DEFAULTS &&
            ((int) hwStatus & DEVICE_HW_STATUS::DELAYS_AT_DEFAULTS) == (int) DEVICE_HW_STATUS::DELAYS_AT_DEFAULTS)
    {
    }
    else
    {
        detailStatus = detailStatus - DEVICE_DETAILED_STATUS::VALUES_AT_DEFAULTS;
    }

    pWorkingDevice->deviceHwStatus.set(hwStatus, pContext);
    pWorkingDevice->deviceDetailedStatus.set(detailStatus, pContext);

    return flag;
}
HARDWARE_ERRORS::HARDWARE_ERRORS Tools::MonitorSettings(BXFISC::GlobalDevice *pGlobalStore, Device *pWorkingDevice,
        const MultiplexingContext *pContext)
{
    //pWorkingDevice-
    //GlobalDevice* pGlobalStore=BXFISCServiceLocator_->getGlobalDevice();
    HARDWARE_ERRORS::HARDWARE_ERRORS flag = HARDWARE_ERRORS::NONE;
    int hwStatus = pWorkingDevice->deviceHwStatus.get(pContext);
    int detailStatus = pWorkingDevice->deviceDetailedStatus.get(pContext);
    float delayPm1 = pWorkingDevice->delayPm1_setting.get(pContext) / pWorkingDevice->delayPm1.get(pContext);
    float delayPm2 = pWorkingDevice->delayPm2_setting.get(pContext) / pWorkingDevice->delayPm2.get(pContext);

    float hv1 = pWorkingDevice->highVoltagePm1_setting.get(pContext) / pWorkingDevice->highVoltagePm1.get(pContext);
    float hv2 = pWorkingDevice->highVoltagePm2_setting.get(pContext) / pWorkingDevice->highVoltagePm2.get(pContext);
    float pos = pWorkingDevice->position_setting.get(pContext) - pWorkingDevice->position.get(pContext);
    if (((int) hwStatus & DEVICE_HW_STATUS::TEST_SETTINGS_OFF) != (int) DEVICE_HW_STATUS::TEST_SETTINGS_OFF)
    {
        hwStatus = hwStatus + DEVICE_HW_STATUS::TEST_SETTINGS_OFF;
    }
    if (((int) hwStatus & DEVICE_HW_STATUS::POSITION_OK) != (int) DEVICE_HW_STATUS::POSITION_OK)
    {
        hwStatus = hwStatus + DEVICE_HW_STATUS::POSITION_OK;
    }
    if (((int) hwStatus & DEVICE_HW_STATUS::HVS_AT_SETTINGS) != (int) DEVICE_HW_STATUS::HVS_AT_SETTINGS)
    {
        hwStatus = hwStatus + DEVICE_HW_STATUS::HVS_AT_SETTINGS;
    }
    if (((int) hwStatus & DEVICE_HW_STATUS::DELAYS_AT_SETTINGS) != (int) DEVICE_HW_STATUS::DELAYS_AT_SETTINGS)
    {
        hwStatus = hwStatus + DEVICE_HW_STATUS::DELAYS_AT_SETTINGS;
    }
    if (((int) detailStatus & DEVICE_DETAILED_STATUS::VALUES_AT_SETTINGS)
            != (int) DEVICE_DETAILED_STATUS::VALUES_AT_SETTINGS)
    {
        detailStatus = detailStatus + DEVICE_DETAILED_STATUS::VALUES_AT_SETTINGS;
    }
    if ((int) (hwStatus & DEVICE_HW_STATUS::TIMING_EE_THERE) != (int) DEVICE_HW_STATUS::TIMING_EE_THERE)
    {
        hwStatus = hwStatus + DEVICE_HW_STATUS::TIMING_EE_THERE;
    }
    if ((int) (hwStatus & DEVICE_HW_STATUS::TIMING_WWE_THERE) != (int) DEVICE_HW_STATUS::TIMING_WWE_THERE)
    {
        hwStatus = hwStatus + DEVICE_HW_STATUS::TIMING_WWE_THERE;
    }
    if (pWorkingDevice->moduleLedPiedestal_setting.get(pContext) == pWorkingDevice->moduleLedPiedestal_off.get(pContext)
            && pWorkingDevice->moduleLedPulse_setting.get(pContext) == pWorkingDevice->moduleLedPulse_off.get(pContext))
    {
    }
    else
    {
        hwStatus = hwStatus - DEVICE_HW_STATUS::TEST_SETTINGS_OFF;
    }
    if (pos > 1 || pos < -1)
    {
        hwStatus = hwStatus - DEVICE_HW_STATUS::POSITION_OK;
    }
    if (hv1 > 0.9 && hv1 < 1.1 && hv2 > 0.9 && hv2 < 1.1)
    {
    }
    else
    {
        hwStatus = hwStatus - DEVICE_HW_STATUS::HVS_AT_SETTINGS;
    }
    if (delayPm1 < 1.05 && delayPm1 > 0.90 && delayPm2 < 1.05 && delayPm2 > 0.90)
    {
    }
    else
    {
        hwStatus = hwStatus - DEVICE_HW_STATUS::DELAYS_AT_SETTINGS;
    }
    long long monitorEE = pGlobalStore->acqStampMonitor.get(pContext) - pGlobalStore->acqStampEE.get(pContext);
    if ((int) (monitorEE / TIMING_ACQSTAMP_TO_SECONDS) > ((int) pWorkingDevice->moduleWdEETimeout.get(pContext)))
    {
        hwStatus = hwStatus - DEVICE_HW_STATUS::TIMING_EE_THERE;
    }
    long long monitorWWE = pGlobalStore->acqStampMonitor.get(pContext) - pGlobalStore->acqStampWWE.get(pContext);
    if ((int) (monitorWWE / TIMING_ACQSTAMP_TO_SECONDS) > ((int) pWorkingDevice->moduleWdEETimeout.get(pContext)))
    {
        hwStatus = hwStatus - DEVICE_HW_STATUS::TIMING_WWE_THERE;
    }
//	printf("Tools Timing %d %d %d\n",(int)(monitorEE/TIMING_ACQSTAMP_TO_SECONDS),(int)(monitorWWE/TIMING_ACQSTAMP_TO_SECONDS),(int)pWorkingDevice->moduleWdEETimeout.get(pContext));
    if (((int) hwStatus & DEVICE_HW_STATUS::HVS_AT_SETTINGS) == (int) DEVICE_HW_STATUS::HVS_AT_SETTINGS
            && ((int) hwStatus & DEVICE_HW_STATUS::POSITION_OK) == (int) DEVICE_HW_STATUS::POSITION_OK
            && ((int) hwStatus & DEVICE_HW_STATUS::DELAYS_AT_SETTINGS) == (int) DEVICE_HW_STATUS::DELAYS_AT_SETTINGS)

    {
    }
    else
    {
        detailStatus = detailStatus - DEVICE_DETAILED_STATUS::VALUES_AT_SETTINGS;
    }
    pWorkingDevice->deviceHwStatus.set(hwStatus, pContext);
    pWorkingDevice->deviceDetailedStatus.set(detailStatus, pContext);

    return flag;
}

HARDWARE_ERRORS::HARDWARE_ERRORS Tools::MonitorStatus(Device *pWorkingDevice, const MultiplexingContext *pContext)
{
    HARDWARE_ERRORS::HARDWARE_ERRORS flag = HARDWARE_ERRORS::NONE;
    int hwStatus = pWorkingDevice->deviceHwStatus.get(pContext);
    int detailStatus = pWorkingDevice->deviceDetailedStatus.get(pContext);
    if (((int) detailStatus & DEVICE_DETAILED_STATUS::STATUS_OK) != (int) DEVICE_DETAILED_STATUS::STATUS_OK)
    {
        detailStatus = detailStatus + DEVICE_DETAILED_STATUS::STATUS_OK;
    }

    if (((int) hwStatus & DEVICE_HW_STATUS::PS_STATUS_28V) == (int) DEVICE_HW_STATUS::PS_STATUS_28V
            && ((int) hwStatus & DEVICE_HW_STATUS::PS_STATUS_INTERNAL) == (int) DEVICE_HW_STATUS::PS_STATUS_INTERNAL
            && ((int) hwStatus & DEVICE_HW_STATUS::HV1_STATUS_OK) == (int) DEVICE_HW_STATUS::HV1_STATUS_OK
            && ((int) hwStatus & DEVICE_HW_STATUS::HV2_STATUS_OK) == (int) DEVICE_HW_STATUS::HV2_STATUS_OK
            && ((int) hwStatus & DEVICE_HW_STATUS::POSITION_LIMIT_POS_OK)
                    == (int) DEVICE_HW_STATUS::POSITION_LIMIT_POS_OK
            && ((int) hwStatus & DEVICE_HW_STATUS::POSITION_LIMIT_NEG_OK)
                    == (int) DEVICE_HW_STATUS::POSITION_LIMIT_NEG_OK)

    {
    }
    else
    {
        detailStatus = detailStatus - DEVICE_DETAILED_STATUS::STATUS_OK;
    }
    pWorkingDevice->deviceHwStatus.set(hwStatus, pContext);
    pWorkingDevice->deviceDetailedStatus.set(detailStatus, pContext);

    return flag;
}
HARDWARE_ERRORS::HARDWARE_ERRORS Tools::MonitorWarnings(Device *pWorkingDevice, const MultiplexingContext *pContext)
{
    HARDWARE_ERRORS::HARDWARE_ERRORS flag = HARDWARE_ERRORS::NONE;
    //first clear all warning flag
    int status = pWorkingDevice->deviceDetailedStatus.get(pContext);
    int hwStatus = pWorkingDevice->deviceHwStatus.get(pContext);

    if (((int) status & DEVICE_DETAILED_STATUS::WARNING_NO) != (int) DEVICE_DETAILED_STATUS::WARNING_NO)
    {
        status = status + DEVICE_DETAILED_STATUS::WARNING_NO;
    }
    if (((int) hwStatus & DEVICE_HW_STATUS::HVS_CURRENTS_NO_WARNING) != (int) DEVICE_HW_STATUS::HVS_CURRENTS_NO_WARNING)
    {
        hwStatus = hwStatus + DEVICE_HW_STATUS::HVS_CURRENTS_NO_WARNING;
    }
    //check hardware
    if (pWorkingDevice->highVoltagePm1_current.get(pContext) < -0.2
            && pWorkingDevice->highVoltagePm2_current.get(pContext) < -0.2) // This is good
    {
    }
    else
    {
        hwStatus = hwStatus - DEVICE_HW_STATUS::HVS_CURRENTS_NO_WARNING;
    }

    //update detailed status
    if (((int) status & DEVICE_DETAILED_STATUS::STATUS_OK) != (int) DEVICE_DETAILED_STATUS::STATUS_OK
            || ((int) status & DEVICE_DETAILED_STATUS::VALUES_AT_DEFAULTS)
                    != (int) DEVICE_DETAILED_STATUS::VALUES_AT_DEFAULTS
            || ((int) status & DEVICE_DETAILED_STATUS::TEST_OFF) != (int) DEVICE_DETAILED_STATUS::TEST_OFF
            || ((int) hwStatus & DEVICE_HW_STATUS::TEST_SETTINGS_OFF) != (int) DEVICE_HW_STATUS::TEST_SETTINGS_OFF
            || ((int) status & DEVICE_DETAILED_STATUS::LOCAL_NO) != (int) DEVICE_DETAILED_STATUS::LOCAL_NO
            || ((int) status & DEVICE_DETAILED_STATUS::TEST_LED_OFF) != (int) DEVICE_DETAILED_STATUS::TEST_LED_OFF
            || ((int) status & DEVICE_DETAILED_STATUS::TEST_INTERNAL_OFF)
                    != (int) DEVICE_DETAILED_STATUS::TEST_INTERNAL_OFF ||
//		((int)hwStatus&DEVICE_HW_STATUS::HVS_CURRENTS_NO_WARNING)!=(int)DEVICE_HW_STATUS::HVS_CURRENTS_NO_WARNING ||		
            ((int) status & DEVICE_DETAILED_STATUS::VALUES_AT_SETTINGS)
                    != (int) DEVICE_DETAILED_STATUS::VALUES_AT_SETTINGS)
    {
        status = status - DEVICE_DETAILED_STATUS::WARNING_NO;
    }
    pWorkingDevice->deviceHwStatus.set(hwStatus, pContext);
    pWorkingDevice->deviceDetailedStatus.set(status, pContext);
    return flag;

}
HARDWARE_ERRORS::HARDWARE_ERRORS Tools::MonitorErrors(Device *pWorkingDevice, const MultiplexingContext *pContext)
{
    HARDWARE_ERRORS::HARDWARE_ERRORS flag = HARDWARE_ERRORS::NONE;
    //first clear all error flags
    int statusOld;
    int status = pWorkingDevice->deviceDetailedStatus.get(pContext);
    int hwStatus = pWorkingDevice->deviceHwStatus.get(pContext);
    statusOld = status;

    if (((int) status & DEVICE_DETAILED_STATUS::ERROR_NO) != (int) DEVICE_DETAILED_STATUS::ERROR_NO)
    {
        status = status + DEVICE_DETAILED_STATUS::ERROR_NO;
    }
    if (((int) status & DEVICE_DETAILED_STATUS::TIMING_THERE) != (int) DEVICE_DETAILED_STATUS::TIMING_THERE)
    {
        status = status + DEVICE_DETAILED_STATUS::TIMING_THERE;
    }
    if (((int) hwStatus & DEVICE_HW_STATUS::TIMING_EE_THERE) == (int) DEVICE_HW_STATUS::TIMING_EE_THERE
            && ((int) hwStatus & DEVICE_HW_STATUS::TIMING_WWE_THERE) == (int) DEVICE_HW_STATUS::TIMING_WWE_THERE)
    {
    }
    else
    {
        status = status - DEVICE_DETAILED_STATUS::TIMING_THERE;
    }
    //check hardware
    if (((int) status & DEVICE_DETAILED_STATUS::LOCAL_NO) != (int) DEVICE_DETAILED_STATUS::LOCAL_NO
            || ((int) hwStatus & DEVICE_HW_STATUS::NO_ERROR_DRIVER_THERE)
                    != (int) DEVICE_HW_STATUS::NO_ERROR_DRIVER_THERE
            || ((int) hwStatus & DEVICE_HW_STATUS::NO_ERROR_ACQUISITION_ON)
                    != (int) DEVICE_HW_STATUS::NO_ERROR_ACQUISITION_ON
            || ((int) status & DEVICE_DETAILED_STATUS::TIMING_THERE) != (int) DEVICE_DETAILED_STATUS::TIMING_THERE
            || ((int) status & DEVICE_DETAILED_STATUS::PIQUET_HARDWARE_IN_PLACE)
                    != (int) DEVICE_DETAILED_STATUS::PIQUET_HARDWARE_IN_PLACE
            || ((int) status & DEVICE_DETAILED_STATUS::PIQUET_CABLING_DONE)
                    != (int) DEVICE_DETAILED_STATUS::PIQUET_CABLING_DONE
            || ((int) status & DEVICE_DETAILED_STATUS::PIQUET_EQUIPMENT_READY_TO_USE)
                    != (int) DEVICE_DETAILED_STATUS::PIQUET_EQUIPMENT_READY_TO_USE ||

            ((int) hwStatus & DEVICE_HW_STATUS::NO_ERROR_MONITOR_ON) != (int) DEVICE_HW_STATUS::NO_ERROR_MONITOR_ON)

    {
        status = status - DEVICE_DETAILED_STATUS::ERROR_NO;
    }
    if (status != statusOld)
    {
        pWorkingDevice->deviceDetailedStatus.set(status, pContext);
    }

    int regErrors = pWorkingDevice->devicePortRegError.get(pContext);

    if ((int) (regErrors & REGERRORS::PORTS_OR_REGS_CHANGE_THROUGH_FESA)
            == (int) (REGERRORS::PORTS_OR_REGS_CHANGE_THROUGH_FESA))
    {
        regErrors = REGERRORS::PORTS_OR_REGS_CHANGE_THROUGH_FESA;
    }
    else
    {
        regErrors = (REGERRORS::REGERRORS) 0;
    }

    REGBASE::REGBASE regBase = (REGBASE::REGBASE) pWorkingDevice->moduleHwRegBase.get(pContext);
    REGCOIN::REGCOIN regCoin = (REGCOIN::REGCOIN) pWorkingDevice->moduleHwRegCoin.get(pContext);
    REGMOTOR::REGMOTOR regMotor = (REGMOTOR::REGMOTOR) pWorkingDevice->moduleHwRegMotor.get(pContext);
    PORTA::PORTA portA = (PORTA::PORTA) pWorkingDevice->moduleHwPortA.get(pContext);
    if ((int) (regBase & REGBASE::START_WINDOW_ON) == (int) (REGBASE::START_WINDOW_ON)) regErrors +=
            REGERRORS::REGBASE_START_WINDOW_ON_BIT3_0;
    if ((int) (regBase & REGBASE::AUTO_RETURN_ON) == (int) (REGBASE::AUTO_RETURN_ON)) regErrors +=
            REGERRORS::REGBASE_AUTO_RETURN_ON_BIT4_1;

    if ((int) (regBase & REGBASE::E_DISABLED) == (int) (REGBASE::E_DISABLED)) regErrors +=
            REGERRORS::REGBASE_E_DISABLED_BIT6_0;

    if ((int) (regBase & REGBASE::BT_DISABLED) == (int) (REGBASE::BT_DISABLED)) regErrors +=
            REGERRORS::REGBASE_BT_DISABLED_BIT7_0;

    if ((int) (portA & PORTA::HV1_ALARM) == (int) (PORTA::HV1_ALARM)) regErrors += REGERRORS::PORTA_HV1_ALARM_BIT0_0;
    if ((int) (portA & PORTA::HV2_ALARM) == (int) (PORTA::HV2_ALARM)) regErrors += REGERRORS::PORTA_HV2_ALARM_BIT1_0;
    if ((int) (portA & PORTA::MOTOR_POWER) == (int) (PORTA::MOTOR_POWER)) regErrors +=
            REGERRORS::PORTA_MOTOR_POWER_BIT2_0;
    if ((int) (portA & PORTA::MOTOR_MODE) != (int) (PORTA::MOTOR_MODE)) regErrors += REGERRORS::PORTA_MOTOR_MODE_BIT3_1;
    if ((int) (portA & PORTA::PS_INT) == (int) (PORTA::PS_INT)) regErrors += REGERRORS::PORTA_PS_INT_BIT6_0;
    if ((int) (portA & PORTA::PS_EXT) == (int) (PORTA::PS_EXT)) regErrors += REGERRORS::PORTA_PS_EXT_BIT7_0;
    if ((int) (regMotor & REGMOTOR::SEQ_AUTO) == (int) (REGMOTOR::SEQ_AUTO)) regErrors +=
            REGERRORS::REGMOTOR_SEQ_AUTO_BIT0_0;
    if ((int) (regCoin & REGCOIN::AND_OR_MODE) == (int) (REGCOIN::AND_OR_MODE)) regErrors +=
            REGERRORS::REGCOIN_AND_OR_MODE_BIT1_0;
    if ((int) (regCoin & REGCOIN::NIM_LEVEL0) == (int) (REGCOIN::NIM_LEVEL0)) regErrors +=
            REGERRORS::REGCOIN_COIN_NIM_LEVEL0_BIT2_0;
    if ((int) (regCoin & REGCOIN::NIM_LEVEL1) == (int) (REGCOIN::NIM_LEVEL1)) regErrors +=
            REGERRORS::REGCOIN_COIN_NIM_LEVEL1_BIT3_0;

    pWorkingDevice->devicePortRegError.set(regErrors, pContext);

    return flag;

}

BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS Tools::ReturnToStartPosition(double start, BXFISC::Device *pDev, const MultiplexingContext * pContext) {

    LowLevel::ReturnToStartPosition(start, pDev->driverNumber.get(pContext));
    return HARDWARE_ERRORS::NONE;

}


HARDWARE_ERRORS::HARDWARE_ERRORS Tools::ScanSetUp(double start, double stop, int delay, int duration,
        uint16_t regMaster, uint16_t regCoin, Device *pDev, const MultiplexingContext *pContext)
{
    start = start * pDev->motorPosition_gain.getCell(GAIN::SET, pContext);
    stop = stop * pDev->motorPosition_gain.getCell(GAIN::SET, pContext);
    LowLevel::ScanSetUp(pDev->driverNumber.get(pContext), start, stop, delay, duration, regMaster, regCoin);
    return HARDWARE_ERRORS::NONE;
}

HARDWARE_ERRORS::HARDWARE_ERRORS Tools::PositionSet(float position, bool once, Device *pWorkingDevice,
        const MultiplexingContext *pContext)
{

    pWorkingDevice->position_setting.set(position, pContext);

    bool condition = true;

    if (!once) {
        if ((int) (pWorkingDevice->event.get(pContext)) != (int) EVENTS::WE
                    && (int) (pWorkingDevice->event.get(pContext)) != (int) EVENTS::WWE) {

        }
        else condition = false;
    }

//    if ((int) (pWorkingDevice->event.get(pContext)) != (int) EVENTS::WE
//            && (int) (pWorkingDevice->event.get(pContext)) != (int) EVENTS::WWE)
    if (condition)
    {
        // movement not allow during the scan.
        //long unsigned int size=NUMBER_OF_GAINS;
        position = position * pWorkingDevice->motorPosition_gain.getCell(GAIN::SET, pContext);
//		printf(" Tools::PositionSet with gain %f\n",position);
        LowLevel::PositionSet(pWorkingDevice->driverNumber.get(pContext), position);
    }
//	LowLevel::DacSilent(pWorkingDevice->driverNumber.get( pContext));
    return HARDWARE_ERRORS::NONE;
}

HARDWARE_ERRORS::HARDWARE_ERRORS Tools::DeviceStateAction(Device *pWorkingDevice, const MultiplexingContext *pContext,
        DEVICE_STATE_SETTING::DEVICE_STATE_SETTING action)
{
    //float newHv=0.0;
    float position = 0.0;
    int status = pWorkingDevice->deviceDetailedStatus.get(pContext);
    int configStatus = pWorkingDevice->deviceDetailedStatusConfig.get(pContext);
    int currentHwStatus = pWorkingDevice->deviceHwStatus.get(pContext);

    HARDWARE_ERRORS::HARDWARE_ERRORS hardWareError = HARDWARE_ERRORS::NONE;

    std::string actionStr = DEVICE_STATE_SETTING::DEVICE_STATE_SETTING_AsString[action];

    //std::string str = DEVICE_STATE_SETTING::DEVICE_STATE_SETTING_AsString[action];

    char command[160];
    //double hv1 , hv2 ;
    unsigned long size;
    double gainPos;
    REGCOIN::REGCOIN regCoin;
    char type[50];

    switch (action)
    {
        case DEVICE_STATE_SETTING::WREBOOT_SERVER:

            sprintf(command, "/usr/local/bin/wreboot -N BXFISC_M");
            printf("%s\n", command);
            system(command);
            break;
        case DEVICE_STATE_SETTING::OFF_MODULE:
            if (((int) configStatus & DEVICE_DETAILED_STATUS::MODULE_ON) == (int) DEVICE_DETAILED_STATUS::MODULE_ON)
            {
                configStatus = status - DEVICE_DETAILED_STATUS::MODULE_ON;
            }
            if (((int) configStatus & DEVICE_DETAILED_STATUS::ERROR_NO) == (int) DEVICE_DETAILED_STATUS::ERROR_NO)
            {
                configStatus = status - DEVICE_DETAILED_STATUS::ERROR_NO;
            }

            if (((int) status & DEVICE_DETAILED_STATUS::ERROR_NO) == (int) DEVICE_DETAILED_STATUS::ERROR_NO)
            {
                status = status - DEVICE_DETAILED_STATUS::ERROR_NO;
            }
            if (((int) status & DEVICE_DETAILED_STATUS::MODULE_ON) == (int) DEVICE_DETAILED_STATUS::MODULE_ON)
            {
                status = status - DEVICE_DETAILED_STATUS::MODULE_ON;
            }
            pWorkingDevice->deviceDetailedStatus.set(status, pContext);
            pWorkingDevice->deviceDetailedStatusConfig.set(configStatus, pContext);

            break;

        case DEVICE_STATE_SETTING::OFF:
            if (((int) status & DEVICE_DETAILED_STATUS::OFF_NO) == (int) DEVICE_DETAILED_STATUS::OFF_NO)
            {

                status = status - DEVICE_DETAILED_STATUS::OFF_NO;
                if (((int) status & DEVICE_DETAILED_STATUS::ON) == (int) DEVICE_DETAILED_STATUS::ON)
                {
                    status = status - DEVICE_DETAILED_STATUS::ON;
                }
                if (((int) configStatus & DEVICE_DETAILED_STATUS::ON) == (int) DEVICE_DETAILED_STATUS::ON)
                {
                    configStatus = status - DEVICE_DETAILED_STATUS::ON;
                }
                pWorkingDevice->deviceDetailedStatus.set(status, pContext);
                pWorkingDevice->deviceDetailedStatusConfig.set(configStatus, pContext);
                Tools::VoltageHighPm1To(-0.0, pWorkingDevice, pContext);
                Tools::VoltageHighPm2To(-0.0, pWorkingDevice, pContext);
                Tools::PositionSet(pWorkingDevice->motorPosition_default.get(pContext), true, pWorkingDevice, pContext);

            }

            break;
        case DEVICE_STATE_SETTING::ON_MODULE:
            if (((int) configStatus & DEVICE_DETAILED_STATUS::MODULE_ON) != (int) DEVICE_DETAILED_STATUS::MODULE_ON)
            {
                configStatus = status + DEVICE_DETAILED_STATUS::MODULE_ON;
            }
            if (((int) configStatus & DEVICE_DETAILED_STATUS::OFF_NO) != (int) DEVICE_DETAILED_STATUS::OFF_NO)
            {
                status = status + DEVICE_DETAILED_STATUS::OFF_NO;
            }
            if (((int) status & DEVICE_DETAILED_STATUS::MODULE_ON) != (int) DEVICE_DETAILED_STATUS::MODULE_ON)
            {
                status = status + DEVICE_DETAILED_STATUS::MODULE_ON;
            }
            if (((int) status & DEVICE_DETAILED_STATUS::OFF_NO) != (int) DEVICE_DETAILED_STATUS::OFF_NO)
            {
                status = status + DEVICE_DETAILED_STATUS::OFF_NO;
            }
            pWorkingDevice->deviceDetailedStatus.set(status, pContext);
            pWorkingDevice->deviceDetailedStatusConfig.set(configStatus, pContext);
            break;

        case DEVICE_STATE_SETTING::ON:
            if (((int) status & DEVICE_DETAILED_STATUS::ON) != (int) DEVICE_DETAILED_STATUS::ON)
            {
                status = status + DEVICE_DETAILED_STATUS::ON;
            }
            if (((int) status & DEVICE_DETAILED_STATUS::OFF_NO) != (int) DEVICE_DETAILED_STATUS::OFF_NO)
            {
                status = status + DEVICE_DETAILED_STATUS::OFF_NO;
            }
            if (((int) status & DEVICE_DETAILED_STATUS::LOCAL_NO) != (int) DEVICE_DETAILED_STATUS::LOCAL_NO)
            {
                status = status + DEVICE_DETAILED_STATUS::LOCAL_NO;
            }
            if (((int) status & DEVICE_DETAILED_STATUS::STANDBY_NO) != (int) DEVICE_DETAILED_STATUS::STANDBY_NO)
            {
                status = status + DEVICE_DETAILED_STATUS::STANDBY_NO;
            }
            if (((int) status & DEVICE_DETAILED_STATUS::REMOTE) != (int) DEVICE_DETAILED_STATUS::REMOTE)
            {
                status = status + DEVICE_DETAILED_STATUS::REMOTE;
            }
            if (((int) status & DEVICE_DETAILED_STATUS::COUNTS_EQUAL_ACQ_TIME_OFF)
                    != (int) DEVICE_DETAILED_STATUS::COUNTS_EQUAL_ACQ_TIME_OFF)
            {
                status = status + DEVICE_DETAILED_STATUS::COUNTS_EQUAL_ACQ_TIME_OFF;
            }
            if (((int) status & DEVICE_DETAILED_STATUS::COUNTS_EQUAL_EQN_NUM_OFF)
                    != (int) DEVICE_DETAILED_STATUS::COUNTS_EQUAL_EQN_NUM_OFF)
            {
                status = status + DEVICE_DETAILED_STATUS::COUNTS_EQUAL_EQN_NUM_OFF;
            }
            if (((int) status & DEVICE_DETAILED_STATUS::TEST_LED_OFF) != (int) DEVICE_DETAILED_STATUS::TEST_LED_OFF)
            {
                status = status + DEVICE_DETAILED_STATUS::TEST_LED_OFF;
            }
            if (((int) status & DEVICE_DETAILED_STATUS::TEST_INTERNAL_OFF)
                    != (int) DEVICE_DETAILED_STATUS::TEST_INTERNAL_OFF)
            {
                status = status + DEVICE_DETAILED_STATUS::TEST_INTERNAL_OFF;
            }
            if (((int) configStatus & DEVICE_DETAILED_STATUS::ON) == (int) DEVICE_DETAILED_STATUS::ON)
            {
                configStatus = status - DEVICE_DETAILED_STATUS::ON;
            }

            pWorkingDevice->deviceDetailedStatusConfig.set(configStatus, pContext);

            pWorkingDevice->deviceDetailedStatus.set(status, pContext);

            break;
        case DEVICE_STATE_SETTING::LOCAL_MODE:
            if (((int) status & DEVICE_DETAILED_STATUS::LOCAL_NO) == (int) DEVICE_DETAILED_STATUS::LOCAL_NO)
            {
                Tools::FlashLed(pWorkingDevice, pContext);
                status = status - DEVICE_DETAILED_STATUS::LOCAL_NO;
                pWorkingDevice->deviceDetailedStatus.set(status, pContext);
            }
            else
            {
                Tools::FlashLed(pWorkingDevice, pContext);
            }
            break;
        case DEVICE_STATE_SETTING::STANDBY:
            if (((int) status & DEVICE_DETAILED_STATUS::STANDBY_NO) == (int) DEVICE_DETAILED_STATUS::STANDBY_NO)
            {
                status = status - DEVICE_DETAILED_STATUS::STANDBY_NO;
                pWorkingDevice->deviceDetailedStatus.set(status, pContext);
            }
            break;
        case DEVICE_STATE_SETTING::INIT_MODULE:
            //if (((int)status&DEVICE_DETAILED_STATUS::NOT_EMC)==(int)DEVICE_DETAILED_STATUS::NOT_EMC )
        {

            //sprintf(command,"/dsc/local/bin/fafi -detector fafi");
            //printf("%s\n",command);
            //system(command);

            sprintf(command, "/dsc/local/bin/fafi %d -initall 0", (int) pWorkingDevice->driverNumber.get(pContext));
            //printf("%s\n",command);
            system(command);

            sprintf(command, "/dsc/local/bin/fafi %d -fff 100", (int) pWorkingDevice->driverNumber.get(pContext));
            //printf("%s\n",command);
            system(command);

            sprintf(command, "/dsc/local/bin/fafi %d -wtreg 6 1 0", (int) pWorkingDevice->driverNumber.get(pContext));
            //printf("%s\n",command);
            system(command);

        }
//			else
//			{
//				sprintf(command,"/user/bdisoft/operational/bin/LYNX603_4.0.0/BXFISC/fafi -detector xemc");						
//				system(command);
//				sprintf(command,"/user/bdisoft/operational/bin/LYNX603_4.0.0/BXFISC/fafi %d -initall 0",(int)pWorkingDevice->driverNumber.get(pContext));
//				system(command);				
//				sprintf(command,"/user/bdisoft/operational/bin/LYNX603_4.0.0/BXFISC/fafi %d -tstg off",(int)pWorkingDevice->driverNumber.get(pContext));
//				system(command);
//				sprintf(command,"/user/bdisoft/operational/bin/LYNX603_4.0.0/BXFISC/fafi %d -fff 100",(int)pWorkingDevice->driverNumber.get(pContext));
//				system(command);
//				sprintf(command,"/user/bdisoft/operational/bin/LYNX603_4.0.0/BXFISC/fafi %d -wtreg 6 1 0",(int)pWorkingDevice->driverNumber.get(pContext));
//				system(command);	
//			}
            Tools::ToDefaults(pWorkingDevice, pContext);
            break;
        case DEVICE_STATE_SETTING::COUNT_BEAM_TO_REMOTE:

            if (((int) status & DEVICE_DETAILED_STATUS::ON) != (int) DEVICE_DETAILED_STATUS::ON)
            {
                status = status + DEVICE_DETAILED_STATUS::ON;
            }
            if (((int) status & DEVICE_DETAILED_STATUS::OFF_NO) != (int) DEVICE_DETAILED_STATUS::OFF_NO)
            {
                status = status + DEVICE_DETAILED_STATUS::OFF_NO;
            }
            if (((int) status & DEVICE_DETAILED_STATUS::LOCAL_NO) != (int) DEVICE_DETAILED_STATUS::LOCAL_NO)
            {
                status = status + DEVICE_DETAILED_STATUS::LOCAL_NO;
            }
            if (((int) status & DEVICE_DETAILED_STATUS::STANDBY_NO) != (int) DEVICE_DETAILED_STATUS::STANDBY_NO)
            {
                status = status + DEVICE_DETAILED_STATUS::STANDBY_NO;
            }
            if (((int) status & DEVICE_DETAILED_STATUS::REMOTE) != (int) DEVICE_DETAILED_STATUS::REMOTE)
            {
                status = status + DEVICE_DETAILED_STATUS::REMOTE;
            }
            if (((int) currentHwStatus & DEVICE_HW_STATUS::NO_ERROR_ACQUISITION_ON)
                    != (int) DEVICE_HW_STATUS::NO_ERROR_ACQUISITION_ON)
            {
                currentHwStatus = currentHwStatus + DEVICE_HW_STATUS::NO_ERROR_ACQUISITION_ON;
            }
            if (((int) currentHwStatus & DEVICE_HW_STATUS::NO_ERROR_MONITOR_ON)
                    != (int) DEVICE_HW_STATUS::NO_ERROR_MONITOR_ON)
            {
                currentHwStatus = currentHwStatus + DEVICE_HW_STATUS::NO_ERROR_MONITOR_ON;
            }
            if (((int) status & DEVICE_DETAILED_STATUS::TEST_OFF) != (int) DEVICE_DETAILED_STATUS::TEST_OFF)
            {
                status = status + DEVICE_DETAILED_STATUS::TEST_OFF;
            }

            if (((int) status & DEVICE_DETAILED_STATUS::COUNTS_EQUAL_ACQ_TIME_OFF)
                    != (int) DEVICE_DETAILED_STATUS::COUNTS_EQUAL_ACQ_TIME_OFF)
            {
                status = status + DEVICE_DETAILED_STATUS::COUNTS_EQUAL_ACQ_TIME_OFF;
            }
            if (((int) status & DEVICE_DETAILED_STATUS::COUNTS_EQUAL_EQN_NUM_OFF)
                    != (int) DEVICE_DETAILED_STATUS::COUNTS_EQUAL_EQN_NUM_OFF)
            {
                status = status + DEVICE_DETAILED_STATUS::COUNTS_EQUAL_EQN_NUM_OFF;
            }
            if (((int) status & DEVICE_DETAILED_STATUS::TEST_LED_OFF) != (int) DEVICE_DETAILED_STATUS::TEST_LED_OFF)
            {
                status = status + DEVICE_DETAILED_STATUS::TEST_LED_OFF;
            }
            if (((int) status & DEVICE_DETAILED_STATUS::TEST_INTERNAL_OFF)
                    != (int) DEVICE_DETAILED_STATUS::TEST_INTERNAL_OFF)
            {
                status = status + DEVICE_DETAILED_STATUS::TEST_INTERNAL_OFF;
            }

            pWorkingDevice->deviceDetailedStatus.set(status, pContext);
            pWorkingDevice->deviceHwStatus.set(currentHwStatus, pContext);

            sprintf(command, "/dsc/local/bin/fafi %d -tstg off", (int) pWorkingDevice->driverNumber.get(pContext));
            system(command);
            sprintf(command, "/dsc/local/bin/fafi %d -wtreg 6 1 0", (int) pWorkingDevice->driverNumber.get(pContext));
            system(command);
            sprintf(command, "/dsc/local/bin/fafi %d -fff 100", (int) pWorkingDevice->driverNumber.get(pContext));
            system(command);

            Tools::ToDefaults(pWorkingDevice, pContext);
            break;
        case DEVICE_STATE_SETTING::TEST_OFF:
            hardWareError = Tools::SettingAllowed((DEVICE_DETAILED_STATUS::DEVICE_DETAILED_STATUS) status);
            if (hardWareError == HARDWARE_ERRORS::NONE)
            {
                if (((int) status & DEVICE_DETAILED_STATUS::TEST_OFF) != (int) DEVICE_DETAILED_STATUS::TEST_OFF)
                {
                    status = status + DEVICE_DETAILED_STATUS::TEST_OFF;
                }
                if (((int) status & DEVICE_DETAILED_STATUS::TEST_OFF) != (int) DEVICE_DETAILED_STATUS::TEST_OFF)
                {
                    status = status + DEVICE_DETAILED_STATUS::TEST_OFF;
                }

                if (((int) status & DEVICE_DETAILED_STATUS::TEST_LED_OFF) != (int) DEVICE_DETAILED_STATUS::TEST_LED_OFF)
                {
                    status = status + DEVICE_DETAILED_STATUS::TEST_LED_OFF;
                }
                if (((int) status & DEVICE_DETAILED_STATUS::TEST_INTERNAL_OFF)
                        != (int) DEVICE_DETAILED_STATUS::TEST_INTERNAL_OFF)
                {
                    status = status + DEVICE_DETAILED_STATUS::TEST_INTERNAL_OFF;
                }
                if (((int) status & DEVICE_DETAILED_STATUS::COUNTS_EQUAL_ACQ_TIME_OFF)
                        != (int) DEVICE_DETAILED_STATUS::COUNTS_EQUAL_ACQ_TIME_OFF)
                {
                    status = status + DEVICE_DETAILED_STATUS::COUNTS_EQUAL_ACQ_TIME_OFF;
                }
                if (((int) status & DEVICE_DETAILED_STATUS::COUNTS_EQUAL_EQN_NUM_OFF)
                        != (int) DEVICE_DETAILED_STATUS::COUNTS_EQUAL_EQN_NUM_OFF)
                {
                    status = status + DEVICE_DETAILED_STATUS::COUNTS_EQUAL_EQN_NUM_OFF;
                }

                sprintf(command, "/dsc/local/bin/fafi %d -tstg off", (int) pWorkingDevice->driverNumber.get(pContext));
                system(command);
                sprintf(command, "/dsc/local/bin/fafi %d -wtreg 6 1 0",
                        (int) pWorkingDevice->driverNumber.get(pContext));
                system(command);
                sprintf(command, "/dsc/local/bin/fafi %d -loc 0", (int) pWorkingDevice->driverNumber.get(pContext));
                system(command);

                Tools::ToDefaults(pWorkingDevice, pContext);
                pWorkingDevice->deviceDetailedStatus.set(status, pContext);
            }
            break;
        case DEVICE_STATE_SETTING::TEST_INTERNAL:
            if (((int) status & DEVICE_DETAILED_STATUS::COUNTS_EQUAL_ACQ_TIME_OFF)
                    != (int) DEVICE_DETAILED_STATUS::COUNTS_EQUAL_ACQ_TIME_OFF)
            {
                status = status + DEVICE_DETAILED_STATUS::COUNTS_EQUAL_ACQ_TIME_OFF;
            }
            if (((int) status & DEVICE_DETAILED_STATUS::COUNTS_EQUAL_EQN_NUM_OFF)
                    != (int) DEVICE_DETAILED_STATUS::COUNTS_EQUAL_EQN_NUM_OFF)
            {
                status = status + DEVICE_DETAILED_STATUS::COUNTS_EQUAL_EQN_NUM_OFF;
            }

            if (((int) status & DEVICE_DETAILED_STATUS::TEST_LED_OFF) == (int) DEVICE_DETAILED_STATUS::TEST_LED_OFF)
            {
                status = status - DEVICE_DETAILED_STATUS::TEST_LED_OFF;
            }
            if (((int) status & DEVICE_DETAILED_STATUS::TEST_INTERNAL_OFF)
                    != (int) DEVICE_DETAILED_STATUS::TEST_INTERNAL_OFF)
            {
                status = status + DEVICE_DETAILED_STATUS::TEST_INTERNAL_OFF;
            }
//            sprintf(command, "/dsc/local/bin/fafi %d -tstg intp", (int) pWorkingDevice->driverNumber.get(pContext));
//            system(command);
//            hardWareError = Tools::VoltagePulseIntTo(pWorkingDevice->moduleLedPulse_internal.get(pContext),
//                    pWorkingDevice, pContext);
            using NsRegistry::ComponentRegistry;
            try {
                auto fisc = ComponentRegistry::instance().get(pWorkingDevice->driverNumber.get(pContext));
                const int32_t piedestalValue = static_cast<int32_t>(pWorkingDevice->moduleLedPiedestal_internal.get(pContext) * (-1000)) ;
                fisc->setPedestal(piedestalValue);

                const int32_t pulseValue = static_cast<int32_t>(pWorkingDevice->moduleLedPulse_internal.get(pContext) * 1000);
                const auto pulseMode = NsADA08::PulseMode(NsADA08::PULSE_MODE::INTERNAL_PERSISTENT);
                fisc->setPulse(pulseMode, pulseValue);

                fisc->setTestGen(pulseMode);

            } catch(std::runtime_error & e) {
                LOG_ERROR_FORMAT_IF(logger, "Delay setting HW failed: %s \n", e.what());
                return HARDWARE_ERRORS::SETTING_NOT_ALLOWED;
            }
            pWorkingDevice->moduleLedPulse_setting.set(pWorkingDevice->moduleLedPulse_internal.get(pContext), pContext);
            if (((int) status & DEVICE_DETAILED_STATUS::TEST_OFF) == (int) DEVICE_DETAILED_STATUS::TEST_OFF)
            {
                status = status - DEVICE_DETAILED_STATUS::TEST_OFF;
            }
            pWorkingDevice->deviceDetailedStatus.set(status, pContext);
            break;
        case DEVICE_STATE_SETTING::TEST_LED:
            if (((int) status & DEVICE_DETAILED_STATUS::COUNTS_EQUAL_ACQ_TIME_OFF)
                    != (int) DEVICE_DETAILED_STATUS::COUNTS_EQUAL_ACQ_TIME_OFF)
            {
                status = status + DEVICE_DETAILED_STATUS::COUNTS_EQUAL_ACQ_TIME_OFF;
            }
            if (((int) status & DEVICE_DETAILED_STATUS::COUNTS_EQUAL_EQN_NUM_OFF)
                    != (int) DEVICE_DETAILED_STATUS::COUNTS_EQUAL_EQN_NUM_OFF)
            {
                status = status + DEVICE_DETAILED_STATUS::COUNTS_EQUAL_EQN_NUM_OFF;
            }
            if (((int) status & DEVICE_DETAILED_STATUS::TEST_LED_OFF) == (int) DEVICE_DETAILED_STATUS::TEST_LED_OFF)
            {
                status = status - DEVICE_DETAILED_STATUS::TEST_LED_OFF;
            }

            using NsRegistry::ComponentRegistry;
            try {
                auto fisc = ComponentRegistry::instance().get(pWorkingDevice->driverNumber.get(pContext));
                const int32_t piedestalValue = static_cast<int32_t>(pWorkingDevice->moduleLedPiedestal_external.get(pContext) * (-1000)) ;
                fisc->setPedestal(piedestalValue);

                const int32_t pulseValue = static_cast<int32_t>(pWorkingDevice->moduleLedPulse_external.get(pContext) * 1000);
                const auto pulseMode = NsADA08::PulseMode(NsADA08::PULSE_MODE::EXTERNAL_PERSISTENT);
                fisc->setPulse(pulseMode, pulseValue);

                fisc->setTestGen(pulseMode);
                LOG_INFO_FORMAT_IF(logger, "Set TEST LED pied: %d pulse: %d", piedestalValue, pulseValue);
            } catch(std::runtime_error & e) {
                LOG_ERROR_FORMAT_IF(logger, "Delay setting HW failed: %s \n", e.what());
                return HARDWARE_ERRORS::SETTING_NOT_ALLOWED;
            }
//            sprintf(command, "/dsc/local/bin/fafi %d -tstg extp", (int) pWorkingDevice->driverNumber.get(pContext));
//            system(command);
//            //printf("/user/bdisoft/operational/bin/LYNX603_4.0.0/BXFISC/fafi %d -tstg extp %s \n",(int)pWorkingDevice->driverNumber.get(pContext),pWorkingDevice->name.get(pContext));
//            //LowLevel::DacVerbose(pWorkingDevice->driverNumber.get(pContext));
//            hardWareError = Tools::VoltagePulseExtTo(pWorkingDevice->moduleLedPulse_external.get(pContext),
//                    pWorkingDevice, pContext);
//            printf("%s \n", pWorkingDevice->msgCommand.get(pContext));
//            hardWareError = Tools::VoltagePedestalTo(pWorkingDevice->moduleLedPiedestal_external.get(pContext),
//                    pWorkingDevice, pContext);
//            printf("%s \n", pWorkingDevice->msgCommand.get(pContext));
            //LowLevel::DacSilent(pWorkingDevice->driverNumber.get(pContext));

            if (((int) status & DEVICE_DETAILED_STATUS::TEST_OFF) == (int) DEVICE_DETAILED_STATUS::TEST_OFF)
            {
                status = status - DEVICE_DETAILED_STATUS::TEST_OFF;
            }
            pWorkingDevice->deviceDetailedStatus.set(status, pContext);
            break;
        case DEVICE_STATE_SETTING::LOCATE_MODULE:
            hardWareError = Tools::SettingAllowed((DEVICE_DETAILED_STATUS::DEVICE_DETAILED_STATUS) status);
            if (hardWareError == HARDWARE_ERRORS::NONE)
            {
                Tools::FlashLed(pWorkingDevice, pContext);
            }
            break;
        case DEVICE_STATE_SETTING::HVS_TO_100V:
        case DEVICE_STATE_SETTING::HVS_TO_1400V:
        case DEVICE_STATE_SETTING::HVS_TO_1900V:
        case DEVICE_STATE_SETTING::HVS_TO_2100V:
        {
            double hv;
            if (action == DEVICE_STATE_SETTING::HVS_TO_100V)
            {
                hv = -0.100;
            }
            if (action == DEVICE_STATE_SETTING::HVS_TO_1400V)
            {
                hv = -1.400;
            }
            if (action == DEVICE_STATE_SETTING::HVS_TO_1900V)
            {
                hv = -1.900;
            }
            if (action == DEVICE_STATE_SETTING::HVS_TO_2100V)
            {
                hv = -2.100;
            }

            hardWareError = Tools::SettingAllowed((DEVICE_DETAILED_STATUS::DEVICE_DETAILED_STATUS) status);
            if (hardWareError == HARDWARE_ERRORS::NONE)
            {

                hardWareError = Tools::VoltageHighPm1To(hv, pWorkingDevice, pContext);
                if (hardWareError == HARDWARE_ERRORS::NONE)
                {
                    pWorkingDevice->highVoltagePm1_setting.set(hv, pContext);
                }
                hardWareError = Tools::VoltageHighPm2To(hv, pWorkingDevice, pContext);
                if (hardWareError == HARDWARE_ERRORS::NONE)
                {
                    pWorkingDevice->highVoltagePm2_setting.set(hv, pContext);
                }
            }
        }

            break;
        case DEVICE_STATE_SETTING::HV1_UP_50:
        case DEVICE_STATE_SETTING::HV1_DOWN_50:
        case DEVICE_STATE_SETTING::HV1_UP_10:
        case DEVICE_STATE_SETTING::HV1_DOWN_5:
            double hv;
            if (action == DEVICE_STATE_SETTING::HV1_UP_10)
            {
                hv = pWorkingDevice->highVoltagePm1_setting.get(pContext) - 0.010;
            }
            if (action == DEVICE_STATE_SETTING::HV1_UP_50)
            {
                hv = pWorkingDevice->highVoltagePm1_setting.get(pContext) - 0.050;
            }
            if (action == DEVICE_STATE_SETTING::HV1_DOWN_50)
            {
                hv = pWorkingDevice->highVoltagePm1_setting.get(pContext) + 0.050;
            }
            if (action == DEVICE_STATE_SETTING::HV1_DOWN_5)
            {
                hv = pWorkingDevice->highVoltagePm1_setting.get(pContext) + 0.005;
            }

            hardWareError = Tools::SettingAllowed((DEVICE_DETAILED_STATUS::DEVICE_DETAILED_STATUS) status);
            if (hardWareError == HARDWARE_ERRORS::NONE)
            {
                hardWareError = Tools::VoltageHighPm1To(hv, pWorkingDevice, pContext);
            }
            break;
        case DEVICE_STATE_SETTING::HV2_UP_50:
        case DEVICE_STATE_SETTING::HV2_DOWN_50:
        case DEVICE_STATE_SETTING::HV2_UP_10:
        case DEVICE_STATE_SETTING::HV2_DOWN_5:
            if (action == DEVICE_STATE_SETTING::HV2_UP_10)
            {
                hv = pWorkingDevice->highVoltagePm2_setting.get(pContext) - 0.010;
            }
            if (action == DEVICE_STATE_SETTING::HV2_UP_50)
            {
                hv = pWorkingDevice->highVoltagePm2_setting.get(pContext) - 0.050;
            }
            if (action == DEVICE_STATE_SETTING::HV2_DOWN_50)
            {
                hv = pWorkingDevice->highVoltagePm2_setting.get(pContext) + 0.050;
            }
            if (action == DEVICE_STATE_SETTING::HV2_DOWN_5)
            {
                hv = pWorkingDevice->highVoltagePm2_setting.get(pContext) + 0.005;
            }

            hardWareError = Tools::SettingAllowed((DEVICE_DETAILED_STATUS::DEVICE_DETAILED_STATUS) status);
            if (hardWareError == HARDWARE_ERRORS::NONE)
            {
                hardWareError = Tools::VoltageHighPm2To(hv, pWorkingDevice, pContext);
            }
            break;

        case DEVICE_STATE_SETTING::HVS_SETTING_TO_DEFAULT:
            hardWareError = Tools::SettingAllowed((DEVICE_DETAILED_STATUS::DEVICE_DETAILED_STATUS) status);
            if (hardWareError == HARDWARE_ERRORS::NONE)
            {
                pWorkingDevice->highVoltagePm1_default.set(pWorkingDevice->highVoltagePm1_setting.get(pContext),
                        pContext);

            }
            if (hardWareError == HARDWARE_ERRORS::NONE)
            {
                pWorkingDevice->highVoltagePm2_default.set(pWorkingDevice->highVoltagePm2_setting.get(pContext),
                        pContext);

            }
            break;

        case DEVICE_STATE_SETTING::HVS_TO_DEFAULT:
            hardWareError = Tools::SettingAllowed((DEVICE_DETAILED_STATUS::DEVICE_DETAILED_STATUS) status);
            if (hardWareError == HARDWARE_ERRORS::NONE)
            {
                hardWareError = Tools::VoltageHighPm1To(pWorkingDevice->highVoltagePm1_default.get(pContext),
                        pWorkingDevice, pContext);
                if (hardWareError == HARDWARE_ERRORS::NONE)
                {
                    pWorkingDevice->highVoltagePm1_setting.set(pWorkingDevice->highVoltagePm1_default.get(pContext),
                            pContext);
                }
            }
            if (hardWareError == HARDWARE_ERRORS::NONE)
            {
                hardWareError = Tools::VoltageHighPm2To(pWorkingDevice->highVoltagePm2_default.get(pContext),
                        pWorkingDevice, pContext);
                if (hardWareError == HARDWARE_ERRORS::NONE)
                {
                    pWorkingDevice->highVoltagePm2_setting.set(pWorkingDevice->highVoltagePm2_default.get(pContext),
                            pContext);
                }
            }
            break;

        case DEVICE_STATE_SETTING::MOVE_TO_NEG_35:
        case DEVICE_STATE_SETTING::MOVE_TO_PLUS_35:
        case DEVICE_STATE_SETTING::MOVE_TO_0:
        case DEVICE_STATE_SETTING::MOVE_PLUS_10:
        case DEVICE_STATE_SETTING::MOVE_NEG_10:
        case DEVICE_STATE_SETTING::MOVE_NEG_LIMIT:
        case DEVICE_STATE_SETTING::MOVE_POS_LIMIT:

            position = pWorkingDevice->position_setting.get(pContext);
            size = NUMBER_OF_GAINS;
            gainPos = pWorkingDevice->motorPosition_gain.getCell(GAIN::SET, pContext);
            switch (action)
            {
                case DEVICE_STATE_SETTING::MOVE_TO_NEG_35:
                    position = -35 / gainPos;
                    break;
                case DEVICE_STATE_SETTING::MOVE_TO_PLUS_35:
                    position = 35 / gainPos;
                    break;
                case DEVICE_STATE_SETTING::MOVE_TO_0:
                    position = 0;
                    break;
                case DEVICE_STATE_SETTING::MOVE_PLUS_10:
                    position = position + (10 / gainPos);
                    break;
                case DEVICE_STATE_SETTING::MOVE_NEG_10:
                    position = position - (10 / gainPos);
                    break;
                case DEVICE_STATE_SETTING::MOVE_NEG_LIMIT:
                    position = -45 / gainPos;
                    break;
                case DEVICE_STATE_SETTING::MOVE_POS_LIMIT:
                    position = 45 / gainPos;
                    break;

                default:
                    ;
            }
            hardWareError = Tools::PositionSet(position, true, pWorkingDevice, pContext);
            break;
        case DEVICE_STATE_SETTING::COUNTS_EQUAL_ACQ_TIME_ON:

            if (((int) status & DEVICE_DETAILED_STATUS::COUNTS_EQUAL_ACQ_TIME_OFF)
                    == (int) DEVICE_DETAILED_STATUS::COUNTS_EQUAL_ACQ_TIME_OFF)
            {
                status = status - DEVICE_DETAILED_STATUS::COUNTS_EQUAL_ACQ_TIME_OFF;
            }
            pWorkingDevice->deviceDetailedStatus.set(status, pContext);
            break;
        case DEVICE_STATE_SETTING::COUNTS_EQUAL_EQN_NUM_ON:
            if (((int) status & DEVICE_DETAILED_STATUS::COUNTS_EQUAL_EQN_NUM_OFF)
                    == (int) DEVICE_DETAILED_STATUS::COUNTS_EQUAL_EQN_NUM_OFF)
            {
                status = status - DEVICE_DETAILED_STATUS::COUNTS_EQUAL_EQN_NUM_OFF;
            }
            pWorkingDevice->deviceDetailedStatus.set(status, pContext);
            break;

        case DEVICE_STATE_SETTING::PORTS_AND_REG_TO_DEFAULTS:

            break;

        case DEVICE_STATE_SETTING::PORTS_AND_REG_CURRENT_TO_DEFAULTS:
            break;
        case DEVICE_STATE_SETTING::COIN_FISC_FISC_DEFAULT:
            Tools::RegCoinGet(&regCoin, pWorkingDevice, pContext);
            if ((int) (regCoin & REGCOIN::EXT_VIEW) == (int) (REGCOIN::EXT_VIEW))
            {
                regCoin = (REGCOIN::REGCOIN) (regCoin - REGCOIN::EXT_VIEW);
                Tools::RegCoinSet(regCoin, pWorkingDevice, pContext);
            }
            break;
        case DEVICE_STATE_SETTING::COIN_EXT:

            Tools::RegCoinGet(&regCoin, pWorkingDevice, pContext);
            if ((int) (regCoin & REGCOIN::EXT_VIEW) != (int) (REGCOIN::EXT_VIEW))
            {
                regCoin = (REGCOIN::REGCOIN) (regCoin + REGCOIN::EXT_VIEW);
                Tools::RegCoinSet(regCoin, pWorkingDevice, pContext);
            }
            break;
        case DEVICE_STATE_SETTING::NONE:
            break;

    }


    char msg[MSG_SIZE];

    sprintf(msg, "%s %s", Tools::TimeString().c_str(), actionStr.c_str());
    pWorkingDevice->msgCommand.set(msg, pContext);

    return hardWareError;
}
HARDWARE_ERRORS::HARDWARE_ERRORS Tools::SettingAllowed(DEVICE_DETAILED_STATUS::DEVICE_DETAILED_STATUS status)
{
    HARDWARE_ERRORS::HARDWARE_ERRORS flag;
    flag = HARDWARE_ERRORS::NONE;

    if (((int) status & DEVICE_DETAILED_STATUS::ALLOW_SETTINGS) != (int) DEVICE_DETAILED_STATUS::ALLOW_SETTINGS)
    {
        flag = HARDWARE_ERRORS::SETTING_NOT_ALLOWED;

    }
    if (((int) status & DEVICE_DETAILED_STATUS::MODULE_ON) != (int) DEVICE_DETAILED_STATUS::MODULE_ON)
    {

        flag = HARDWARE_ERRORS::MODULE_NOT_ON;

    }
    return flag;

}

HARDWARE_ERRORS::HARDWARE_ERRORS Tools::VoltagePulseIntTo(float voltage, Device *pWorkingDevice,
        const MultiplexingContext *pContext)
{
    char infoStr[MSG_SIZE];
    HARDWARE_ERRORS::HARDWARE_ERRORS hardwareError = HARDWARE_ERRORS::NONE;

    //LowLevel::DacVerbose(pWorkingDevice->driverNumber.get(pContext));
    //LowLevel::DacSilent(pWorkingDevice->driverNumber.get(pContext));
    hardwareError = LowLevel::DacSet(pWorkingDevice->driverNumber.get(pContext),
            HARDWARE_DAC_CHANNELS07::LED_PULSE_INT_8, voltage, infoStr);
    char msg[MSG_SIZE];
    sprintf(msg, "%s pulse internal to [%2.3f]", Tools::TimeString().c_str(), voltage);
    //LowLevel::DacSilent(pWorkingDevice->driverNumber.get(pContext));
    pWorkingDevice->moduleLedPulse_setting.set(voltage, pContext);

    pWorkingDevice->msgCommand.set(msg, pContext);
    return hardwareError;
}

HARDWARE_ERRORS::HARDWARE_ERRORS Tools::ModuleNameGet(int driverNumber, char *name)
{
    int flag = LowLevel::GetModuleName(driverNumber, name);
    if ((int) flag >= 0)
    {
        return HARDWARE_ERRORS::NONE;
    }
    else
    {
        return HARDWARE_ERRORS::UNABLE_TO_OPEN_DRIVER; //LowLevel::GetName(driverNumber,name);
    }
}

HARDWARE_ERRORS::HARDWARE_ERRORS Tools::VoltagePulseExtTo(float voltage, Device *pWorkingDevice,
        const MultiplexingContext *pContext)
{
    char infoStr[MSG_SIZE];
    HARDWARE_ERRORS::HARDWARE_ERRORS hardwareError = HARDWARE_ERRORS::NONE;

    //LowLevel::DacVerbose(pWorkingDevice->driverNumber.get(pContext));
    hardwareError = LowLevel::DacSet(pWorkingDevice->driverNumber.get(pContext),
            HARDWARE_DAC_CHANNELS07::LED_PULSE_EXT_5, voltage, infoStr);
    char msg[MSG_SIZE];
    sprintf(msg, "%s pulse external to [%2.3f]", Tools::TimeString().c_str(), voltage);
    //LowLevel::DacSilent(pWorkingDevice->driverNumber.get(pContext));
    pWorkingDevice->moduleLedPulse_setting.set(voltage, pContext);

    pWorkingDevice->msgCommand.set(msg, pContext);
    return hardwareError;
}

HARDWARE_ERRORS::HARDWARE_ERRORS Tools::VoltageHighPm1To(float hv, Device *pWorkingDevice,
        const MultiplexingContext *pContext)
{
    HARDWARE_ERRORS::HARDWARE_ERRORS hardwareError = HARDWARE_ERRORS::NONE;
    hardwareError = Tools::VoltageHighLowPm1To((float) hv, pWorkingDevice, pContext);
    pWorkingDevice->highVoltagePm1_offsetNumber.set(0, pContext);

    return hardwareError;
}
HARDWARE_ERRORS::HARDWARE_ERRORS Tools::VoltageHighPm2To(float hv, Device *pWorkingDevice,
        const MultiplexingContext *pContext)
{
    HARDWARE_ERRORS::HARDWARE_ERRORS hardwareError = HARDWARE_ERRORS::NONE;
    hardwareError = Tools::VoltageHighLowPm2To((float) hv, pWorkingDevice, pContext);
    pWorkingDevice->highVoltagePm2_offsetNumber.set(0, pContext);

    return hardwareError;
}
HARDWARE_ERRORS::HARDWARE_ERRORS Tools::VoltageHighLowPm1To(float hv, Device *pWorkingDevice,
        const MultiplexingContext *pContext)
{
    char infoStr[MSG_SIZE];
    HARDWARE_ERRORS::HARDWARE_ERRORS hardwareError = HARDWARE_ERRORS::NONE;
    int statusHw = pWorkingDevice->deviceHwStatus.get(pContext);

    double hv_min = pWorkingDevice->highVoltagePm1_min.get(pContext);

    if (hv >= hv_min)
    {
        LowLevel::DacVerbose(pWorkingDevice->driverNumber.get(pContext));

        hardwareError = LowLevel::DacSet(pWorkingDevice->driverNumber.get(pContext), HARDWARE_DAC_CHANNELS07::HV1_0, hv,
                infoStr);

        LowLevel::DacSilent(pWorkingDevice->driverNumber.get(pContext));
    }
    else
    {
        hardwareError = HARDWARE_ERRORS::HIGH_VOLTAGE_TO_HIGH;

    }

    char msg[MSG_SIZE];
    sprintf(msg, "%s hv1 to [%2.3f]", Tools::TimeString().c_str(), pWorkingDevice->highVoltagePm1_setting.get(pContext));
    pWorkingDevice->msgCommand.set(msg, pContext);
    return hardwareError;
}
HARDWARE_ERRORS::HARDWARE_ERRORS Tools::VoltageHighLowPm2To(float hv, Device *pWorkingDevice,
        const MultiplexingContext *pContext)
{
    char infoStr[MSG_SIZE];
    HARDWARE_ERRORS::HARDWARE_ERRORS hardwareError = HARDWARE_ERRORS::NONE;
    int statusHw = pWorkingDevice->deviceHwStatus.get(pContext);

    double hv_min = pWorkingDevice->highVoltagePm2_min.get(pContext);

    if (hv >= hv_min)
    {
        LowLevel::DacVerbose(pWorkingDevice->driverNumber.get(pContext));
        hardwareError = LowLevel::DacSet(pWorkingDevice->driverNumber.get(pContext), HARDWARE_DAC_CHANNELS07::HV2_1, hv, infoStr);

        LowLevel::DacSilent(pWorkingDevice->driverNumber.get(pContext));
    }
    else
    {
        hardwareError = HARDWARE_ERRORS::HIGH_VOLTAGE_TO_HIGH;

    }

    char msg[MSG_SIZE];
    sprintf(msg, "%s hv1 to [%2.3f]", Tools::TimeString().c_str(),
            pWorkingDevice->highVoltagePm2_setting.get(pContext));

    pWorkingDevice->msgCommand.set(msg, pContext);

    return hardwareError;

}
HARDWARE_ERRORS::HARDWARE_ERRORS Tools::VoltageOffsetControl(Device *pWorkingDevice,
        const MultiplexingContext *pContext)
{
    HARDWARE_ERRORS::HARDWARE_ERRORS hardwareError = HARDWARE_ERRORS::NONE;

    int currentHwStatus = pWorkingDevice->deviceHwStatus.get(pContext);
    float hvPm1offset = pWorkingDevice->highVoltagePm1.get(pContext)
            - pWorkingDevice->highVoltagePm1_setting.get(pContext);
    float hvPm2offset = pWorkingDevice->highVoltagePm2.get(pContext)
            - pWorkingDevice->highVoltagePm2_setting.get(pContext);
    int flag = 0;

    if (((int) currentHwStatus & DEVICE_HW_STATUS::HV_OFFSET_ON) == (int) DEVICE_HW_STATUS::HV_OFFSET_ON)
    {

        flag = 1;
        if (pWorkingDevice->highVoltagePm1_offsetNumber.get(pContext)
                < pWorkingDevice->highVoltagePm1_offsetNumMax.get(pContext))
        {

            if (pWorkingDevice->highVoltagePm1_setting.get(pContext) < -1.0 && hvPm1offset > -0.100
                    && hvPm1offset < 0.100 && (hvPm1offset > 0.005 || hvPm1offset < -0.005))
            {

                float hvPm1Change = pWorkingDevice->highVoltagePm1_offset.get(pContext) + hvPm1offset / 2;
                if (hvPm1Change < -0.100 || hvPm1Change > 0.100)
                {

                    hvPm1Change = 0.0;
                }
                hvPm1Change = 0.0;

                pWorkingDevice->highVoltagePm1_offset.set(hvPm1Change, pContext);
                //Tools::VoltageHighLowPm1To(pWorkingDevice->highVoltage_setting.get(pContext),pWorkingDevice,pContext);
                hardwareError = Tools::VoltageHighLowPm1To(pWorkingDevice->highVoltagePm1_setting.get(pContext),
                        pWorkingDevice, pContext);

            }
            if (pWorkingDevice->highVoltagePm2_setting.get(pContext) < -1.0 && hvPm2offset > -0.100
                    && hvPm2offset < 0.100 && (hvPm2offset > 0.005 || hvPm2offset < -0.005))
            {

                float hvPm2Change = pWorkingDevice->highVoltagePm2_offset.get(pContext) + hvPm2offset / 2;
                if (hvPm2Change < -0.100 || hvPm2Change > 0.100)
                {

                    hvPm2Change = 0.0;
                }
                hvPm2Change = 0.0;

                pWorkingDevice->highVoltagePm2_offset.set(hvPm2Change, pContext);

                hardwareError = Tools::VoltageHighLowPm2To(pWorkingDevice->highVoltagePm2_setting.get(pContext),
                        pWorkingDevice, pContext);

            }

            pWorkingDevice->highVoltagePm1_offsetNumber.set(
                    pWorkingDevice->highVoltagePm1_offsetNumber.get(pContext) + 1, pContext);
            pWorkingDevice->highVoltagePm2_offsetNumber.set(
                    pWorkingDevice->highVoltagePm2_offsetNumber.get(pContext) + 1, pContext);

        }

    }

    return hardwareError;
}

HARDWARE_ERRORS::HARDWARE_ERRORS Tools::VoltageThresholdPm1To(float voltage, Device *pWorkingDevice,
        const MultiplexingContext *pContext)
{
    char infoStr[MSG_SIZE];
    HARDWARE_ERRORS::HARDWARE_ERRORS hardwareError = HARDWARE_ERRORS::NONE;

    //LowLevel::DacVerbose(pWorkingDevice->driverNumber.get(pContext));
    hardwareError = LowLevel::DacSet(pWorkingDevice->driverNumber.get(pContext), HARDWARE_DAC_CHANNELS07::THRES1_2,
            voltage, infoStr);
    pWorkingDevice->moduleThresholdPm1_setting.set(voltage, pContext);
    char msg[MSG_SIZE];
    sprintf(msg, "%s threshold 1 to [%2.3f]", Tools::TimeString().c_str(), voltage);
    //LowLevel::DacSilent(pWorkingDevice->driverNumber.get(pContext));
    pWorkingDevice->msgCommand.set(msg, pContext);
    return hardwareError;
}
HARDWARE_ERRORS::HARDWARE_ERRORS Tools::VoltageThresholdPm2To(float voltage, Device *pWorkingDevice,
        const MultiplexingContext *pContext)
{
    char infoStr[MSG_SIZE];
    HARDWARE_ERRORS::HARDWARE_ERRORS hardwareError = HARDWARE_ERRORS::NONE;

    //LowLevel::DacVerbose(pWorkingDevice->driverNumber.get(pContext));
    hardwareError = LowLevel::DacSet(pWorkingDevice->driverNumber.get(pContext), HARDWARE_DAC_CHANNELS07::THRES2_3,
            voltage, infoStr);
    pWorkingDevice->moduleThresholdPm2_setting.set(voltage, pContext);
    char msg[MSG_SIZE];
    sprintf(msg, "%s threshold 2 to [%2.3f]", Tools::TimeString().c_str(), voltage);
    //LowLevel::DacSilent(pWorkingDevice->driverNumber.get(pContext));
    pWorkingDevice->msgCommand.set(msg, pContext);
    return hardwareError;
}
HARDWARE_ERRORS::HARDWARE_ERRORS Tools::VoltagePedestalTo(float voltage, Device *pWorkingDevice,
        const MultiplexingContext *pContext)
{
    char infoStr[MSG_SIZE];
    HARDWARE_ERRORS::HARDWARE_ERRORS hardwareError = HARDWARE_ERRORS::NONE;

    //LowLevel::DacVerbose(pWorkingDevice->driverNumber.get(pContext));
    if (pWorkingDevice->driverChannel.get(pContext) == 0)
    {
        hardwareError = LowLevel::DacSet(pWorkingDevice->driverNumber.get(pContext), HARDWARE_DAC_CHANNELS07::LED_PED_4,
                voltage, infoStr);
    }
    pWorkingDevice->moduleLedPiedestal_setting.set(voltage, pContext);
    char msg[MSG_SIZE];
    sprintf(msg, "%s pedestal to [%2.3f]", Tools::TimeString().c_str(), voltage);
    //LowLevel::DacSilent(pWorkingDevice->driverNumber.get(pContext));
    pWorkingDevice->msgCommand.set(msg, pContext);
    return hardwareError;
}
HARDWARE_ERRORS::HARDWARE_ERRORS Tools::SettingAllowedCesar(DEVICE_DETAILED_STATUS::DEVICE_DETAILED_STATUS status)
{
    HARDWARE_ERRORS::HARDWARE_ERRORS flag;
    flag = HARDWARE_ERRORS::NONE;

    if (((int) status & DEVICE_DETAILED_STATUS::ALLOW_SETTINGS) != (int) DEVICE_DETAILED_STATUS::ALLOW_SETTINGS)
    {
        flag = HARDWARE_ERRORS::SETTING_NOT_ALLOWED;

    }
    if (((int) status & DEVICE_DETAILED_STATUS::MODULE_ON) != (int) DEVICE_DETAILED_STATUS::MODULE_ON)
    {
        flag = HARDWARE_ERRORS::MODULE_NOT_ON;

    }
    if (((int) status & DEVICE_DETAILED_STATUS::PIQUET_EQUIPMENT_READY_TO_USE)
            != (int) DEVICE_DETAILED_STATUS::PIQUET_EQUIPMENT_READY_TO_USE)
    {
        flag = HARDWARE_ERRORS::EQUIPMENT_NOT_READY_TO_USE;

    }
    if (((int) status & DEVICE_DETAILED_STATUS::ON) != (int) DEVICE_DETAILED_STATUS::ON)
    {
        flag = HARDWARE_ERRORS::EQUIPMENT_NOT_ON;

    }
    if (((int) status & DEVICE_DETAILED_STATUS::LOCAL_NO) != (int) DEVICE_DETAILED_STATUS::LOCAL_NO)
    {
        flag = HARDWARE_ERRORS::LOCAL_MODE;

    }
    return flag;

}

HARDWARE_ERRORS::HARDWARE_ERRORS Tools::EE(Device *pDev, const MultiplexingContext *pContext)
{
    int coin[MAX_FIFO_SIZE], pm1[MAX_FIFO_SIZE], pm2[MAX_FIFO_SIZE], coinE[MAX_FIFO_SIZE];
    double pos[MAX_FIFO_SIZE];
    uint32_t fifoSize = BXFISC::MAX_FIFO_SIZE;
    int numberInFIFO;

    int state = pDev->deviceDetailedStatus.get(pContext);
    int stateHw = pDev->deviceHwStatus.get(pContext);
    if (((int) stateHw & DEVICE_HW_STATUS::NO_ERROR_DRIVER_THERE) == (int) DEVICE_HW_STATUS::NO_ERROR_DRIVER_THERE
            && ((int) state & DEVICE_DETAILED_STATUS::MODULE_ON) == (int) DEVICE_DETAILED_STATUS::MODULE_ON
            && ((int) stateHw & DEVICE_HW_STATUS::NO_ERROR_ACQUISITION_ON)
                    == (int) DEVICE_HW_STATUS::NO_ERROR_ACQUISITION_ON
            && pDev->moduleHardwareMonitorWd.get(pContext) > 10)
    {
//	int state=pDev->deviceDetailedStatus.get(pContext);
        for (unsigned int i = 0; i < MAX_FIFO_SIZE; i++)
        {
            coin[i] = 0L;
            pm1[i] = 0L;
            pm2[i] = 0L;
            coinE[i] = 0L;
            pos[i] = 0.0;
        }
        LowLevel::EE(pDev->driverNumber.get(pContext), pm1, pm2, coin, coinE, pos, &numberInFIFO);

        for (int i = 0; i < numberInFIFO; i++)
        {
            auto gain = pDev->motorPosition_gain.getCell(GAIN::GET, pContext);
            if(gain == 0) {
                throw FesaException(__FILE__, __LINE__, "Tools: incorrect gain = 0");
            }
            pos[i] = pos[i] /gain;
        }

        pDev->countNumber.set(numberInFIFO, pContext);
        for (unsigned int i = numberInFIFO - 1; i < MAX_FIFO_SIZE && i > 0; i--)
        {
            coin[i] = coin[i] - coin[i - 1];
            pm1[i] = pm1[i] - pm1[i - 1];
            pm2[i] = pm2[i] - pm2[i - 1];
            coinE[i] = coinE[i] - coinE[i - 1];
        }
        for (unsigned int i = 0; i < MAX_FIFO_SIZE; i++)
        {
            if (coin[i] < 0) coin[i] = 0;
            if (pm1[i] < 0) pm1[i] = 0;
            if (pm2[i] < 0) pm2[i] = 0;
            if (coinE[i] < 0) coinE[i] = 0;
        }

        pDev->countArray.set(coin, fifoSize, pContext);
        pDev->countPm1Array.set(pm1, fifoSize, pContext);
        pDev->countPm2Array.set(pm2, fifoSize, pContext);
        pDev->countExtCoincidenceArray.set(coinE, fifoSize, pContext);
        pDev->motorPositionArray.set(pos, fifoSize, pContext);
        pDev->countNumber.set(numberInFIFO, pContext);

        if (pDev->phaIs.get(pContext) == YES_NO::YES)
        {
            unsigned long size = MAX_PHA_SIZE;
            int pha[MAX_PHA_SIZE];
            Tools::EmcPhaGet(pDev, pContext, pha);
            pDev->phaData.set(pha, size, pContext);

            EMCREG::EMCREG reg;
            Tools::EmcPhaRegGet(pDev, pContext, &reg);
            pDev->phaReg.set(reg, pContext);
            size = NUMBER_OF_EMC_DACS;
            double dacs[4];
            Tools::EmcPhaDacGet(pDev, pContext, dacs);

            pDev->phaDacs.set(dacs, size, pContext);

        }
    }
    return HARDWARE_ERRORS::NONE;
}

HARDWARE_ERRORS::HARDWARE_ERRORS Tools::WE(Device *pWorkingDevice, const MultiplexingContext *pContext)
{

    LowLevel::WE(pWorkingDevice->driverNumber.get(pContext));
    return HARDWARE_ERRORS::NONE;
}

HARDWARE_ERRORS::HARDWARE_ERRORS Tools::ManualChangeOfPortsOrRegs(Device *pWorkingDevice,
        const MultiplexingContext *pContext)
{
    int statusHw = pWorkingDevice->deviceHwStatus.get(pContext);
    if (((int) statusHw & DEVICE_HW_STATUS::NO_MANUAL_CHANGE_TO_REGS_PORTS)
            == (int) DEVICE_HW_STATUS::NO_MANUAL_CHANGE_TO_REGS_PORTS)
    {
        statusHw = statusHw - DEVICE_HW_STATUS::NO_MANUAL_CHANGE_TO_REGS_PORTS;
        pWorkingDevice->deviceHwStatus.set(statusHw, pContext);
    }
    return HARDWARE_ERRORS::NONE;

}

HARDWARE_ERRORS::HARDWARE_ERRORS Tools::EmcPhaRegGet(Device *pWorkingDevice, const MultiplexingContext *pContext,
        EMCREG::EMCREG *reg)
{
    uint16_t emcReg;

    HARDWARE_ERRORS::HARDWARE_ERRORS hardWareError = HARDWARE_ERRORS::NONE;
    int status = pWorkingDevice->deviceDetailedStatus.get(pContext);
    if (((int) status & DEVICE_DETAILED_STATUS::MODULE_ON) == (int) DEVICE_DETAILED_STATUS::MODULE_ON
            && pWorkingDevice->phaIs.get(pContext) == YES_NO::YES

            )
    {
        hardWareError = LowLevel::EmcPhaRegGet(pWorkingDevice->driverNumber.get(pContext), &emcReg);
        *reg = (EMCREG::EMCREG) emcReg;
    }
    else
    {
        *reg = (EMCREG::EMCREG) 0;
    }
    return hardWareError;
}

}
