/*
 * SpeedConverter.h
 *
 *  Created on: Mar 15, 2021
 *      Author: arlis
 */

#ifndef SRC_BXFISC_COMMON_SPEEDCONVERTER_H_
#define SRC_BXFISC_COMMON_SPEEDCONVERTER_H_

namespace NsSpeedConverter
{

class SpeedConverter
{
    constexpr static double otherMovementsSpeedVoltage = 2.855;
    constexpr static double maxVoltage = 5.0;

    double speedToVoltage(const double speedInMmPerSecond);
public:
    SpeedConverter() = default;
    virtual ~SpeedConverter() = default;

    int16_t convertSpeedToDAC(const double speedInMmPerSecond);
};

} /* namespace NsSpeedConverter */

#endif /* SRC_BXFISC_COMMON_SPEEDCONVERTER_H_ */
