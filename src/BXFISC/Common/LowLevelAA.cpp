#include "LowLevelInclude.h"
#include "LowLevelAA.h"

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/ioctl.h>

#ifdef SCINT
#include "LowLevelScint.h"
#endif


#ifdef CET
//#include "helper.h"
#include "LowLevelCet.h"
//extern int EaSetADCRRAM0(int driver, unsigned short data);
//extern int EaSetADCONF(int driver, unsigned short data);
//extern int EaGetADCFIFO(int driver, unsigned short *data);
#endif

#ifdef CED
#include "LowLevelCet.h"
extern int EaSetADCRRAM0(int driver, unsigned short data);
extern int EaSetADCONF(int driver, unsigned short data);
extern int EaGetADCFIFO(int driver, unsigned short *data);

extern int EaSetADCRRAM0motor(int driver, unsigned short data);
extern int EaSetADCONFmotor(int driver, unsigned short data);
extern int EaGetADCFIFOmotor(int driver, unsigned short *data);

#endif

#ifdef DWC

#include "LowLevelDwc.h"
extern int EaSetADCRRAM0(int driver, unsigned short data);
extern int EaSetADCONF(int driver, unsigned short data);
extern int EaGetADCFIFO(int driver, unsigned short *data);
#endif

#ifdef MWPC

#include "LowLevelMwpc.h"
extern int EaSetADCRRAM0(int driver, unsigned short data);
extern int EaSetADCONF(int driver, unsigned short data);
extern int EaGetADCFIFO(int driver, unsigned short *data);
#endif
#ifdef IFC

#include "LowLevelIfc.h"
extern int EaSetADCRRAM0(int driver, unsigned short data);
extern int EaSetADCONF(int driver, unsigned short data);
extern int EaGetADCFIFO(int driver, unsigned short *data);
#endif

#ifdef FISC
#include "LowLevelFisc.h"

void Waitcy_H(int32_t lNbcy)/*attente cycles CPU 1=1 uS environ*/
{
        //long lCy;
        //lNbcy*=12L;
        //for (lCy=0;lCy<=lNbcy;lCy++);
	usleep(lNbcy);
}
#endif

#ifdef EMC
#define FISC 1
#include "LowLevelFisc.h"

void Waitcy_H(long lNbcy)/*attente cycles CPU 1=1 uS environ*/
{
//        long lCy;
//        lNbcy*=12L;
//        for (lCy=0;lCy<=lNbcy;lCy++);
    	usleep(INbcy);

}

#endif
static int AAVerbose[16] ;

LowLevelAA::LowLevelAA()
{
	std::cout << "LowLevelAA class created \n" ;
//	printf("test class created \n") ;
}

LowLevelAA::~LowLevelAA()
{
	std::cout << "lowLevelAA class removed \n" ;
}


int LowLevelAA::Calibrate(int module)
{
  int devDriver = LowLevelDriver::getDriver(module);
  EaSetADCRRAM0 (devDriver,0x2002);
  EaSetADCONF (devDriver,2);
  EaSetADCONF (devDriver,4);
  EaSetADCONF (devDriver,8);
  return 1;	 
}

/*
 *  pass the aa value / by the gain and minus the offset 
 */
float  LowLevelAA::DoGainOffset(float value,int gain,int offset )
{
	 float gaind , offsetd;
	 float valueNew;
	 gaind =((float)gain)/100000.00;
     if ( offset == 0 )
     {
		  offsetd=0.0;
	 } else 
	 { 
		  offsetd=((float)offset)/100000.00;
	 }
	 valueNew = (value/gaind)+(offsetd/gaind); 
//    printf("Value %f %f gaind %f gain %d offsetd %f offset %d value\n",valueNew,value,gaind,gain,offsetd,offset);

	 return valueNew ;
	
}

/*
 * change a 12 bit value to -10.00 to +10.00
 */
  
float   LowLevelAA::AaUShortToFloat(unsigned short value)
{
	 return ( ((20.00*(float)value)+20.0)/4096.00 ) -10.000 ;
}


int LowLevelAA::ReadAll(int module,unsigned short *aaShort,float *aa)
{
	int channel;
	  	LowLevelAA::ReadAllRaw(module,aaShort);

	for ( channel=0 ; channel<8 ; channel++ )
	{
		aa[channel]=(float)LowLevelAA::AAConvert(module,(BX_AA)channel ,aaShort);
	}
//  	if ( LowLevelAA::IsVerbose(module) )
// 	{	 		
    		for ( channel = 0 ; channel < 8 ; channel++ )
  		{
  			char infoStr[160];
      	  	sprintf(infoStr,"channel [%d] hex [%04x] real [%2.5f]"
	      	,channel,aaShort[channel],aa[channel]);
      	  	//printf("x %s\n ",infoStr);
//	      	std::cout << "COUT::XXX LowLevelAA::ReadAll " << infoStr << "\n" ; 
//	      	
	    }
//  	}
	
	return 1 ;
}
#ifdef CED
int LowLevelAA::ReadMotorAll(int module,unsigned short *aaShort,float *aa)
{
	
	int channel;
	LowLevelAA::ReadMotorAllRaw(module,aaShort);
	for ( channel=0 ; channel<8 ; channel++ )
	{
		aa[channel]=( ((20.00*(float)aaShort[channel]+20.0)/4096.00 ) -10.000 );
	}
//  	if ( LowLevelAA::IsVerbose(module) )
// 	{	 		
//    	for ( channel = 0 ; channel < 8 ; channel++ )
//  		{
//  			char infoStr[160];
//     	  	sprintf(infoStr,"channel [%d] hex [%04x] real [%2.5f]"
//	      	,channel,aaShort[channel],aa[channel]);
//	      	std::cout << "COUT::XXX LowLevelAA::ReadMotorAll " << infoStr << "\n" ; 
//	      	
//	    } 
//  	}
	
	return 1 ;
}

int LowLevelAA::ReadMotorAllRaw(int module,unsigned short *aa)
{
	//int i ;	
	int devDriver = LowLevelDriver::getDriver(module);

 
	EaSetADCRRAM0motor (devDriver,0x2002);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCONFmotor (devDriver,2);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCONFmotor (devDriver,1);
	LowLevelDriver::Waitcy_H(20);
	EaGetADCFIFOmotor (devDriver,&aa[0]);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCRRAM0motor (devDriver,0x2002);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCONFmotor (devDriver,2);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCONFmotor (devDriver,1);
	LowLevelDriver::Waitcy_H(20);
	EaGetADCFIFOmotor (devDriver,&aa[0]);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCRRAM0motor (devDriver,0x2006);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCONFmotor (devDriver,2);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCONFmotor (devDriver,1);
	LowLevelDriver::Waitcy_H(20);
	EaGetADCFIFOmotor (devDriver,&aa[1]);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCRRAM0motor (devDriver,0x200a);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCONFmotor (devDriver,2);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCONFmotor (devDriver,1);
	LowLevelDriver::Waitcy_H(20);
	EaGetADCFIFOmotor (devDriver,&aa[2]);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCRRAM0motor (devDriver,0x200e);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCONFmotor (devDriver,2);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCONFmotor (devDriver,1);
	LowLevelDriver::Waitcy_H(20);
	EaGetADCFIFOmotor (devDriver,&aa[3]);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCRRAM0motor (devDriver,0x2012);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCONFmotor (devDriver,2);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCONFmotor (devDriver,1);
	LowLevelDriver::Waitcy_H(20);
	EaGetADCFIFOmotor (devDriver,&aa[4]);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCRRAM0motor (devDriver,0x2016);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCONFmotor (devDriver,2);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCONFmotor (devDriver,1);
	LowLevelDriver::Waitcy_H(20);
	EaGetADCFIFOmotor (devDriver,&aa[5]);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCRRAM0motor (devDriver,0x201a);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCONFmotor (devDriver,2);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCONFmotor (devDriver,1);
	LowLevelDriver::Waitcy_H(20);
	EaGetADCFIFOmotor (devDriver,&aa[6]);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCRRAM0motor (devDriver,0x201e);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCONFmotor (devDriver,2);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCONFmotor (devDriver,1);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCONFmotor (devDriver,1);
	LowLevelDriver::Waitcy_H(20);
	EaGetADCFIFOmotor (devDriver,&aa[7]);
	LowLevelDriver::Waitcy_H(20);
	   /*
		*
		* every 100 monitor cycles recalibrate the dac's
		*
		*/
	if ( LowLevelDriver::GetMonitorWD(module) % 1000 == 0 )
	{			
		   LowLevelAA::CalibrateMotor(module);
	}
	for ( int i=0;i<8;i++) // 12 bit AA
	{ 	
		aa[i]=aa[i]&0xfff;
	}
	return 1 ;
}

int LowLevelAA::CalibrateMotor(int module)
{
  int devDriver = LowLevelDriver::getDriver(module);
  EaSetADCRRAM0motor (devDriver,0x2002);
  EaSetADCONFmotor (devDriver,2);
  EaSetADCONFmotor (devDriver,4);
  EaSetADCONFmotor (devDriver,8);
  return 1;	 
}
#endif
#ifdef FISC
int LowLevelAA::ReadMpx(int module,unsigned short *aaShort,float *aaFloat)
{
  int channel ;
  int devDriver = LowLevelDriver::getDriver(module);
  unsigned short regBase;
  BxfiscGetREGBASRW(devDriver,&regBase);
  regBase=regBase&0x01F8;
  for ( channel = 0 ; channel < 7 ; channel++ )
  {

   BxfiscSetREGBASRW(devDriver,(unsigned short)(regBase+channel));
    Waitcy_H(20);
    Waitcy_H(20);
  BxfiscSetADCRRAMO(devDriver,0x2002+(7*4));
    Waitcy_H(20);
   BxfiscSetADCONF(devDriver,2);
    Waitcy_H(20);
   BxfiscSetADCONF(devDriver,1);
    Waitcy_H(20);
   BxfiscGetADCFIFO(devDriver,&aaShort[channel]);
    Waitcy_H(20);
    aaShort[channel] =(int)aaShort[channel]&0xfff;
	aaFloat[channel]=LowLevelAA::AaUShortToFloat(aaShort[channel]);

  }
  if ( LowLevelAA::IsVerbose(module) )
  {
  	//char logMsg[160];
    for ( channel = 0 ; channel < 7 ; channel++ )
  	{
  		char infoStr[160];
      	sprintf(infoStr," channel [%d] hex [%04x] real [%2.5f]"
	     	,channel,aaShort[channel],aaFloat[channel]);
      	std::cout << "COUT::XXX LowLevelAA::ReadMpx" << infoStr << "\n" ;      
    } 
  }
  return 1 ;
}
#endif
/*
 *
 * Every cycle of the monitor all the dac's are read.
 *
 */
int LowLevelAA::ReadAllRaw(int module,unsigned short *aa)
{
	//int i ;	
	int devDriver = LowLevelDriver::getDriver(module);

 
	EaSetADCRRAM0 (devDriver,0x2002);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCONF (devDriver,2);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCONF (devDriver,1);
	LowLevelDriver::Waitcy_H(20);
	EaGetADCFIFO (devDriver,&aa[0]);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCRRAM0 (devDriver,0x2002);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCONF (devDriver,2);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCONF (devDriver,1);
	LowLevelDriver::Waitcy_H(20);
	EaGetADCFIFO (devDriver,&aa[0]);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCRRAM0 (devDriver,0x2006);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCONF (devDriver,2);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCONF (devDriver,1);
	LowLevelDriver::Waitcy_H(20);
	EaGetADCFIFO (devDriver,&aa[1]);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCRRAM0 (devDriver,0x200a);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCONF (devDriver,2);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCONF (devDriver,1);
	LowLevelDriver::Waitcy_H(20);
	EaGetADCFIFO (devDriver,&aa[2]);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCRRAM0 (devDriver,0x200e);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCONF (devDriver,2);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCONF (devDriver,1);
	LowLevelDriver::Waitcy_H(20);
	EaGetADCFIFO (devDriver,&aa[3]);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCRRAM0 (devDriver,0x2012);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCONF (devDriver,2);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCONF (devDriver,1);
	LowLevelDriver::Waitcy_H(20);
	EaGetADCFIFO (devDriver,&aa[4]);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCRRAM0 (devDriver,0x2016);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCONF (devDriver,2);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCONF (devDriver,1);
	LowLevelDriver::Waitcy_H(20);
	EaGetADCFIFO (devDriver,&aa[5]);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCRRAM0 (devDriver,0x201a);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCONF (devDriver,2);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCONF (devDriver,1);
	LowLevelDriver::Waitcy_H(20);
	EaGetADCFIFO (devDriver,&aa[6]);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCRRAM0 (devDriver,0x201e);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCONF (devDriver,2);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCONF (devDriver,1);
	LowLevelDriver::Waitcy_H(20);
	EaSetADCONF (devDriver,1);
	LowLevelDriver::Waitcy_H(20);
	EaGetADCFIFO (devDriver,&aa[7]);
	LowLevelDriver::Waitcy_H(20);
	   /*
		*
		* every 100 monitor cycles recalibrate the dac's
		*
		*/
	if ( LowLevelDriver::GetMonitorWD(module) % 1000 == 0 )
	{			
		   LowLevelAA::Calibrate(module);
	}
	for ( int i=0;i<8;i++) // 12 bit AA
	{ 	
		aa[i]=aa[i]&0xfff;
	}
	return 1 ;
}

float LowLevelAA::AAConvert(int module,BX_AA channel , unsigned short *aa)
{
	int gain;
	int offset;

#ifdef SCINT
	int *rom =LowLevelDriver::GetRom(module);
	switch ( channel )
	{
		case AA_CH0_HV1 :
			gain	= rom[ROM_READHV1GAIN];
			offset  = rom[ROM_READHV1OFFS];
			break;
		case AA_CH1_HV2 :
			gain	= rom[ROM_READHV1GAIN];
			offset  = rom[ROM_READHV1OFFS];
			break;
		case AA_CH2_THRES1 :
			gain	= rom[ROM_READTHR1GAIN];
			offset  = rom[ROM_READTHR1OFFS];
			break;
		case AA_CH3_THRES2 :
			gain	= rom[ROM_READTHR2GAIN];
			offset  = rom[ROM_READTHR2OFFS];
			break;
		case AA_CH4_HVC1 :
			gain	= rom[ROM_READIHV1GAIN];
			offset  = rom[ROM_READIHV1OFFS];
			break;
		case AA_CH5_HVC2 :
			gain	= rom[ROM_READIHV2GAIN];
			offset  = rom[ROM_READIHV2OFFS];
			break;

		case AA_CH6_MOT1 :
			gain	= rom[ROM_READIMOT1GAIN];
			offset  = rom[ROM_READIMOT1OFFS];
			break;
		case AA_CH7_MOT2 :
			gain	= rom[ROM_READIMOT2GAIN];
			offset  = rom[ROM_READIMOT2OFFS];
			break;
	}
	if ( channel == AA_CH6_MOT1 ) 
	{
		return LowLevelAA::DoGainOffset(LowLevelAA::AaUShortToFloat(aa[channel]),gain,offset)/rom[ROM_RESIMOT1]/100000.00 ;
	}
	if ( channel == AA_CH7_MOT2 ) 
	{
		return LowLevelAA::DoGainOffset(LowLevelAA::AaUShortToFloat(aa[channel]),gain,offset)/rom[ROM_RESIMOT2]/100000.00 ;
	}
#endif
#ifdef FISC
	int *rom =LowLevelDriver::GetRom(module);
	switch ( channel )
	{
		case AA_CH0_HV1 :
			gain	= rom[ROM_READHV1GAIN];
			offset  = rom[ROM_READHV1OFFS];
			break;
		case AA_CH1_HV2 :
			gain	= rom[ROM_READHV1GAIN];
			offset  = rom[ROM_READHV1OFFS];
			break;
		case AA_CH2_THRES1 :
		
			gain	= rom[ROM_READTHR1GAIN];
			offset  = rom[ROM_READTHR1OFFS]*-1;
			break;
		case AA_CH3_THRES2 :
			gain	= rom[ROM_READTHR2GAIN];
			offset  = rom[ROM_READTHR2OFFS]*-1;
			break;
		case AA_CH4_HVC1 :
			gain	= rom[ROM_READIHV1GAIN];
			offset  = rom[ROM_READIHV1OFFS];
			break;
		case AA_CH5_HVC2 :
			gain	= rom[ROM_READIHV2GAIN];
			offset  = rom[ROM_READIHV2OFFS];
			break;

		case AA_CH6_MOT1 :
			gain	= rom[ROM_READIMOTGAIN];
			offset  = rom[ROM_READIMOTOFFS];
			break;
		case AA_CH7_MOT2 :
			gain	= 1;
			offset  = 0;
			break;
	}

#endif
#ifdef CET
	int *rom =LowLevelDriver::GetRom(module);
	switch ( channel )
	{
		case AA_CH0_HV1 :
			gain	= rom[ROM_READHV1GAIN];
			offset  = rom[ROM_READHV1OFFS];
			break;
		case AA_CH1_HV2 :
			gain	= 1;
			offset  = 0;
#ifdef CED
			gain	= rom[ROM_READHV2GAIN];
			offset  = rom[ROM_READHV2OFFS];
#endif
			break;
		case AA_CH2_THRES1 :
			gain	= rom[ROM_READTHR1GAIN];
			offset  = rom[ROM_READTHR1OFFS];
			break;
		case AA_CH3_THRES2 :
			gain	= rom[ROM_READTHR2GAIN];
			offset  = rom[ROM_READTHR2OFFS];
			break;
		case AA_CH4_HVC1 :
			gain	= rom[ROM_READIHV1GAIN];
			offset  = rom[ROM_READIHV1OFFS];
			break;
		case AA_CH5_HVC2 :
			gain	= 1;
			offset  = 0;
#ifdef CED
			gain	= rom[ROM_READIHV1GAIN];
			offset  = rom[ROM_READIHV1OFFS];
#endif			
			break;
		case AA_CH6_MOT1 :
			gain	= rom[ROM_TEMPS1MV];
			offset  = rom[ROM_TEMPOFFS1MV];

			break;

		case AA_CH7_MOT2 :
			gain	= rom[ROM_TEMPS2MV];
			offset  = rom[ROM_TEMPOFFS2MV];		
		
			break;
	}
	
#endif
#ifdef DWC
	int *rom =LowLevelDriver::GetRom(module);
	switch ( channel )
	{
		case AA_CH0_HV1 :
			gain	= rom[ROM_READHV1GAIN];
			offset  = rom[ROM_READHV1OFFS];
			break;
		case AA_CH1_HV2 :
			gain	= rom[ROM_READHV2GAIN];
			offset  = rom[ROM_READHV2OFFS];
			break;
		case AA_CH2_THRES1 :
			gain	= rom[ROM_READHV3GAIN];
			offset  = rom[ROM_READHV3OFFS];
			break;
		case AA_CH3_THRES2 :
			gain	= rom[ROM_READHV4GAIN];
			offset  = rom[ROM_READHV4OFFS];
			break;
		case AA_CH4_HVC1 :
			gain	= rom[ROM_READIHV1GAIN];
			offset  = rom[ROM_READIHV1OFFS];
			break;
		case AA_CH5_HVC2 :
			gain	= rom[ROM_READIHV3GAIN];
			offset  = rom[ROM_READIHV3OFFS];
			break;
		case AA_CH6_MOT1 :
			gain	= rom[ROM_READIHV3GAIN];
			offset  = rom[ROM_READIHV3OFFS];
			break;
		case AA_CH7_MOT2 :
			gain	= rom[ROM_READIHV4GAIN];
			offset  = rom[ROM_READIHV4OFFS];
			break;
	}
#endif
#ifdef CED
	if ( channel==AA_CH6_MOT1 || channel==AA_CH7_MOT2 )
	{
        float fGain=225.0;/*gain arbitraire~gain theorique,la variation du gain
        a peu d'influence sur la mesure*/
        float fOffset;
        float fVal0,fTemp;
        float fCurrent;
        float fRpt100;

		if ( channel==AA_CH6_MOT1 )
		{		
			fOffset=rom[ROM_TEMPOFFS1MV]*0.00000001;
			fRpt100=rom[ ROM_RES40DEG1]/100000.0000;
			fCurrent=rom[ROM_TEMPS1MV]*0.00000001/fRpt100;
			fGain=rom[ROM_TEMPGAIN1]/100000;
//			printf("DBG: ADC ADA08 aa  ROM_TEMPOFFS1MV 		is: %f\n",(double)rom[ROM_TEMPOFFS1MV]/100000.00	);
//			printf("DBG: ADC ADA08 aa  ROM_RES40DEG1		is: %f\n",(double)rom[ ROM_RES40DEG1]/100000.00	);
//			printf("DBG: ADC ADA08 aa  ROM_TEMPS1MV 		is: %f\n",(double)rom[ROM_TEMPS1MV]/100000.00);	
			
		} else
        {
        	fOffset=rom[ROM_TEMPOFFS2MV]*0.00000001;
			fRpt100=rom[ ROM_RES40DEG2]/100000.0000;
			fCurrent=rom[ROM_TEMPS2MV]*0.00000001/fRpt100;
			fGain=rom[ROM_TEMPGAIN2]/100000;
//			printf("DBG: ADC ADA08 aa  ROM_TEMPOFFS2MV 		is: %f\n",(double)rom[ROM_TEMPOFFS1MV]/100000.00	);
//			printf("DBG: ADC ADA08 aa  ROM_RES40DEG2		is: %f\n",(double)rom[ ROM_RES40DEG1]/100000.00	);
//			printf("DBG: ADC ADA08 aa  ROM_TEMPS2MV 		is: %f\n",(double)rom[ROM_TEMPS2MV]/100000.00);	
			
        }
        
 //		printf("DBG: ADC ADA08 aa  ROM_TEMPOFFS1MV 		is: %f\n",(double)rom[ROM_TEMPOFFS1MV]/100000.00	);
//		printf("DBG: ADC ADA08 aa  ROM_RES40DEG1		is: %f\n",(double)rom[ ROM_RES40DEG1]/100000.00	);
//		printf("DBG: ADC ADA08 aa  ROM_TEMPS2MV 		is: %f\n",(double)rom[ROM_TEMPS2MV]/100000.00);	
//		printf("%s\t%16.12f\n","DBG: ADC ADA08 Read is:",fVal0);
//		printf("%s\t%16.12f\n","DBG: fGain          is:",fGain);
//		printf("%s\t%16.12f\n","DBG: fOffset FPROM  is:",fOffset);
//		printf("%s\t%16.12f\n","DBG: fCurrent FPROM is:",fCurrent);
//		printf("%s\t%16.12f\n","DBG: fRpt100 is:",fRpt100);


        
//   		fTemp=(((fRpt100*fCurrent+((0.00-fOffset)/fGain))/fCurrent)-100.0)/0.384;
////		printf("%s%9.4f\n","T40.00deg",fTemp);
//		fTemp=(((fRpt100*fCurrent+((-6.03-fOffset)/fGain))/fCurrent)-100.0)/0.384;
////		printf("%s%9.4f\n","T25.79deg",fTemp);
//		fTemp=(((fRpt100*fCurrent+((6.345-fOffset)/fGain))/fCurrent)-100.0)/0.384;
////		printf("%s%9.4f\n","T54.30deg",fTemp);
//		
//		fTemp=(((fRpt100*fCurrent+((fVal0-fOffset)/fGain))/fCurrent)-100.0)/0.384;
//		

//        printf("*************************************************************************************");
//        printf("LowLevelAA::AAConvert the gain should be in the hunreds %f !!!!!\n",fGain);

        fVal0=(20.0*(float)aa[channel]+20.0)/4096.0-10.0;        /*fVal0=0.0;*/
        fTemp=(((fRpt100*fCurrent+((fVal0-fOffset)/fGain))/fCurrent)-100.0)/0.384;

 	
// 		printf("fValO %f  code is (20.0*(float)aa[channel]+20.0)/4096.0-10.0; \n",fVal0);
//        printf("fRpt100 %f 	fCurrent %f fVal0 %f fOffset %f fGain %f \n",fRpt100,fCurrent,fVal0,fOffset,fGain);
//        printf("Code is (((fRpt100*fCurrent+((fVal0-fOffset)/fGain))/fCurrent)-100.0)/0.384; \n");
//		printf("temp %9.4f  %x\n",fTemp,aa[channel]);
 
        
        
 		
		

		return(fTemp);
	
	} else
	{

	return LowLevelAA::DoGainOffset(LowLevelAA::AaUShortToFloat(aa[channel]),gain,offset);
		
	}
#else	
	return LowLevelAA::DoGainOffset(LowLevelAA::AaUShortToFloat(aa[channel]),gain,offset);
#endif

}


void LowLevelAA::Verbose(int module)
{
	AAVerbose[module]=1 ;
}
int LowLevelAA::IsVerbose(int module)
{
	return AAVerbose[module] ;
}
void LowLevelAA::Silent(int module)
{
	AAVerbose[module]=0 ;
}


