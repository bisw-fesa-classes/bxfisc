#ifndef _LowLevelDriver_H_
#define _LowLevelDriver_H_
#include <iostream>
#include <vector>
#include <unordered_map>
#include <BXFISC/Common/Rom.h>

#ifdef SCINT
#include <libbxscint_dgII.h>
#endif
#ifdef FISC
#include <libbxfisc_dgII.h>
#endif

#ifdef EMC
#include <BxfiscIoctlAccess.h>
#endif

class LowLevelDriver
{
public:
	LowLevelDriver();
	~LowLevelDriver();
	static int	Free(int module);
	static int	Open(int module);
	static int	OkOrOpen(int module);
	static int	Close(int module);
	static int	CloseDown();
	static int	romReadBlock(int module,int offset,int number,char* ,int* data);
	static void romReadBlockSilent(int module);
	static void romReadBlockVerbose(int module);
	static int  isRomReadBlockVerbose(int module);
	static int* GetRom(int module);
	static struct refStruct* GetRef(int module);
	static int	adaInit(int module);
	static int	ipcInit(int module);
	static int	RomInit(int module);
	static int	RefInit(int module);
	static int  getDriver(int module);
	static int  getName(int module,char* name);
	static int  FlashLed(int module);
	static std::string	TimeString();
//	static int  SetPort(int module,BXSCINT_LL_PORTS port,unsigned short data);
	static int  GetMonitorWD(int module);
	static void Waitcy_H(int32_t lNbcy);
	static void UseHardCodedRefs(int module);
//	static int  SetPort(int module,BXSCINT_LL_PORTS port,unsigned short data);

	static void MemoryAccessInit(int driverNumber);
	static void MemoryAccessSilentOn(char* file ,int line,char* msg );
	static void MemoryAccessSilentOff(char* file ,int line,char* msg );
	static void MemoryAccessMsg(char* file ,int line,char* msg );
	static unsigned short MemoryAccessRead(char* file ,int line,char* msg,uint32_t address);
	static void MemoryAccessWrite(char* file ,int line,char* msg,uint32_t address,unsigned short value);


#ifndef SCINT
	static unsigned short GetPort(int module,int port);
	static int SetPort(int module,int port,unsigned short data);

	static int	BaseRegGet(int module, unsigned short* 	baseReg);
#endif
//	static int  SetPort(int module,int port,unsigned short data);
	static int EmcGainSet(int module,int32_t gain);
	static int EmcPhaGet(int module,int32_t *pha);
	static int EmcPhaDacsGet(int module,double *dacs);
	static int EmcPhaDacSet(int module,int which,double dac);
	static int EmcPhaRegSet(int module , unsigned short reg);
	static int EmcPhaRegGet(int module , unsigned short *reg);

	static int	RegBaseGet(int module, unsigned short* 	reg);
	static int	RegCoinGet(int module, unsigned short* 	reg);
	static int	RegMotorGet(int module, unsigned short* 	reg);
	static int	RegBaseSet(int module, unsigned short 	reg);
	static int	RegCoinSet(int module, unsigned short 	reg);
	static int	RegMotorSet(int module, unsigned short 	reg);


	static int	BaseRegSet(int module, unsigned short 	reg);
	static int  BaseRegDisableExtraction(int module);
	static int  BaseRegEnableExtraction(int module);
	static int  BaseRegDisableAutoReturn(int module);
	static int  BaseRegEnableAutoReturn(int module);
	static int  BaseRegStartWindowOn(int module);
	static int  BaseRegStartWindowOff(int module);
	static int  BaseRegAutoReturnOnStop(int module);
	static int  BaseRegAutoReturnOnEE(int module);
	static int  BaseRegBTEnabled(int module);
	static int  BaseRegBTDisabled(int module);

#ifdef EMC
//	static int  SetPort(int module,int port,unsigned short data);
	static int EmcGainSet(int module,long int gain);
	static int EmcPhaGet(int module,long int *pha);
	static int EmcPhaDacsGet(int module,double *dacs);
	static int EmcPhaDacSet(int module,int which,double dac);
	static int EmcPhaRegSet(int module , unsigned short reg);
	static int EmcPhaRegGet(int module , unsigned short *reg);

	static int	BaseRegSet(int module, unsigned short 	baseReg);
	static int	PalRegSet(int module, unsigned short 	baseReg);
	static int	PalRegGet(int module, unsigned short* 	baseReg);

	static int  BaseRegDisableExtraction(int module);
	static int  BaseRegEnableExtraction(int module);

    static int	RegBaseGet(int module, unsigned short* 	reg);
	static int	RegCoinGet(int module, unsigned short* 	reg);
	static int	RegMotorGet(int module, unsigned short* 	reg);
	static int	RegBaseSet(int module, unsigned short 	reg);
	static int	RegCoinSet(int module, unsigned short 	reg);
	static int	RegMotorSet(int module, unsigned short 	reg);


#endif
#ifdef DWC
	static int	Plu(int module,short *ptr);
	static int	PluReset(int module,int pluBit);
	static void PluPrint(int module);
	static int	PluAdrSet(int module,unsigned short address);
	static void TestGenToPerm(int module);
	static void CalibMode(int module,int mode);
	static int	StatusRegGet(int module, unsigned short* reg);
	static void ReadIpCounters(int module ,long *counters);
#endif

#ifdef CET
	static	int ValveControl(int module,int what,int time);
	static	int FreqGet(int module);
#endif
#ifdef CED
	static	int ValveControl(int module,int what,int time);
	static	int FreqGet(int module);
	static int 															MotorPulseIsEnableWhich(int driverNumber);
	static int 									MotorPulseIsEnable(int driverNumber);


	static int MotorPulseEnable(int driverNumber,int which, int onOff);
	static int MotorPulse(int driverNumber, int which,int ms);

	static int MotorOn(int driverNumber,int which, int onOff);
	static int MotorStatus(int driverNumber,unsigned short* );
	static int DacMotorSet(int driverNumber,int channel,float voltage,char* msg);
	static int MotorWait(int ms);
    static int getNameMotor(int module,char* name);


#endif


#ifdef MWPC
	static int   RegisterGet(int module,int which);
	static int   RegisterSet(int module,int which,int value);
	static int   MotorEnable(int module,bool onOff);
	static int   MotorMove(int module,int where);
	static float RomRead(int module,int location);
	static int 	 Ram16Get(int module,int which);
	static int   RamAcqGet(int module,int which, unsigned short* values) ;
	static int   DacRegisterSet(int module,unsigned short hv);

#endif

#ifdef IFC
	static int GetModuleRam(int module,int address,int size,unsigned short *ptr);
	static int SetModuleRam(int module,int address,unsigned short data);
	static int GetModuleAcq(int module,int address,int size,unsigned short *ptr);

#endif

private :


	using RomMap = std::unordered_map<int32_t, NsRom::Rom>;
	using RefMap = std::unordered_map<int32_t, refStruct>;

	static RomMap rom;
	static RefMap ref;

	constexpr static int SZ = 16;
    static int monitorWD;
	static std::vector<int> romVerbose;
	static std::vector<int> refsHardCoded;
	static std::vector<int> drivers;

	static void UseFesaRefs(int module);
	static int UsingFesaRefs(int module);
};
#endif
