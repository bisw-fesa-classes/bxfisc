#ifndef _LowLevelAA_H_
#define _LowLevelAA_H_
#include <iostream>
//#include "../genDrivers/VME-EaipEascint#Mon.Jan.03-15.44/include/EaipEascintIoctAccess.h"

class LowLevelAA
{
public:
	LowLevelAA();
	~LowLevelAA();
	static void Verbose(int module);
	static int	IsVerbose(int module);
	static void Silent(int module);
	static int Calibrate(int module);
	static int ReadAll(int module,unsigned short *aaShort,float *aa);
#ifdef FISC	
	static int ReadMpx(int module,unsigned short *aaShort,float *aa);
#endif
#ifdef EMC	
	static int ReadMpx(int module,unsigned short *aaShort,float *aa);
#endif
#ifdef CED	
	static int ReadMotorAllRaw(int module,unsigned short *aa);
	static int ReadMotorAll(int module,unsigned short *aaShort,float *aa);
	static int CalibrateMotor(int module);
#endif

	static int ReadAllRaw(int module,unsigned short *aa);
    static int RawToFloat(int module,unsigned short *aaRaw,float *aa);
	static float  AAConvert(int module,BX_AA channel , unsigned short *aa);
	
private :
	
	static float  DoGainOffset(float value,int gain,int offset );
	static float  AaUShortToFloat(unsigned short value);

};
#endif
