/*
 * EeSpeedHandler.cpp
 *
 *  Created on: Mar 15, 2021
 *      Author: arlis
 */

#include "EeSpeedHandler.h"
#include <BXFISC/Common/SpeedSetter.h>

namespace NsSpeedConverter
{

EeSpeedHandler::EeSpeedHandler(BXFISC::Device * dev, const fesa::MultiplexingContext * con) : d(dev), c(con)
{}

void EeSpeedHandler::setSpeed(const BXFISC::EVENTS::EVENTS ev, const double speed) {

    using NsSpeedConverter::SpeedSetter;
    static_cast<void>(speed); // this is not used here
    SpeedSetter setter;
    setter.setSpeed(maxSpeed, d, c);
}

} /* namespace NsSpeedConverter */
