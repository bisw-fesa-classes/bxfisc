/*
 * Rom.h
 *
 *  Created on: Apr 27, 2021
 *      Author: arlis
 */

#ifndef SRC_BXDWC_COMMON_ROM_H_
#define SRC_BXDWC_COMMON_ROM_H_

#include <array>
#include <cstdint>

namespace NsRom
{

class Rom
{
public:
    Rom() = default;
    virtual ~Rom() = default;

    int32_t & operator[](int index) {
        return rom.at(index);
    }

    int32_t * data() {
        return rom.data();
    }

private:
    static constexpr int SZ = 1024;
    std::array<int32_t, SZ> rom;
};

} /* namespace NsMovingAverage */

#endif /* SRC_BXDWC_COMMON_ROM_H_ */
