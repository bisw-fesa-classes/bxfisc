#ifndef _TOOLS_H_
#define _TOOLS_H_

#include <string>
#include <BXFISC/GeneratedCode/Device.h>
#include <BXFISC/GeneratedCode/GenRTActions.h>
#include <fesa-core/Exception/FesaException.h>



namespace BXFISC_TOOL {
class Tools
{
public:
	Tools();
	virtual ~Tools();
    static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS ToDefaults(BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext);


	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS	SettingAllowed(BXFISC::DEVICE_DETAILED_STATUS::DEVICE_DETAILED_STATUS status);
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS	SettingAllowedCesar(BXFISC::DEVICE_DETAILED_STATUS::DEVICE_DETAILED_STATUS status);
	static std::string HardwareErrorMsg(BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS errorCode );
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS DeviceStateAction(BXFISC::Device *pWorkingDevice ,const MultiplexingContext * pContext,BXFISC::DEVICE_STATE_SETTING::DEVICE_STATE_SETTING action );
		static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS	DelayInfo(BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext,
 		double* delay,double* delay_setting,
		double* delayWidth,double* elayWidth_setting,
		double* delayRanges_max,double* delayRanges_min,
		double* delayWidthRanges_max,double* delayWidthRanges_min);
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS DelaySet(BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext,int channel,double delayValue, double widthValue);
		
//	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS LowlevelAction(BXSCINTDevice::BXSCINTDevice *pWorkingDevice ,const MultiplexingContext * pContext,DEVICE_LOWLEVEL_ACTION::DEVICE_LOWLEVEL_ACTION action, float value, HARDWARE_DAC_CHANNELS07::HARDWARE_DAC_CHANNELS07 dacAction);
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS FlashLed(BXFISC::Device *pWorkingDevice ,const MultiplexingContext * pContext);
//	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS HardMoveScintIn(BXSCINTDevice::BXSCINTDevice *pWorkingDevice,const MultiplexingContext * pContext);
//	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS HardRestoreHv(BXSCINTDevice::BXSCINTDevice *pWorkingDevice,const MultiplexingContext * pContext);
//	static INOUT_LEIR::INOUT_LEIR           InOutPosition(BXSCINTDevice::BXSCINTDevice *pWorkingDevice,const MultiplexingContext * pContext);
//	static INOUT::INOUT                     InOutDecode(short* ports,HARDWARE_POS_CHANNELS01::HARDWARE_POS_CHANNELS01  channel );
	static BXFISC::DEVICE_STATUS::DEVICE_STATUS DeviceStatus(BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext);
	static BXFISC::DEVICE_MODE::DEVICE_MODE DeviceMode(BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext);
	static BXFISC::DEVICE_CONTROL::DEVICE_CONTROL	DeviceControl(BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext);
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS MonitorAA(int module, float* aa);
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS	EmcPhaRegGet(BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext,BXFISC::EMCREG::EMCREG *reg);

	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS EmcGainSet(BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext,long int gain);
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS EmcPhaGet(BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext,int *pha);
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS EmcPhaRegSet(BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext,BXFISC::EMCREG::EMCREG reg);
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS EmcPhaDacGet(BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext,double* dacs);
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS EmcPhaDacSet(BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext,BXFISC::EMCDACS::EMCDACS which,double dacs);

//	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS MonitorPorts(int module, short* ports);
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS MonitorStatus(BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext);
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS MonitorSettings(BXFISC::GlobalDevice *pGlobalStore ,BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext);
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS MonitorDefaults(BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext);
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS MonitorWarnings(BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext);
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS MonitorErrors(BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext);
	
//	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS MonitorSettings(BXSCINTDevice::BXSCINTDevice *pWorkingDevice,const MultiplexingContext * pContext);
//	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS PositionGoTo(int module,int channel, int position );
    static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS VoltageHighPm1To(float hv, BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext) ;
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS VoltageHighPm2To(float hv, BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext) ;
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS VoltageHighLowPm1To(float hv, BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext) ;
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS VoltageHighLowPm2To(float hv, BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext) ;
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS VoltageOffsetControl(BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext) ;
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS VoltageThresholdPm1To(      float voltage	,BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext) ;
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS VoltageThresholdPm2To(      float voltage	,BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext) ;
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS VoltagePulseIntTo(   float voltage	,BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext) ;
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS VoltagePulseExtTo(   float voltage	,BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext) ;
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS VoltagePedestalTo(float voltage	,BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext) ;
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS CountsGet(long int* counts,BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext) ;
	static std::string TimeString();
	
	
	
//	static int 								LowlevelGetDriver(int module);
//	static void Tools::ConvertPortToBits(char* bitsChar,short port);
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS PositionGet(float *position,BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext);
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS PositionSet(float position, bool once, BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext);
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS ScanSetUp(double start,double stop,int delay,int duration,uint16_t regMaster,uint16_t regCoin, BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext);
    static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS ReturnToStartPosition(double start, BXFISC::Device *pWorkingDevice, const MultiplexingContext * pContext);

	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS AAsGet(uint16_t * aaShort,float *aaFloat,BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext);
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS AAsGetMpx(uint16_t* aaShort,float *aaFloat,BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext);
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS DacsGet(uint16_t* dacShorts, float* dacFloats ,BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext);
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS DacsMpcGet(uint16_t* dacShorts,float* dacFloats,BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext);
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS DacsBaseGet(uint16_t* dacShorts,float* dacFloats,BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext);
	static void								DacConvertToHexChars(char* dacChars,short dac);
	static void								AAConvertToHexChars(char* aaChars,short aa);
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS PortsGet(uint16_t* ports,BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext);
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS PortsSet( BXFISC::REGBLANK::REGBLANK       port, BXFISC::PORT_ACTION::PORT_ACTION which, BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext);
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS RegBaseGet(BXFISC::REGBASE::REGBASE       *reg, BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext);
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS RegCoinGet(BXFISC::REGCOIN::REGCOIN       *reg, BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext);
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS RegMasterGet(BXFISC::REGMASTER::REGMASTER *reg, BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext);
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS RegMotorGet(BXFISC::REGMOTOR::REGMOTOR    *reg, BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext);
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS RegBaseSet(uint16_t       reg, BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext, bool force);
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS RegCoinSet(BXFISC::REGCOIN::REGCOIN       reg, BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext);
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS RegMotorSet(BXFISC::REGMOTOR::REGMOTOR    reg, BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext);
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS	EE(BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext);
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS	WE(BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext);
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS ModuleNameGet(int module,char* name);
	static BXFISC::HARDWARE_ERRORS::HARDWARE_ERRORS ManualChangeOfPortsOrRegs(BXFISC::Device *pWorkingDevice,const MultiplexingContext * pContext);
};

}
#endif //_TOOLS_H_
