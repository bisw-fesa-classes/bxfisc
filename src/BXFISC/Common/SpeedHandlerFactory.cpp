/*
 * SpeedHandlerFactory.cpp
 *
 *  Created on: Mar 15, 2021
 *      Author: arlis
 */

#include "SpeedHandlerFactory.h"
#include <BXFISC/Common/AdjustSpeedHandler.h>
#include <BXFISC/Common/EeSpeedHandler.h>

namespace NsSpeedConverter
{

std::unique_ptr<ISpeedHandler> SpeedHandlerFactory::get(const BXFISC::EVENTS::EVENTS ev, BXFISC::Device * d, const fesa::MultiplexingContext * c) {

    using namespace BXFISC::EVENTS;
    std::unique_ptr<ISpeedHandler> ret;
    switch(ev) {
        case EVENTS::EE: {
            ret = std::unique_ptr<ISpeedHandler>( new EeSpeedHandler{d, c} );
            break;
        }
        default: {
            ret = std::unique_ptr<ISpeedHandler>( new AdjustSpeedHandler{d, c} );
        }
    }
    return ret;
}

} /* namespace NsSpeedConverter */
