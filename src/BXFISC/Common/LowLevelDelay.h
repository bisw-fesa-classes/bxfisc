#ifndef _LowLevelDelay_H_
#define _LowLevelDelay_H_
#include <iostream>
//#include "../genDrivers/VME-EaipEascint#Mon.Jan.03-15.44/include/EaipEascintIoctAccess.h"

class LowLevelDelay
{
public:
	LowLevelDelay();
	~LowLevelDelay();
#ifndef DWC
	static int  IfRangesNotInMemLoad(int module);
	static int	FindRanges(int module);
	static int  Init(int module);
	static void Write(int module,int channel,double* delayValue, double* widthValue);
	static void Verbose(int module);
	static int	IsVerbose(int module);
	static void Silent(int module);
		static  int DelayInfo(int module, int channel,
 		double* delay ,double* delay_setting,
		double* delayWidth,double* delayWidth_setting,
		double* delayRanges_max,double* delayRanges_min,
		double* delayWidthRanges_max,double* delayWidthRanges_min);
	
#endif
	
private :

#ifndef DWC
	static void RangeSet(int module,int whichRange ,int channel);
	static void PhosWrite( int module, int channel, int delayNum,  int value );
	static void Write0( int module , int channel,int delayInt,int widthInt);
	static void Write2( int module , int channel,int delayInt,int widthInt);
	static void Write3( int module , int channel,int delayInt,int widthInt);
#endif 	
};
#endif
