/*
 * AdjustSpeedHandler.cpp
 *
 *  Created on: Mar 15, 2021
 *      Author: arlis
 */

#include "AdjustSpeedHandler.h"
#include <BXFISC/Common/SpeedSetter.h>

namespace NsSpeedConverter
{

AdjustSpeedHandler::AdjustSpeedHandler(BXFISC::Device * dev, const fesa::MultiplexingContext * con) : d(dev), c(con)
{}

void AdjustSpeedHandler::setSpeed(const BXFISC::EVENTS::EVENTS ev, const double speed) {

    using NsSpeedConverter::SpeedSetter;
    SpeedSetter setter;
    setter.setSpeed(speed, d, c);
}

} /* namespace NsSpeedConverter */
