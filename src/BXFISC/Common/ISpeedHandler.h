/*
 * ISpeedHandler.h
 *
 *  Created on: Mar 15, 2021
 *      Author: arlis
 */

#ifndef SRC_BXFISC_COMMON_ISPEEDHANDLER_H_
#define SRC_BXFISC_COMMON_ISPEEDHANDLER_H_

#include <BXFISC/GeneratedCode/TypeDefinition.h>
//#includ

namespace NsSpeedConverter {


class ISpeedHandler {
public:

    ISpeedHandler() = default;
    virtual ~ISpeedHandler() = default;
    virtual void setSpeed(const BXFISC::EVENTS::EVENTS ev, const double speed) = 0;
};

}



#endif /* SRC_BXFISC_COMMON_ISPEEDHANDLER_H_ */
