#include "LowLevelInclude.h"
//#include "LowLevelDriver.h"
//#include "MemoryAccces.h"
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <algorithm>

#include "LowLevelFisc.h"
#ifdef EMC
#include "LowLevelFisc.h"
#define FISC 1
#endif

#define ROM_ADR_HV1			0x150
#define ROM_ADR_HV1_PART2	0x17c
#define ROM_ADR_HV2			0x1B0
#define ROM_ADR_HV2_PART2	0x1DC
#define ROM_ADR_LEDPIED1	0x2d0
#define ROM_ADR_LEDPIED2	0x330
#define ROM_ADR_LEDPULSE	0x360
#define ROM_ADR_MOT1		0x390
#define ROM_ADR_MOT2		0x3F0
#define ROM_ADR_THRES1		0x210
#define ROM_ADR_THRES2		0x270

LowLevelDriver::RomMap LowLevelDriver::rom = {};
LowLevelDriver::RefMap LowLevelDriver::ref = {};
std::vector<int> LowLevelDriver::romVerbose = std::vector<int>(SZ, 0);
std::vector<int> LowLevelDriver::refsHardCoded = std::vector<int>(SZ, 0);
std::vector<int> LowLevelDriver::drivers = std::vector<int>(SZ, 0);
int LowLevelDriver::monitorWD = 0;

LowLevelDriver::LowLevelDriver()
{
}

LowLevelDriver::~LowLevelDriver()
{
}

int LowLevelDriver::OkOrOpen(int module)
{
//
//
    if (drivers[module] <= 0)
    {
        LowLevelDriver::Open(module);
    }
    return drivers[module];
}

int LowLevelDriver::getDriver(int module)
{
    int devDriver;
//    printf("LowLevelDriver::getDriver %s %d befor \n",__FILE__,__LINE__);

    devDriver = LowLevelDriver::OkOrOpen(module);
//    printf("%s %d afer\n",__FILE__,__LINE__);

    return devDriver;

}
int LowLevelDriver::Open(int module)
{
    //int devDriver ;
    char deviceName[128];
//	driver code needs updating
////	BxfiscD.l00.c00
    sprintf(deviceName, "/dev/bxfisc.%d", module);
    drivers[module] = BxfiscEnableAccess(module, 0);

    return drivers[module];
}
int LowLevelDriver::Close(int module)
{
    //int temp=drivers[module];

    EaDisableAccess(module);
    drivers[module] = 0;
    return 1;
}
int LowLevelDriver::FlashLed(int module)
{

    int devDriver = LowLevelDriver::OkOrOpen(module);

    EaSetLOCIPD(devDriver, 0);

    return 1;
}

int LowLevelDriver::romReadBlock(int module, int offset, int number, char *info, int *myData)
{
    return 1;
}

int LowLevelDriver::CloseDown()
{
    for(int module = 0; module < SZ; ++module) {
//        LowLevelDriver::Free(module);
        LowLevelDriver::Close(module);
    }
    return 1;

}
int LowLevelDriver::Free(int module)
{
//    if (rom[module] != nullptr)
//    {
//        printf("Free up rom memory for module [%d]\n", module);
////        free(&rom[module]);
//        delete [] rom[module];
//    }
//    if (ref[module] != nullptr)
//    {
//        printf("Free up ref memory for module [%d]\n", module);
////        free(&ref[module]);
//        delete ref[module];
//    }
    return 1;
}
int LowLevelDriver::RefInit(int module)
{
    if (ref.find(module) != ref.end())
    {

    }
    else
    {
        refStruct rs;
        if (!LowLevelDriver::UsingFesaRefs(module))
        {
            for (int channel = 0; channel < 2; channel++)
            {
                rs.signalThreshold[channel] = -0.03;

            }
            rs.hv1Ref = 1.8;
            rs.hv1Ref = 1.9;
            rs.delay1[0] = 60.0;
            rs.delay1[1] = 10.0;
            rs.delay2[0] = 60.0;
            rs.delay2[1] = 10.0;
            rs.CTL = 0;

            ref.emplace(module, std::move(rs));
        }
        else
        {

        }
    }
    return 1;
}

int LowLevelDriver::GetMonitorWD(int module)
{
    if (LowLevelDriver::UsingFesaRefs(module))
    {
        return 0;
    }
    else
    {
        return monitorWD;
    }
}

int LowLevelDriver::RomInit(int module)
{

    if (rom.find(module) != rom.end())
    {
    }
    else
    {
        int devDriver = LowLevelDriver::getDriver(module);

        NsRom::Rom r;
        uint16_t array[4] {};
        int32_t romValue = 0;
        for (uint16_t i = 0; i < ROM_SIZE; i++)
        {
            const uint16_t address = static_cast<uint16_t>(4 * i);
            EaSetFHRWTADR(devDriver, address);
            EaGetFHRW(devDriver, &array[0]);
            EaSetFHRWTADR(devDriver, static_cast<uint16_t>(address + 1));
            EaGetFHRW(devDriver, &array[1]);
            EaSetFHRWTADR(devDriver, static_cast<uint16_t>(address + 2));
            EaGetFHRW(devDriver, &array[2]);
            EaSetFHRWTADR(devDriver, static_cast<uint16_t>(address + 3));
            EaGetFHRW(devDriver, &array[3]);
            for (int j = 0; j < 4; j++)
            {
                array[j] = array[j] & 0x00ff;
            }
            romValue = (array[3] << 24) | (array[2] << 16) | (array[1] << 8) | array[0];
            /* if the number is negivate */
            if (romValue < 0)
            {
                romValue = -(romValue & 0x7FFFFFFF);
            }
            r[i] = romValue;
        }
        rom.emplace(module, std::move(r));
    }
    return 1;
}

int* LowLevelDriver::GetRom(int module)
{
    if (rom.find(module) == rom.end())
    {
        LowLevelDriver::RomInit(module);
    }
    return rom.at(module).data();
}
struct refStruct* LowLevelDriver::GetRef(int module)
{
    //struct refStruct *refPtr ;
    if (ref.find(module) == ref.end())
    {
        LowLevelDriver::RefInit(module);
    }
    return &ref.at(module);
}

void LowLevelDriver::romReadBlockVerbose(int module)
{
    romVerbose[module] = 1;
}
int LowLevelDriver::isRomReadBlockVerbose(int module)
{
    return romVerbose[module];
}
void LowLevelDriver::romReadBlockSilent(int module)
{
    romVerbose[module] = 0;
}

void LowLevelDriver::UseFesaRefs(int module)
{
    refsHardCoded[module] = 0;
}
void LowLevelDriver::UseHardCodedRefs(int module)
{
    refsHardCoded[module] = 1;
}

int LowLevelDriver::UsingFesaRefs(int module)
{
    if (refsHardCoded[module] == 0)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

#ifndef SCINT

unsigned short LowLevelDriver::GetPort(int module, int port)
{
    unsigned short myDataOut;
    int devDriver = LowLevelDriver::getDriver(module);
    {
        switch (port)
        {
            case 0:
                EaGetADAPADR(devDriver, &myDataOut);
                break;
            case 1:
                EaGetADAPBDR(devDriver, &myDataOut);
                break;
            case 2:
                EaGetADAPCDR(devDriver, &myDataOut);
                break;
            default:
                break;
        }
        myDataOut = myDataOut & 0xff;
        //printf("LowLevelDriver::GetPort %d %d %x\n",module,port,myDataOut);

    }
    return myDataOut;
}

int LowLevelDriver::SetPort(int module, int port, unsigned short myDataIn)
{
//  unsigned short myDataOut;
//	printf("LowLevelDriver::SetPort module %d port %d value %02x\n",module,port,myDataIn);
    int devDriver = LowLevelDriver::getDriver(module);
    {
        switch (port)
        {
            case 0:
                EaSetADAPADR(devDriver, myDataIn);
                break;
            case 1:
                EaSetADAPBDR(devDriver, myDataIn);
                break;
            case 2:
                EaSetADAPCDR(devDriver, myDataIn);
                break;
            default:
                printf("LowLevelDriver::SetPort Error unkown port %d!!!\n", port);
                break;
        }
    }
    return 1;
}

#endif

#ifndef SCINT
int LowLevelDriver::BaseRegGet(int module, unsigned short *baseReg)
{
    int devDriver = LowLevelDriver::getDriver(module);
    EaGetREGBASRW(devDriver, baseReg);
    return 1;
}

#endif

int LowLevelDriver::EmcGainSet(int module, int32_t gain)
{
//TODO_DONE pointers fisc gainset 82c 82e 830 832

    auto rom = LowLevelDriver::GetRom(module);
    int devDriver = LowLevelDriver::getDriver(module);

    int dac0 = 0, dac1 = 0;
    uint16_t dac0S, dac1S;
    switch (gain)
    {
        case 1:
            dac0 = rom[3608 / 4] / 100000;
            dac1 = rom[3612 / 4] / 100000;
            break;
        case 2:
            dac0 = rom[3616 / 4] / 100000;
            dac1 = rom[3620 / 4] / 100000;
            break;
        case 3:
            dac0 = rom[3624 / 4] / 100000;
            dac1 = rom[3628 / 4] / 100000;
            break;
        case 4:
            dac0 = rom[3632 / 4] / 100000;
            dac1 = rom[3636 / 4] / 100000;
            break;
    }
    printf("%s %d dacs %d %d  %d %d  %d %d  %d %d   ", __FILE__, __LINE__, rom[3608 / 4] / 100000,
            rom[3612 / 4] / 100000, rom[3616 / 4] / 100000, rom[3620 / 4] / 100000, rom[3624 / 4] / 100000,
            rom[3628 / 4] / 100000, rom[3632 / 4] / 100000, rom[3636 / 4] / 100000);
    double temp;
    temp = ((double) dac0 + 2750.0) * 4096.0 / 5500.0;
    dac0S =  temp;
    temp = ((double) dac1 + 2750.0) * 4096.0 / 5500.0;
    dac1S = temp;

    EaSetPHADACSEL(devDriver, (gain - 1));
    EaSetPHADAC0(devDriver, dac0S);
    EaSetPHADACCTL(devDriver, 0);
    EaSetPHADAC1(devDriver, dac1S);
    EaSetPHADACCTL(devDriver, 0);
    return 1;
}
int LowLevelDriver::EmcPhaGet(int module, int32_t *pha)
{
    //TODO pointers fisc phaget
#define ADR_MAIN		0xDE100000
#define ADR_OFFSET		0x00001000

    int wasData = 0;
    //unsigned long ptrNew;

    unsigned short reg;
    unsigned short phaData;
    int devDriver = LowLevelDriver::getDriver(module);

    for (unsigned short i = 0; i < 1024; i++)
    {
        *(pha + i) = 0;
    }

    LowLevelDriver::EmcPhaRegGet(module, &reg);

    if ((reg & 1) != 1)
    {
        LowLevelDriver::EmcPhaRegSet(module, reg + 1);
    }
    for (unsigned short i = 0; i < 1024; i++)
    {

        EaSetPHAADR(devDriver, i);
        LowLevelDriver::Waitcy_H(1);

        LowLevelDriver::Waitcy_H(50);
        EaGetPHARAM(devDriver, &phaData);

        *(pha + i) = phaData;
        if (*(pha + i) != 0)
        {
            wasData++;
        }

    }
//	printf("%s %d data was seen %d time in the pha \n",__FILE__,__LINE__,wasData);

    LowLevelDriver::EmcPhaRegGet(module, &reg);
    if ((reg & 1) == 1)
    {
        LowLevelDriver::EmcPhaRegSet(module, reg - 1);
    }
    return 1;
}
int LowLevelDriver::EmcPhaDacsGet(int module, double *dacs)
{
    //TODO_DONE pointers fisc phadacsGet 0x830
#define ADR_MAIN		0xDE100000
#define ADR_OFFSET		0x00001000
    unsigned short shortDacs[4];
    int devDriver = LowLevelDriver::getDriver(module);
    EaGetPHADAC0(devDriver, &shortDacs[0]);
    EaGetPHADAC1(devDriver, &shortDacs[1]);
    EaGetPHADAC2(devDriver, &shortDacs[2]);
    EaGetPHADAC3(devDriver, &shortDacs[3]);
    for (unsigned int i = 0; i < 4; i++)
    {
        dacs[i] = 2.75 * ((2.0 * (double) (shortDacs[i] & 0xfff) / 4096.0) - 1.0);
    }
    return 1;
}
int LowLevelDriver::EmcPhaDacSet(int module, int which, double dac)
{
    //TODO pointers fisc phadacsset
    int dacValue;
    //unsigned long ptrNew;
    double voltage = ((dac + 2.750) / 5.500) * 4096;
    int devDriver = LowLevelDriver::getDriver(module);
    dacValue = (int) voltage;
    if (dacValue < 0) dacValue = 0;
    if (dacValue > 4095) dacValue = 4095;
    if (which == 0)
    {
//		ptrNew = (ADR_MAIN + ( ADR_OFFSET*(module) ) + 0x830) ;
        EaSetPHADAC0(devDriver, dacValue);
    }
    else if (which == 1)
    {
//		ptrNew =  (ADR_MAIN + ( ADR_OFFSET*(module) ) + 0x832) ;
        EaSetPHADAC1(devDriver, dacValue);
    }
    else if (which == 2)
    {
//		ptrNew =  (ADR_MAIN + ( ADR_OFFSET*(module) ) + 0x834) ;
        EaSetPHADAC2(devDriver, dacValue);
    }
    else if (which == 3)
    {
//		ptrNew =  (ADR_MAIN + ( ADR_OFFSET*(module) ) + 0x836) ;
        EaSetPHADAC3(devDriver, dacValue);
    }
//	MemoryAccess::MemoryAccessWrite(__FILE__,__LINE__,"",ptrNew,dacValue);
//	ptrNew =  (ADR_MAIN + ( ADR_OFFSET*(module) ) + 0x82E) ;
//	MemoryAccess::MemoryAccessWrite(__FILE__,__LINE__,"",ptrNew,0);
    EaSetPHADACCTL(devDriver, 0);
    printf("LowLevelDriver::EmcPhaDacSet which %d voltage %f set v %f set dac %x\n", which, (float) dac,
            (float) dacValue, (unsigned short) dacValue);
    return 1;
}

int LowLevelDriver::EmcPhaRegSet(int module, unsigned short reg)
{
//TODO pointers fisc pharegset
#define ADR_MAIN		0xDE100000
#define ADR_OFFSET		0x00001000
    uint32_t ptrNew;
    ptrNew = (ADR_MAIN + ( ADR_OFFSET * (module)) + 0x850);
    int devDriver = LowLevelDriver::getDriver(module);

    EaSetPHAREG(devDriver, reg);

    return 1;
}

int LowLevelDriver::EmcPhaRegGet(int module, unsigned short *reg)
{
    //TODO pointers fisc pharegget

    int devDriver = LowLevelDriver::getDriver(module);

    EaGetPHAREG(devDriver, reg);
    *reg = *reg & 0xffff;

    return 1;
}

int LowLevelDriver::BaseRegSet(int module, unsigned short baseReg)
{

    int devDriver = LowLevelDriver::getDriver(module);
    baseReg = baseReg|0x30;

    EaSetREGBASRW(devDriver, baseReg);
    return 1;
}
int LowLevelDriver::RegBaseSet(int module, unsigned short baseReg)
{
    LowLevelDriver::BaseRegSet(module, baseReg);
    return 1;
}

int LowLevelDriver::RegCoinSet(int module, unsigned short reg)
{
//	unsigned short reg;
    int devDriver = LowLevelDriver::getDriver(module);
    EaSetREGPALWADR(devDriver, reg);
    return 1;
}
int LowLevelDriver::RegCoinGet(int module, unsigned short *reg)
{
//	unsigned short reg;
    int devDriver = LowLevelDriver::getDriver(module);
    EaGetREGPALRADR(devDriver, reg);
    return 1;
}
int LowLevelDriver::RegMotorGet(int module, unsigned short *reg)
{
    //TODO pointers fisc regmotorget
#define ADR_MAIN		0xDE100000
#define ADR_OFFSET		0x00001000
    uint32_t ptrNew;
    int devDriver = LowLevelDriver::getDriver(module);
    ptrNew = (ADR_MAIN + ( ADR_OFFSET * (module)) + 0x872);
    // *reg=(MemoryAccess::MemoryAccessRead(__FILE__,__LINE__,"",ptrNew)&0xff);
    EaGetREGMOTRW(devDriver, reg);
    *reg = *reg & 0xff;
    return 1;
}
int LowLevelDriver::RegMotorSet(int module, unsigned short reg)
{
    //TODO pointers fisc regmotorset
#define ADR_MAIN		0xDE100000
#define ADR_OFFSET		0x00001000
    uint32_t ptrNew;
    int devDriver = LowLevelDriver::getDriver(module);
    ptrNew = (ADR_MAIN + ( ADR_OFFSET * (module)) + 0x872);
// 	MemoryAccess::MemoryAccessWrite(__FILE__,__LINE__,"",ptrNew,reg);
    EaSetREGMOTRW(devDriver, reg);

    return 1;

}

int LowLevelDriver::BaseRegDisableExtraction(int module)
{
    unsigned short baseReg;
    int devDriver = LowLevelDriver::getDriver(module);
    EaGetREGBASRW(devDriver, &baseReg);
    if ((int) (baseReg & BIT6) != (int) BIT6)
    {
        baseReg += BIT6;
    }
    baseReg = baseReg|0x30;
    EaSetREGBASRW(devDriver, baseReg);
    return 1;
}
int LowLevelDriver::BaseRegEnableExtraction(int module)
{
    unsigned short baseReg;
    int devDriver = LowLevelDriver::getDriver(module);
    EaGetREGBASRW(devDriver, &baseReg);
    if ((int) (baseReg & BIT6) == (int) BIT6)
    {
        baseReg -= BIT6;

    }
    //Force bit 4 and 5 (requested by Inaki)
    baseReg = baseReg|0x30;
    EaSetREGBASRW(devDriver, baseReg);

    return 1;
}

int LowLevelDriver::BaseRegEnableAutoReturn(int module) {
    uint16_t baseReg;
    int devDriver = LowLevelDriver::getDriver(module);
    EaGetREGBASRW(devDriver, &baseReg);
    if ((baseReg & BIT4) == (int) BIT4)
    {
        baseReg -= BIT4;

    }
    baseReg = baseReg|0x30;
    EaSetREGBASRW(devDriver, baseReg);

    return 1;
}

int LowLevelDriver::BaseRegDisableAutoReturn(int module) {
    uint16_t baseReg;
    int devDriver = LowLevelDriver::getDriver(module);
    EaGetREGBASRW(devDriver, &baseReg);
    if ((int) (baseReg & BIT4) != (int) BIT4)
    {
        baseReg += BIT4;
    }
    baseReg = baseReg|0x30;
    EaSetREGBASRW(devDriver, baseReg);
    return 1;
}

int LowLevelDriver::BaseRegStartWindowOn(int module) {
    uint16_t baseReg;
    int devDriver = LowLevelDriver::getDriver(module);
    EaGetREGBASRW(devDriver, &baseReg);
    if ((int) (baseReg & BIT3) == (int) BIT3)
    {
        baseReg -= BIT3;
    }
    baseReg = baseReg|0x30;
    EaSetREGBASRW(devDriver, baseReg);
    return 1;
}

int LowLevelDriver::BaseRegStartWindowOff(int module) {
    uint16_t baseReg;
    int devDriver = LowLevelDriver::getDriver(module);
    EaGetREGBASRW(devDriver, &baseReg);
    if ((int) (baseReg & BIT3) != (int) BIT3)
    {
        baseReg += BIT3;
    }
    baseReg = baseReg|0x30;
    EaSetREGBASRW(devDriver, baseReg);
    return 1;
}

int LowLevelDriver::BaseRegAutoReturnOnStop(int module) {
    uint16_t baseReg;
    int devDriver = LowLevelDriver::getDriver(module);
    EaGetREGBASRW(devDriver, &baseReg);
    if ((int) (baseReg & BIT5) == (int) BIT5)
    {
        baseReg -= BIT5;
    }
    baseReg = baseReg|0x30;
    EaSetREGBASRW(devDriver, baseReg);
    return 1;
}

int LowLevelDriver::BaseRegAutoReturnOnEE(int module) {
    uint16_t baseReg;
    int devDriver = LowLevelDriver::getDriver(module);
    EaGetREGBASRW(devDriver, &baseReg);
    if ((int) (baseReg & BIT5) != (int) BIT5)
    {
        baseReg += BIT5;
    }
    baseReg = baseReg|0x30;
    EaSetREGBASRW(devDriver, baseReg);
    return 1;
}

int LowLevelDriver::BaseRegBTDisabled(int module) {
    uint16_t baseReg;
    int devDriver = LowLevelDriver::getDriver(module);
    EaGetREGBASRW(devDriver, &baseReg);
    if ((int) (baseReg & BIT7) == (int) BIT7)
    {
        baseReg -= BIT7;
    }
    baseReg = baseReg|0x30;
    EaSetREGBASRW(devDriver, baseReg);
    return 1;
}

int LowLevelDriver::BaseRegBTEnabled(int module) {
    uint16_t baseReg;
    int devDriver = LowLevelDriver::getDriver(module);
    EaGetREGBASRW(devDriver, &baseReg);
    if ((int) (baseReg & BIT7) != (int) BIT7)
    {
        baseReg += BIT7;
    }
    baseReg = baseReg|0x30;
    EaSetREGBASRW(devDriver, baseReg);
    return 1;
}


#ifdef EMC
int	LowLevelDriver::PalRegGet(int module, unsigned short* reg)
{
//TODO_DONE pointers emc palregget
//	 int devDriver = LowLevelDriver::getDriver(module);
//	 unsigned short *ptr;
//	 ptr = (unsigned short*) (ADR_MAIN + ( ADR_OFFSET*(module) ) + 0x804) ;  //EMC
// 	 *baseReg=(*ptr&0xff);
    int devDriver = LowLevelDriver::getDriver(module);
 	EaGetREGPALRADR(devDriver,reg);
	return 1 ;
}
int	LowLevelDriver::PalRegSet(int module, unsigned short reg)
{
//TODO_DONE pointers emc palregset

////#define ADR_MAIN		0xDE110000
////#define ADR_OFFSET		0x00001000
//	 unsigned short *ptr;
//	 ptr = (unsigned short*) (ADR_MAIN + ( ADR_OFFSET*(module) ) + 0x806) ; //EMC

//	 *ptr=baseReg;
	 int devDriver = LowLevelDriver::getDriver(module);
     EaSetREGPALWADR(devDriver,reg);

	return 1 ;
}
#endif

int LowLevelDriver::adaInit(int module)
{
    unsigned short dummey;
    int driver = LowLevelDriver::getDriver(module);
    EaGetADACRO(driver, &dummey); /* read ADACRO clears pointer              */
    usleep(5000);

    EaSetADACRO(driver, 0x22); /* Port A Path Polarity in Non-Invert Mode */
    EaSetADACRO(driver, 0x00);
    EaSetADACRO(driver, 0x2a); /* Port B Path Polarity in Non-Invert Mode */
    EaSetADACRO(driver, 0x00);
    EaSetADACRO(driver, 0x05); /* Port C Path Polarity in Non-Invert Mode */
    EaSetADACRO(driver, 0x00);

    EaSetADACRO(driver, 0x23); /* Port A myData Direction Register */
    EaSetADACRO(driver, 0xf3);
    EaSetADACRO(driver, 0x2b); /* Port A myData Direction Register */
    EaSetADACRO(driver, 0xc2);
    EaSetADACRO(driver, 0x06); /* Port A myData Direction Register */
    EaSetADACRO(driver, 0x0e);

    EaSetADACRO(driver, 0x01); /* Change Master Configuration Control Register */
    EaSetADACRO(driver, 0xf7);

    EaSetADAPADR(driver, 0xec); /* port a */
    EaSetADAPBDR(driver, 0x2c); /* port b */
    EaSetADAPCDR(driver, 0x02); /* port c */

    return 1;
}

int LowLevelDriver::ipcInit(int module)
{
    unsigned short dummey;
    int driver = LowLevelDriver::getDriver(module);
    EaGetADACRO(driver, &dummey); /* read ADACRO clears pointer              */
    EaSetADACRO(driver, 0); /* clear ADACRO */
    EaGetADACRO(driver, &dummey); /* read ADACRO clears pointer              */

    EaSetADACRO(driver, 1); /* point to master control */
    EaSetADACRO(driver, 0xb7); /* fifo off */

    EaSetIPCCONFIG(driver, 1); /* init the counters */
    return 1;
}

int LowLevelDriver::getName(int module, char *name)
{
    uint16_t myData;
    int driver = LowLevelDriver::getDriver(module);

    //if ( driver <= 0 )
    //{
    //	strcpy(name,"ERROR !!");
    //	return -1;
    //}
#ifndef IFC
    const int nameSize = 8;
    const int nameBase = 0x20;
    for (int i = 0; i < nameSize; i++)
    {
        EaSetFHRWTADR(driver, (i + nameBase));
        EaGetFHRW(driver, &myData);
        name[i] = (char) myData;
    }
    // sizeof name arr is 9, easy
    name[nameSize] = '\0';
#else
	printf("*****************\n");
	EaGetRomIFC(driver,0x4a,&myData);
    name[0]=myData;
	EaGetRomIFC(driver,0x4c,&myData);
    name[1]=myData;
	EaGetRomIFC(driver,0x4d,&myData);
    name[2]=myData;
    name[3]=0;
    printf("Module name is %s\n",name);
	printf("*****************\n");
#endif
    return 1;
}

void LowLevelDriver::Waitcy_H(int32_t lNbcy)/*attente cycles CPU 1=1 uS environ*/
{
//        long lCy;
//        printf("*\n");
//        lNbcy*=12L;
//        for (lCy=0;lCy<=lNbcy;lCy++);
    usleep(lNbcy);
}

std::string LowLevelDriver::TimeString()
{
    struct tm *t;
    time_t now;
    time(&now);
    t = localtime(&now);

    //LowLevel::sayHelloStatic();
    char timeNow[60];
    // year month and date include
    //sprintf(timeNow,"%2.2d/%2.2d/%4d %2.2d:%2.2d:%2.2d\n",
    //                    t->tm_mday,t->tm_mon+1,t->tm_year+1900,t->tm_hour,t->tm_min,t->tm_sec);
    sprintf(timeNow, "%2.2d:%2.2d:%2.2d ", t->tm_hour, t->tm_min, t->tm_sec);

    std::string msg = timeNow;
    return msg;
}
