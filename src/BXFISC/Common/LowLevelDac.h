#ifndef _LowLevelDac_H_
#define _LowLevelDac_H_

#include <BXFISC/Common/LowLevelInclude.h>

class LowLevelDac
{

public:

    LowLevelDac();
    ~LowLevelDac();
    static int Init(int module);
    static int SetVoltage(int module, int channel, char* action, double voltage, int iGain, int iOffset,
            int iMaxAllowed, int iMinAllowed, double userMaxAllowed, double userMinAllowed, char* msg);

    static int ReadAll(int module, float* valueD, unsigned short* valueS);
    static int ReadBase(int module, float* valueD, unsigned short* valueS);
    static int ReadMpc(int module, float* valueD, unsigned short* valueS);
    static int DacRawSet(int module, BX_DAC channel, int value);
    static int DacMpcRawSet(int module, BX_DAC_MPC channel, int value);
    static int DacBaseRawSet(int module, BX_DAC_BASE channel, int value);
    static int MotorToPosition(int module, float position);

#ifdef EMC
	static int ReadBase(int module, float* valueD,unsigned short* valueS);
	static int ReadMpc(int module, float* valueD,unsigned short* valueS);
	static int DacRawSet(int module, BX_DAC channel,int value);
	static int DacMpcRawSet(int module, BX_DAC_MPC channel,int value);
	static int DacBaseRawSet(int module,BX_DAC_BASE channel,int value);
	static int MotorToPosition(int module,float position);
	
#endif
    static void Verbose(int module);
    static int IsVerbose(int module);
    static void Silent(int module);

private:

};
#endif
