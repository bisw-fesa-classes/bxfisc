#include "LowLevelSignalTimers.h"
#include "LowLevelInclude.h"
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <chrono>

#include "LowLevelFisc.h"

static int SignalTimersVerbose[16];

LowLevelSignalTimers::LowLevelSignalTimers()
{
    std::cout << "LowLevelSignalTimers class created \n";
//	printf("test class created \n") ;
}

LowLevelSignalTimers::~LowLevelSignalTimers()
{
    std::cout << "lowLevelSignalTimers class removed \n";
}

int LowLevelSignalTimers::InitTimers(int module)
{
    unsigned short value;
    int devDriver = LowLevelDriver::getDriver(module);
    EaGetADACRO(devDriver, &value); /* read ADACRO clears pointer                */
    EaSetADACRO(devDriver, 0x17); /* Change timer 1 time constant LSB register */
    EaSetADACRO(devDriver, 0xd0); /* 2khz --> fifo= 1khz                       */
    EaSetADACRO(devDriver, 0x16); /* change timer 1 time constant msb register */
    EaSetADACRO(devDriver, 0x07); /* 2khz                                      */
    EaSetADACRO(devDriver, 0x1c); /* change timer 1 mode specification register*/
    EaSetADACRO(devDriver, 0xdc);
    EaSetADACRO(devDriver, 0x0a); /* change timer 1 command register           */
    EaSetADACRO(devDriver, 0x25);

    EaSetADACRO(devDriver, 0x19); /* Change timer 2 time constant LSB register */
    EaSetADACRO(devDriver, 0xff);
    EaSetADACRO(devDriver, 0x18); /* change timer 2 time constant msb register */
    EaSetADACRO(devDriver, 0xff);
    EaSetADACRO(devDriver, 0x1d); /* change timer 2 mode specification register*/
    EaSetADACRO(devDriver, 0xc0);
    EaSetADACRO(devDriver, 0x0b); /* change timer 2 command register           */
    EaSetADACRO(devDriver, 0x04);

    EaSetADACRO(devDriver, 0x1b); /* Change timer 3 time constant LSB register */
    EaSetADACRO(devDriver, 0xc8); /* 5khz-->BT=10khz */
    EaSetADACRO(devDriver, 0x1a); /* change timer 3 time constant msb register */
    EaSetADACRO(devDriver, 0x00); /* 5khz-->BT=10khz */
    EaSetADACRO(devDriver, 0x1e); /* change timer 3 mode specification register*/
    EaSetADACRO(devDriver, 0xdc);
    EaSetADACRO(devDriver, 0x0c); /* change timer 3 command register           */
    EaSetADACRO(devDriver, 0x24);

    EaSetADACRO(devDriver, 0x01); /* change master configuration control register */
    EaSetADACRO(devDriver, 0xE7);
    return 1;

}
void LowLevelSignalTimers::Restore(int module,int channel)
{
}

void LowLevelSignalTimers::Verbose(int module)
{
    SignalTimersVerbose[module] = 1;
}
int LowLevelSignalTimers::IsVerbose(int module)
{
    return SignalTimersVerbose[module];
}
void LowLevelSignalTimers::Silent(int module)
{
    SignalTimersVerbose[module] = 0;
}
void LowLevelSignalTimers::FifoFreqSet(int module, int freqReq)
{
    int devDriver = LowLevelDriver::getDriver(module);
    unsigned int freqInt = 0;
    unsigned short freqLow, freqHigh, value;
    float freq;
    freq = 2000000 / freqReq; // 100 hz
    freqInt = (unsigned int) freq;
    freqLow = (unsigned short) (freqInt & 0x000ff);
    freqHigh = (unsigned short) ((freqInt & 0xff00) >> 8);

    EaGetADACRO(devDriver, &value);
//  	EaSetADACRO(devDriver,(unsigned short)0);

    EaSetADACRO(devDriver, (unsigned short) 0x17); // LSB register
    EaSetADACRO(devDriver, (unsigned short) freqLow); // the value at rom loaction 680
    EaSetADACRO(devDriver, (unsigned short) 0x16); // MSB register
    EaSetADACRO(devDriver, (unsigned short) freqHigh); // the value at rom loaction 681
                                                       // fifo set to x hz
    EaSetADACRO(devDriver, (unsigned short) 0x1c); // Timer 1 Mode Specification register
    EaSetADACRO(devDriver, (unsigned short) 0xdc);
    EaSetADACRO(devDriver, (unsigned short) 0x0a); // Timer 1 Command Register
    EaSetADACRO(devDriver, (unsigned short) 0x35);

    return;
}
int LowLevelSignalTimers::FifoFreqGet(int module)
{
    unsigned short data;
    unsigned int freqInt = 0;
    int devDriver = LowLevelDriver::getDriver(module);
    float freq = 0;
    EaGetADACRO(devDriver, &data);
    EaSetADACRO(devDriver, 0x16);
    EaGetADACRO(devDriver, &data);
    freqInt = ((unsigned int) data) << 8;
    EaSetADACRO(devDriver, 0x17);
    EaGetADACRO(devDriver, &data);
    freq = (1000000.0 / (freqInt + data) * 2);
    freqInt = (int) freq;
    //printf("LowLevelSignalTimers::FifoFreqGet low %d %d %d\n",freqInt >> 8 ,data,(int)freqInt);

    return freqInt;

}

void LowLevelSignalTimers::FifoRead(int module, float *counts)
{
    unsigned short data[2];
    int devDriver = LowLevelDriver::getDriver(module);
    EaGetIPCFIFO(devDriver, &data[0]);
    EaGetIPCFIFO(devDriver, &data[1]);
    data[0] = data[0] & 0xffff;
    data[1] = data[1] & 0xffff;
    *(counts + 0) = (float) data[0] + (float) (((unsigned int) (data[1]) << 16));
    EaGetIPCFIFO(devDriver, &data[0]);
    EaGetIPCFIFO(devDriver, &data[1]);
    data[0] = data[0] & 0xffff;
    data[1] = data[1] & 0xffff;
    *(counts + 1) = (float) data[0] + (float) (((unsigned int) (data[1]) << 16));
    EaGetIPCFIFO(devDriver, &data[0]);
    EaGetIPCFIFO(devDriver, &data[1]);
    data[0] = data[0] & 0xffff;
    data[1] = data[1] & 0xffff;
    *(counts + 2) = (float) data[0] + (float) (((unsigned int) (data[1]) << 16));
    EaGetIPCFIFO(devDriver, &data[0]);
    EaGetIPCFIFO(devDriver, &data[1]);
    data[0] = data[0] & 0xffff;
    data[1] = data[1] & 0xffff;
    *(counts + 3) = (float) data[0] + (float) (((unsigned int) (data[1]) << 16));

    return;
}

int LowLevelSignalTimers::ReadCounters(int module, double *ptrDouble)
{
    int devDriver = LowLevelDriver::getDriver(module);
    uint16_t low;
    uint16_t high;
    uint32_t intHigh;
    uint32_t intLow;
    //printf("%s %d start \n",__FILE__,__LINE__);

    EaGetIPC1LOW(devDriver, &low);
    usleep(2000);
    EaGetIPC1HIG(devDriver, &high);
    usleep(2000);

    intHigh = (unsigned int) (high) << 16;
    intLow = (unsigned int) (low);

    //printf("%s %d %xstart \n",__FILE__,__LINE__,intHigh,intLow);

    *(ptrDouble + 0) = (double) (intHigh + (unsigned int) intLow);

    EaGetIPC2LOW(devDriver, &low);
    usleep(2000);
    EaGetIPC2HIG(devDriver, &high);
    usleep(2000);
    intHigh = (unsigned int) (high) << 16;
    intLow = (unsigned int) (low);
    *(ptrDouble + 1) = (double) (intHigh + (unsigned int) intLow);

    EaGetIPC3LOW(devDriver, &low);
    usleep(2000);
    EaGetIPC3HIG(devDriver, &high);
    intHigh = (unsigned int) (high) << 16;
    intLow = (unsigned int) (low);
    usleep(2000);
    *(ptrDouble + 2) = (double) (intHigh + (unsigned int) intLow);

    EaGetIPC4LOW(devDriver, &low);
    usleep(2000);
    EaGetIPC4HIG(devDriver, &high);
    usleep(2000);
    intHigh = (unsigned int) (high) << 16;
    intLow = (unsigned int) (low);
    *(ptrDouble + 3) = (double) (intHigh + (unsigned int) intLow);
    return (1);

}

int   LowLevelSignalTimers::FifoIsEmpty(int module)
{
  int devDriver = LowLevelDriver::getDriver(module);

  int status;
  unsigned short reg ;
  EaGetREGBASRW(devDriver,&reg);
  if ( (reg & 0x8000) == 0x8000 )
  {
    status= 1 ;
  }else
  {
    status= 0 ;
  }
  return status;

}

int  LowLevelSignalTimers::FifoEmptyNow(int module )
{
	int devDriver = LowLevelDriver::getDriver(module);
	int i;
	unsigned short dummy ;
	for( i=0 ; i<8191 && LowLevelSignalTimers::FifoIsEmpty(module)==0; i++ ) // why the check on 8191 ???
	{
		EaGetADCFFREAD(devDriver,&dummy);

	}
	return i;
}


int  LowLevelSignalTimers::FifoCountsEmptyNow(int module)
{
  int i;
  int devDriver = LowLevelDriver::getDriver(module);

  unsigned short dummy ;
  for( i=0 ; i<8191 && LowLevelSignalTimers::FifoCountsIsEmpty(module)==0; i++ ) // why the check on 8191 ???
  {
     EaGetIPCFIFO(devDriver,&dummy);
  }
  return i;
}
int   LowLevelSignalTimers::FifoCountsIsEmpty(int module)
{
  int devDriver = LowLevelDriver::getDriver(module);
  int status;
  unsigned short reg ;
  EaGetIPCSTAT(devDriver,&reg);
  if ( (reg & 0x1) == 0x1 )
  {
    status= 1 ;
  }else
  {
    status= 0 ;
  }
  return status;
}


void LowLevelSignalTimers::FifoPositionsToArray(int module,double *pos,int* count,int max )
{
  float position;
  int i;
  uint16_t data,reg;
  float magic = 4.9888+4.9932;
  int devDriver = LowLevelDriver::getDriver(module);

  //if ( FISC_DEBUG==flag ) { fiscLogMsg("FiscTools::fifoPositionsToSpillArray "); }
  EaGetREGBASRW(devDriver,&reg);

  for( i=0 ; i<8191 && (reg & 0x8000) != 0x8000 && i < max ; i++ ) // why the check on 8191 ???
  {
    EaGetADCFFREAD(devDriver,&data);// get the position
  	EaGetREGBASRW(devDriver,&reg); 	// check if it is empty

    data=data&0xfff;
    if ( data & 0x800 )
    {
      position = ((double)(data-4095.00)*magic/4095.0)*10;
    } else
    {
      position = ((float)(data&0x07FF)*magic/4095.0)*10;
    }
    *(pos+i)=position;
  }
  *count=i;
}

int LowLevelSignalTimers::ReturnToStartPosition(const double start, const int32_t module) {


    LowLevelDac::DacMpcRawSet(module, DAC_MPC_MOTOR_SPEED_6, 4095); // full speed

    const int devDriver = LowLevelDriver::getDriver(module);
    // conversion: desired position [mm] to DAC register value
    const int16_t startValueMv = static_cast<int16_t>((start / 10.0 + 5.0) * 4096 / 10);

    EaSetMDAC0(devDriver, startValueMv);
    EaSetMOTORPREG(devDriver, 0); // GO!
    return 0;
}

int LowLevelSignalTimers::ScanSetUp(int module,double start,double stop,int delay, int time, unsigned short regMaster,unsigned short regCoin)
{
    using namespace std::chrono;

//	LowLevelDriver::BaseRegDisableExtraction(module);


//	LowLevelDac::MotorToPosition(module,start);
	int* rom =LowLevelDriver::GetRom(module);
	int devDriver = LowLevelDriver::getDriver(module);
	int dacValue;
//	int delayInt;
	uint16_t data[4],value;
//
//	if ( delay < 1 ) delay =1;
//	if ( stop >start )
//	{
//		stop=stop+0.7;
//	}
//	if ( stop <start )
//	{
//		stop=stop-0.7;
//	}
////--  EaSetMOTORPREG(  devDriver,(unsigned short)0);
//
////--freg=2000000/counter
////--freq=8k(fifo size)/time
////--counter=2000000*time/8k
////--counter=244.140625*time;
//
////-- watch out could be 8 not 8000 !!!!!!!!!!!
////--  float fifo=244.140625*time/8000; // now at 1k buffer size.
  float fifo=244.140625*time/8000; // now at 1k buffer size.
  fifo=fifo*65.10351/1.017;
  int fifoInt=(unsigned int)fifo;
////--  sprintf(logMsg,"FiscTools::fifoScanSetup fifo  fre [%d] time [%d] MSB [%04x] LSB [%04x]",fifoInt ,time ,data[0],data[1]);fiscLogMsg(logMsg);
  data[0]=(unsigned short)(fifoInt&0x000ff);
  data[1]=(unsigned short)((fifoInt&0xff00)>>8);
//
////-- printf("FiscTools::fifoScanSetup fifo  [%d] [%4f] MSB [%04x] LSB [%04x]\n",fifoInt,fifo,data[1],data[0]);
 	LowLevelSignalTimers::FifoFreqSet(module,100);
    EaGetADACRO(devDriver,&value);
////-- 1 ms delay
//  delayInt=(unsigned int)(delay)*10;
//
//
//  data[2]=(unsigned short)(delayInt&0xff);
//  data[3]=(unsigned short)(delayInt>>8);
//
//  EaSetADACRO(devDriver,(unsigned short)0x19);
//  EaSetADACRO(devDriver,(unsigned short)data[2]);
//  EaSetADACRO(devDriver,(unsigned short)0x18);
//  EaSetADACRO(devDriver,(unsigned short)data[3]);
//
////-- printf("FiscTools::fifoScanSetup delay [%04x] [%04x] [%04x] [%04x]\n",0x19,data[2],0x18,data[3]);
  EaSetADACRO(devDriver,(unsigned short)0x1d);
  EaSetADACRO(devDriver,(unsigned short)0x71);
  EaSetADACRO(devDriver,(unsigned short)0x0b);
  EaSetADACRO(devDriver,(unsigned short)0x34);
////--  printf("FiscTools::fifoScanSetup [0x1d] [0x71] [0x0b] [0x34]\n");
  EaSetADACRO(devDriver,(unsigned short)0x01);
  EaSetADACRO(devDriver,(unsigned short)regMaster);
//
//	if ( start==stop )
//	{
//
////--	    printf("FiscTools::fifoScanSetup base register to %02x\n",BIT5);
//		EaSetREGBASRW(devDriver,BIT5    );
//
//
//	}
//
////-- will return at EE
////--	EaSetREGBASRW(devDriver,BIT5    );
//
    EaSetREGPALWADR(devDriver,(unsigned short)regCoin);
//
//
//
	EaSetPHAZERO(devDriver,(unsigned short)0x00);
	EaSetIPCCONFIG(devDriver,(unsigned short)0x01);
	EaSetIPCCONFIG(devDriver,(unsigned short)0x00);
//
////-- stop
//	if ( start!=stop )
//	{
//		dacValue = (int)((((float)stop/10.00)+(float)5.00)*(4096/10));
//		LowLevelDac::DacBaseRawSet(module,DAC_BASE_STOP_1 ,dacValue);
//		LowLevelDac::DacMpcRawSet(module,DAC_MPC_STOP_MIN_3 ,dacValue-25);
//		LowLevelDac::DacMpcRawSet(module,DAC_MPC_STOP_MAX_2 ,dacValue+25);
//	}
////-- park
//
//
//	if ( start==stop )
//  	{
//	   	LowLevelSignalTimers::MotorSpeedSet(module,2.855);
////--		EaSetMOTORPREG(  devDriver,(unsigned short)0);
//	} else
//	{
//
//
//	   	float slop    =rom[(2476/4)]/100000.00000;
//    	float offset  =rom[(2480/4)]/100000.00000;
////--    	printf("\nMush check float to in convertion slop %d %f offsett %d %f time %d delay %d\n"
////--    		,rom[(2476/4)],slop
////--    		,rom[(2480/4)],offset
////--    		,time,delay );
////--		printf("LowLevelSignalTimers::ScanSetUp %d %d\n",delay,time);
//
//		  float speed=MotorSpeedCal(start,stop,(float)(time/1000),(float)delay/1000.00,slop,offset);
//
////--		  float speed=MotorSpeedCal(start,stop,(float)(time/1000.000),(float)delay/1000.00,slop,offset);
//		  printf("LowLevelSignalTimers::ScanSetUp Start Stop Speed is %f %f %f %f\n",(float)start,(float)stop,(float)speed,(float)time);
//    	LowLevelSignalTimers::MotorSpeedSet(module,speed);
////--		EaSetMOTORPREG(  devDriver,(unsigned short)0);
//	}

//	LowLevelDriver::BaseRegEnableExtraction( module);

	return 1;
}

float LowLevelSignalTimers::MotorSpeedCal(float start,float stop,float time,float delay,float slop,float offset)
  {
    float move;
    float dacSpeed;
    move=stop-start;
    dacSpeed=move/(time-delay);
    dacSpeed=(dacSpeed-offset)/slop;
    return ( dacSpeed );
  }

int LowLevelSignalTimers::MotorSpeedSet(int module,float speed)
{
  int dacRaw=(int)(((speed*1000.00)+5000.00)*4096.00/10000);
  LowLevelDac::DacMpcRawSet(module,DAC_MPC_MOTOR_SPEED_6 ,dacRaw);
  return 1;
}



//void LowLevelSignalTimers::TstGenMode(int module,int mode,char* msg)
//
//{
//	printf("LowLevelSignalTimers::TstGenMode to do !!!!!!!!!!!!!!!!\n");
//}

int LowLevelSignalTimers::PositionOnlyStart(int driverNumber)
{
	int devDriver = LowLevelDriver::getDriver(driverNumber);
  	// setting up the timmer
  	unsigned short registerBase,registerBaseOld;

  	EaGetREGBASRW(devDriver,&registerBase);
  	registerBaseOld=registerBase;

	if ( registerBase & BASE_EXTRACTION_DISABLE_bit6 == 0x0000 )
	{
		registerBase=registerBase+BASE_EXTRACTION_DISABLE_bit6;
		registerBase = registerBase|0x30;
		EaSetREGBASRW(devDriver,registerBase);
	}
  	EaGetREGBASRW(devDriver,&registerBase);
  	EaSetADACRO (devDriver,(unsigned short)0x17);
  	EaSetADACRO(devDriver,(unsigned short)0x02);
  	EaSetADACRO(devDriver,(unsigned short)0x16);
  	EaSetADACRO(devDriver,(unsigned short)0x00);
  	EaSetADACRO(devDriver,(unsigned short)0x1c);
  	EaSetADACRO(devDriver,(unsigned short)0x5c);
  	EaSetADACRO(devDriver,(unsigned short)0x1c);
  	EaSetADACRO(devDriver,(unsigned short)0x4c);
  	EaSetADACRO(devDriver,(unsigned short)0x1c);
  	EaSetADACRO(devDriver,(unsigned short)0x44);
//
  	// now for the acq
  	EaSetADACRO(devDriver,(unsigned short)0x0a);
  	EaSetADACRO(devDriver,(unsigned short)0x27);
  	EaSetADACRO(devDriver,(unsigned short)0x0a);
  	EaSetADACRO(devDriver,(unsigned short)0x27);
  	EaSetADACRO(devDriver,(unsigned short)0x0a);
  	EaSetADACRO(devDriver,(unsigned short)0x27);
  	EaSetADACRO(devDriver,(unsigned short)0x0a);
  	EaSetADACRO(devDriver,(unsigned short)0x27);
  	EaSetADACRO(devDriver,(unsigned short)0x0a);
  	EaSetADACRO(devDriver,(unsigned short)0x27);
  	EaSetADACRO(devDriver,(unsigned short)0x0a);
  	EaSetADACRO(devDriver,(unsigned short)0x27);
  	EaSetADACRO(devDriver,(unsigned short)0x0a);
  	EaSetADACRO(devDriver,(unsigned short)0x27);
  	EaSetADACRO(devDriver,(unsigned short)0x0a);
  	EaSetADACRO(devDriver,(unsigned short)0x27);

  	registerBaseOld=registerBase;
	if ( registerBase != registerBaseOld )
	{
	    registerBaseOld = registerBaseOld|0x30;
		EaSetREGBASRW(devDriver,registerBaseOld);
	}

  	int i=0;//=LowLevelSignalTimers::FifoEmptyNow(driverNumber);


	if ( LowLevelSignalTimers::IsVerbose(driverNumber) )
	{
		std::cout << "COUT::XXX LowLevel::FifoPositionOnlyStart records in fifo [" << i << "]\n" ;
	}


  	return  1;

}

int LowLevelSignalTimers::PositionOnlyRead(int driverNumber,float *position)
{

  int i;
  unsigned short datax;
  unsigned short regx ;
  float magic = 4.9999+5.0001;  // hardware guys cannot count in the heads
  int devDriver = LowLevelDriver::getDriver(driverNumber);
  // there was a sleep here !!
  for( i=0 ; i<8191 ; i++ ) // why the check on 8191 ???
  //for( i=0 ; i<10; i++ ) // why the check on 8191 ???
  {
    BxfiscGetADCFFREAD(devDriver,&datax);
  	BxfiscGetREGBASRW(devDriver,&regx);
    if ( (regx & 0x8000) == 0x8000 )
  	{
  		// the fifo is emply
  		break;
  	}
  }
  datax=datax&0xfff;

  if ( datax & 0x800 )
  {
    *position = ((float)(datax-4095.00)*magic/4095.0)*10;
  } else
  {
    *position = ((float)(datax&0x07FF)*magic/4095.0)*10;
  }
  if ( LowLevelSignalTimers::IsVerbose(driverNumber) )
  {
  	printf("FiscTools::fifoPositionsToArray fininshed last pos [%f] \n",*position);
  }
  //LowLevelSignalTimers::FifoFreqSet(driverNumber,100);

  return  1;

}


void LowLevelSignalTimers::FifoCountsToArray(int module, int32_t *coin, int32_t *pm1, int32_t *pm2, int32_t *coinE,
        int *count, int max)
{

    int i;
    uint16_t data[2];
    uint16_t reg;
    int devDriver = LowLevelDriver::getDriver(module);
    EaGetIPCSTAT(devDriver, &reg); // is it empty

    for (i = 0; i < 8191 && (reg & 0x1) != 0x1 && i < max; i++)
    {
        EaGetIPCFIFO(devDriver, &data[0]);
        EaGetIPCFIFO(devDriver, &data[1]);
        data[0] = data[0] & 0xffff;
        data[1] = data[1] & 0xffff;
        coin[i] = (long) data[0] + (long) (((unsigned int) (data[1]) << 16));
        EaGetIPCFIFO(devDriver, &data[0]);
        EaGetIPCFIFO(devDriver, &data[1]);
        data[0] = data[0] & 0xffff;
        data[1] = data[1] & 0xffff;
        pm1[i] = (long) data[0] + (long) (((unsigned int) (data[1]) << 16));
        EaGetIPCFIFO(devDriver, &data[0]);
        EaGetIPCFIFO(devDriver, &data[1]);
        data[0] = data[0] & 0xffff;
        data[1] = data[1] & 0xffff;
        pm2[i] = (long) data[0] + (long) (((unsigned int) (data[1]) << 16));
        EaGetIPCFIFO(devDriver, &data[0]);
        EaGetIPCFIFO(devDriver, &data[1]);
        data[0] = data[0] & 0xffff;
        data[1] = data[1] & 0xffff;
        coinE[i] = (long) data[0] + (long) (((unsigned int) (data[1]) << 16));
        EaGetIPCSTAT(devDriver, &reg); // is it empty

    }

    *count = i;
}

void LowLevelSignalTimers::TstGenMode(int module, int mode, char *msg)
{


}

