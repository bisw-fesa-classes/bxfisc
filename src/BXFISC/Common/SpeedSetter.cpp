/*
 * SpeedSetter.cpp
 *
 *  Created on: Mar 15, 2021
 *      Author: arlis
 */

#include "SpeedSetter.h"

#include <BXFISC/Common/LowLevelDac.h>
#include <BXFISC/Common/SpeedConverter.h>


namespace NsSpeedConverter
{

void SpeedSetter::setSpeed(const double speed, BXFISC::Device * d, const fesa::MultiplexingContext * c) {
    const double speedGain = d->speedGain.get(c);
    const int32_t driver = d->driverNumber.get(c);
    NsSpeedConverter::SpeedConverter sc;
    LowLevelDac::DacMpcRawSet(driver, BX_DAC_MPC::DAC_MPC_MOTOR_SPEED_6, sc.convertSpeedToDAC(speed * speedGain));
}


} /* namespace NsSpeedConverter */
