#ifndef _LowLevelSignalTimers_H_
#define _LowLevelSignalTimers_H_
#include <iostream>
//#include "../genDrivers/VME-EaipEascint#Mon.Jan.03-15.44/include/EaipEascintIoctAccess.h"

class LowLevelSignalTimers
{
public:
	LowLevelSignalTimers();
	~LowLevelSignalTimers();
	static	void	Verbose(		int module);
	static	int		IsVerbose(		int module);
	static	void	Silent(			int module);
	static	void	Restore(		int module,int channel);
	static	int		InitTimers(		int module);

	static	int   	FifoFreqGet(	int module);

	static	void	FifoFreqSet(	int module,int freq);
	static	void	FifoRead(		int module,float *counts);
	static int		ReadCounters(	int module,double* ptrCounts);
	static	void 	FifoCountsToArray(int module,int32_t *coin,int32_t *pm1,int32_t *pm2,int32_t *coinE,int* count,int max );


	static 	void 	FifoPositionsToArray(int module,double *pos,int* count,int max );

	static	int		FifoIsEmpty(	int module);
	static	int		FifoCountsIsEmpty(	int module);
	static	int		FifoEmptyNow(	int module );
	static	int		FifoCountsEmptyNow(	int module );
	static int 		ScanSetUp(int module,double start,double stop,int delay,int time, unsigned short regMaster,unsigned short regCoin);
	static int      ReturnToStartPosition(const double start, const int32_t driver);
	static float 	MotorSpeedCal(float start,float stop,float time,float delay,float slop, float offset);
	static int 		MotorSpeedSet(int module,float speed);

	static int		PositionOnlyRead(int driverNumber,float *position);
	static int 		PositionOnlyStart(int driverNumber);

#ifdef EMC

	static 	void 	FifoPositionsToArray(int module,double *pos,int* count,int max );

	static	int		FifoIsEmpty(	int module);
	static	int		FifoCountsIsEmpty(	int module);
	static	int		FifoEmptyNow(	int module );
	static	int		FifoCountsEmptyNow(	int module );
	static int 		ScanSetUp(int module,double start,double stop,int delay,int time, unsigned short regMaster,unsigned short regCoin);
	static float 	MotorSpeedCal(float start,float stop,float time,float delay,float slop, float offset);
	static int 		MotorSpeedSet(int module,float speed);

	static int		PositionOnlyRead(int driverNumber,float *position);
	static int 		PositionOnlyStart(int driverNumber);
#endif

	static	void	TstGenMode(		int module,int mode,char* debugStr);

private :
//#ifdef FISC
//
//   	static int 		baseRegGet(int deviceDriver,FISC_BASE_REG which , unsigned short* bitValue);
//  	static int 		baseRegSet(int deviceDriver,FISC_BASE_REG which   unsigned short  bitValue);
//#endif
//


};
#endif
