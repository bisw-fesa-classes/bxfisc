/*
 * AdjustSpeedHandler.h
 *
 *  Created on: Mar 15, 2021
 *      Author: arlis
 */

#ifndef SRC_BXFISC_COMMON_AdjustSpeedHandler_H_
#define SRC_BXFISC_COMMON_AdjustSpeedHandler_H_

#include "ISpeedHandler.h"
#include <BXFISC/GeneratedCode/TypeDefinition.h>
#include <BXFISC/GeneratedCode/Device.h>

namespace NsSpeedConverter
{

class AdjustSpeedHandler : public ISpeedHandler
{
private:
    BXFISC::Device * d;
    const fesa::MultiplexingContext * c;
public:
    AdjustSpeedHandler(BXFISC::Device * d, const fesa::MultiplexingContext * c);
    virtual ~AdjustSpeedHandler() = default;
    virtual void setSpeed(const BXFISC::EVENTS::EVENTS ev, const double speed) override;

};

} /* namespace NsSpeedConverter */

#endif /* SRC_BXFISC_COMMON_AdjustSpeedHandler_H_ */
