#include "LowLevelInclude.h"

#include "LowLevelDelay.h"
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/ioctl.h>

#include "LowLevelFisc.h"

#ifdef EMC
#include "LowLevelFisc.h"
#define FISC 1
#endif


#ifdef RANGE0123

#define RANGES 4

#define DEL_I2C_DELAY			300



#define ROM_DELAY1_ADR          0x578
#define ROM_DELAY1_SIZE         73
#define ROM_DELAY1_WIDTH_ADR    0x7C0
#define ROM_DELAY1_WIDTH_SIZE   25

#define ROM_DELAY1_1_ADR        0x8fc
#define ROM_DELAY1_1_SIZE       1
#define ROM_DELAY1_1_WIDTH_ADR  0xb54
#define ROM_DELAY1_1_WIDTH_SIZE 1


#define ROM_DELAY1_2_ADR        0x900
#define ROM_DELAY1_2_SIZE       25
#define ROM_DELAY1_2_WIDTH_ADR  0xb5c
#define ROM_DELAY1_2_WIDTH_SIZE 1

#define ROM_DELAY1_3_ADR        0x970
#define ROM_DELAY1_3_SIZE       25
#define ROM_DELAY1_3_WIDTH_ADR  0xbb8
#define ROM_DELAY1_3_WIDTH_SIZE 25


#define ROM_DELAY2_ADR          0x69c
#define ROM_DELAY2_SIZE         73
#define ROM_DELAY2_WIDTH_ADR    0x824
#define ROM_DELAY2_WIDTH_SIZE   25 

#define ROM_DELAY2_1_ADR        0xa28
#define ROM_DELAY2_1_SIZE       1
#define ROM_DELAY2_1_WIDTH_ADR  0xb58
#define ROM_DELAY2_1_WIDTH_SIZE 1

#define ROM_DELAY2_2_ADR        0xa2c
#define ROM_DELAY2_2_SIZE       25
#define ROM_DELAY2_2_WIDTH_ADR  0xb60
#define ROM_DELAY2_2_WIDTH_SIZE 1

#define ROM_DELAY2_3_ADR        0xa9c
#define ROM_DELAY2_3_SIZE       25
#define ROM_DELAY2_3_WIDTH_ADR  0xc1c
#define ROM_DELAY2_3_WIDTH_SIZE 25

int delayInfo[8][4]=
{
	{ROM_DELAY1_ADR,	ROM_DELAY1_SIZE,	ROM_DELAY1_WIDTH_ADR,	ROM_DELAY1_WIDTH_SIZE	},
	{ROM_DELAY1_1_ADR,	ROM_DELAY1_1_SIZE,	ROM_DELAY1_1_WIDTH_ADR,	ROM_DELAY1_1_WIDTH_SIZE	},
	{ROM_DELAY1_2_ADR,	ROM_DELAY1_2_SIZE,	ROM_DELAY1_2_WIDTH_ADR,	ROM_DELAY1_2_WIDTH_SIZE	},
	{ROM_DELAY1_3_ADR,	ROM_DELAY1_3_SIZE,	ROM_DELAY1_3_WIDTH_ADR,	ROM_DELAY1_3_WIDTH_SIZE	},
	{ROM_DELAY2_ADR,	ROM_DELAY2_SIZE,	ROM_DELAY2_WIDTH_ADR,	ROM_DELAY2_WIDTH_SIZE	},
	{ROM_DELAY2_1_ADR,	ROM_DELAY2_1_SIZE,	ROM_DELAY2_1_WIDTH_ADR,	ROM_DELAY2_1_WIDTH_SIZE	},
	{ROM_DELAY2_2_ADR,	ROM_DELAY2_2_SIZE,	ROM_DELAY2_2_WIDTH_ADR,	ROM_DELAY2_2_WIDTH_SIZE	},
	{ROM_DELAY2_3_ADR,	ROM_DELAY2_3_SIZE,	ROM_DELAY2_3_WIDTH_ADR,	ROM_DELAY2_3_WIDTH_SIZE	}
};

struct romDelay
{
	double delay[4][2];
	double width[4][2];
} ;


static struct romDelay romDelayChannel[16][2];  
static int DelayVerbose[16] ;

static int findNearestValueInARange(double value,int *ptrInt,int numberOfValues,double *foundValue);
static int findNearestDelayValueInARange(int module,double value,		int channel,int range,double* delayValue);
static int findNearestWidthValueInARange(int module,double value,	int channel,int range,double* delayValue);
#endif


#define RANGES 1

#define DEL_I2C_DELAY			300


#define ROM_DELAY1_ADR          0x578
#define ROM_DELAY1_SIZE         73
#define ROM_DELAY1_WIDTH_ADR    0x7C0
#define ROM_DELAY1_WIDTH_SIZE   25

#define ROM_DELAY2_ADR          0x69c
#define ROM_DELAY2_SIZE         73
#define ROM_DELAY2_WIDTH_ADR    0x824
#define ROM_DELAY2_WIDTH_SIZE   25 

int delayInfo[2][4]=
{
	{ROM_DELAY1_ADR,	ROM_DELAY1_SIZE,	ROM_DELAY1_WIDTH_ADR,	ROM_DELAY1_WIDTH_SIZE	},
	{ROM_DELAY2_ADR,	ROM_DELAY2_SIZE,	ROM_DELAY2_WIDTH_ADR,	ROM_DELAY2_WIDTH_SIZE	},
};

struct romDelay
{
	double delay[1][2];
	double width[1][2];
} ;


static struct romDelay romDelayChannel[4][2];  
static int DelayVerbose[16] ;

static int findNearestValueInARange(double value,int *ptrInt,int numberOfValues,double *foundValue);
static int findNearestDelayValueInARange(int module,double value,		int channel,int range,double* delayValue);
static int findNearestWidthValueInARange(int module,double value,	int channel,int range,double* delayValue);

LowLevelDelay::LowLevelDelay()
{
	std::cout << "LowLevelDelay class created \n" ;
//	printf("test class created \n") ;
}

LowLevelDelay::~LowLevelDelay()
{
	std::cout << "lowLevelDelay class removed \n" ;
}
#ifndef NO_DELAYS
int 	LowLevelDelay::DelayInfo(int module,int channel
 		,double* delay,double* delay_setting
		,double* delayWidth,double* delayWidth_setting
		,double* delayRanges_max,double* delayRanges_min
		,double* delayWidthRanges_max,double* delayWidthRanges_min)
{

//		printf("LowLevelDelay::DelayInfo hello \n");
		LowLevelDelay::Verbose(module);
		LowLevelDelay::IfRangesNotInMemLoad(module);
		LowLevelDelay::Silent(module);
//		char infoStr[160];
		for ( int i=0 ; i < RANGES ; i++ )
		{
			delayRanges_max[i]=romDelayChannel[module][channel].delay[i][0];
			delayRanges_min[i]=romDelayChannel[module][channel].delay[i][1];
			delayWidthRanges_max[i]=romDelayChannel[module][channel].width[i][0];
			delayWidthRanges_min[i]=romDelayChannel[module][channel].width[i][1];
		}
//		 for ( int j=0 ; j < 2 ; j++ )
//	 {
//		for ( int i=0 ; i < 4 ; i++ )
//		{
//
//			{
//				sprintf(infoStr,"Channel %d Range %d Max %10f Mim=n %10f MaxW %10f MinW %10f\n",j,i
//				,romDelayChannel[module][j].delay[i][0],romDelayChannel[module][j].delay[i][1]
//				,romDelayChannel[module][j].width[i][0],romDelayChannel[module][j].width[i][1]);
//				std::cout << "COUT:"<<LowLevelDriver::TimeString().c_str()<< "XXX LowLevelDelay::FindRanges        driver " 
//				<< module << infoStr ;
//				
//				
//			}
//
//			
//		}
//	 }
		return  0;
}

int  findNearestValueInARange(double value,int *ptrInt,int numberOfValues,double *foundValue)
{

	 int i ;
	 int aboveFound=-1;	 
	 int belowFound=-1;
	 int lowestValue=-1;
	 
	 int highestValue=-1;
	 
     int32_t below ;
	 int32_t above ;	 
	 int valueInt;
	 valueInt=(int)(value*100000.00);
	 
	 for ( i=0;i<numberOfValues;i++) /* finding the lowest value which is greater then 0 */
	 {
		  if( *(ptrInt+i)  > 0 )
		  {
			   lowestValue=i;
			   break ;
		  }
	 }
	 for ( i=0;i<numberOfValues;i++) /* finding the grestest value which is greater then 0 */
	 {
		  if( *(ptrInt+i)  > 0 )
		  {
			   highestValue=i;
		  }
	 }
  

	 for ( i=0;i<numberOfValues;i++) /* looking for the value just higher*/
	 {
		  if( *(ptrInt+i)  > 0 )
		  {
			   if ( valueInt >= *(ptrInt+i) )
			   {
					aboveFound=i; /* the value in ram is higher then the asked */
			   } else
			   {
					break ;
			   }			   
		  }		  
	 }

	 for ( i=numberOfValues;i>0;i--) /* looking for the value just lower */
	 {
		  
		  if( *(ptrInt+i-1) > 0 )
		  {
			   if ( valueInt <=*(ptrInt+i-1) )
			   {
			        belowFound=i-1; /* the value in ram is lower then asked */					
			   }else
			   {
					break;
			   }
			   
		  }		  
	 }

	 if ( aboveFound==-1 ) /* value was to low */
	 {
		  *foundValue=*(ptrInt+lowestValue)/100000.00;		  
/*	      biswLogMkEntry(LogName,BISW_STATUS,"[%04d] %s delay values ask %d match %d index %d[tp low]",
						 __LINE__,EnvDesc.hwConfPtr->instanceName,valueInt,*(ptrInt+belowFound),0);		
*/
		  return lowestValue ;		  
	 }
	 if ( belowFound==-1 ) /* value was to high */
	 {
		  *foundValue=*(ptrInt+highestValue)/100000.00;		  

/*	      biswLogMkEntry(LogName,BISW_STATUS,"[%04d] %s delay values ask %d match %d index %d[to high]",
						 __LINE__,EnvDesc.hwConfPtr->instanceName,valueInt,*(ptrInt+i-1),i-1);		
*/		  return highestValue ;		  
	 }
	 if ( belowFound==aboveFound )
	 {
		  *foundValue=*(ptrInt+aboveFound)/100000.00;		  
         
/*	      biswLogMkEntry(LogName,BISW_STATUS,"[%04d] %s delay match values ask %d match %d index %d equal",
						 __LINE__,EnvDesc.hwConfPtr->instanceName,valueInt,*(ptrInt+aboveFound),aboveFound);		
*/		  return aboveFound;
	 }
	 below=*(ptrInt+belowFound)-valueInt;
	 above=valueInt-*(ptrInt+aboveFound);

/*     sprintf(EnvDesc.hwConfPtr->debug,"v[%d],av[%d]bv[%d]b[%d]a[%d]"
			 ,valueInt,*(ptrInt+aboveFound),*(ptrInt+belowFound),below,above);
	 
*/
	 if ( below < above )
	 {
		  *foundValue=*(ptrInt+belowFound)/100000.00;		  
/*
	      biswLogMkEntry(LogName,BISW_STATUS,"[%04d] %s delay values ask %d match %d index %d belowfound",
						 __LINE__,EnvDesc.hwConfPtr->instanceName,valueInt,*(ptrInt+belowFound),belowFound);		
*/
		  return belowFound ;		  
	 }
		  *foundValue=*(ptrInt+aboveFound)/100000.00;		  
/*
	      biswLogMkEntry(LogName,BISW_STATUS,"[%04d] %s delay values ask %d match %d index %d abovefound",
						 __LINE__,EnvDesc.hwConfPtr->instanceName,valueInt,*(ptrInt+aboveFound),aboveFound);		
*/
	 return aboveFound ;
	      	 
}

int LowLevelDelay::IfRangesNotInMemLoad(int module)
{
	if ( romDelayChannel[module][0].delay[0][0] == 0.0 )
	{
		LowLevelDelay::FindRanges(module);
	}
	return 1;
}

int LowLevelDelay::FindRanges(int module)
{	 
	char infoStr[80];
	int *rom = LowLevelDriver::GetRom(module);
	double value;
	// Max 0

 	findNearestValueInARange(200,&rom[ROM_DELY1_0],72,&value);
	romDelayChannel[module][0].delay[0][0]=value;
	// Min 0
	findNearestValueInARange(1,&rom[ROM_DELY1_0],72,&value);
	romDelayChannel[module][0].delay[0][1]=value;
	// Max w1
	findNearestValueInARange(200,&rom[ROM_WDTH1_0],25,&value);
	romDelayChannel[module][0].width[0][0]=value;
	// Min w0
	findNearestValueInARange(1,&rom[ROM_WDTH1_0],25,&value);
	romDelayChannel[module][0].width[0][1]=value;
	// Max 1
#ifdef RANGE0123	
	findNearestValueInARange(200,&rom[ROM_DELY1R1_0],1,&value);
	romDelayChannel[module][0].delay[1][0]=value;
	// Min 1
	findNearestValueInARange(1,  &rom[ROM_DELY1R1_0],1,&value);
	romDelayChannel[module][0].delay[1][1]=value;
	// Max w1
	findNearestValueInARange(200,&rom[ROM_WDTH1R1],1,&value);
	romDelayChannel[module][0].width[1][0]=value;
	// Min w1
	findNearestValueInARange(1,  &rom[ROM_WDTH1R1],1,&value);
	romDelayChannel[module][0].width[1][1]=value;
	// Max 
	findNearestValueInARange(200,&rom[ROM_DELY1R2_0],25,&value);
	romDelayChannel[module][0].delay[2][0]=value;	
	// Min
	findNearestValueInARange(1,  &rom[ROM_DELY1R2_0],25,&value);
	romDelayChannel[module][0].delay[2][1]=value;
	// Max
	findNearestValueInARange(200,&rom[ROM_WDTH1R2],1,&value);
	romDelayChannel[module][0].width[2][0]=value;
	// Min
	findNearestValueInARange(1,  &rom[ROM_WDTH1R2],1,&value);
	romDelayChannel[module][0].width[2][1]=value;
	// Max
	findNearestValueInARange(200,&rom[ROM_DELY1R3_0],25,&value);
	romDelayChannel[module][0].delay[3][0]=value;
	// Min
	findNearestValueInARange(1,  &rom[ROM_DELY1R3_0],25,&value);
	romDelayChannel[module][0].delay[3][1]=value;
	// Max
	findNearestValueInARange(200,&rom[ROM_WDTH1R3_0],25,&value);
	romDelayChannel[module][0].width[3][0]=value;
	// Min
	findNearestValueInARange(1,  &rom[ROM_WDTH1R3_0],25,&value);
	romDelayChannel[module][0].width[3][1]=value;
#endif
	// Max 1.0
	findNearestValueInARange(200,&rom[ROM_DELY2_0],72,&value);
	romDelayChannel[module][1].delay[0][0]=value;
	// Min 1.0
	findNearestValueInARange(1,&rom[ROM_DELY2_0],72,&value);
	romDelayChannel[module][1].delay[0][1]=value;
	// Max 1.0w
	findNearestValueInARange(200,&rom[ROM_WDTH2_0],25,&value);
	romDelayChannel[module][1].width[0][0]=value;
	// Min 1.0w
	findNearestValueInARange(1,&rom[ROM_WDTH2_0],25,&value);
	romDelayChannel[module][1].width[0][1]=value;
#ifdef RANGE0123
	// Max 1.1
	findNearestValueInARange(200,&rom[ROM_DELY2R1_0],1,&value);
	romDelayChannel[module][1].delay[1][0]=value;
	// Min 1.1
	findNearestValueInARange(1,  &rom[ROM_DELY2R1_0],1,&value);
	romDelayChannel[module][1].delay[1][1]=value;
	// Max 1.1w
	findNearestValueInARange(200,&rom[ROM_WDTH2R1],1,&value);
	romDelayChannel[module][1].width[1][0]=value;
	// Min 1.1w
	findNearestValueInARange(1,  &rom[ROM_WDTH2R1],1,&value);
	romDelayChannel[module][1].width[1][1]=value;
	// Max 1.2
	findNearestValueInARange(200,&rom[ROM_DELY2R2_0],25,&value);
	romDelayChannel[module][1].delay[2][0]=value;
	// Min 1.2
	findNearestValueInARange(1,  &rom[ROM_DELY2R2_0],25,&value);
	romDelayChannel[module][1].delay[2][1]=value;
	// Max 1.2w
	findNearestValueInARange(200,&rom[ROM_WDTH2R2],1,&value);
	romDelayChannel[module][1].width[2][0]=value;
	// Min 1.2w
	findNearestValueInARange(1,  &rom[ROM_WDTH2R2],1,&value);
	romDelayChannel[module][1].width[2][1]=value;
	// Max 1.3
	findNearestValueInARange(200,&rom[ROM_DELY2R3_0],25,&value);
	romDelayChannel[module][1].delay[3][0]=value;
	// Min 1.3
	findNearestValueInARange(1,  &rom[ROM_DELY2R3_0],25,&value);
	romDelayChannel[module][1].delay[3][1]=value;
	// Max 1.3w
	findNearestValueInARange(200,&rom[ROM_WDTH2R3_0],25,&value);
	romDelayChannel[module][1].width[3][0]=value;
	// Min 1.3w
	findNearestValueInARange(1,  &rom[ROM_WDTH2R3_0],25,&value);
	romDelayChannel[module][1].width[3][1]=value;
#endif		
	 for ( int j=0 ; j < 2 ; j++ )
	 {
		for ( int i=0 ; i < RANGES ; i++ )
		{
		
		if ( LowLevelDriver::isRomReadBlockVerbose(module) )
			{
				sprintf(infoStr,"Channel %d Range %d Max %10f Mim=n %10f MaxW %10f MinW %10f\n",j,i
				,romDelayChannel[module][j].delay[i][0],romDelayChannel[module][j].delay[i][1]
				,romDelayChannel[module][j].width[i][0],romDelayChannel[module][j].width[i][1]);
				std::cout << "COUT:"<<LowLevelDriver::TimeString().c_str()<< "XXX LowLevelDelay::FindRanges        driver " 
				<< module << infoStr ;
				
				
			}

			
		}
	 }

	 return 1 ;
	 
}

void LowLevelDelay::Write(int module,int channel,double* delayValue, double* widthValue)
{
	int dataArray[ROM_DELAY1_SIZE];
	LowLevelDelay::IfRangesNotInMemLoad(module);
	int whichRange;
	int delayInt ;
	int widthInt ;
	double ddelay ,dwidth;
	
//	LowLevelDelay::Init(module);
//printf ("*** %d %d %f %f \n",module,channel,delayValue, widthValue);
//printf ("**0 %f %f\n",romDelayChannel[module][channel].delay[0][0],romDelayChannel[module][channel].delay[0][1]);
//printf ("**0 %f %f\n",romDelayChannel[module][channel].delay[1][0],romDelayChannel[module][channel].delay[1][1]);
//printf ("**0 %f %f\n",romDelayChannel[module][channel].delay[2][0],romDelayChannel[module][channel].delay[2][1]);
	ddelay= *delayValue;
	dwidth= *widthValue;
	if ( RANGES==1 )
	{
		whichRange=0;
	} else 
	{
		if ( *delayValue >= romDelayChannel[module][channel].delay[0][1] )
		{
			whichRange=0 ;
		} else 
		{
			if ( *delayValue >= romDelayChannel[module][channel].delay[3][1] )
			{
				whichRange=3 ;
			} else
			{
				if ( *delayValue >= romDelayChannel[module][channel].delay[2][1] )
				{
					whichRange=2 ;
				} else
				{
					whichRange=1 ;
				}
			}
		}
		
	}
	delayInt	=findNearestDelayValueInARange(module,*delayValue,channel,whichRange,delayValue);
	widthInt    =findNearestWidthValueInARange(module,*widthValue,channel,whichRange,widthValue);
///////////////////
	//printf ("LowLevelDelay::Write m %d c %d r %d d %f w %f d %d w %d \n",module,channel,whichRange,*delayValue, *widthValue,delayInt,widthInt);
	//LowLevelDelay::Init(module);
#ifdef RANGE0123	
	LowLevelDelay::RangeSet(module,whichRange ,channel);
#endif

	printf("Arek delay: %d, width: %d\n", delayInt, widthInt);
	switch ( whichRange )
	{
	case 0:
		LowLevelDelay::Write0(module,channel,delayInt,widthInt);
		break ;
	case 1:
		break;
	case 2:
		LowLevelDelay::Write2(module,channel,delayInt,widthInt);
		break ;
	case 3:
		LowLevelDelay::Write3(module,channel,delayInt,widthInt);
		break ;
	}
	//printf ("LowLevelDelay::Write %d %d %f %f %d %d \n",module,channel,*delayValue, *widthValue,delayInt,widthInt);
  
  
}
	
void LowLevelDelay::RangeSet(int module,int whichRange ,int channel)
{
	int devDriver = LowLevelDriver::getDriver(module);
	int delayPhos[4];

	if ( LowLevelDelay::IsVerbose(module) )
	{
		char msg[160];
      	sprintf(msg, " [%04d] %d delay write ch[%d] range [%d]"
		  , __LINE__
		  ,module
		  ,channel
		  ,whichRange
	      );

		std::cout << "COUT:"<< "       " << "XXX owLevelDelay::RangeSet           driver " 
						<<  module << msg <<"\n" ; 
		
 	}  
	 
	switch ( whichRange )
	{
	   case 0:
			PhosWrite(module,channel,0,0);	
			PhosWrite(module,channel,1,3);	
			PhosWrite(module,channel,2,0);	
			PhosWrite(module,channel,3,0);
			break ;
	   case 1:
			PhosWrite(module,channel,0,25);	
			PhosWrite(module,channel,1,25);	
			PhosWrite(module,channel,2,25);	
			PhosWrite(module,channel,3,25);	
			break ;
	   case 2:
			PhosWrite(module,channel,0,0);	
			PhosWrite(module,channel,1,25);	
			PhosWrite(module,channel,2,25);	
			PhosWrite(module,channel,3,25);	
			break ;
	   case 3:
			PhosWrite(module,channel,0,0);	
			PhosWrite(module,channel,1,3);	
			PhosWrite(module,channel,2,0);	
			PhosWrite(module,channel,3,25);	
			break ;
	   default:
			PhosWrite(module,channel,0,0);	
			PhosWrite(module,channel,1,3);	
			PhosWrite(module,channel,2,0);	
			PhosWrite(module,channel,3,0);	

			break ;
	}
	
  	
	unsigned short	   rangeRegister;
//	 unsigned short *ptr;
//	 ptr = (unsigned short*) (ADR_MAIN + ( ADR_OFFSET*(module-1) ) +  ADR_PHOSIO30) ;
//	 ptr = (unsigned short*) (ADR_PHOSIO+( ADR_OFFSET*(module) )) +0x30 ;
//	rangeRegister= *ptr;
	EaGetPHOSIO30  (devDriver,&rangeRegister);
    rangeRegister=rangeRegister&0x000F;
	if ( channel == 0 )
	{
		rangeRegister=(rangeRegister&0x000c)+whichRange;			
	} else
	{
		rangeRegister=(rangeRegister&0x0003)+(whichRange<<2);			
	}
	EaSetPHOSIO30  (devDriver,rangeRegister);

//	*ptr=rangeRegister;

	if ( LowLevelDelay::IsVerbose(module) )
	{
		char msg[160];
      	sprintf(msg ," [%04d] %d delay write ch[%d] range [%d] delay PHOSIO30 set to [%d]"
		  , __LINE__
		  ,module
		  ,channel
		  ,whichRange

	      ,rangeRegister);
		
		std::cout << "COUT:"<<"       "<< "XXX owLevelDelay::RangeSet           driver " 
						<<  module << msg <<"\n" ; 
		
 	}  
  
}

void LowLevelDelay::Write2(int module,int channel,int delayInt,int widthInt)
{
	PhosWrite(module,channel,0,delayInt);
}
void LowLevelDelay::Write3(int module,int channel,int delayInt,int widthInt)
{
	PhosWrite( module,channel,0,0);
	PhosWrite( module,channel,1,widthInt);
	PhosWrite( module,channel,2,delayInt);  
}


void LowLevelDelay::Write0(int module,int channel,int delayInt,int widthInt)
{
	int devDriver = LowLevelDriver::getDriver(module); 
	int delayPhos[4];
  
	if ( delayInt >=0 & delayInt <= 48 & widthInt >= 0 & widthInt <= 24 )
	{
		LowLevelDelay::PhosWrite( module,channel,0,0 );
		LowLevelDelay::PhosWrite( module,channel,1,widthInt);
        delayPhos[0]=0;		  
        delayPhos[1]=widthInt;		  
		if ( delayInt >= 0 & delayInt <= 24 )		  {
			LowLevelDelay::PhosWrite( module,channel,2,delayInt );
			LowLevelDelay::PhosWrite( module,channel,3,0);
			delayPhos[2]= delayInt;		  
			delayPhos[3]= 0;		  			   
 		}
		if ( delayInt >=25 & delayInt <= 48 )
		{
			LowLevelDelay::PhosWrite(module,channel,2,24 );
			LowLevelDelay::PhosWrite(module,channel,3,delayInt-24);
			delayPhos[2]= 24;		  
			delayPhos[3]= delayInt-24;		  			   			   
		}
	}
	if ( delayInt >= 49 & delayInt <= 72 & widthInt >= 0 & widthInt <= 24 )
	{
		LowLevelDelay::PhosWrite(module,channel,0,delayInt-48         );	
		LowLevelDelay::PhosWrite(module,channel,1,(delayInt+widthInt-48));  
		LowLevelDelay::PhosWrite(module,channel,2,24                  );
		LowLevelDelay::PhosWrite(module,channel,3,24                  );
		delayPhos[0]= delayInt-48;
		delayPhos[1]= (delayInt+widthInt-48);
		delayPhos[2]= 24;
		delayPhos[3]= 24;
	 }
  
	 printf("LowLevelDelay::Write0 return\n");
  }
  
// void  Waitcy_H(long lNbcy)/*attente cycles CPU 1=1 uS environ*/
// {
////	unsigned long lNbcy;
////	{
//	long lCy;
//        lNbcy*=12L;
//	for (lCy=0;lCy<=lNbcy;lCy++);
//	}
  
void LowLevelDelay::PhosWrite( int module, int channel,int delayNum,int value    )
{
	uint16_t data;

//	unsigned short	   rangeRegister;
	uint16_t *ptr;
//	 ptr = (unsigned short*) (ADR_MAIN + ( ADR_OFFSET*(module-1) ) +  ADR_PHOSIO30) ;
//	 ptr = (unsigned short*) (ADR_PHOSIO+( ADR_OFFSET*(module) ))  ;

	int devDriver = LowLevelDriver::getDriver(module);
	usleep(DEL_I2C_DELAY);
//	Waitcy_H(1000);
//printf("LowLevelDelay::PhosWrite before EaSetPHOSIO\n");
	EaSetPHOSIO(devDriver,(uint16_t)(channel<<=3));
//	Waitcy_H(1000);
//	data=(uint16_t)(channel<<3);
//	*ptr=data;
//	Waitcy_H(1000);
	usleep(DEL_I2C_DELAY);	 
    /*   I2Cstart(); */
//printf("LowLevelDelay::PhosWrite before EaSetPHOSIOPLUS\n");
    EaSetPHOSIOPLUS  (devDriver,(uint16_t)0xc5);
//	*(ptr+1)=(uint16_t)0xc5;
//	Waitcy_H(1000);
	usleep(DEL_I2C_DELAY);
     /*   end  */
//printf("LowLevelDelay::PhosWrite before EaSetPHOSIO\n");
	EaSetPHOSIO  (devDriver,(uint16_t)((delayNum<<5)+value));
//	data=(uint16_t)((delayNum<<5)+value);
//	*ptr=data;
	usleep(DEL_I2C_DELAY);
//	Waitcy_H(1000);
//	Waitcy_H(1000);
     /*   I2Cstop();  */
	EaSetPHOSIOPLUS  (devDriver,(uint16_t)0xc3);
//	*(ptr+1)=(unsigned short)0xc3;
	usleep(DEL_I2C_DELAY); 
//	Waitcy_H(1000);
//	printf("LowLevelDelay::PhosWrite channel %d delay %d value %d chex %x ahex %x\n"
//		,channel
//		,delayNum
//		,value
//		,(channel<<3)
//		,((delayNum<<5)+value)
//		);
//printf("LowLevelDelay::PhosWrite return\n");

}

int LowLevelDelay::Init(int module)
{
	int devDriver = LowLevelDriver::getDriver(module);
	EaSetPHOSIO3F  (devDriver,0x0000);
	sleep(10);
	EaSetPHOSIOPLUS  (devDriver,0x0080);
	usleep(DEL_I2C_DELAY);
	sleep(1);	 
	EaSetPHOSIO  (devDriver,0x0055);
    usleep(DEL_I2C_DELAY);
	sleep(1);	 
	EaSetPHOSIOPLUS  (devDriver,0x00A0);
	usleep(DEL_I2C_DELAY);
	sleep(1);	 
	EaSetPHOSIO  (devDriver,0x0018);
	usleep(DEL_I2C_DELAY);
	sleep(1);	 
	EaSetPHOSIOPLUS  (devDriver,0x00C1);
	usleep(DEL_I2C_DELAY);	 
	sleep(1);	 
	return 1 ;
}
void LowLevelDelay::Verbose(int module)
{
	DelayVerbose[module]=1 ;
}
int LowLevelDelay::IsVerbose(int module)
{
	return DelayVerbose[module] ;
}
void LowLevelDelay::Silent(int module)
{
	DelayVerbose[module]=0 ;
}

static int findNearestDelayValueInARange(int module,double value,		int channel,int range,double *delayValue)
{
	int *rom = LowLevelDriver::GetRom(module);
	int valueInt;
	if ( channel == 0 )
	{
		switch ( range )
		{
#ifdef RANGE0123
			case 1	:
			// Max 1
			valueInt=findNearestValueInARange(value,&rom[ROM_DELY1R1_0],1,delayValue);
			break ;
			case 2:
			// Max 2 
			valueInt=findNearestValueInARange(value,&rom[ROM_DELY1R2_0],25,delayValue);
			break ;
			case 3 :
			// Max 3
			valueInt=findNearestValueInARange(value,&rom[ROM_DELY1R3_0],25,delayValue);
			break ;		
#endif
			case 0	:
			default :
			// Max 0
 			valueInt=findNearestValueInARange(value,&rom[ROM_DELY1_0],72,delayValue);		
 			break ;
		}
	}else
	{
		switch ( range )
		{
#ifdef RANGE0123
			case 1	:
			// Max 1
			valueInt=findNearestValueInARange(value,&rom[ROM_DELY2R1_0],1,delayValue);
			break ;
			case 2	:
			// Max 2 
			valueInt=findNearestValueInARange(value,&rom[ROM_DELY2R2_0],25,delayValue);
			break ;
			case 3 :
			// Max 3
			valueInt=findNearestValueInARange(value,&rom[ROM_DELY2R3_0],25,delayValue);	
			break ;
#endif
			case 0	:
			default :
			// Max 0
 			valueInt=findNearestValueInARange(value,&rom[ROM_DELY2_0],72,delayValue);		
			break ;
		}
	}
	return valueInt ;
}

static int findNearestWidthValueInARange(int module,double value,		int channel,int range,double* ptrValue)
{
	int *rom = LowLevelDriver::GetRom(module);
	int valueInt;
	if ( channel == 0 )
	{
		switch ( range )
		{
#ifdef RANGE0123
			case 1:
			// Max w1
			valueInt=findNearestValueInARange(value,&rom[ROM_WDTH1R1],1,ptrValue);
			break ;
			case 2 :
			// Max w2
			valueInt=findNearestValueInARange(value,&rom[ROM_WDTH1R2],1,ptrValue);
			break;
			case 3:
			// Max w3
			valueInt=findNearestValueInARange(value,&rom[ROM_WDTH1R3_0],25,ptrValue);
			break;
#endif
			case 0:
			default :
			// Max w0
			valueInt=findNearestValueInARange(value,&rom[ROM_WDTH1_0],25,ptrValue);
			break ;
			
		}
	}else
	{
		switch ( range )
		{
#ifdef RANGE0123
			case 1:
			// Max w1
			valueInt=findNearestValueInARange(value,&rom[ROM_WDTH2R1],1,ptrValue);
			break ;
			case 2 :
			// Max w2
			valueInt=findNearestValueInARange(value,&rom[ROM_WDTH2R2],1,ptrValue);
			break ;
			case 3 :
			// Max w3
			valueInt=findNearestValueInARange(value,&rom[ROM_WDTH2R3_0],25,ptrValue);
			break ;
#endif			
			case 0 :
			default :
			// Max w0
			valueInt=findNearestValueInARange(value,&rom[ROM_WDTH2_0],25,ptrValue);
			break ;
		}
	}
	return valueInt ;
}
#endif

