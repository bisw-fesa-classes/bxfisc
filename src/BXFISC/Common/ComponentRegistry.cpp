/*
 * ComponentRegistry.cpp
 *
 *  Created on: Oct 28, 2020
 *      Author: arlis
 */

#include "ComponentRegistry.hpp"
#include <stdexcept>

namespace NsRegistry
{

ComponentRegistry::ComponentRegistry() : registry{}
{
    // TODO Auto-generated constructor stub

}

ComponentRegistry & ComponentRegistry::instance() {
    static ComponentRegistry cr;
    return cr;
}

ComponentRegistry::CardPtr ComponentRegistry::get(const uint32_t driverNumber) {

    if(driverNumber >= registry.size()) {
        throw std::out_of_range("Driver number out of range!");
    }

    if(registry.at(driverNumber) == nullptr) {
        registry.at(driverNumber) = std::make_shared<Card>(driverNumber);
    }

    return registry.at(driverNumber);
}


} /* namespace NsRegistry */
