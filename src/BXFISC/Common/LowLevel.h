#ifndef _LowLevel_H_
#define _LowLevel_H_
#include <iostream>
#include <time.h>
#include <string>

#include <BXFISC/GeneratedCode/Device.h>
#include <BXFISC/GeneratedCode/GenRTActions.h>
#include <BXFISC/GeneratedCode/Device.h>
#include <BXFISC/GeneratedCode/GlobalDevice.h>
#include <BXFISC/GeneratedCode/TypeDefinition.h>
//#include "BXSCINTTypeDefinition.h"


//#include "../genDrivers/VME-EaipEascint#Mon.Jan.03-15.44/include/EaipEascintIoctAccess.h"



#include <iostream>
#include <time.h>
#include <string>


namespace BXFISC {




class LowLevel
{
//
//#include <LowLevelScint.h>
//#endif

public:
	LowLevel();
	~LowLevel();

	static HARDWARE_ERRORS::HARDWARE_ERRORS DacsBaseGet(unsigned short* dacsShort ,float* dacsFloat,int driverNumber);
	static HARDWARE_ERRORS::HARDWARE_ERRORS DacsMpcGet(unsigned short* dacsShort ,float* dacsFloat,int driverNumber);
	static HARDWARE_ERRORS::HARDWARE_ERRORS AAGetMpx(int driverNumber,unsigned short *aaShort,float *aaFloat);
	static HARDWARE_ERRORS::HARDWARE_ERRORS PositionSet(int driverNumber, float position);
	static HARDWARE_ERRORS::HARDWARE_ERRORS	EE(int module,int32_t* pm1,int32_t* pm2,int32_t* coin,int32_t* coinE,double *pos ,int* numberInFIFO);
	static HARDWARE_ERRORS::HARDWARE_ERRORS EmcGainSet(int module,int32_t gain);
	static HARDWARE_ERRORS::HARDWARE_ERRORS EmcPhaGet(int module,int32_t *pha);
	static HARDWARE_ERRORS::HARDWARE_ERRORS EmcPhaDacsGet(int module,double *dacs);
	static HARDWARE_ERRORS::HARDWARE_ERRORS EmcPhaDacSet(int module,EMCDACS::EMCDACS which,double dac);
	static HARDWARE_ERRORS::HARDWARE_ERRORS EmcPhaRegSet(int module , unsigned short reg);
	static HARDWARE_ERRORS::HARDWARE_ERRORS EmcPhaRegGet(int module , unsigned short *reg);
    static HARDWARE_ERRORS::HARDWARE_ERRORS FifoPositionGet(float *position,double gain,int driverNumber);
	static HARDWARE_ERRORS::HARDWARE_ERRORS ScanSetUp(int module,double start,double stop,int delay, int time,unsigned short regMaster,unsigned short regCoin);
	static HARDWARE_ERRORS::HARDWARE_ERRORS ReturnToStartPosition(const double start, const int32_t driver);

	static HARDWARE_ERRORS::HARDWARE_ERRORS RegBaseGet(int module,unsigned short *reg);
	static HARDWARE_ERRORS::HARDWARE_ERRORS RegCoinGet(int module,unsigned short *reg);
	static HARDWARE_ERRORS::HARDWARE_ERRORS RegMotorGet(int module,unsigned short *reg);
	static HARDWARE_ERRORS::HARDWARE_ERRORS RegBaseSet(int module,unsigned short 	reg);
	static HARDWARE_ERRORS::HARDWARE_ERRORS RegCoinSet(int module,unsigned short 	reg);
	static HARDWARE_ERRORS::HARDWARE_ERRORS RegMotorSet(int module,unsigned short 	reg);


	static HARDWARE_ERRORS::HARDWARE_ERRORS StatusHv1(unsigned short portA,int* status,char* msg);
	static HARDWARE_ERRORS::HARDWARE_ERRORS StatusHv2(unsigned short portA,int* status,char* msg);
	static HARDWARE_ERRORS::HARDWARE_ERRORS StatusPsInt(unsigned short portA,int* status,char* msg);
	static HARDWARE_ERRORS::HARDWARE_ERRORS StatusPs28v(unsigned short portA,int* status,char* msg);
	static HARDWARE_ERRORS::HARDWARE_ERRORS FlashLed(int driverNumber);
	static HARDWARE_ERRORS::HARDWARE_ERRORS InitRom(int driverNumber);
	static HARDWARE_ERRORS::HARDWARE_ERRORS InitIpc(int driverNumber);
	static HARDWARE_ERRORS::HARDWARE_ERRORS InitAda(int driverNumber);
	static HARDWARE_ERRORS::HARDWARE_ERRORS InitDac(int driverNumber);



#ifndef SCINT
	static HARDWARE_ERRORS::HARDWARE_ERRORS GetPort(int driverNumber,int whichPort ,unsigned short *values);

#endif










//	static HARDWARE_ERRORS::HARDWARE_ERRORS StatusRegGet( int module,unsigned short *reg);
 	static HARDWARE_ERRORS::HARDWARE_ERRORS	DelayInfo(int module, int channel,
 	double* delay ,double* delay_setting,
	double* delayWidth,double* delayWidth_setting,
	double* delayRanges_max,double* delayRanges_min,
	double* delayWidthRanges_max,double* delayWidthRanges_min);


	static HARDWARE_ERRORS::HARDWARE_ERRORS InitDelay(int driverNumber);
	static HARDWARE_ERRORS::HARDWARE_ERRORS DelayFindRanges(int driverNumber);
	static void 							DelayVerbose(int driverNumber);
	static int  							DelayIsVerbose(int driverNumber);
	static HARDWARE_ERRORS::HARDWARE_ERRORS DelaySet (int driverNumber,int channel,double* delayValue, double* widthValue);
	static void 							DelaySilent(int driverNumber);




	static HARDWARE_ERRORS::HARDWARE_ERRORS Counters(int, double*);

	static HARDWARE_ERRORS::HARDWARE_ERRORS	WE(int module);

	static HARDWARE_ERRORS::HARDWARE_ERRORS BaseRegGet(int module,unsigned short *reg);
	static HARDWARE_ERRORS::HARDWARE_ERRORS AA_READ(int driverNumber);
	static HARDWARE_ERRORS::HARDWARE_ERRORS AA_GET(int driverNumber,float* aa);
	static HARDWARE_ERRORS::HARDWARE_ERRORS AA_CALIBRATE(int driverNumber);
	static void 							AAVerbose(int driverNumber);
	static int  							AAIsVerbose(int driverNumber);
	static void 							AASilent(int driverNumber);
	static HARDWARE_ERRORS::HARDWARE_ERRORS AAGet(int driverNumber,unsigned short *aaShort,float *aaFloat);


	static HARDWARE_ERRORS::HARDWARE_ERRORS DAC_GET(int driverNumber);
	static HARDWARE_ERRORS::HARDWARE_ERRORS DacGetRom(int driverNumber,struct romStruct *rom );
	static HARDWARE_ERRORS::HARDWARE_ERRORS DacsGet(unsigned short* dacsShort ,float* dacsFloat,int driverNumber);
	static void 							DacVerbose(int driverNumber);
	static int  							DacIsVerbose(int driverNumber);
	static HARDWARE_ERRORS::HARDWARE_ERRORS DacSet(int driverNumber,HARDWARE_DAC_CHANNELS07::HARDWARE_DAC_CHANNELS07 channel,float voltage,char* msg, bool do_log = false, bool do_not_invert = false);
	static void 							DacSilent(int driverNumber);
	static HARDWARE_ERRORS::HARDWARE_ERRORS FifoFregGet(int driverNumber);
	// below used by scint and fisc
	static HARDWARE_ERRORS::HARDWARE_ERRORS SetPort(int driverNumber,int whichPort ,unsigned short  value);
//	static HARDWARE_ERRORS::HARDWARE_ERRORS FifoIsEmpty(int driverNumber);
//	static HARDWARE_ERRORS::HARDWARE_ERRORS FifoEmpty(int driverNumber);
//	static HARDWARE_ERRORS::HARDWARE_ERRORS FifoReadout(int driverNumber);
//	static void 							FifoVerbose(int driverNumber);
//	static int  							FifoIsVerbose(int driverNumber);
//	static void  							FifoSilent(int driverNumber);



	static HARDWARE_ERRORS::HARDWARE_ERRORS MonitorAA(int module, float* aa);


	static HARDWARE_ERRORS::HARDWARE_ERRORS WireSpacing(double voltage, double *spacing);
	static int								getDriver(int module);
	static HARDWARE_ERRORS::HARDWARE_ERRORS GetModuleName(int module,char* name);
	static HARDWARE_ERRORS::HARDWARE_ERRORS MemoryAccessInit(int driverNumber);
	static HARDWARE_ERRORS::HARDWARE_ERRORS MemoryAccessSilentOff(char *file,int line, char*msg);
	static HARDWARE_ERRORS::HARDWARE_ERRORS MemoryAccessSilentOn(char *file,int line, char*msg);


	static std::string	TimeString();
	static void RomReadBlockSilent(int driverNumber);
	static void RomReadBlockVerbose(int driverNumber);
	static int  IsRomReadBlockVerbose(int driverNumber);
//	static int  delayRangesOk(int module);
//	static int	delayFindRanges(int module);

private :
//	static int	delayFindNearestValueInARange(double value,int *ptrInt,int numberOfValues,double *foundValue)

};
}
#endif
