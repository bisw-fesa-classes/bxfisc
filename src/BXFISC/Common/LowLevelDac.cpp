#include "LowLevelInclude.h"
#include "LowLevelDac.h"
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/ioctl.h>

#include "LowLevelFisc.h"

#include <cmw-log/Logger.h>
namespace
{

CMW::Log::Logger& logger = CMW::Log::LoggerFactory::getLogger("FESA.USR.BXFISC.RealTime.EeEventAction");

} // namespace

static int dacVerbose[16];

LowLevelDac::LowLevelDac()
{

}

LowLevelDac::~LowLevelDac()
{

}

int LowLevelDac::Init(int module)
{
    int devDriver = LowLevelDriver::getDriver(module);
//	LowLevelDriver::OkOrOpen(module);

    EaSetADADAC0(devDriver, 0x2000); /* channel 0 set to zero  */
    EaSetADADAC1(devDriver, 0x2000); /* channel 1 set to zero  */
    EaSetADADAC2(devDriver, 0x2000); /* channel 2 set to zero  */
    EaSetADADAC3(devDriver, 0x2000); /* channel 3 set to zero  */
    EaSetADADAC4(devDriver, 0x2000); /* channel 4 set to zero  */
    EaSetADADAC5(devDriver, 0x2000); /* channel 5 set to zero  */
    EaSetADADAC6(devDriver, 0x2000); /* channel 6 set to zero  */
    EaSetADADAC7(devDriver, 0x2000); /* channel 7 set to zero  */
    EaSetADADACOUT(devDriver, 0x2); /* register to ouputs     */
    return 1;
}

int LowLevelDac::ReadAll(int module, float *valuesD, unsigned short *valuesS)
{
    int devDriver = LowLevelDriver::getDriver(module);
    EaGetADADAC0(devDriver, &valuesS[0]);
    EaGetADADAC1(devDriver, &valuesS[1]);
    EaGetADADAC2(devDriver, &valuesS[2]);
    EaGetADADAC3(devDriver, &valuesS[3]);
    EaGetADADAC4(devDriver, &valuesS[4]);
    EaGetADADAC5(devDriver, &valuesS[5]);
    EaGetADADAC6(devDriver, &valuesS[6]);
    EaGetADADAC7(devDriver, &valuesS[7]);
    for (int channel = 0; channel < 8; channel++)
    {
        valuesS[channel] = (valuesS[channel] >> 2) & 0xfff;
        valuesD[channel] = 10.0 * ((2.0 * (float) valuesS[channel] / 4096) - 1.0);
    }

    return 1;
}

int LowLevelDac::ReadBase(int module, float *valuesF, unsigned short *valuesS)
{
    unsigned short dacUnsignedShort[4];
    int devDriver = LowLevelDriver::getDriver(module);
    int channel;
    EaGetMDAC0(devDriver, &dacUnsignedShort[0]);
    EaGetMDAC1(devDriver, &dacUnsignedShort[1]);
    EaGetMDAC2(devDriver, &dacUnsignedShort[2]);
    EaGetMDAC3(devDriver, &dacUnsignedShort[3]);
    for (channel = 0; channel < 4; channel++)
    {
        valuesS[channel] = (int) (dacUnsignedShort[channel] & 0xFFF);
        valuesF[channel] = (10000.0 * ((float) valuesS[channel] / 4096.0) - 5000.0) / 1000;
    }

    return 1;
}
int LowLevelDac::ReadMpc(int module, float *valuesF, unsigned short *valuesS)
{
    unsigned short dacUnsignedShort[8];
    int channel;
    int devDriver = LowLevelDriver::getDriver(module);
    EaGetDACM0(devDriver, &dacUnsignedShort[0]);
    EaGetDACM1(devDriver, &dacUnsignedShort[1]);
    EaGetDACM2(devDriver, &dacUnsignedShort[2]);
    EaGetDACM3(devDriver, &dacUnsignedShort[3]);
    EaGetDACM4(devDriver, &dacUnsignedShort[4]);
    EaGetDACM5(devDriver, &dacUnsignedShort[5]);
    EaGetDACM6(devDriver, &dacUnsignedShort[6]);
    EaGetDACM7(devDriver, &dacUnsignedShort[7]);
    for (channel = 0; channel < 8; channel++)
    {
        valuesS[channel] = (int) (dacUnsignedShort[channel] & 0xFFF);
        valuesF[channel] = 5.0 * ((2.0 * (float) valuesS[channel] / 4096.0) - 1.0);
    }

    return 1;
}

int LowLevelDac::SetVoltage(int module, int channel, char *action, double voltage, int iGain, int iOffset, int iMaxAllowed, int iMinAllowed, double userMaxAllowed, double userMinAllowed, char *msg)
{

    int devDriver = LowLevelDriver::getDriver(module);
    double gain, offset, maxAllowed, minAllowed;
    int32_t dacValue;
    double voltageToSet;

    if((channel == 0 || channel == 1) && voltage < 0) {

        voltage = -voltage;
    }

    if (voltage > userMaxAllowed || voltage < userMinAllowed)
    {

        sprintf(msg,
                "Error LowLevelDac::setVoltage [%04d] %d %s Out of range setVoltage channel %d gain %d range[%.3f]max %.3f min %.3f ",
                __LINE__, module, action, channel, iGain, voltage, userMaxAllowed, userMinAllowed);

        if (LowLevelDac::IsVerbose(module))
        {
            printf("%s\n", msg);
        };

        return ( LOWLEVELDAC_ERROR_VOLTAGE_TO_LARGE);
    }
    gain = (double) iGain / 100000;
    offset = (double) iOffset / 100000;
    maxAllowed = (double) iMaxAllowed / 100000;
    minAllowed = (double) iMinAllowed / 100000;


    voltageToSet = (voltage / gain) - (offset);

    if (voltageToSet > (maxAllowed / gain))
    {
        sprintf(msg,
                "Error LowLevelDac::setVoltage [%04d] %d %s Voltage to large Ch %d V%.3f,G%.3f,OF%.3fdac%.3fmax%.3fmin%.3f ",
                __LINE__, module, action, channel, voltage, gain, offset, voltageToSet, (maxAllowed / gain),
                (minAllowed / gain));
        if (LowLevelDac::IsVerbose(module))
        {
            printf("%s\n", msg);
        };
        return ( LOWLEVELDAC_ERROR_VOLTAGE_TO_LARGE);

    }
    if (voltageToSet < (minAllowed / gain))
    {
        sprintf(msg,
                "Error LowLevelDac::setVoltage [%04d] %d %s Voltage to small Ch %d V%.3f,G%.3f,OF%.3fdac%.3fmax%.3fmin%.3f ",
                __LINE__, module, action, channel, voltage, gain, offset, voltageToSet, (maxAllowed / gain),
                (minAllowed / gain));
        if (LowLevelDac::IsVerbose(module))
        {
            printf("%s\n", msg);
        };

        return ( LOWLEVELDAC_ERROR_VOLTAGE_TO_SMALL);
    }

    /*   for pmt voltages 1v = 1Kv */
    voltageToSet -= 10;
    voltageToSet *= 8192;
    voltageToSet /= 10;
    dacValue = voltageToSet;
    dacValue &= 0x3FFF;
    sprintf(msg, "%s dac[%d]to[%.3f]g[%.3f]o[%.3f]dacV[%.3f]dac[%x][%d] ", action, channel, voltage, gain, offset,
            voltageToSet, dacValue, dacValue);

    switch (channel)
    {
        case 0:
	   EaSetADADAC0 (devDriver,dacValue);
            break;
        case 1:
	   EaSetADADAC1 (devDriver,dacValue);
            break;
        case 2:
            EaSetADADAC2(devDriver, dacValue);
            break;
        case 3:
            EaSetADADAC3(devDriver, dacValue);
            break;
        case 4:
            EaSetADADAC4(devDriver, dacValue);
            break;
        case 5:
            EaSetADADAC5(devDriver, dacValue);
            break;
        case 6:
            EaSetADADAC6(devDriver, dacValue);
            break;
        case 7:
            EaSetADADAC7(devDriver, dacValue);
            break;
        default:
            break;
    }
    EaSetADADACOUT(devDriver, 2); /* write the dac register to the outputs */


    return (1);
}

void LowLevelDac::Verbose(int module)
{
    dacVerbose[module] = 1;
}
int LowLevelDac::IsVerbose(int module)
{
    return dacVerbose[module];
}
void LowLevelDac::Silent(int module)
{
    dacVerbose[module] = 0;
}

int LowLevelDac::DacRawSet(int module, BX_DAC channel, int value)
{

    int status;
    int devDriver = LowLevelDriver::getDriver(module);
    switch (channel)
    {
        case DAC_HV1_0: // HV 1
            EaSetADADAC0(devDriver, (unsigned short) value);
            break;
        case DAC_HV2_1: // HV2
            EaSetADADAC1(devDriver, (unsigned short) value);
            break;
        case DAC_THRES1_2: // Threshold PM1
            EaSetADADAC2(devDriver, (unsigned short) value);
            break;
        case DAC_THRES2_3: // Threshold PM2
            EaSetADADAC3(devDriver, (unsigned short) value);
            break;
        case DAC_LED_PED_4: // LED Peidestal
            EaSetADADAC4(devDriver, (unsigned short) value);
            break;
        case DAC_LED_PULSE_EXT_5: // LED Pules High
        case DAC_LED_PULSE_INT_8:
            EaSetADADAC5(devDriver, (unsigned short) value);
            break;
        case DAC_REF_5VP_6: // Motor Cursor Vref+5v
            EaSetADADAC6(devDriver, (unsigned short) value);
            break;
        case DAC_REF_5VN_7: // Motor Cursor Vref-5v
            EaSetADADAC7(devDriver, (unsigned short) value);
            break;
        default:
            status = -99999;
            break;
    }
    // end of switch 

    // write all the outputs of the dac
    // realy I should only be writing 1 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    EaSetADADACOUT(devDriver, (unsigned short) 0x2);

    return 1;;

}

int LowLevelDac::DacMpcRawSet(int module, BX_DAC_MPC channel, int value)
{
    int status;
    int devDriver = LowLevelDriver::getDriver(module);
    switch (channel)
    {
        case DAC_MPC_START_MAX_0:
            EaSetDACM0(devDriver, (uint16_t) value);
            EaSetDACM03(devDriver, (uint16_t) 0); // Write dac 0 to 3
            break;
        case DAC_MPC_START_MIN_1:
            EaSetDACM1(devDriver, (uint16_t) value);
            EaSetDACM03(devDriver, (uint16_t) 0); // Write dac 0 to 3
            break;
        case DAC_MPC_STOP_MAX_2:
            EaSetDACM2(devDriver, (uint16_t) value);
            EaSetDACM03(devDriver, (uint16_t) 0); // Write dac 0 to 3
            break;
        case DAC_MPC_STOP_MIN_3:
            EaSetDACM3(devDriver, (uint16_t) value);
            EaSetDACM03(devDriver, (uint16_t) 0); // Write dac 0 to 3
            break;
        case DAC_MPC_GARAGE_MAX_4:
            EaSetDACM4(devDriver, (uint16_t) value);
            EaSetDACM47(devDriver, (uint16_t) 0); // Write dac 4 to 7
            break;
        case DAC_MPC_GARAGE_MIN_5:
            EaSetDACM5(devDriver, (uint16_t) value);
            EaSetDACM47(devDriver, (uint16_t) 0); // Write dac 4 to 7
            break;
        case DAC_MPC_MOTOR_SPEED_6:
            EaSetDACM6(devDriver, (uint16_t) value);
            EaSetDACM47(devDriver, (uint16_t) 0); // Write dac 4 to 7
            break;
        case DAC_MPC_MOTOR_DIRECTION_7:
            EaSetDACM7(devDriver, (uint16_t) value);
            EaSetDACM47(devDriver, (uint16_t) 0); // Write dac 4 to 7
            break;
        default:
            status = -9999;
            break;
    } // end of switch

    // write all the outputs of the dac
    // realy I should only be writing 1 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    EaSetADADACOUT(devDriver, (unsigned short) 0x2);

    if (LowLevelDac::IsVerbose(module))
    {
        char logMsg[160];
        sprintf(logMsg, "channel 0-7 [%d] value [%6d] [%04x]", channel, value, value);
        std::cout << "COUT::XXX  LowLevelDac::DacRawMpcSet " << logMsg << "\n";
    }
    return 1;
}

int LowLevelDac::DacBaseRawSet(int module, BX_DAC_BASE channel, int value)
{
    int status = 1;
    int devDriver = LowLevelDriver::getDriver(module);
    switch (channel)
    {
        case DAC_BASE_START_0:
            EaSetMDAC0(devDriver, (unsigned short) value);
            break;
        case DAC_BASE_STOP_1:
            EaSetMDAC1(devDriver, (unsigned short) value);
            break;
        case DAC_BASE_GARAGE_2:
            EaSetMDAC2(devDriver, (unsigned short) value);
            break;
        case DAC_BASE_SPARE_3:
            EaSetMDAC3(devDriver, (unsigned short) value);
            break;
        default:
            status = -9999;
            break;
    } // end of switch

    // write all the outputs of the dac
    // realy I should only be writing 1 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    EaSetADADACOUT(devDriver, (unsigned short) 0x2);

    if (LowLevelDac::IsVerbose(module))
    {
        char logMsg[160];
        sprintf(logMsg, "channel 0-4 [%d] value [%6d] [%04x]", channel, value, value);
        std::cout << "COUT::XXX  LowLevelDac::DacRawBaseSet " << logMsg << "\n";
    }

    return status;
}

int LowLevelDac::MotorToPosition(int module, float position)
{
//	  printf("LowLevelDac::MotorToPosition %f %d \n",position,module);
    int dacValue = (int) ((((float) position / 10.00) + (float) 5.00) * (4096 / 10));
    int window = 20;
    // printf("LowLevelDac::MotorToPosition %d %f %d\n",dacValue,position,module);

    if (dacValue > 4095) dacValue = 4095;
    if (dacValue < 1) dacValue = 1;
    int *rom = LowLevelDriver::GetRom(module);
    int devDriver = LowLevelDriver::getDriver(module);

    LowLevelDac::DacBaseRawSet(module, DAC_BASE_START_0, dacValue);
    // start+
    LowLevelDac::DacMpcRawSet(module, DAC_MPC_START_MIN_1, dacValue - window); // start-
    LowLevelDac::DacMpcRawSet(module, DAC_MPC_START_MAX_0, dacValue + window);
//  printf("LowLevelDac::MotorToPosition Start done\n");

    // stop
    LowLevelDac::DacBaseRawSet(module, DAC_BASE_STOP_1, dacValue);
    LowLevelDac::DacMpcRawSet(module, DAC_MPC_STOP_MIN_3, dacValue - window);
    LowLevelDac::DacMpcRawSet(module, DAC_MPC_STOP_MAX_2, dacValue + window);
//  sleep(1);
//enum BX_DAC_MPC 	{ DAC_MPC_START_MAX_0=0, DAC_MPC_START_MIN_1=1, DAC_MPC_STOP_MAX_2=2, DAC_MPC_STOP_MIN_3=3, DAC_MPC_GARAGE_MAX_4=4, DAC_MPC_GARAGE_MIN_5=5, DAC_MPC_MOTOR_SPEED_6=6, DAC_MPC_MOTOR_DIRECTION_7=7} ;
//enum BX_DAC_BASE 	{ DAC_BASE_START_0=0, DAC_BASE_STOP_1=1, DAC_BASE_GARAGE_2=2, DAC_BASE_SPARE_3=3} ;
//  printf("LowLevelDac::MotorToPosition Stop done\n");

    // park
    LowLevelDac::DacBaseRawSet(module, DAC_BASE_GARAGE_2, dacValue);
    LowLevelDac::DacMpcRawSet(module, DAC_MPC_GARAGE_MIN_5, dacValue - window);
    LowLevelDac::DacMpcRawSet(module, DAC_MPC_GARAGE_MAX_4, dacValue + window);

//  printf("LowLevelDac::MotorToPosition Park done\n");

    // speed
    LowLevelDac::DacMpcRawSet(module, DAC_MPC_MOTOR_SPEED_6, rom[ROM_DEFSPEED]);
    // direction
    LowLevelDac::DacMpcRawSet(module, DAC_MPC_MOTOR_DIRECTION_7, rom[ROM_DEFDIRECTMOT]);

//  printf("LowLevelDac::MotorToPosition speed done\n");

    EaSetMOTORPREG(devDriver, (unsigned short) 0);
    //pDev->Motor.set(shmMotor);
    //pDev->DacBase.set(dacBase);

    if (LowLevelDac::IsVerbose(module))
    {
        char logMsg[160];
        sprintf(logMsg, "position [%f] [%04x] [%d]", position, dacValue, dacValue);
        std::cout << "COUT::XXX  LowLevelDac::MotorToPosition " << logMsg << "\n";
    }

//   printf("LowLevelDac::MotorToPosition done\n");

    return 1;

}
