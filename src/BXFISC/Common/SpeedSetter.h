/*
 * SpeedSetter.h
 *
 *  Created on: Mar 15, 2021
 *      Author: arlis
 */

#ifndef SRC_BXFISC_COMMON_SPEEDSETTER_H_
#define SRC_BXFISC_COMMON_SPEEDSETTER_H_

#include <BXFISC/GeneratedCode/Device.h>

namespace NsSpeedConverter
{

class SpeedSetter
{
public:
    SpeedSetter() = default;
    virtual ~SpeedSetter() = default;

    void setSpeed(const double speed, BXFISC::Device * d, const fesa::MultiplexingContext * c);
};

} /* namespace NsSpeedConverter */

#endif /* SRC_BXFISC_COMMON_SPEEDSETTER_H_ */
