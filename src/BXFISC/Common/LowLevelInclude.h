#ifndef LOW_LEVEL_INCLUDE_HPP
#define LOW_LEVEL_INCLUDE_HPP



#define LOWLEVELDAC_ERROR_VOLTAGE_TO_LARGE -1
#define LOWLEVELDAC_ERROR_VOLTAGE_TO_SMALL -2

#define ROM_SIZE_HV			8
#define ROM_SIZE_LEDPIED	2
#define ROM_SIZE_LEDPULSE	2
#define ROM_SIZE_MOT		3
#define ROM_SIZE_THRES		4

#define ROM_LED_PULSE_INTERNAL	0
#define ROM_LED_PULSE_EXTERNAL	1
#define ROM_LED_PIED_GAIN		0
#define ROM_LED_PIED_OFFSET		1
#define ROM_HV_SET_GAIN			0
#define ROM_HV_SET_OFF			1
#define ROM_HV_GET_GAIN			2
#define ROM_HV_GET_OFF			3
#define ROM_HV_CUR_GAIN         4
#define ROM_HV_CUR_OFF          5
#define ROM_HV_MAX				6
#define ROM_HV_MAX_OFFSET		7
#define ROM_MOT_GAIN			0
#define ROM_MOT_OFFSET			1
#define ROM_MOT_RES				2
#define ROM_THRES_GET_GAIN		0
#define ROM_THRES_GET_OFFSET    1
#define ROM_THRES_SET_GAIN		2
#define ROM_THRES_SET_OFFSET	3

#define REF_DELAY				0
#define REF_DELAY_WIDTH			1

#define DEFAULTS_LED_PIED		0
#define DEFAULTS_LED_PIED_OFF	9.8
#define DEFAULTS_LED_PULSE		2.9
#define DEFAULTS_LED_PULSE_INT	9.0

#ifndef DWC
#define DAC_CHANNEL_0_HV1		0
#define DAC_CHANNEL_1_HV2		1
#define DAC_CHANNEL_2_THRES_1	2
#define DAC_CHANNEL_3_THRES_2   3
#define DAC_CHANNEL_6_PIED_2	6
#define DAC_CHANNEL_4_PIED_1	4
#define DAC_CHANNEL_5_PULSE		5
#define DAC_CHANNEL_7			7
#endif

#define TST_GEN_MODE_OFF			0
#define TST_GEN_MODE_BUST_INT_CH1   1
#define TST_GEN_MODE_BUST_INT_CH2   2
#define TST_GEN_MODE_BUST_EXT_CH1   3
#define TST_GEN_MODE_BUST_EXT_CH2   4
#define TST_GEN_MODE_PERM_INT_CH1   5
#define TST_GEN_MODE_PERM_INT_CH2   6
#define TST_GEN_MODE_PERM_EXT_CH1   7
#define TST_GEN_MODE_PERM_EXT_CH2   8

//struct romStruct
//{
//	int hv1[ROM_SIZE_HV];
//	int hv2[ROM_SIZE_HV];
//	int ledPied1[ROM_SIZE_LEDPIED];   // gain,offset
//	int ledPied2[ROM_SIZE_LEDPIED];	  // gain,offset
//	int ledPulse[ROM_SIZE_LEDPULSE];  // internal,external
//	int mot1[ROM_SIZE_MOT];
//	int mot2[ROM_SIZE_MOT];
//	int thres1[ROM_SIZE_THRES];
//	int thres2[ROM_SIZE_THRES];
//
//};
#define ROM_SIZE 1024

struct refStruct
{
	double signalThreshold[2];
	double hv1Ref;
	double hv2Ref;
	double delay1[2];
	double delay2[2];
	unsigned short CTL;
};
#define BIT0 	1
#define BIT1 	2
#define BIT2 	4
#define BIT3 	8
#define BIT4 	16
#define BIT5 	32
#define BIT6 	64
#define BIT7 	128





enum BX_AA 	{AA_CH0_HV1=0,AA_CH1_HV2=1,AA_CH2_THRES1=2,AA_CH3_THRES2=3,AA_CH4_HVC1=4,AA_CH5_HVC2=5,AA_CH6_MOT1=6,AA_CH7_MOT2=7} ;
enum BX_DAC	{DAC_HV1_0=0, DAC_HV2_1=1, DAC_THRES1_2=2, DAC_THRES2_3=3, DAC_LED_PED_4=4, DAC_LED_PULSE_EXT_5=5, DAC_REF_5VP_6=6, DAC_REF_5VN_7=7, DAC_LED_PULSE_INT_8=8} ;

enum BX_DAC_MPC 	{ DAC_MPC_START_MAX_0=0, DAC_MPC_START_MIN_1=1, DAC_MPC_STOP_MAX_2=2, DAC_MPC_STOP_MIN_3=3, DAC_MPC_GARAGE_MAX_4=4, DAC_MPC_GARAGE_MIN_5=5, DAC_MPC_MOTOR_SPEED_6=6, DAC_MPC_MOTOR_DIRECTION_7=7} ;
enum BX_DAC_BASE 	{ DAC_BASE_START_0=0, DAC_BASE_STOP_1=1, DAC_BASE_GARAGE_2=2, DAC_BASE_SPARE_3=3} ;

#ifdef EMC

enum BX_DAC_MPC 	{ DAC_MPC_START_MAX_0=0, DAC_MPC_START_MIN_1=1, DAC_MPC_STOP_MAX_2=2, DAC_MPC_STOP_MIN_3=3, DAC_MPC_GARAGE_MAX_4=4, DAC_MPC_GARAGE_MIN_5=5, DAC_MPC_MOTOR_SPEED_6=6, DAC_MPC_MOTOR_DIRECTION_7=7} ;
enum BX_DAC_BASE 	{ DAC_BASE_START_0=0, DAC_BASE_STOP_1=1, DAC_BASE_GARAGE_2=2, DAC_BASE_SPARE_3=3} ;

#endif

enum BXSCINT_LL_POSITION { BXSCINT_ENDSWITCHS=-2,BXSCINT_MOVING=-1,BXSCINT_OUT=0,BXSCINT_IN=1 };
enum BXSCINT_LL_CHANNEL { CHANNEL_0=0,CHANNEL_1=1 };
enum BXSCINT_LL_PORTS { PORT_A=0,PORT_B=1,PORT_C=2 };

#include "LowLevel.h"
#include "LowLevelDac.h"
#include "LowLevelDelay.h"
#include "LowLevelAA.h"
#include "LowLevelDriver.h"
#include "LowLevelSignalTimers.h"


#endif
