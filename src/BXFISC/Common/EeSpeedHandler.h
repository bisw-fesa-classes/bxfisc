/*
 * EeSpeedHandler.h
 *
 *  Created on: Mar 15, 2021
 *      Author: arlis
 */

#ifndef SRC_BXFISC_COMMON_EESPEEDHANDLER_H_
#define SRC_BXFISC_COMMON_EESPEEDHANDLER_H_

#include "ISpeedHandler.h"
#include <BXFISC/GeneratedCode/Device.h>


namespace NsSpeedConverter
{

class EeSpeedHandler : public ISpeedHandler
{
private:
    constexpr static double maxSpeed = 70;

    BXFISC::Device * d;
    const fesa::MultiplexingContext * c;
public:
    EeSpeedHandler(BXFISC::Device * d, const fesa::MultiplexingContext * c);
    virtual ~EeSpeedHandler() = default;

    virtual void setSpeed(const BXFISC::EVENTS::EVENTS ev, const double speed) override;
};

} /* namespace NsSpeedConverter */

#endif /* SRC_BXFISC_COMMON_EESPEEDHANDLER_H_ */
