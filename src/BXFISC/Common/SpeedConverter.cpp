/*
 * SpeedConverter.cpp
 *
 *  Created on: Mar 15, 2021
 *      Author: arlis
 */

#include "SpeedConverter.h"
#include <iostream>

namespace NsSpeedConverter
{

double SpeedConverter::speedToVoltage(const double speedInMmPerSecond) {
    const double speedVoltage = [](const double s) {
            if (s < 0 || s > 65) return otherMovementsSpeedVoltage; // ~40 mm/s
//            auto val = static_cast<double>((s - 0.44) / 13.62);
            // this formula was derived empirically by Inaki, we use it instead of the one from the documentation
            auto val = static_cast<double>(((1.2 * s) + 0.1) / 11.7);
            return val;
        }(speedInMmPerSecond);
    return speedVoltage;
}

int16_t SpeedConverter::convertSpeedToDAC(const double speedInMmPerSecond) {

    const auto speedVoltage = speedToVoltage(speedInMmPerSecond);
    /*
     * Explanation: 2048 is half a range on the DAC,
     *  it is added to the ratio of a speed turned into voltage divided by the dac resolution
     * speedVoltage
     * */
    const double voltageRange = maxVoltage * 2;
    const int32_t dacRange = 4096;
    const double resolution = voltageRange / dacRange;
    const int16_t regValue = static_cast<int16_t>(dacRange/2 + (speedVoltage/resolution));

    return regValue;
}


} /* namespace NsSpeedConverter */
