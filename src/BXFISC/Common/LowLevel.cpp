#include "LowLevelInclude.h"
#include "LowLevel.h"
#include "LowLevelFisc.h"
//#include "MemoryAccess.h"
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <string.h>
#include <math.h>

using namespace BXFISC;

LowLevel::LowLevel()
{
}

LowLevel::~LowLevel()
{
}

HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::FlashLed(int driverNumber)
{
    HARDWARE_ERRORS::HARDWARE_ERRORS hardWareError = HARDWARE_ERRORS::NONE;
    //std::cout << "COUT:"<<LowLevel::TimeString().c_str()<< "XXX LowLevel::FlashLed               driver "
    //					<< driverNumber << "\n" ;
    LowLevelDriver::FlashLed(driverNumber);
    return hardWareError;

}
HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::InitRom(int driverNumber)
{
//	HARDWARE_ERRORS::HARDWARE_ERRORS hardWareError;
//    hardWareError=HARDWARE_ERRORS::NONE;
    std::cout << "COUT:" << LowLevel::TimeString().c_str() << "XXX LowLevel::InitRom                driver "
            << driverNumber << "\n";
    LowLevelDriver::romReadBlockVerbose(driverNumber);
    LowLevelDriver::RomInit(driverNumber);
    return HARDWARE_ERRORS::NONE;
}

HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::InitIpc(int driverNumber)
{

    LowLevelDriver::ipcInit(driverNumber);
    return HARDWARE_ERRORS::NONE;
}
HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::InitAda(int driverNumber)
{
    std::cout << "COUT:" << LowLevel::TimeString().c_str() << "XXX LowLevel::InitAda                driver "
            << driverNumber << "\n";
    LowLevelDriver::adaInit(driverNumber);
    return HARDWARE_ERRORS::NONE;
}
HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::InitDac(int driverNumber)
{
    std::cout << "COUT:" << LowLevel::TimeString().c_str() << "XXX LowLevel::InitDac                driver "
            << driverNumber << "\n";

    LowLevelDac::Init(driverNumber);
    return HARDWARE_ERRORS::NONE;
}

HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::AA_CALIBRATE(int driverNumber)
{
    HARDWARE_ERRORS::HARDWARE_ERRORS hardWareError;
    hardWareError = HARDWARE_ERRORS::NONE;
    std::cout << "COUT:" << LowLevel::TimeString().c_str() << "XXX LowLevel::AA_CALIBRATE           driver "
            << driverNumber << "\n";
    LowLevelAA::Calibrate(driverNumber);
    return hardWareError;
}

HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::AA_READ(int driverNumber)
{
    double values[8];
    unsigned short valuesRaw[8];

    char infoStr[80];
    HARDWARE_ERRORS::HARDWARE_ERRORS hardWareError;
    hardWareError = HARDWARE_ERRORS::NONE;
    LowLevelAA::ReadAllRaw(driverNumber, valuesRaw);
    for (int channel = 0; channel < 8; channel++)
    {
        values[channel] = LowLevelAA::LowLevelAA::AAConvert(driverNumber, (BX_AA) channel, valuesRaw);
    }
    if (LowLevel::AAIsVerbose(driverNumber))
    {
        sprintf(infoStr, "AA  Ch0 Hv1     [%4x] [%7.4f]kv", valuesRaw[0], values[0]);
        std::cout << "COUT:" << LowLevel::TimeString().c_str() << "XXX LowLevel::AA_READ                driver "
                << driverNumber << infoStr << "\n";
        sprintf(infoStr, "AA  Ch1 Hv2     [%4x] [%7.4f]kv", valuesRaw[1], values[1]);
        std::cout << "COUT:" << LowLevel::TimeString().c_str() << "XXX LowLevel::AA_READ                driver "
                << driverNumber << infoStr << "\n";
        sprintf(infoStr, "AA  Ch2 Thres1  [%4x] [%7.4f]*10 for mV", valuesRaw[2], values[2]);
        std::cout << "COUT:" << LowLevel::TimeString().c_str() << "XXX LowLevel::AA_READ                driver "
                << driverNumber << infoStr << "\n";
        sprintf(infoStr, "AA  Ch3 Thres2  [%4x] [%7.4f]*10 for mv", valuesRaw[3], values[3]);
        std::cout << "COUT:" << LowLevel::TimeString().c_str() << "XXX LowLevel::AA_READ                driver "
                << driverNumber << infoStr << "\n";
        sprintf(infoStr, "AA  Ch4 HvCur1  [%4x] [%7.4f]mA", valuesRaw[4], values[4]);
        std::cout << "COUT:" << LowLevel::TimeString().c_str() << "XXX LowLevel::AA_READ                driver "
                << driverNumber << infoStr << "\n";
        sprintf(infoStr, "AA  Ch5 HvCur2  [%4x] [%7.4f]mA", valuesRaw[5], values[5]);
        std::cout << "COUT:" << LowLevel::TimeString().c_str() << "XXX LowLevel::AA_READ                driver "
                << driverNumber << infoStr << "\n";
        sprintf(infoStr, "AA  Ch6 PosCur1 [%4x] [%7.4f]", valuesRaw[6], values[6]);
        std::cout << "COUT:" << LowLevel::TimeString().c_str() << "XXX LowLevel::AA_READ                driver "
                << driverNumber << infoStr << "\n";
        sprintf(infoStr, "AA  Ch7 PosCur2 [%4x] [%7.4f]", valuesRaw[7], values[7]);
        std::cout << "COUT:" << LowLevel::TimeString().c_str() << "XXX LowLevel::AA_READ                driver "
                << driverNumber << infoStr << "\n";
    }
    return hardWareError;
}

HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::AA_GET(int driverNumber, float *aa)
{
    unsigned short valuesRaw[8];

    char infoStr[80];
    HARDWARE_ERRORS::HARDWARE_ERRORS hardWareError;
    hardWareError = HARDWARE_ERRORS::NONE;
    LowLevelAA::ReadAllRaw(driverNumber, valuesRaw);
    for (int channel = 0; channel < 8; channel++)
    {
        aa[channel] = (float) LowLevelAA::LowLevelAA::AAConvert(driverNumber, (BX_AA) channel, valuesRaw);
    }
    if (LowLevel::AAIsVerbose(driverNumber))
    {

        sprintf(infoStr, "AA  Ch0 Hv1     [%4x] [%7.4f]kv", valuesRaw[0], aa[0]);
        std::cout << "COUT:" << LowLevel::TimeString().c_str() << "XXX LowLevel::AA_READ                driver "
                << driverNumber << infoStr << "\n";
        sprintf(infoStr, "AA  Ch1 Hv2     [%4x] [%7.4f]kv", valuesRaw[1], aa[1]);
        std::cout << "COUT:" << LowLevel::TimeString().c_str() << "XXX LowLevel::AA_READ                driver "
                << driverNumber << infoStr << "\n";
        sprintf(infoStr, "AA  Ch2 Thres1  [%4x] [%7.4f]*10 for mV", valuesRaw[2], aa[2]);
        std::cout << "COUT:" << LowLevel::TimeString().c_str() << "XXX LowLevel::AA_READ                driver "
                << driverNumber << infoStr << "\n";
        sprintf(infoStr, "AA  Ch3 Thres2  [%4x] [%7.4f]*10 for mv", valuesRaw[3], aa[3]);
        std::cout << "COUT:" << LowLevel::TimeString().c_str() << "XXX LowLevel::AA_READ                driver "
                << driverNumber << infoStr << "\n";
        sprintf(infoStr, "AA  Ch4 HvCur1  [%4x] [%7.4f]mA", valuesRaw[4], aa[4]);
        std::cout << "COUT:" << LowLevel::TimeString().c_str() << "XXX LowLevel::AA_READ                driver "
                << driverNumber << infoStr << "\n";
        sprintf(infoStr, "AA  Ch5 HvCur2  [%4x] [%7.4f]mA", valuesRaw[5], aa[5]);
        std::cout << "COUT:" << LowLevel::TimeString().c_str() << "XXX LowLevel::AA_READ                driver "
                << driverNumber << infoStr << "\n";
        sprintf(infoStr, "AA  Ch6 PosCur1 [%4x] [%7.4f]", valuesRaw[6], aa[6]);
        std::cout << "COUT:" << LowLevel::TimeString().c_str() << "XXX LowLevel::AA_READ                driver "
                << driverNumber << infoStr << "\n";
        sprintf(infoStr, "AA  Ch7 PosCur2 [%4x] [%7.4f]", valuesRaw[7], aa[7]);
        std::cout << "COUT:" << LowLevel::TimeString().c_str() << "XXX LowLevel::AA_READ                driver "
                << driverNumber << infoStr << "\n";
    }
    return hardWareError;

}

HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::AAGet(int driverNumber, unsigned short *aaShort, float *aaFloat)
{
    //LowLevelAA::Verbose(driverNumber);
    LowLevelAA::ReadAll(driverNumber, aaShort, aaFloat);
    //LowLevelAA::Silent(driverNumber);

    return HARDWARE_ERRORS::NONE;
}

HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::DAC_GET(int driverNumber)
{

    float values[8];
    unsigned short valuesS[8];
    char infoStr[80];

    LowLevelDac::ReadAll(driverNumber, &values[0], &valuesS[0]);
    HARDWARE_ERRORS::HARDWARE_ERRORS hardWareError = HARDWARE_ERRORS::NONE;
    sprintf(infoStr, " DAC Ch0 HV1     [%4x] [%7.4f] ", valuesS[0], values[0]);
    std::cout << "COUT:" << LowLevel::TimeString().c_str() << "XXX LowLevel::DAC_GET                driver "
            << driverNumber << infoStr << "\n";
    sprintf(infoStr, " DAC Ch1 HV2     [%4x] [%7.4f] ", valuesS[1], values[1]);
    std::cout << "COUT:" << LowLevel::TimeString().c_str() << "XXX LowLevel::DAC_GET                driver "
            << driverNumber << infoStr << "\n";
    sprintf(infoStr, " DAC Ch2 TH1     [%4x] [%7.4f] ", valuesS[2], values[2]);
    std::cout << "COUT:" << LowLevel::TimeString().c_str() << "XXX LowLevel::DAC_GET                driver "
            << driverNumber << infoStr << "\n";
    sprintf(infoStr, " DAC Ch3 TH2     [%4x] [%7.4f] ", valuesS[3], values[3]);
    std::cout << "COUT:" << LowLevel::TimeString().c_str() << "XXX LowLevel::DAC_GET                driver "
            << driverNumber << infoStr << "\n";
    sprintf(infoStr, " DAC Ch4 PE1     [%4x] [%7.4f] ", valuesS[4], values[4]);
    std::cout << "COUT:" << LowLevel::TimeString().c_str() << "XXX LowLevel::DAC_GET                driver "
            << driverNumber << infoStr << "\n";
    sprintf(infoStr, " DAC Ch5 TH      [%4x] [%7.4f] ", valuesS[5], values[5]);
    std::cout << "COUT:" << LowLevel::TimeString().c_str() << "XXX LowLevel::DAC_GET                driver "
            << driverNumber << infoStr << "\n";
    sprintf(infoStr, " DAC Ch6 PE2     [%4x] [%7.4f] ", valuesS[6], values[6]);
    std::cout << "COUT:" << LowLevel::TimeString().c_str() << "XXX LowLevel::DAC_GET                driver "
            << driverNumber << infoStr << "\n";
    sprintf(infoStr, " DAC Ch7 PS      [%4x] [%7.4f] ", valuesS[7], values[7]);
    std::cout << "COUT:" << LowLevel::TimeString().c_str() << "XXX LowLevel::DAC_GET                driver "
            << driverNumber << infoStr << "\n";
    return hardWareError;

}

HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::DacsGet(unsigned short *valuesS, float *values, int driverNumber)
{

//	float values[8];
//	unsigned short valuesS[8];
//	char infoStr[80];

    LowLevelDac::ReadAll(driverNumber, &values[0], &valuesS[0]);
    HARDWARE_ERRORS::HARDWARE_ERRORS hardWareError = HARDWARE_ERRORS::NONE;
    return hardWareError;

}

void LowLevel::RomReadBlockSilent(int driverNumber)
{
    LowLevelDriver::romReadBlockSilent(driverNumber);
}
void LowLevel::RomReadBlockVerbose(int driverNumber)
{
    LowLevelDriver::romReadBlockVerbose(driverNumber);
}
int LowLevel::IsRomReadBlockVerbose(int driverNumber)
{
    return LowLevelDriver::isRomReadBlockVerbose(driverNumber);
}

int LowLevel::AAIsVerbose(int driverNumber)
{
    return LowLevelAA::IsVerbose(driverNumber);
}
void LowLevel::AASilent(int driverNumber)
{
    LowLevelAA::Silent(driverNumber);
}
void LowLevel::AAVerbose(int driverNumber)
{
    LowLevelAA::Verbose(driverNumber);
}

int LowLevel::DacIsVerbose(int driverNumber)
{
    return LowLevelDac::IsVerbose(driverNumber);
}
void LowLevel::DacSilent(int driverNumber)
{
    LowLevelDac::Silent(driverNumber);
}
void LowLevel::DacVerbose(int driverNumber)
{
    LowLevelDac::Verbose(driverNumber);
}

int LowLevel::getDriver(int module)
{
    return LowLevelDriver::getDriver(module);
}
HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::FifoFregGet(int driverNumber)
{
    char infoStr[80];

    HARDWARE_ERRORS::HARDWARE_ERRORS hardWareError = HARDWARE_ERRORS::NONE;
    int i = LowLevelSignalTimers::FifoFreqGet(driverNumber);
    sprintf(infoStr, " Fifo freq is [%7d]hz\n", i);
    //printf( "LowLevel::FifoFregGet Fifo freq is [%7d]hz\n",i);
    std::cout << "COUT:" << LowLevel::TimeString().c_str() << "XXX LowLevel::FifoFregGet            driver "
            << driverNumber << infoStr << "\n";
    return hardWareError;
}

HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::Counters(int driverNumber, double *counts)
{
    HARDWARE_ERRORS::HARDWARE_ERRORS hardWareError = HARDWARE_ERRORS::NONE;
    LowLevelSignalTimers::ReadCounters(driverNumber, counts);
    return hardWareError;
}

HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::GetModuleName(int driverNumber, char *name)
{

    int flag = LowLevelDriver::getName(driverNumber, name);
    if (flag >= 0)
    {
        return HARDWARE_ERRORS::NONE;
    }
    else
    {
        return HARDWARE_ERRORS::UNABLE_TO_OPEN_DRIVER;
    }
}

HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::MemoryAccessInit(int driverNumber)
{
#ifndef FISC
#ifndef SCINT
    memoryAccessInit();
#endif
#endif

    return HARDWARE_ERRORS::NONE;
}
HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::MemoryAccessSilentOn(char *file, int line, char *msg)
{
#ifndef FISC
#ifndef SCINT
    memoryAccessSilentOn(file, line, msg);
#endif
#endif
    return HARDWARE_ERRORS::NONE;
}
HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::MemoryAccessSilentOff(char *file, int line, char *msg)
{
#ifndef FISC
#ifndef SCINT
    memoryAccessSilentOff(file, line, msg);
#endif
#endif
    return HARDWARE_ERRORS::NONE;
}

#ifdef EMC

HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::FifoFreqGetEmc(	int module,int *freq)
{


	*freq=LowLevelSignalTimers::FifoFreqGet(module);
	return HARDWARE_ERRORS::NONE ;

}
HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::FifoFreqSet(	int module,int freq)
{
	LowLevelSignalTimers::FifoFreqSet(module,freq);
	return HARDWARE_ERRORS::NONE ;

}


HARDWARE_ERRORS::HARDWARE_ERRORS  LowLevel::BaseRegSet( int module ,unsigned short reg)
{
	LowLevelDriver::BaseRegSet(module,reg);

	return HARDWARE_ERRORS::NONE ;
}

HARDWARE_ERRORS::HARDWARE_ERRORS  LowLevel::PalRegSet( int module ,unsigned short reg)
{
	LowLevelDriver::PalRegSet(module,reg);

	return HARDWARE_ERRORS::NONE ;
}

HARDWARE_ERRORS::HARDWARE_ERRORS  LowLevel::PalRegGet( int module ,unsigned short *reg)
{
	LowLevelDriver::PalRegGet(module,reg);

	return HARDWARE_ERRORS::NONE ;
}

#endif

HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::StatusHv1(unsigned short portA, int *status, char *msg)
{
    if (portA & BIT0)
    {
        strcpy(msg, "BAD");
    }
    else
    {
        strcpy(msg, "OK");
    }
    if (portA & BIT0)
    {
        *status = 1;
    }
    else
    {
        *status = 0;
    }
    return HARDWARE_ERRORS::NONE;
}
HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::StatusHv2(unsigned short portA, int *status, char *msg)
{
    if (portA & BIT1)
    {
        strcpy(msg, "BAD");
    }
    else
    {
        strcpy(msg, "OK");
    };
    if (portA & BIT1)
    {
        *status = 1;
    }
    else
    {
        *status = 0;
    }
    return HARDWARE_ERRORS::NONE;
}
HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::StatusPsInt(unsigned short portA, int *status, char *msg)
{
    if (portA & BIT6)
    {
        strcpy(msg, "BAD");
    }
    else
    {
        strcpy(msg, "OK");
    };
    if (portA & BIT6)
    {
        *status = 1;
    }
    else
    {
        *status = 0;
    }
    return HARDWARE_ERRORS::NONE;
}
HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::StatusPs28v(unsigned short portA, int *status, char *msg)
{

    if (portA & BIT7)
    {
        strcpy(msg, "BAD");
    }
    else
    {
        strcpy(msg, "OK");
    };
    if (portA & BIT7)
    {
        *status = 1;
    }
    else
    {
        *status = 0;
    }
    return HARDWARE_ERRORS::NONE;
}

HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::DacSet(int driverNumber,
        HARDWARE_DAC_CHANNELS07::HARDWARE_DAC_CHANNELS07 channel, float voltage, char *msg,
        bool do_log, bool do_not_invert)
{
    int *rom = LowLevelDriver::GetRom(driverNumber);



    switch (channel)
    {

        case HARDWARE_DAC_CHANNELS07::HV1_0: {
            LowLevelDac::SetVoltage(driverNumber, DAC_CHANNEL_0_HV1, "HV1", voltage, rom[ROM_SETHV1GAIN],
                    rom[ROM_SETHV1OFFS], 1000000, -1000000, 10.0, -10.0, msg);
            break;
        }
        case HARDWARE_DAC_CHANNELS07::HV2_1: {
            LowLevelDac::SetVoltage(driverNumber, DAC_CHANNEL_1_HV2, "HV2", voltage, rom[ROM_SETHV2GAIN],
                    rom[ROM_SETHV2OFFS], 1000000, -1000000, 10.0, -10.0, msg);
            break;
        }
        case HARDWARE_DAC_CHANNELS07::THRES1_2: {
            LowLevelDac::SetVoltage(driverNumber, DAC_CHANNEL_2_THRES_1, "THRES1", voltage, rom[ROM_SETTHR1GAIN],
                    rom[ROM_SETTHR1OFFS], 1000000, -1000000, 10.0, -10.0, msg);
            break;
        }
        case HARDWARE_DAC_CHANNELS07::THRES2_3: {
            LowLevelDac::SetVoltage(driverNumber, DAC_CHANNEL_3_THRES_2, "THRES2", voltage, rom[ROM_SETTHR2GAIN],
                    rom[ROM_SETTHR2OFFS], 1000000, -1000000, 10.0, -10.0, msg);
            break;
        }
        case HARDWARE_DAC_CHANNELS07::LED_PED_4: {
            LowLevelDac::SetVoltage(driverNumber, DAC_CHANNEL_4_PIED_1, "Ped", voltage, rom[ROM_SETPIEDESTALGAIN],
                    rom[ROM_SETPIEDESTALOFFS], 1000000, -1000000, 10.0, -10.0, msg);
            break;
        }
        case HARDWARE_DAC_CHANNELS07::LED_PULSE_EXT_5: {
            LowLevelDac::SetVoltage(driverNumber, DAC_CHANNEL_5_PULSE, "Pulse External", voltage,
                    rom[ROM_SETEXTPULSEGAIN], 0, 1000000, -1000000, 10.0, -10.0, msg);
            break;
        }
        case HARDWARE_DAC_CHANNELS07::REF_5VP_6: {
            LowLevelDac::SetVoltage(driverNumber, DAC_CHANNEL_6_PIED_2, "Ref 5v", voltage, 1, 0, 1000000, -1000000,
                    10.0, -10.0, msg);
            break;
        }
        case HARDWARE_DAC_CHANNELS07::REF_5VN_7: {
            LowLevelDac::SetVoltage(driverNumber, DAC_CHANNEL_7, "Ref -5v", voltage, 1, 0, 1000000, -1000000, 10.0,
                    -10.0, msg);
            break;
        }
        case HARDWARE_DAC_CHANNELS07::LED_PULSE_INT_8: {
            LowLevelDac::SetVoltage(driverNumber, DAC_CHANNEL_5_PULSE, "Pulse Internal", voltage,
                    rom[ROM_SETINTPULSEGAIN], 0, 1000000, -1000000, 10.0, -10.0, msg);
            break;
        }
        default: {
            throw std::runtime_error("Wrong ADC channel!");
        }

    }

    return HARDWARE_ERRORS::NONE;
}

std::string LowLevel::TimeString()
{
    struct tm *t;
    time_t now;
    time(&now);
    t = localtime(&now);

    //LowLevel::sayHelloStatic();
    char timeNow[60];
    // year month and date include
    //sprintf(timeNow,"%2.2d/%2.2d/%4d %2.2d:%2.2d:%2.2d\n",
    //                    t->tm_mday,t->tm_mon+1,t->tm_year+1900,t->tm_hour,t->tm_min,t->tm_sec);
    sprintf(timeNow, "%2.2d:%2.2d:%2.2d ", t->tm_hour, t->tm_min, t->tm_sec);

    std::string msg = timeNow;
    return msg;
}
//#ifndef SCINT

HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::BaseRegGet(int module, unsigned short *reg)
{
    LowLevelDriver::BaseRegGet(module, reg);
    //printf("LowLevel::BaseRegGet %d \n",*reg);
    return HARDWARE_ERRORS::NONE;
}
//#endif

//#ifndef NO_DELAYS
HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::DelayInfo(int module, int channel, double *delay, double *delay_setting,
        double *delayWidth, double *delayWidth_setting, double *delayRanges_max, double *delayRanges_min,
        double *delayWidthRanges_max, double *delayWidthRanges_min)
{

    LowLevelDelay::DelayInfo(module, channel, delay, delay_setting, delayWidth, delayWidth_setting, delayRanges_max,
            delayRanges_min, delayWidthRanges_max, delayWidthRanges_min);
    return HARDWARE_ERRORS::NONE;

    return HARDWARE_ERRORS::NONE;
}

int LowLevel::DelayIsVerbose(int driverNumber)
{
    return LowLevelDelay::IsVerbose(driverNumber);
}
void LowLevel::DelaySilent(int driverNumber)
{
    LowLevelDelay::Silent(driverNumber);
}
void LowLevel::DelayVerbose(int driverNumber)
{
    LowLevelDelay::Verbose(driverNumber);
}

HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::DelaySet(int driverNumber, int channel, double *delayValue,
        double *widthValue)
{
    HARDWARE_ERRORS::HARDWARE_ERRORS hardWareError = HARDWARE_ERRORS::NONE;

    LowLevelDelay::Write(driverNumber, (BXSCINT_LL_CHANNEL) channel, delayValue, widthValue);

    return hardWareError;
//(int module,BXSCINT_LL_CHANNEL channel,double delayValue, double widthValue);
}

#ifdef RANGE0123

HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::InitDelay(int driverNumber)
{
	std::cout << "COUT:"<<LowLevel::TimeString().c_str()<< "XXX LowLevel::InitDelay              driver "
						<< driverNumber << "\n" ;
	LowLevelDelay::Init(driverNumber);
	return HARDWARE_ERRORS::NONE;
}
HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::DelayFindRanges(int driverNumber)
{
	HARDWARE_ERRORS::HARDWARE_ERRORS hardWareError =HARDWARE_ERRORS::NONE;
	LowLevelDelay::FindRanges(driverNumber);
	return hardWareError;

}



HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::SignalTstGenMode(int drivernumber,int channel,DEVICE_LOWLEVEL_ACTION::DEVICE_LOWLEVEL_ACTION action,char* infoStr)
{

	switch ( action )
	{
		case DEVICE_LOWLEVEL_ACTION::TST_SET_INT:
			if ( channel == 0 )
			{
				LowLevelSignalTimers::TstGenMode(drivernumber,TST_GEN_MODE_PERM_INT_CH1,infoStr);
			}else
			{
				LowLevelSignalTimers::TstGenMode(drivernumber,TST_GEN_MODE_PERM_INT_CH2,infoStr);
			}
			break ;
		case DEVICE_LOWLEVEL_ACTION::TST_SET_EXT:
			if ( channel == 0 )
			{
				LowLevelSignalTimers::TstGenMode(drivernumber,TST_GEN_MODE_PERM_EXT_CH1,infoStr);
			}else
			{
				LowLevelSignalTimers::TstGenMode(drivernumber,TST_GEN_MODE_PERM_EXT_CH2,infoStr);
			}
			break ;

		case DEVICE_LOWLEVEL_ACTION::TST_SET_OFF:

			LowLevelSignalTimers::TstGenMode(drivernumber,TST_GEN_MODE_OFF,infoStr);

			break ;

		case DEVICE_LOWLEVEL_ACTION::HV_GET:
		case DEVICE_LOWLEVEL_ACTION::HV1_SET_VALUE:
		case DEVICE_LOWLEVEL_ACTION::HV2_SET_VALUE:
		case DEVICE_LOWLEVEL_ACTION::POSITION_GET:
		case DEVICE_LOWLEVEL_ACTION::POSITION_SET_IN:
		case DEVICE_LOWLEVEL_ACTION::POSITION_SET_OUT:
		case DEVICE_LOWLEVEL_ACTION::STATUS:
		case DEVICE_LOWLEVEL_ACTION::AA_READ:
		case DEVICE_LOWLEVEL_ACTION::AA_CALIBRATE:
		case DEVICE_LOWLEVEL_ACTION::DAC_SET_CHANNEL_VALUE:
		case DEVICE_LOWLEVEL_ACTION::DAC_GET:
		case DEVICE_LOWLEVEL_ACTION::DELAY_RANGE_GET:
//#ifdef SCINT
//		case DEVICE_LOWLEVEL_ACTION::DELAY_SET_VALUE:
//#endif
//		case DEVICE_LOWLEVEL_ACTION::DELAY2_SET_VALUE:
		case DEVICE_LOWLEVEL_ACTION::COUNTER_GET:
		case DEVICE_LOWLEVEL_ACTION::FIFO_FREQ_GET:
		case DEVICE_LOWLEVEL_ACTION::FIFO_FREQ_SET_VALUE:
		case DEVICE_LOWLEVEL_ACTION::FIFO_ISEMPTY:
		case DEVICE_LOWLEVEL_ACTION::FIFO_EMPTY:
		case DEVICE_LOWLEVEL_ACTION::FIFO_READOUT:
		case DEVICE_LOWLEVEL_ACTION::INIT_ADA:
		case DEVICE_LOWLEVEL_ACTION::INIT_DAC:
		case DEVICE_LOWLEVEL_ACTION::INIT_IPC:
		case DEVICE_LOWLEVEL_ACTION::INIT_ROM:
		case DEVICE_LOWLEVEL_ACTION::INIT_DELAY:
		case DEVICE_LOWLEVEL_ACTION::FLASH_LED:
			sprintf(infoStr,"Problem with the action");
			break;

	}

	return  HARDWARE_ERRORS::NONE;
}

#endif

HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::RegBaseGet(int module, unsigned short *reg)
{
    return LowLevel::BaseRegGet(module, reg);
}

HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::RegCoinGet(int module, unsigned short *reg)
{
    return (HARDWARE_ERRORS::HARDWARE_ERRORS) LowLevelDriver::RegCoinGet(module, reg);
}

HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::RegMotorGet(int module, unsigned short *reg)
{

    return (HARDWARE_ERRORS::HARDWARE_ERRORS) LowLevelDriver::RegMotorGet(module, reg);

}

HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::RegBaseSet(int module, unsigned short reg)
{
    return (HARDWARE_ERRORS::HARDWARE_ERRORS) LowLevelDriver::BaseRegSet(module, reg);
}

HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::RegCoinSet(int module, unsigned short reg)
{

    return (HARDWARE_ERRORS::HARDWARE_ERRORS) LowLevelDriver::RegCoinSet(module, reg);
}

HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::RegMotorSet(int module, unsigned short reg)
{
    return (HARDWARE_ERRORS::HARDWARE_ERRORS) LowLevelDriver::RegMotorSet(module, reg);
}

HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::EE(int module, int32_t *pm1, int32_t *pm2, int32_t *coin, int32_t *coinE,
        double *pos, int *numberInFIFO)
{
    int ccounts; //=LowLevelSignalTimers::FifoCountsEmptyNow(module);
    int cpos; //=LowLevelSignalTimers::FifoEmptyNow(module );
    LowLevelSignalTimers::FifoCountsToArray(module, coin, pm1, pm2, coinE, &ccounts, MAX_FIFO_SIZE);
    LowLevelSignalTimers::FifoPositionsToArray(module, pos, &cpos, MAX_FIFO_SIZE);
//	printf("LowLevel::EE ccounts %d cpos %d\n",ccounts,cpos);
//	for ( int i=0 ; i<cpos ; i++ )
//	{
//		if ( i%100==0 )
//		{
//			printf("LowLevel::EE %i %2.5f %09d %09d %09d\n",i,pos[i],(int)pm1[i],(int)pm2[i],(int)coin[i]);
//		}
//	}

    *numberInFIFO = ccounts;
    return HARDWARE_ERRORS::NONE;
}

HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::WE(int module)
{
//	printf("WE freg is %d\n",	LowLevelSignalTimers::FifoFreqGet(module));

    return HARDWARE_ERRORS::NONE;
}

HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::EmcPhaGet(int module, int32_t *pha)
{
//	    printf("LowLevel::EmcPhaGet\n");
    LowLevelDriver::EmcPhaGet(module, pha);
    return HARDWARE_ERRORS::NONE;

}

HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::EmcGainSet(int module, int32_t gain)
{
//	    printf("LowLevel::EmcPhaGet %d\n",gain);
    LowLevelDriver::EmcGainSet(module, gain);
    return HARDWARE_ERRORS::NONE;

}
HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::EmcPhaDacsGet(int module, double *dacs)
{
    LowLevelDriver::EmcPhaDacsGet(module, dacs);
    return HARDWARE_ERRORS::NONE;
}
HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::EmcPhaDacSet(int module, EMCDACS::EMCDACS which, double dac)
{
    LowLevelDriver::EmcPhaDacSet(module, (int) which, dac);

    return HARDWARE_ERRORS::NONE;
}
HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::EmcPhaRegSet(int module, unsigned short reg)
{
    LowLevelDriver::EmcPhaRegSet(module, reg);
    return HARDWARE_ERRORS::NONE;
}

HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::EmcPhaRegGet(int module, unsigned short *reg)
{
    LowLevelDriver::EmcPhaRegGet(module, reg);
    return HARDWARE_ERRORS::NONE;
}

HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::ReturnToStartPosition(const double start, const int32_t driver)
{
    LowLevelSignalTimers::ReturnToStartPosition(start, driver);
    return HARDWARE_ERRORS::NONE;
}

HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::ScanSetUp(int module, double start, double stop, int delay, int time,
        unsigned short regMaster, unsigned short regCoin)
{
    //time=time/1000000 ; // going to nsecs
//			printf("\nHi there\n");

    LowLevelSignalTimers::ScanSetUp(module, start, stop, delay, time, regMaster, regCoin);
    return HARDWARE_ERRORS::NONE;
}

HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::FifoPositionGet(float *position, double gain, int driverNumber)
{

    unsigned short baseReg, baseRegOld;
    LowLevelDriver::BaseRegGet(driverNumber, &baseRegOld);
    baseReg = baseRegOld;

    if ((int) (baseReg & REGBASE::E_DISABLED) != (int) REGBASE::E_DISABLED)
    {
        baseReg += REGBASE::E_DISABLED;
        LowLevelDriver::BaseRegSet(driverNumber, baseReg);
    }

    LowLevelSignalTimers::PositionOnlyStart(driverNumber);
    LowLevelSignalTimers::PositionOnlyRead(driverNumber, position);
    *position = (*position) / gain;

    if (baseReg != baseRegOld)
    {
        LowLevelDriver::BaseRegSet(driverNumber, baseRegOld);
    }
    return HARDWARE_ERRORS::NONE;
}

HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::PositionSet(int driverNumber, float position)
{
    LowLevelSignalTimers::MotorSpeedSet(driverNumber, 2.855);
    LowLevelDac::MotorToPosition(driverNumber, position);
    return HARDWARE_ERRORS::NONE;
}

HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::AAGetMpx(int driverNumber, unsigned short *aaShort, float *aaFloat)
{
    LowLevelAA::ReadMpx(driverNumber, aaShort, aaFloat);
    return HARDWARE_ERRORS::NONE;
}

HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::DacsMpcGet(unsigned short *valuesS, float *values, int driverNumber)
{

//	float values[8];
//	unsigned short valuesS[8];
//	char infoStr[80];

    LowLevelDac::ReadMpc(driverNumber, &values[0], &valuesS[0]);
    HARDWARE_ERRORS::HARDWARE_ERRORS hardWareError = HARDWARE_ERRORS::NONE;
    return hardWareError;

}

HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::DacsBaseGet(unsigned short *valuesS, float *values, int driverNumber)
{

//	float values[8];
//	unsigned short valuesS[8];
//	char infoStr[80];

    LowLevelDac::ReadBase(driverNumber, &values[0], &valuesS[0]);
    HARDWARE_ERRORS::HARDWARE_ERRORS hardWareError = HARDWARE_ERRORS::NONE;
    return hardWareError;

}

HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::GetPort(int driverNumber, int whichPort, unsigned short *port)
{

//	printf("LowLevel::GetPort %d %x\n",whichPort,*port);
    *port = LowLevelDriver::GetPort(driverNumber, (BXSCINT_LL_PORTS) whichPort);
    return HARDWARE_ERRORS::NONE;
}

HARDWARE_ERRORS::HARDWARE_ERRORS LowLevel::SetPort(int driverNumber, int whichPort, unsigned short port)
{

//	printf("LowLevel::GetPort %d %x\n",whichPort,*port);
//	printf("LowLevel::SetPort drivernumber %d which %d , value %02x\n",driverNumber,whichPort,port);
    LowLevelDriver::SetPort(driverNumber, whichPort, port);
    return HARDWARE_ERRORS::NONE;
}

