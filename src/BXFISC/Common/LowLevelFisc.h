#ifndef LOW_LEVEL_FISC_HPP
#define LOW_LEVEL_FISC_HPP

#include <libbxfisc_dgII.h>

#define BIT0 1
#define BIT1 2
#define BIT2 4
#define BIT3 8
#define BIT4 16
#define BIT5 32
#define BIT6 64
#define BIT7 128
#define BIT8 256


#define EaEnableAccess		BxfiscEnableAccess
#define EaDisableAccess		BxfiscDisableAccess
#define EaGetADAIP			BxfiscGetADAIP
#define EaSetADAIP			BxfiscSetADAIP
#define EaGetADCRRAM0		BxfiscGetADCRRAMO
#define EaSetADCRRAM0		BxfiscSetADCRRAMO
#define EaGetADCONF			BxfiscGetADCONF
#define EaSetADCONF			BxfiscSetADCONF
#define EaGetADCFIFO		BxfiscGetADCFIFO
#define EaSetADCFIFO		BxfiscSetADCFIFO
#define EaGetADADAC0		BxfiscGetADADAC0
#define EaSetADADAC0		BxfiscSetADADAC0
#define EaGetADADAC1		BxfiscGetADADAC1
#define EaSetADADAC1		BxfiscSetADADAC1
#define EaGetADADAC2		BxfiscGetADADAC2


#define EaSetADADAC2        BxfiscSetADADAC2
#define EaGetADADAC3        BxfiscGetADADAC3
#define EaSetADADAC3        BxfiscSetADADAC3
#define EaGetADADAC4        BxfiscGetADADAC4
#define EaSetADADAC4        BxfiscSetADADAC4
#define EaGetADADAC5        BxfiscGetADADAC5
#define EaSetADADAC5        BxfiscSetADADAC5
#define EaGetADADAC6        BxfiscGetADADAC6
#define EaSetADADAC6        BxfiscSetADADAC6
#define EaGetADADAC7        BxfiscGetADADAC7
#define EaSetADADAC7        BxfiscSetADADAC7
#define EaGetADAPCDR        BxfiscGetADAPCDR
#define EaSetADAPCDR        BxfiscSetADAPCDR
#define EaGetADAPBDR        BxfiscGetADAPBDR
#define EaSetADAPBDR        BxfiscSetADAPBDR
#define EaGetADAPADR        BxfiscGetADAPADR
#define EaSetADAPADR        BxfiscSetADAPADR
#define EaGetADACRO         BxfiscGetADAPCR0
#define EaSetADACRO         BxfiscSetADAPCR0
#define EaGetADADACOUT      BxfiscGetADADACOUT
#define EaSetADADACOUT      BxfiscSetADADACOUT
#define EaGetIPCID          BxfiscGetIPCID
#define EaSetIPCID          BxfiscSetIPCID
#define EaGetIPCFIFO        BxfiscGetIPCIDFIFO
#define EaSetIPCFIFO        BxfiscSetIPCIDFIFO
#define EaGetIPCSTAT        BxfiscGetIPCIDSTAT
#define EaSetIPCSTAT        BxfiscSetIPCIDSTAT
#define EaGetIPCCONFIG      BxfiscGetIPCIDCONFIG
#define EaSetIPCCONFIG      BxfiscSetIPCIDCONFIG
#define EaGetIPC1LOW        BxfiscGetIPCID1LOW
#define EaGetIPC1HIG        BxfiscGetIPCID1HIGH
#define EaGetIPC2LOW        BxfiscGetIPCID2LOW
#define EaGetIPC2HIG        BxfiscGetIPCID2HIGH
#define EaGetIPC3LOW        BxfiscGetIPCID3LOW
#define EaGetIPC3HIG        BxfiscGetIPCID3HIGH
#define EaGetIPC4LOW        BxfiscGetIPCID4LOW
#define EaGetIPC4HIG        BxfiscGetIPCID4HIGH
#define EaGetPHOSID         BxfiscGetPHOSID
#define EaSetPHOSID         BxfiscSetPHOSID
#define EaGetPHOSIO         BxfiscGetPHOSIO
#define EaSetPHOSIO         BxfiscSetPHOSIO
#define EaGetPHOSIOPLUS     BxfiscGetPHOSIOPLUS
#define EaSetPHOSIOPLUS     BxfiscSetPHOSIOPLUS
#define EaGetPHOSIO30       BxfiscGetPHOSIO30
#define EaSetPHOSIO30       BxfiscSetPHOSIO30
#define EaGetPHOSIO3F       BxfiscGetPHOSIO3F
#define EaSetPHOSIO3F       BxfiscSetPHOSIO3F
#define EaGetLOCIPD         BxfiscGetLOCIPD
#define EaSetLOCIPD         BxfiscSetLOCIPD
#define EaGetFHRWTADR       BxfiscGetFHWRTADR
#define EaSetFHRWTADR       BxfiscSetFHWRTADR
#define EaGetFHRW           BxfiscGetFHRW
#define EaGetREGPALRADR     BxfiscGetREGPALRADR
#define EaSetREGPALRADR     BxfiscSetREGPALRADR
#define EaGetREGPALWADR     BxfiscGetREGPALWADR
#define EaSetREGPALWADR     BxfiscSetREGPALWADR
#define EaGetADCFFREAD      BxfiscGetADCFFREAD
#define EaSetADCFFREAD      BxfiscSetADCFFREAD
#define EaGetADCOSREAD      BxfiscGetADCOSREAD
#define EaSetADCOSREAD      BxfiscSetADCOSREAD
#define EaGetMDAC0          BxfiscGetMDAC0
#define EaSetMDAC0          BxfiscSetMDAC0
#define EaGetMDAC1          BxfiscGetMDAC1
#define EaSetMDAC1          BxfiscSetMDAC1
#define EaGetMDAC2          BxfiscGetMDAC2
#define EaSetMDAC2          BxfiscSetMDAC2
#define EaGetMDAC3          BxfiscGetMDAC3
#define EaSetMDAC3          BxfiscSetMDAC3
#define EaGetDACM0          BxfiscGetDACM0
#define EaSetDACM0          BxfiscSetDACM0
#define EaGetDACM1          BxfiscGetDACM1
#define EaSetDACM1          BxfiscSetDACM1
#define EaGetDACM2          BxfiscGetDACM2
#define EaSetDACM2          BxfiscSetDACM2
#define EaGetDACM3          BxfiscGetDACM3
#define EaSetDACM3          BxfiscSetDACM3
#define EaGetDACM4          BxfiscGetDACM4
#define EaSetDACM4          BxfiscSetDACM4
#define EaGetDACM5          BxfiscGetDACM5
#define EaSetDACM5          BxfiscSetDACM5
#define EaGetDACM6          BxfiscGetDACM6
#define EaSetDACM6          BxfiscSetDACM6
#define EaGetDACM7          BxfiscGetDACM7
#define EaSetDACM7          BxfiscSetDACM7
#define EaGetPHAREG         BxfiscGetPHAREG
#define EaSetPHAREG         BxfiscSetPHAREG
#define EaGetPHAADR         BxfiscGetPHAADR
#define EaSetPHAADR         BxfiscSetPHAADR
#define EaGetPHARAM         BxfiscGetPHARAM
#define EaSetPHARAM         BxfiscSetPHARAM
#define EaGetMCNTR          BxfiscGetMCNTR
#define EaSetMCNTR          BxfiscSetMCNTR
#define EaGetREGBASRW       BxfiscGetREGBASRW
#define EaSetREGBASRW       BxfiscSetREGBASRW
#define EaGetMOTCK          BxfiscGetMOTCK
#define EaSetMOTCK          BxfiscSetMOTCK
#define EaGetMDAC03         BxfiscGetMDAC03
#define EaSetMDAC03         BxfiscSetMDAC03
#define EaGetPHAZERO        BxfiscGetPHAZERO
#define EaSetPHAZERO        BxfiscSetPHAZERO
#define EaSetPHAZERO        BxfiscSetPHAZERO
#define EaSetREGMOTRW       BxfiscSetREGMOTRW
#define EaGetDACM03         BxfiscGetDACM03
#define EaSetDACM03         BxfiscSetDACM03
#define EaGetDACM47         BxfiscGetDACM47
#define EaSetDACM47         BxfiscSetDACM47
#define EaGetMOTORPREG      BxfiscGetMOTORPREG
#define EaSetMOTORPREG      BxfiscSetMOTORPREG
#define EaGetGOMOTSTOP      BxfiscGetGOMOTSTOP
#define EaSetGOMOTSTOP      BxfiscSetGOMOTSTOP
#define EaGetGOMOTPARK      BxfiscGetGOMOTPARK
#define EaSetGOMOTPARK      BxfiscSetGOMOTPARK
#define EaGetREGWADR        BxfiscGetREGWADR
#define EaSetREGWADR        BxfiscSetREGWADR
#define EaSetPHADACSEL      BxfiscSetPHADACSEL
#define EaGetPHADAC0        BxfiscGetPHADAC0
#define EaGetPHADAC1        BxfiscGetPHADAC1
#define EaGetPHADAC2        BxfiscGetPHADAC2
#define EaGetPHADAC3        BxfiscGetPHADAC3
#define EaSetPHADAC0        BxfiscSetPHADAC0
#define EaSetPHADAC1        BxfiscSetPHADAC1
#define EaSetPHADAC2        BxfiscSetPHADAC2
#define EaSetPHADAC3        BxfiscSetPHADAC3
#define EaGetREGMOTRW      BxfiscGetREGMOTRW
#define EaSetPHADACCTL      BxfiscSetPHADACCTL



#define	ROM_SETHV1GAIN			336/4	//	0.2992		*
#define	ROM_SETHV1OFFS			340/4	//	0.0015		*
#define	ROM_READHV1GAIN			344/4	//	3.2080		*
#define	ROM_READHV1OFFS			348/4	//	0.0000		*
#define	ROM_READIHV1GAIN		352/4	//	3.2143		*
#define	ROM_READIHV1OFFS		356/4	//	-0.0097		*
#define	ROM_SETHV1MAX			380/4	//	3000.0000	*
#define	ROM_HV1OFFSMAX			384/4	//	50.0000		*
#define	ROM_SETHV2GAIN			432/4	//	0.2999		*
#define	ROM_SETHV2OFFS			436/4	//	0.0012		*
#define	ROM_READHV2GAIN			440/4	//	3.2080		*
#define	ROM_READHV2OFFS			444/4	//	0.0000		*
#define	ROM_READIHV2GAIN		448/4	//	3.2031		*
#define	ROM_READIHV2OFFS		452/4	//	0.0049		*
#define	ROM_SETHV2MAX			476/4	//	3000.0000	*
#define	ROM_HV2OFFSMAX			480/4	//	50.0000		*
#define	ROM_SETTHR1GAIN			528/4	//	0.0977		*
#define	ROM_SETTHR1OFFS			532/4	//	0.0017		*
#define	ROM_READTHR1GAIN		536/4	//	10.1578		*
#define	ROM_READTHR1OFFS		540/4	//	0.0054		*
#define	ROM_SETTHR2GAIN			624/4	//	0.0974		*
#define	ROM_SETTHR2OFFS			628/4	//	0.0019		*
#define	ROM_READTHR2GAIN		632/4	//	10.1833		*
#define	ROM_READTHR2OFFS		636/4	//	0.0097		*
#define	ROM_SETPIEDESTALGAIN	720/4	//	0.2515		*
#define	ROM_SETPIEDESTALOFFS	724/4	//	0.0195		*
#define	ROM_SETINTPULSEGAIN		864/4	//	0.0271		*
#define	ROM_SETEXTPULSEGAIN		868/4	//	1.0001		*
#define	ROM_READIMOTGAIN		912/4	//	9.8998		*
#define	ROM_READIMOTOFFS		916/4	//	0.0065		*
#define	ROM_RESIMOT				920/4	//	0.4700		*
#define	ROM_PULSINT00			1048/4	//	0.0000		PulseDac[V]=0
#define	ROM_PULSINT01			1052/4	//	-0.0480		PulseDac[V]=1
#define	ROM_PULSEXT10			1140/4	//	-10.0000	PulseDac[V]=10
#define	ROM_DELFCCOINC1			1208/4	//	66.2000		Delay CLEAN_AND OUT  COINC front1
#define	ROM_DELFCCOINC2			1212/4	//	66.2000		Delay CLEAN_AND OUT  COINC front2
#define	ROM_DELRCCOINC1			1224/4	//	69.2000		Delay CLEAN_AND OUT  COINC Rear1
#define	ROM_DELRCCOINC2			1228/4	//	69.2000		Delay CLEAN_AND OUT  COINC Rear2
#define	ROM_DELY1_0				1400/4	//	56.2600		Start linearite delay programmable canal 1 IN Analog-OUT PM1 front
#define	ROM_DELY1_68				1672/4	//	124.0800	*
#define	ROM_DELY1_69				1676/4	//	-999.0000	*
#define	ROM_DELY1_70				1680/4	//	-999.0000	*
#define	ROM_DELY1_71				1684/4	//	-999.0000	*
#define	ROM_DELY1_72				1688/4	//	-999.0000	*
#define	ROM_DELY2_0				1692/4	//	56.3200		Start linearite delay programmable canal 2 IN Analog-OUT PM2 front
#define	ROM_DELY2_68				1964/4	//	123.5000	*
#define	ROM_DELY2_69				1968/4	//	-999.0000	*
#define	ROM_DELY2_70				1972/4	//	-999.0000	*
#define	ROM_DELY2_71				1976/4	//	-999.0000	*
#define	ROM_DELY2_72				1980/4	//	-999.0000	*
#define	ROM_WDTH1_0				1984/4	//	-999.0000	Start linearite largeur programmable canal 1 OUT PM1 front
#define	ROM_WDTH1_1				1988/4	//	7.0400		*
#define	ROM_WDTH1_24				2080/4	//	31.9400		*
#define	ROM_WDTH2_0				2084/4	//	-999.0000	Start linearite largeur programmable canal 2 OUT PM2 front
#define	ROM_WDTH2_1				2088/4	//	6.1800		*
#define	ROM_WDTH2_24				2180/4	//	31.7200		*
#define	ROM_OFDAC7741			2200/4	//	0.0015		Offset DAC7741
#define	ROM_GDAC7741P			2204/4	//	4.9980		Value maxi DAC7741
#define	ROM_GDAC7741M			2208/4	//	-4.9878		Value min DAC7741
#define	ROM_OFDAC7725A			2300/4	//	0.0010		Offset DAC7725-0
#define	ROM_PDAC7725A			2304/4	//	4.9897		Value maxi DAC7725-0
#define	ROM_MDAC7725A			2308/4	//	-4.9851		Value min DAC7725-0
#define	ROM_OFDAC7725B			2312/4	//	0.0000		Offset DAC7725-1
#define	ROM_PDAC7725B			2316/4	//	4.9891		Value maxi DAC7725-1
#define	ROM_MDAC7725B			2320/4	//	-4.9858		Value min DAC7725-1
#define	ROM_OFDAC7725C			2324/4	//	0.0010		Offset DAC7725-2
#define	ROM_PDAC7725C			2328/4	//	4.9901		Value maxi DAC7725-2
#define	ROM_MDAC7725C			2332/4	//	-4.9847		Value min DAC7725-2
#define	ROM_OFDAC7725D			2336/4	//	0.0008		Offset DAC7725-3
#define	ROM_PDAC7725D			2340/4	//	4.9889		Value maxi DAC7725-3
#define	ROM_MDAC7725D			2344/4	//	-4.9844		Value min DAC7725-3
#define	ROM_OFDAC7725E			2348/4	//	0.0016		Offset DAC7725-4
#define	ROM_PDAC7725E			2352/4	//	4.9892		Value maxi DAC7725-4
#define	ROM_MDAC7725E			2356/4	//	-4.9833		Value min DAC7725-4
#define	ROM_OFDAC7725F			2360/4	//	0.0003		Offset DAC7725-5
#define	ROM_PDAC7725F			2364/4	//	4.9878		Value maxi DAC7725-5
#define	ROM_MDAC7725F			2368/4	//	-4.9841		Value min DAC7725-5
#define	ROM_OFDAC7725G			2372/4	//	0.0145		Offset DAC7725-6
#define	ROM_PDAC7725G			2376/4	//	4.9974		Value maxi DAC7725-6
#define	ROM_MDAC7725G			2380/4	//	-4.9719		Value min DAC7725-6
#define	ROM_OFDAC7725H			2384/4	//	0.0001		Offset DAC7725-7
#define	ROM_PDAC7725H			2388/4	//	4.9874		Value maxi DAC7725-7
#define	ROM_MDAC7725H			2392/4	//	-4.9839		Value min DAC7725-7
#define	ROM_CURSPOS16B			2396/4	//	4.9906		Pos REF 16bit cursor
#define	ROM_CURSPOS12B			2400/4	//	4.9461		Pos FAFI 12bit cursor
#define	ROM_CURSNEG16B			2404/4	//	-4.9832		Neg REF 16bit cursor
#define	ROM_CURSNEG12B			2408/4	//	-4.9454		Neg FAFI 12bit cursor
#define	ROM_MOTOROUTMAX			2412/4	//	26.1606		Value maxi MotorOut
#define	ROM_MOTOROUTMIN			2416/4	//	-25.5679	Value mini MotorOut
#define	ROM_MOTOROUTOFS			2420/4	//	-0.0054		Value offset MotorOut
#define	ROM_TACHIMAX			2424/4	//	4.8902		Value maxi Tachi
#define ROM_TACHIMIN			2428/4	//	-4.8977		Value mini Tachi
#define	ROM_TACHIOFS			2432/4	//	-0.0073		Value offset Tachi
#define	ROM_POSINMAX			2436/4	//	4.9929		Value maxi POSIN
#define	ROM_POSINMIN			2440/4	//	-4.9903		Value mini POSIN
#define	ROM_POSINOFS			2444/4	//	-0.0013		Value offset POSIN
#define	ROM_CURSPOSSENS			2448/4	//	4.9488		Value Positive sens
#define	ROM_PHAOFS				2452/4	//	3.1432		Value PHA offset
#define	ROM_MONP5V				2456/4	//	4.7480		Monitor Supply +5V
#define	ROM_MONP12V				2460/4	//	11.9668		Monitor Supply +12V
#define	ROM_MONM12V				2464/4	//	-11.9041	Monitor Supply -12V
#define	ROM_MONM5V				2468/4	//	-4.8961		Monitor Supply -5V
#define	ROM_MON33V				2472/4	//	3.2946		Monitor Supply3.3V
#define	ROM_SPD01_10SLOPE		2476/4	//	14.1789		Speedslope01..10
#define	ROM_SPD01_10OFFSET		2480/4	//	-0.4673		Speedoffset01..10
#define	ROM_DEFMOTGARAGEWMIN	2600/4	//	553.0000	Default Motor GARAGE Window MIN[-3650mV-BIN]
#define	ROM_DEFMOTGARAGE		2604/4	//	573.0000	Default Motor GARAGE[-3600mV-BIN]
#define	ROM_DEFMOTGARAGEWMAX	2608/4	//	594.0000	Default Motor GARAGE Window MAX[-3550mV-BIN]
#define	ROM_DEFMOTSTARTWMIN		2612/4	//	963.0000	Default Motor START Window MIN[-2650mV-BIN]
#define	ROM_DEFMOTSTART			2616/4	//	984.0000	Default Motor START[-2600mV-BIN]
#define	ROM_DEFMOTSTARTWMAX		2620/4	//	1004.0000	Default Motor START Window MAX[-2550mV-BIN]
#define	ROM_DEFMOTSTOPWMIN		2624/4	//	3092.0000	Default Motor STOP Window MIN[+2550mV-BIN]
#define	ROM_DEFMOTSTOP			2628/4	//	3113.0000	Default Motor STOP[+2600mV-BIN]
#define	ROM_DEFMOTSTOPWMAX		2632/4	//	3133.0000	Default Motor STOP Window MAX[+2650mV-BIN]
#define	ROM_DEFSPEED			2636/4	//	2806.0000	Default speed[+1850mV-BIN]
#define	ROM_DEFDIRECTMOT		2640/4	//	2048.0000	Default Direct motor dac[0mV-BIN]
#define	ROM_DEFMOTSPARECMD		2644/4	//	0.0000		Default Motor spare register[BIN]
#define	ROM_DEFBASEREG			2648/4	//	128.0000	Default base register[BIN]
#define	ROM_DEFPALSREG			2652/4	//	0.0000		Default PAL register[BIN]
#define	ROM_DEFMOTREG			2656/4	//	0.0000		Default MOTOR register[BIN]
#define	ROM_DEFPHAREG			2660/4	//	0.0000		Default PHA register[BIN]
#define	ROM_DEFADAPBDIR			2664/4	//	238.0000	Default ADA08 B port direction[0x00ee-BIN]
#define	ROM_DEFADAPADR			2668/4	//	251.0000	Default A port data[0x00fb-BIN]
#define	ROM_DEFADAPBDR			2672/4	//	204.0000	Default B port data[0x00cc-BIN]
#define	ROM_DEFADAPCDR			2676/4	//	15.0000		Default C port data[0x000f-BIN]
#define	ROM_DEFTIM1LSB			2680/4	//	32.0000		Default Timer 1 LSB[0x0020-BIN]
#define	ROM_DEFTIM1MSB			2684/4	//	78.0000		Default Timer 1 MSB[0x004e-BIN]
#define	ROM_DEFTIM2LSB			2688/4	//	10.0000		Default Timer 2 LSB[0x000a-BIN]
#define	ROM_DEFTIM2MSB			2692/4	//	0.0000		Default Timer 2 MSB[0x0000-BIN]
#define	ROM_DEFTIM3LSB			2696/4	//	200.0000	Default Timer 3 LSB[0x00c8-BIN]
#define	ROM_DEFTIM3MSB			2700/4	//	0.0000		Default Timer 3 MSB[0x0000-BIN]
#define	ROM_DEFCURSREFP			3100/4	//	12288.0000	Default Cursor positive value[+5V-BIN]
#define	ROM_DEFCURSREFM			3104/4	//	4096.0000	Default Cursor negative value[-5V-BIN]
#define	ROM_DEFTHR1				3108/4	//	7782.0000	Default threshold PM1[-50mV-BIN]
#define	ROM_DEFTHR2				3112/4	//	7782.0000	Default threshold PM2[-50mV-BIN]
#define	ROM_DEFHV1				3116/4	//	8192.0000	Default HV PM1[0V-BIN]
#define	ROM_DEFHV2				3120/4	//	8192.0000	Default HV PM2[0V-BIN]
#define	ROM_DEFPIED				3124/4	//	8192.0000	Default Piedestal[0V-BIN]
#define	ROM_DEFPULSHIGH			3128/4	//	8192.0000	Default Pulse High[0V-BIN]
#define	ROM_DELAYCARDVERSION	3200/4	//	2000.0000	2000->Old DELAY CARD
#define	ROM_CURVCC				3300/4	//	2.7800		CURRENT+5V
#define	ROM_CURP12V				3304/4	//	0.1560		CURRENT+12V
#define	ROM_CURM12V				3308/4	//	0.3290		CURRENT-12V
#define	ROM_SPD0250				3400/4	//	3.1288		0.25
#define	ROM_SPD0500				3404/4	//	6.5906		0.50
#define	ROM_SPD0750				3408/4	//	10.1365		0.75
#define	ROM_SPD1000				3412/4	//	13.6842		1.00
#define	ROM_SPD1250				3416/4	//	17.2757		1.25
#define	ROM_SPD1500				3420/4	//	20.8000		1.50
#define	ROM_SPD1750				3424/4	//	24.2991		1.75
#define	ROM_SPD2000				3428/4	//	27.9570		2.00
#define	ROM_SPD2250				3432/4	//	31.5152		2.25
#define	ROM_SPD2500				3436/4	//	34.8993		2.50
#define	ROM_SPD2750				3440/4	//	38.5185		2.75
#define	ROM_SPD3000				3444/4	//	41.9355		3.00
#define	ROM_SPD3250				3448/4	//	45.2174		3.25
#define	ROM_SPD3500				3452/4	//	48.5981		3.50
#define	ROM_SPD3750				3456/4	//	52.0000		3.75
#define	ROM_SPD4000				3460/4	//	55.3191		4.00
#define	ROM_SPD4250				3464/4	//	58.4270		4.25
#define	ROM_SPD4500				3468/4	//	61.9048		4.50
#define	ROM_SPD4750				3472/4	//	65.0000		4.75
#define	ROM_SPD5000				3476/4	//	67.5325		5.00
#define	ROM_SPDSCANDIST			3500/4	//	52.0000		Scan[mm] speed calibration
#define	ROM_SPD11_20SLOPE		3504/4	//	13.0623		Speedslope11..20
#define	ROM_SPD11_20OFFSET		3508/4	//	2.8289		Speedoffset11..20


enum FISC_BASE_REG
{
  BASE_FALSE=0,
  BASE_MPX509_0_FALSE_bit0         =BIT0,
  BASE_MPX509_1_bit1               =BIT1,
  BASE_MPX509_2_bit2               =BIT2,
  BASE_START_WINDOW_OFF_bit3       =BIT3,
  BASE_AUTO_RETURN_OFF_bit4        =BIT4,
  BASE_AUTO_RETURN_EE_OFF_bit_5    =BIT5,
  BASE_EXTRACTION_DISABLE_bit6     =BIT6,
  BASE_TST_GEN_bit7                =BIT7,
  BASE_BT_FREQ_bit8                =BIT8
};

//###( 0x000 ) ####ID0#####
//###( 0x002 ) ####ID2#####
//###( 0x004 ) ####ID4#####
//###( 0x006 ) ####ID6#####
//###( 0x008 ) ####ID8#####
//###( 0x00a ) ####IDA#####
//###( 0x00c ) ####IDC#####
//###( 0x00e ) ####IDE#####
//###(0x100): ####ADAIP#####
//###( 0x102 ) ####ADAIP02#####
//###( 0x104 ) ####ADAIP04#####
//###( 0x106 ) ####ADAIP06#####
//###( 0x108 ) ####ADAIP08#####
//###( 0x10a ) ####ADAIP0A#####
//###( 0x10c ) ####ADAIP0C#####
//###( 0x10e ) ####ADAIP0E#####
//###(0x200) ####ADCRRAMO#####
//###(0x220) ####ADCONF#####
//###(0x230) ####ADCFIFO#####
//###(0x240) ####ADADAC0#####
//###(0x242) ####ADADAC1#####
//###(0x244) ####ADADAC2#####
//###(0x246) ####ADADAC3#####
//###(0x248) ####ADADAC4#####
//###(0x24a) ####ADADAC5#####
//###(0x24c) ####ADADAC6#####
//###(0x24e) ####ADADAC7#####
//###(0x250) ####ADAPCDR#####
//###(0x252) ####ADAPBDR#####
//###(0x254) ####ADAPADR#####
//###(0x256) ####ADAPCR0#####
//###(0x278) ####ADADACOUT#####
//###(0x300) ####IPCID#####
//###( 0x302 ) ####IPCID2#####
//###( 0x304 ) ####IPCID4#####
//###( 0x306 ) ####IPCID6#####
//###( 0x308 ) ####IPCID8#####
//###( 0x30a ) ####IPCIDA#####
//###( 0x30c ) ####IPCIDC#####
//###( 0x30e ) ####IPCIDE#####
//###(0x400) ####IPCIDFIFO#####
//###(0x87e) ####REGWADR#####
//###(0x402) ####IPCIDSTAT#####
//###(0x404) ####IPCIDCONFIG#####
//###(0x410) ####IPCID1LOW#####
//###(0x412) ####IPCID1HIGH#####
//###(0x414) ####IPCID2LOW#####
//###(0x416) ####IPCID2HIGH#####
//###(0x418) ####IPCID3LOW#####
//###(0x41a) ####IPCID3HIGH#####
//###(0x41c) ####IPCID4LOW#####
//###(0x41e) ####IPCID4HIGH#####
//###(0x500) ####PHOSID#####
//###(0x600) ####PHOSIO#####
//###(0x602) ####PHOSIOPLUS#####
//###(0x660) ####PHOSIO30#####
//###(0x87e) ####REGWADR#####
//###(0x67e) ####PHOSIO3F#####
//###(0x700) ####LOCIPD#####
//###(0x800) ####FHWRTADR#####
//###(0x802) ####FHRW#####
//###(0x804) ####REGPALRADR#####
//###(0x806) ####REGPALWADR#####
//###(0x808) ####ADCFFREAD#####
//###(0x80a) ####ADCOSREAD#####
//###(0x82c)####PHADACSEL#####
//###(0x82e)####PHADACCTL#####
//###(0x830)####PHADAC0
//###(0x87e) ####REGWADR#####
//###(0x832)####PHADAC1#####
//###(0x834)####PHADAC2#####
//###(0x836)####PHADAC3#####
//###(0x838) ####MDAC0#####
//###(0x83a) ####MDAC1#####
//###(0x83c) ####MDAC2#####
//###(0x83e) ####MDAC3#####
//###(0x840) ####DACM0#####
//###(0x842) ####DACM1#####
//###(0x844) ####DACM2#####
//###(0x846) ####DACM3#####
//###(0x848) ####DACM4#####
//###(0x84a) ####DACM5#####
//###(0x84c) ####DACM6#####
//###(0x84e) ####DACM7#####
//###(0x850) ####PHAREG#####
//###(0x852) ####PHAADR#####
//###(0x854) ####PHARAM#####
//###(0x860) ####MCNTR#####
//###(0x868) ####REGBASRW#####
//###(0x86a) ####MOTCK#####
//###(0x86c) ####MDAC03#####
//###(0x86e) ####PHAZERO#####
//###(0x872) ####REGMOTRW#####
//###(0x874) ####DACM03#####
//###(0x876) ####DACM47#####
//###(0x878) ####MOTORPREG#####
//###(0x87a) ####GOMOTSTOP#####
//###(0x87c) ####GOMOTPARK#####
//###(0x87e) ####REGWADR#####
//###( 0x000 ) ####ID0#####
//###( 0x002 ) ####ID2#####
//###( 0x004 ) ####ID4#####
//###( 0x006 ) ####ID6#####
//###( 0x008 ) ####ID8#####
//###( 0x00a ) ####IDA#####
//###( 0x00c ) ####IDC#####
//###( 0x00e ) ####IDE#####
//###(0x100)####  ADAIP#####
//###(0x102)#### ADAIP#####
//###(0x104)#### ADAIP#####
//###(0x106)####  ADAIP#####
//###(0x108)####ADAIP#####
//###(0x10A)#### ADAIP#####
//###(0x10E)####ADAIP#####
//###(0x200)####ADCRRAMO#####
//###(0x220) ####ADCONF#####
//###(0x230) ####ADCFIFO#####
//###(0x240) ####ADADAC0#####
//###(0x242) ####ADADAC1#####
//###(0x244) ####ADADAC2#####
//###( 0x002 ) ####ID2#####
//###( 0x004 ) ####ID4#####
//###( 0x006 ) ####ID6#####
//###( 0x008 ) ####ID8#####
//###( 0x00a ) ####IDA#####
//###( 0x00c ) ####IDC#####
//###( 0x00e ) ####IDE#####
//###(0x100)####ADAIP#####
//###( 0x102 ) ####ADAIP02#####
//###( 0x104 ) ####ADAIP04#####
//###( 0x106 ) ####ADAIP06#####
//###( 0x108 ) ####ADAIP08#####
//###( 0x10a ) ####ADAIP0A#####
//###( 0x10c ) ####ADAIP0C#####
//###( 0x10e ) ####ADAIP0E#####
//###(0x200) ####ADCRRAMO#####
//###(0x220) ####ADCONF#####
//###(0x230) ####ADCFIFO#####
//###(0x240) ####ADADAC0#####
//###(0x242) ####ADADAC1#####
//###(0x244) ####ADADAC2#####
//###(0x246) ####ADADAC3#####
//###(0x248) ####ADADAC4#####
//###(0x24a) ####ADADAC5#####
//###(0x24c) ####ADADAC6#####
//###(0x24e) ####ADADAC7#####
//###(0x250) ####ADAPCDR#####
//###(0x252) ####ADAPBDR#####
//###(0x254) ####ADAPADR#####
//###(0x256) ####ADAPCR0#####
//###(0x278) ####ADADACOUT#####
//###(0x300) ####IPCID#####
//###( 0x302 ) ####IPCID2#####
//###( 0x304 ) ####IPCID4#####
//###( 0x306 ) ####IPCID6#####
//###( 0x308 ) ####IPCID8#####
//###( 0x30a ) ####IPCIDA#####
//###( 0x30c ) ####IPCIDC#####
//###( 0x30e ) ####IPCIDE#####
//###(0x400) ####IPCIDFIFO#####
//###(0x87e) ####REGWADR#####
//###(0x402) ####IPCIDSTAT#####
//###(0x404) ####IPCIDCONFIG#####
//###(0x410) ####IPCID1LOW#####
//###(0x412) ####IPCID1HIGH#####
//###(0x414) ####IPCID2LOW#####
//###(0x416) ####IPCID2HIGH#####
//###(0x418) ####IPCID3LOW#####
//###(0x41a) ####IPCID3HIGH#####
//###(0x41c) ####IPCID4LOW#####
//###(0x41e) ####IPCID4HIGH#####
//###(0x500) ####PHOSID#####
//###(0x600) ####PHOSIO#####
//###(0x602) ####PHOSIOPLUS#####
//###(0x660) ####PHOSIO30#####
//###(0x87e) ####REGWADR#####
//###(0x67e) ####PHOSIO3F#####
//###(0x700) ####LOCIPD#####
//###(0x800) ####FHWRTADR#####
//###(0x802) ####FHRW#####
//###(0x804) ####REGPALRADR#####
//###(0x806) ####REGPALWADR#####
//###(0x808) ####ADCFFREAD#####
//###(0x80a) ####ADCOSREAD#####
//###(0x82c)####PHADACSEL#####
//###(0x82e)####PHADACCTL#####
//###(0x830)####PHADAC0#####
//###(0x832)####PHADAC1#####
//###(0x834)####PHADAC2#####
//###(0x836)####PHADAC3#####
//###(0x838) ####MDAC0#####
//###(0x83a) ####MDAC1#####
//###(0x83c) ####MDAC2#####
//###(0x83e) ####MDAC3#####
//###(0x840) ####DACM0#####
//###(0x842) ####DACM1#####
//###(0x844) ####DACM2#####
//###(0x846) ####DACM3#####
//###(0x848) ####DACM4#####
//###(0x84a) ####DACM5#####
//###(0x84c) ####DACM6#####
//###(0x84e) ####DACM7#####
//###(0x850) ####PHAREG#####
//###(0x852) ####PHAADR#####
//###(0x854) ####PHARAM#####
//###(0x860) ####MCNTR#####
//###(0x868) ####REGBASRW#####
//###(0x86a) ####MOTCK#####
//###(0x86c) ####MDAC03#####
//###(0x86e) ####PHAZERO#####
//###(0x872) ####REGMOTRW#####
//###(0x874) ####DACM03#####
//###(0x876) ####DACM47#####
//###(0x878) ####MOTORPREG#####
//###(0x87a) ####GOMOTSTOP#####
//###(0x87c) ####GOMOTPARK#####
//###(0x87e) ####REGWADR#####
//###(0x246) ####ADADAC3#####
//###(0x248) ####ADADAC4#####
//###(0x24a) ####ADADAC5#####
//###(0x24c) ####ADADAC6#####
//###(0x24e) ####ADADAC7#####
//###(0x250) ####ADAPCDR#####
//###(0x252) ####ADAPBDR#####
//###(0x254) ####ADAPADR#####
//###(0x256) ####ADAPCR0#####
//###(0x278) ####ADADACOUT#####
//###(0x300) ####IPCID#####
//###( 0x302 ) #### IPCID2#####
//###( 0x304 ) #### IPCID4#####
//###( 0x306 ) #### IPCID6#####
//###( 0x308 ) #### IPCID8#####
//###( 0x30a ) #### IPCIDA#####
//###( 0x30c ) #### IPCIDC#####
//###( 0x30e ) #### IPCIDE#####
//###(0x400) ####IPCIDFIFO#####
//###(0x402) ####IPCIDSTAT#####
//###(0x404) ####IPCIDCONFIG#####
//###(0x500) ####PHOSID#####
//###(0x600) ####PHOSIO#####
//###(0x602) ####PHOSIOPLUS#####
//###(0x660) ####PHOSIO30#####
//###(0x67e) ####PHOSIO3F#####
//###(0x700) ####LOCIPD#####
//###(0x800) ####FHWRTADR#####
//###(0x802) ####FHRW#####
//###(0x804) ####REGPALRADR#####
//###(0x806) ####REGPALWADR#####
//###(0x808) ####ADCFFREAD#####
//###(0x80a) ####ADCOSREAD#####
//###(0x82c)#### PHADACSEL#####
//###(0x82e)#### PHADACCTL#####
//###(0x830)#### PHADAC0#####
//###(0x832)#### PHADAC1#####
//###(0x834)#### PHADAC2#####
//###(0x836)#### PHADAC3#####
//###(0x838) ####MDAC0#####
//###(0x83a) ####MDAC1#####
//###(0x83c) ####MDAC2#####
//###(0x83e) ####MDAC3#####
//###(0x840) ####DACM0#####
//###(0x842) ####DACM1#####
//###(0x844) ####DACM2#####
//###(0x846) ####DACM3#####
//###(0x848) ####DACM4#####
//###(0x84a) ####DACM5#####
//###(0x84c) ####DACM6#####
//###(0x84e) ####DACM7#####
//###(0x850) ####PHAREG#####
//###(0x852) ####PHAADR#####
//###(0x854) ####PHARAM#####
//###(0x860) ####MCNTR#####
//###(0x868) ####REGBASRW#####
//###(0x86a) ####MOTCK#####
//###(0x86c) ####MDAC03#####
//###(0x86e) ####PHAZERO#####
//###(0x872) ####REGMOTRW#####
//###(0x874) ####DACM03#####
//###(0x876) ####DACM47#####
//###(0x878) ####MOTORPREG#####
//###(0x87a) ####GOMOTSTOP#####
//###(0x87c) ####GOMOTPARK#####
//###(0x87e) ####REGWADR#####

#define ADR_OFFSET                0x00001000
#define ADR_MAIN          0xDE100000

#define ID0      0x000
#define ID2      0x002
#define ID4      0x004
#define ID6      0x006
#define ID8      0x008
#define IDA      0x00a
#define IDA      0x00a
#define IDC      0x00c
#define IDE      0x00e


#define ADAIP02      0x102
#define ADAIP04      0x104
#define ADAIP06      0x106
#define ADAIP08      0x108
#define ADAIP0A      0x10a
#define ADAIP0E      0x10e
#define IPCID2       0x302
#define IPCID4       0x304
#define IPCID6      0x306
#define IPCID8      0x308
#define IPCIDA       0x30a
#define IPCIDC       0x30c
#define IPCIDE       0x30e
#define ADAIP       0x100
//#define  ADAIP      0x102
//#define  ADAIP      0x104
//#define   ADAIP     0x106
//#define ADAIP       0x108
//#define  ADAIP      0x10A
//#define ADAIP       0x10E
#define ADAADCRRAM0        0x200
#define ADAADCONF      0x220
#define ADAADCFIFO     0x230
#define ADADAC0     0x240
#define ADADAC1     0x242
#define ADADAC2     0x244
#define ADADAC3     0x246
#define ADADAC4     0x248
#define ADADAC5     0x24a
#define ADADAC6     0x24c
#define ADADAC7     0x24e
#define ADAPCDR     0x250
#define ADAPBDR     0x252
#define ADAPADR     0x254
#define ADACR0     0x256
#define ADADACOUT       0x278
#define IPCID       0x300
#define IPCFIFO       0x400
#define IPCSTAT       0x402
#define IPCCONFIG     0x404
#define IPCID1LOW       0x410
#define IPCCNTR          0x410
#define IPCID1HIGH      0x412
#define IPCID2LOW       0x414
#define IPCID2HIGH      0x416
#define IPCID3LOW       0x418
#define IPCID3HIGH      0x41a
#define IPCID4LOW       0x41c
#define IPCID4HIGH      0x41e
#define PHOSID      0x500
#define PHOSIO      0x600
#define PHOSIOPLUS      0x602
#define PHOSIO30        0x660
#define PHOSIO3F        0x67e
#define LOCIPD      0x700
#define FHRWTADR        0x800
#define FHR        0x802
#define REGPALRADR      0x804
#define REGPALWADR      0x806
#define ADCFFREAD       0x808
#define ADCOSREAD       0x80a
#define  PHADACSEL      0x82c
#define  PHADACCTL      0x82e
#define  PHADAC0        0x830
#define  PHADAC1        0x832
#define  PHADAC2        0x834
#define  PHADAC3        0x836
#define MDAC0       0x838
#define MDAC1       0x83a
#define MDAC2       0x83c
#define MDAC3       0x83e
#define DACM0       0x840
#define DACM1       0x842
#define DACM2       0x844
#define DACM3       0x846
#define DACM4       0x848
#define DACM5       0x84a
#define DACM6       0x84c
#define DACM6       0x84c
#define DACM7       0x84e
#define PHAREG      0x850
#define PHAADR      0x852
#define PHARAM      0x854
#define MCNTR       0x860
#define REGBASRW        0x868
#define REGRADR           0x868

#define MOTCK       0x86a
#define MDAC03      0x86c
#define PHAZERO     0x86e
#define REGMOTRW        0x872
#define DACM03      0x874
#define DACM47      0x876
#define MOTORPREG       0x878
#define GOMOTSTOP       0x87a
#define GOMOTPARK       0x87c
#define REGWADR     0x87e



#endif
