// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#include <BXFISC/RealTime/WbtEventAction.h>

#include <fesa-core/RealTime/RTEvent.h>

#include <BXFISC/GeneratedCode/ServiceLocator.h>

#include <cmw-log/Logger.h>
#include <iostream>
namespace
{

CMW::Log::Logger &logger = CMW::Log::LoggerFactory::getLogger("FESA.USR.BXFISC.RealTime.WbtEventAction");

} // namespace

#define LOG_DIAG_IF(topic,message)                           \
	{                                                    \
		DiagnosticUtils::DiagnosticMessage diagMsg;  \
		diagMsg.side = DiagnosticUtils::user;        \
		diagMsg.source = DiagnosticUtils::rt;        \
		diagMsg.fesaClass = "BXFISC"; \
		diagMsg.name = "WbtEventAction"; \
		diagMsg.action = DiagnosticUtils::undefined; \
		diagMsg.msg = message;                       \
		BXFISCServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
	}

namespace BXFISC
{

WbtEventAction::WbtEventAction(fesa::RTActionConfig &rtActionConfig, const fesa::AbstractServiceLocator *serviceLocator,
        const std::map<std::string, const fesa::AbstractServiceLocator*> &serviceLocatorRelatedClasses) :
                WbtEventActionBase(rtActionConfig, serviceLocator, serviceLocatorRelatedClasses)
{
}

WbtEventAction::~WbtEventAction()
{
}

void WbtEventAction::execute(fesa::RTEvent *pEvt)
{
    fesa::MultiplexingContext *pContext = pEvt->getMultiplexingContext();
    //long long timeStamp;
    for (std::vector<Device*>::iterator itr = deviceCol_.begin(); itr != deviceCol_.end(); ++itr)
    {
        try
        {
            Device *pDev = (*itr);
            if ((int) (pDev->event.get(pContext)) != (int) EVENTS::WBT)
            {
                pDev->event.set(EVENTS::WBT, pContext);
            }
        }
        catch (fesa::FesaException &exception)
        {
            std::cout << __FILE__ << " " << __LINE__ << "EX- in the device loop :( " << exception.getMessage()
                    << std::endl;
        }
    }

}

} // BXFISC
