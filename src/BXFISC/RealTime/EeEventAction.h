// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#ifndef BXFISC_EeEventAction_H_
#define BXFISC_EeEventAction_H_



#include <BXFISC/GeneratedCode/GenRTActions.h>

#include <BXFISC/GeneratedCode/Device.h>


namespace BXFISC
{
class EeEventAction : public EeEventActionBase 
{
	public:

		EeEventAction (fesa::RTActionConfig& rtActionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses);

		virtual  ~EeEventAction();
		void execute(fesa::RTEvent* pEvt);
};
}
#endif
