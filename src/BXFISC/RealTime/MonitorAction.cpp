// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#include <BXFISC/RealTime/MonitorAction.h>
#include <BXFISC/Common/LowLevel.h>

#include <fesa-core/RealTime/RTEvent.h>

#include <BXFISC/GeneratedCode/ServiceLocator.h>
#include "../Common/Tools.h"
#include <cmw-log/Logger.h>
#include <iostream>

using namespace BXFISC_TOOL;

namespace
{

CMW::Log::Logger &logger = CMW::Log::LoggerFactory::getLogger("FESA.USR.BXFISC.RealTime.MonitorAction");

} // namespace

#define LOG_DIAG_IF(topic,message)                           \
	{                                                    \
		DiagnosticUtils::DiagnosticMessage diagMsg;  \
		diagMsg.side = DiagnosticUtils::user;        \
		diagMsg.source = DiagnosticUtils::rt;        \
		diagMsg.fesaClass = "BXFISC"; \
		diagMsg.name = "MonitorAction"; \
		diagMsg.action = DiagnosticUtils::undefined; \
		diagMsg.msg = message;                       \
		BXFISCServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
	}

namespace BXFISC
{

MonitorAction::MonitorAction(fesa::RTActionConfig &rtActionConfig, const fesa::AbstractServiceLocator *serviceLocator,
        const std::map<std::string, const fesa::AbstractServiceLocator*> &serviceLocatorRelatedClasses) :
                MonitorActionBase(rtActionConfig, serviceLocator, serviceLocatorRelatedClasses)
{
}

MonitorAction::~MonitorAction()
{
}

void MonitorAction::execute(fesa::RTEvent *pEvt)
{
    const fesa::MultiplexingContext *pContext = pEvt->getMultiplexingContext();
    char msg[MSG_SIZE];
    long long acqStamp;
    acqStamp = getSystemTime();
    acqStamp = (acqStamp / TIMING_ACQSTAMP_TO_NS);
    GlobalDevice *pGlobalStore = BXFISCServiceLocator_->getGlobalDevice();

    pGlobalStore->acqStampMonitor.set(acqStamp, pContext);

    for (std::vector<Device*>::iterator itr = deviceCol_.begin(); itr != deviceCol_.end(); ++itr)
    {
        Device *pDev = (*itr);
        char moduleName[NAME_SIZE];

        int status = pDev->deviceDetailedStatus.get(pContext);
        int statusHw = pDev->deviceHwStatus.get(pContext);
        if (pDev->moduleHardwareMonitorWd.get(pContext) < 0)
        {
            pDev->moduleHardwareMonitorWd.set(100, pContext);
        }

        if (pDev->moduleHardwareMonitorWd.get(pContext) == 0)
        {

            if (pDev->deviceDetailedStatusConfig.get(pContext) == 0 && pDev->deviceHwStatusConfig.get(pContext) == 00)
            {

                pDev->deviceDetailedStatusConfig.set(0xffffffff, pContext);
                pDev->deviceHwStatusConfig.set(0xffffffff, pContext);
                pDev->counterSampling.set(100, pContext);
                pDev->delayPm1_default.set(10, pContext);
                pDev->delayWidthPm1_default.set(10, pContext);
                pDev->highVoltagePm1_default.set(-1.4, pContext);
                pDev->highVoltagePm1_min.set(-2.1, pContext);
                pDev->delayPm1_default.set(10, pContext);
                pDev->delayWidthPm2_default.set(10, pContext);
                pDev->highVoltagePm2_default.set(-1.4, pContext);
                pDev->highVoltagePm2_min.set(-2.1, pContext);
                pDev->moduleLedPiedestal_off.set(0, pContext);
                pDev->moduleLedPiedestal_internal.set(0, pContext);
                pDev->moduleLedPiedestal_external.set(1.6, pContext);
                pDev->moduleLedPulse_off.set(0, pContext);
                pDev->moduleLedPulse_internal.set(0.3, pContext);
                pDev->moduleLedPulse_external.set(3, pContext);
                pDev->moduleThresholdPm1_default.set(-0.025, pContext);
                pDev->moduleThresholdPm2_default.set(-0.025, pContext);
            }
            long unsigned int size = NUMBER_OF_GAINS;
            if (pDev->motorPosition_gain.getCell(0, pContext) < 0.1)
            {

                double gain[NUMBER_OF_GAINS] =
                { 1.0, 1.0 };
                pDev->motorPosition_gain.set(gain, size, pContext);
            }
            pGlobalStore->acqStampStartUp.set(acqStamp, pContext);

            pDev->deviceDetailedStatus.set(pDev->deviceDetailedStatusConfig.get(pContext), pContext);
            pDev->deviceHwStatus.set(pDev->deviceHwStatusConfig.get(pContext), pContext);
            if (pDev->deviceHwStatus.get(pContext) == 0)
            { //server must have just started
                pDev->deviceHwStatus.set(0xffffffff, pContext);
            }
            status = pDev->deviceDetailedStatus.get(pContext);
            if (((int) status & DEVICE_DETAILED_STATUS::INITIALIZING_NO)
                    == (int) DEVICE_DETAILED_STATUS::INITIALIZING_NO)
            {
                // power up mode
                status = status - DEVICE_DETAILED_STATUS::INITIALIZING_NO;
                pDev->deviceDetailedStatus.set(status, pContext);

            }

            if (pDev->moduleHwRegMaster_default.get(pContext) == 0)
            {
                pDev->moduleHwRegMaster_default.set(0xe4, pContext);
                pDev->moduleHwRegBase_default.set(0x88, pContext);
                pDev->moduleHwRegCoin_default.set(0x0, pContext);
                pDev->moduleHwRegMotor_default.set(0x0, pContext);
            }

            pDev->moduleHwRegBase_setting.set(pDev->moduleHwRegBase_default.get(pContext), pContext);
            pDev->moduleHwRegMaster_setting.set(pDev->moduleHwRegMaster_default.get(pContext), pContext);
            pDev->moduleHwRegCoin_setting.set(pDev->moduleHwRegCoin_default.get(pContext), pContext);
            pDev->moduleHwRegMotor_setting.set(pDev->moduleHwRegMotor_default.get(pContext), pContext);
            pDev->moduleHwPortA_setting.set(pDev->moduleHwPortA_default.get(pContext), pContext);
            pDev->moduleHwPortB_setting.set(pDev->moduleHwPortB_default.get(pContext), pContext);
            pDev->moduleHwPortC_setting.set(pDev->moduleHwPortC_default.get(pContext), pContext);

            if (pDev->scanExecute_max.get(pContext) <= 0 || pDev->scanExecute_max.get(pContext) > 100)
            {
                pDev->scanExecute_max.set(100, pContext);
            }
            status = pDev->deviceDetailedStatus.get(pContext);

        }
        if (((int) statusHw & DEVICE_HW_STATUS::NO_ERROR_MONITOR_ON) == (int) DEVICE_HW_STATUS::NO_ERROR_MONITOR_ON
                && ((int) status & DEVICE_DETAILED_STATUS::MODULE_ON) == (int) DEVICE_DETAILED_STATUS::MODULE_ON
                && ((int) statusHw & DEVICE_HW_STATUS::NO_ERROR_DRIVER_THERE)
                        == (int) DEVICE_HW_STATUS::NO_ERROR_DRIVER_THERE)
        {
            float position;

            if (pDev->moduleHardwareMonitorWd.get(pContext) == 1)
            {

                float hv1 = pDev->highVoltagePm1_setting.get(pContext);
                float hv2 = pDev->highVoltagePm2_setting.get(pContext);

                if (pDev->phaIs.get(pContext) == YES_NO::YES)
                {

                    Tools::EmcPhaRegSet(pDev, pContext, (EMCREG::EMCREG) pDev->phaReg.get(pContext));
                    double dacs[NUMBER_OF_EMC_DACS];
                    double dacPdev;
                    Tools::EmcPhaDacGet(pDev, pContext, dacs);
                    for (unsigned int i = 0; i < NUMBER_OF_EMC_DACS; i++)
                    {
                        dacPdev = pDev->phaDacs.getCell(i, pContext);
                        if (dacPdev != 0.0 && dacPdev != dacs[i])
                        {
                            Tools::EmcPhaDacSet(pDev, pContext, (EMCDACS::EMCDACS) i, dacPdev);

                        }
                    }

                }
                double delay, delay_setting;
                double delayWidth, delayWidth_setting;
                double delayRanges_max[2], delayRanges_min[2];
                double delayWidthRanges_max[2], delayWidthRanges_min[2];

                Tools::DelayInfo(pDev, pContext, &delay, &delay_setting, &delayWidth, &delayWidth_setting,
                        &delayRanges_max[0], &delayRanges_min[0], &delayWidthRanges_max[0], &delayWidthRanges_min[0]);

                Tools::VoltageHighPm1To(hv1, pDev, pContext);
                Tools::VoltageHighPm2To(hv2, pDev, pContext);

                Tools::ModuleNameGet(pDev->driverNumber.get(pContext), moduleName);

                pDev->moduleName.set(moduleName, pContext);
                if (((int) status & DEVICE_DETAILED_STATUS::INITIALIZING_NO)
                        != (int) DEVICE_DETAILED_STATUS::INITIALIZING_NO)
                {
                    status = status + DEVICE_DETAILED_STATUS::INITIALIZING_NO;
                    pDev->deviceDetailedStatus.set(status, pContext);

                }
            }

            if (pDev->event.get(pContext) != (int) EVENTS::WWE && pDev->event.get(pContext) != (int) EVENTS::WE
                    && pDev->moduleHardwareMonitorWd.get(pContext) > 10)
            {
                Tools::PositionGet(&position, pDev, pContext);

                pDev->position.set(position, pContext);
                if (position > (pDev->motorPosition_max.get(pContext) + 5))
                {
                    pDev->motorPosition_max.set(position - 5, pContext);
                }
                if (position < pDev->motorPosition_min.get(pContext) - 5)
                {
                    pDev->motorPosition_min.set(position + 5, pContext);
                }

            }
            if (pDev->moduleHardwareMonitorWd.get(pContext) > 10)
            {
                if (pDev->doIpcReset.get(pContext))
                {
                    LowLevel::InitIpc(pDev->driverNumber.get(pContext));
                    pDev->doIpcReset.set(false, pContext);
                }

                unsigned short aaShort[CHANNELS_AA_NUMBER];
                float aaFloat[CHANNELS_AA_NUMBER];
                Tools::AAsGet(aaShort, aaFloat, pDev, pContext);

                REGBASE::REGBASE regBase;
                REGCOIN::REGCOIN regCoin;
                REGMOTOR::REGMOTOR regMotor;
                Tools::RegBaseGet(&regBase, pDev, pContext);
                Tools::RegCoinGet(&regCoin, pDev, pContext);
                Tools::RegMotorGet(&regMotor, pDev, pContext);
                pDev->moduleHwRegBase.set(regBase, pContext);
                pDev->moduleHwRegCoin.set(regCoin, pContext);
                pDev->moduleHwRegMotor.set(regMotor, pContext);
                unsigned short ports[3];
                Tools::PortsGet(ports, pDev, pContext);
                pDev->moduleHwPortA.set((PORTA::PORTA) ports[0], pContext);
                pDev->moduleHwPortB.set((PORTB::PORTB) ports[1], pContext);
                pDev->moduleHwPortC.set((PORTC::PORTC) ports[2], pContext);

                if ((int) ports[0] & (int) PORTA::LIMIT_NEG)
                {
                    if (statusHw & DEVICE_HW_STATUS::POSITION_LIMIT_NEG_OK)
                    {
                        statusHw = statusHw - DEVICE_HW_STATUS::POSITION_LIMIT_NEG_OK;
                    }
                }
                else
                {
                    if ((int) (statusHw & DEVICE_HW_STATUS::POSITION_LIMIT_NEG_OK) == 0)
                    {
                        statusHw = statusHw + DEVICE_HW_STATUS::POSITION_LIMIT_NEG_OK;
                    }
                }
                if ((int) ports[0] & (int) PORTA::LIMIT_POS)
                {
                    if ((int) (statusHw & DEVICE_HW_STATUS::POSITION_LIMIT_POS_OK))
                    {
                        statusHw = statusHw - DEVICE_HW_STATUS::POSITION_LIMIT_POS_OK;
                    }
                }
                else

                if ((int) (statusHw & DEVICE_HW_STATUS::POSITION_LIMIT_POS_OK) == (int) 0)
                {
                    statusHw = statusHw + DEVICE_HW_STATUS::POSITION_LIMIT_POS_OK;
                }

                if ((int) ports[0] & (int) PORTA::HV1_ALARM)
                {
                    pDev->highVoltagePm1_status.set(0, pContext);
                    if (statusHw & DEVICE_HW_STATUS::HV1_STATUS_OK)
                    {
                        statusHw = statusHw - DEVICE_HW_STATUS::HV1_STATUS_OK;
                    }
                }
                else
                {
                    pDev->highVoltagePm1_status.set(1, pContext);
                    if ((int) (statusHw & DEVICE_HW_STATUS::HV1_STATUS_OK) == 0)
                    {
                        statusHw = statusHw + DEVICE_HW_STATUS::HV1_STATUS_OK;
                    }
                }
                if ((int) ports[0] & (int) PORTA::HV2_ALARM)
                {
                    pDev->highVoltagePm2_status.set(0, pContext);
                    if ((int) (statusHw & DEVICE_HW_STATUS::HV2_STATUS_OK))
                    {
                        statusHw = statusHw - DEVICE_HW_STATUS::HV2_STATUS_OK;
                    }
                }
                else
                {
                    pDev->highVoltagePm2_status.set(1, pContext);
                    if ((int) (statusHw & DEVICE_HW_STATUS::HV2_STATUS_OK) == (int) 0)
                    {
                        statusHw = statusHw + DEVICE_HW_STATUS::HV2_STATUS_OK;
                    }
                }

                if ((int) ports[0] & (int) PORTA::PS_INT)
                {
                    if (statusHw & DEVICE_HW_STATUS::PS_STATUS_INTERNAL)
                    {
                        statusHw = statusHw - DEVICE_HW_STATUS::PS_STATUS_INTERNAL;
                    }
                }
                else
                {
                    if ((int) (statusHw & DEVICE_HW_STATUS::PS_STATUS_INTERNAL) == 0)
                    {
                        statusHw = statusHw + DEVICE_HW_STATUS::PS_STATUS_INTERNAL;
                    }
                }
                if ((int) ports[0] & (int) PORTA::PS_EXT)
                {
                    if ((int) (statusHw & DEVICE_HW_STATUS::PS_STATUS_28V))
                    {
                        statusHw = statusHw - DEVICE_HW_STATUS::PS_STATUS_28V;
                    }
                }
                else
                {
                    if ((int) (statusHw & DEVICE_HW_STATUS::PS_STATUS_28V) == (int) 0)
                    {
                        statusHw = statusHw + DEVICE_HW_STATUS::PS_STATUS_28V;
                    }
                }
                pDev->deviceHwStatus.set((DEVICE_HW_STATUS::DEVICE_HW_STATUS) statusHw, pContext);
                Tools::VoltageOffsetControl(pDev, pContext);
                Tools::MonitorDefaults(pDev, pContext);
                Tools::MonitorSettings(pGlobalStore, pDev, pContext);
                Tools::MonitorStatus(pDev, pContext);
                Tools::MonitorWarnings(pDev, pContext);
                Tools::MonitorErrors(pDev, pContext);
            }

            sprintf(msg, " wd[%03d] acq[%03d] hv1[%2.2f] hv2[%2.2f] pos[%2.1f] pm1[%5d] pm2[%5d] coin[%5d]"

            , pDev->moduleHardwareMonitorWd.get(pContext), pDev->moduleHardwareAcquisitionWd.get(pContext),
                    pDev->highVoltagePm1.get(pContext), pDev->highVoltagePm2.get(pContext),
                    pDev->position.get(pContext), (int) pDev->countsPm1.get(pContext),
                    (int) pDev->countsPm2.get(pContext), (int) pDev->counts.get(pContext));
            pDev->msgMonitor.set(msg, pContext);

            if (pDev->moduleHardwareMonitorWd.get(pContext) % 10 == 0
                    && (int) (statusHw & DEVICE_HW_STATUS::DEBUG_OFF) != (int) DEVICE_HW_STATUS::DEBUG_OFF)
            {
            }
            pDev->moduleHardwareMonitorWd.set((pDev->moduleHardwareMonitorWd.get(pContext) + 1), pContext);
        }

        else
        {
            if (((int) status & DEVICE_DETAILED_STATUS::ERROR_NO) == (int) DEVICE_DETAILED_STATUS::ERROR_NO)
            {
                status = status - DEVICE_DETAILED_STATUS::ERROR_NO;
                pDev->deviceDetailedStatus.set((DEVICE_DETAILED_STATUS::DEVICE_DETAILED_STATUS) status, pContext);

            }

        }
    }

}

} // BXFISC
