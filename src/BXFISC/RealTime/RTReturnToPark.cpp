
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#include <BXFISC/GeneratedCode/ServiceLocator.h>
#include <BXFISC/RealTime/RTReturnToPark.h>

#include <cmw-log/Logger.h>
#include <BXFISC/Common/LowLevelFisc.h>
#include <BXFISC/Common/LowLevelDriver.h>

namespace
{

cmw::log::Logger& logger = cmw::log::LoggerFactory::getLogger("FESA.USR.BXFISC.RealTime.RTReturnToPark");

} // namespace

#define LOG_DIAG_IF(topic,message) \
{ \
    DiagnosticsDefs::DiagnosticMessage diagMsg(DiagnosticsDefs::Side::user, DiagnosticsDefs::Source::rt); \
    diagMsg.fesaClass = "BXFISC"; \
    diagMsg.name = "RTReturnToPark"; \
    diagMsg.action = DiagnosticsDefs::Action::undefined; \
    diagMsg.msg = message; \
    BXFISCServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
}

#define LOG_DIAG_DEVICE_IF(topic,message,device) \
{ \
    if (device->isLoggable()) \
    { \
        LOG_DIAG_IF(topic,message); \
    } \
}

namespace BXFISC
{

RTReturnToPark::RTReturnToPark(fesa::RTActionConfig& rtActionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses) :
     RTReturnToParkBase(rtActionConfig, serviceLocator, serviceLocatorRelatedClasses) 
{
}

RTReturnToPark::~RTReturnToPark()
{
}

void RTReturnToPark::sendFilamentToPark(Device * d, const fesa::MultiplexingContext * c) {
    const auto driver = LowLevelDriver::getDriver(d->driverNumber.get(c));
    ::EaSetGOMOTPARK(driver, 0);
}


// This method is called whenever an event triggers this action; this is where custom code must be written.
// A real time action operates on a device collection, this is why a loop which iterates over the device collection
// of this real time action is there.
void RTReturnToPark::execute(fesa::RTEvent* pEvt)
{
    const fesa::MultiplexingContext* context = pEvt->getMultiplexingContext();
    const Devices& devices = getFilteredDeviceCollection(pEvt);
    for (Devices::const_iterator it = devices.begin(); it != devices.end(); ++it)
    {
        try
        {
            Device* device = *it;
            sendFilamentToPark(device, context);
            
        }
        catch (const fesa::FesaException& exception)
        {
            LOG_ERROR_IF(logger, exception.getMessage());
        }
    }
}

} // BXFISC
