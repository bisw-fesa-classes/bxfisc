// FESA framework
// Use this code as a starting-point to develop your own equipment class

#include <BXFISC/RealTime/CES.h>

#include <BXFISC/GeneratedCode/CustomEventSourceGen.h>
#include <BXFISC/GeneratedCode/ServiceLocator.h>

#include <cmw-log/Logger.h>

namespace 
{

CMW::Log::Logger& logger = CMW::Log::LoggerFactory::getLogger("FESA.USR.BXFISC.RealTime.CES");

} // namespace

#define LOG_DIAG_IF(topic,message) \
	{ \
		DiagnosticsDefs::DiagnosticMessage diagMsg(DiagnosticsDefs::Side::user, DiagnosticsDefs::Source::event); \
		diagMsg.fesaClass = "BXFISC"; \
		diagMsg.name = "CES"; \
		diagMsg.action = DiagnosticsDefs::Action::undefined; \
		diagMsg.msg = message; \
		BXFISCServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
	}

namespace BXFISC
{

CES::CES(const fesa::AbstractServiceLocator* serviceLocator,
	const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses) :
	CESBase ("CES", serviceLocator, serviceLocatorRelatedClasses)
{
}

CES::~CES()
{
}

void CES::connect(const boost::shared_ptr<fesa::EventElement>& eventElement)
{
}

void CES::wait(boost::shared_ptr<fesa::RTEvent>& eventToFire)
{
	// This code provides an example on how a custom-event-source can look like.
	struct timespec time;
	time.tv_sec = 5;	// wake-up every 1 second
	time.tv_nsec = 0;

	// Typically there is some blocking call, like this one in a event-source.
	nanosleep(&time, &time);

	// Here you can create your own event by filling the eventToFire with the needed information.
	// Use the Event-Enumeration defined in the header file of this class!
	// Check the BaseClass of this class for more options!
	createEvent(eventToFire, CES::defaultEvent);

	// Once this method returns, the fesa-core will process our event and post it to all interested schedulers.
	// After that, this method will be called again.
}

} // BXFISC
