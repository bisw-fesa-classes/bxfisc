#include <BXFISC/RealTime/RTDeviceClass.h>

#include <BXFISC/GeneratedCode/ServiceLocator.h>
#include <fesa-core/Core/AbstractEvent.h>
#include <BXFISC/Common/ComponentRegistry.hpp>

#include <cmw-log/Logger.h>
#include <BXFISC/Common/Tools.h>
#include <BXFISC/Common/LowLevelDriver.h>

#include <string>
#include <sstream>
#include <vector>

namespace 
{

CMW::Log::Logger& logger = CMW::Log::LoggerFactory::getLogger("FESA.USR.BXFISC.RealTime.RTDeviceClass");

} // namespace

namespace BXFISC
{

RTDeviceClass* RTDeviceClass::instance_ = NULL;

RTDeviceClass::RTDeviceClass () :
	RTDeviceClassGen()
{
}

RTDeviceClass::~RTDeviceClass()
{
}

RTDeviceClass* RTDeviceClass::getInstance()
{
	if (instance_ == NULL)
	{
		instance_ = new RTDeviceClass();
	}
	return instance_;
}

void RTDeviceClass::releaseInstance()
{
	if (instance_ != NULL) 
	{
		delete instance_;
		instance_ = NULL;
	}
}

void RTDeviceClass::specificInit()
{
    using NsRegistry::ComponentRegistry;

    const NoneContext context;
    const Devices& deviceCol = BXFISCServiceLocator_->getDeviceCollection();
    for (Devices::const_iterator it = deviceCol.begin(); it != deviceCol.end(); ++it) {

        auto pDev = *it;
        pDev->phaGain.set(EMCGAIN::INVALID, &context);

        std::stringstream s;
        s << "/dsc/local/bin/fafi " << pDev->driverNumber.get(&context) << "  -initall 0";
        system(s.str().c_str());


        uint32_t size = NUMBER_OF_EMC_DACS;
        double dacs[4];
        BXFISC_TOOL::Tools::EmcPhaDacGet(pDev, &context, dacs);
        pDev->phaDacs.set(dacs, size, &context);

        const int delay1 = pDev->delayPm1_default.get(&context);
        const int delay2 = pDev->delayPm2_default.get(&context);
        const int width1 = pDev->delayWidthPm1_default.get(&context);
        const int width2 = pDev->delayWidthPm1_default.get(&context);

        pDev->delayPm1_setting.set(delay1, &context);
        pDev->delayPm2_setting.set(delay2, &context);
        pDev->delayPm1.set(delay1, &context);
        pDev->delayPm2.set(delay2, &context);

        pDev->delayWidthPm1_setting.set(width1, &context);
        pDev->delayWidthPm2_setting.set(width2, &context);
        pDev->delayWidthPm1.set(width1, &context);
        pDev->delayWidthPm2.set(width2, &context);

        pDev->highVoltagePm1_offset.set(0, &context);
        pDev->highVoltagePm2_offset.set(0, &context);

        pDev->highVoltagePm1_setting.set(pDev->highVoltagePm1_default.get(&context), &context);
        pDev->highVoltagePm2_setting.set(pDev->highVoltagePm2_default.get(&context), &context);

        BXFISC_TOOL::Tools::RegBaseSet(0x30, pDev, &context, true);
        BXFISC_TOOL::Tools::DelaySet(pDev, &context, 0, delay1, width1);
        BXFISC_TOOL::Tools::DelaySet(pDev, &context, 1, delay2, width2);
    }

}

void RTDeviceClass::specificShutDown() {

    LowLevelDriver::CloseDown();
}

} // BXFISC
