// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#ifndef BXFISC_MonitorAction_H_
#define BXFISC_MonitorAction_H_



#include <BXFISC/GeneratedCode/GenRTActions.h>

#include <BXFISC/GeneratedCode/Device.h>


namespace BXFISC
{
class MonitorAction : public MonitorActionBase 
{
	public:

		MonitorAction (fesa::RTActionConfig& rtActionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses);

		virtual  ~MonitorAction();
		void execute(fesa::RTEvent* pEvt);
};
}
#endif
