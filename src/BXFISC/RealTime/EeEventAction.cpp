// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#include <BXFISC/RealTime/EeEventAction.h>

#include <fesa-core/RealTime/RTEvent.h>
#include <fesa-core/Synchronization/NoneContext.h>
#include <fesa-core/Exception/FesaException.h>
#include <fesa-core-cern/Synchronization/TimingContext.h>

#include <BXFISC/GeneratedCode/ServiceLocator.h>
#include <BXFISC/Common/Tools.h>

//#include <BXSCINT/Common/UTILS.h>
#include <cmw-log/Logger.h>
#include <iostream>
#include <cstdlib>
#include <boost/lexical_cast.hpp>

using namespace BXFISC_TOOL;

namespace
{

CMW::Log::Logger &logger = CMW::Log::LoggerFactory::getLogger("FESA.USR.BXFISC.RealTime.EeEventAction");

} // namespace

#define LOG_DIAG_IF(topic,message)                           \
	{                                                    \
		DiagnosticUtils::DiagnosticMessage diagMsg;  \
		diagMsg.side = DiagnosticUtils::user;        \
		diagMsg.source = DiagnosticUtils::rt;        \
		diagMsg.fesaClass = "BXFISC"; \
		diagMsg.name = "EeEventAction"; \
		diagMsg.action = DiagnosticUtils::undefined; \
		diagMsg.msg = message;                       \
		BXFISCServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
	}

namespace BXFISC
{

EeEventAction::EeEventAction(fesa::RTActionConfig &rtActionConfig, const fesa::AbstractServiceLocator *serviceLocator,
        const std::map<std::string, const fesa::AbstractServiceLocator*> &serviceLocatorRelatedClasses) :
                EeEventActionBase(rtActionConfig, serviceLocator, serviceLocatorRelatedClasses)
{
}

EeEventAction::~EeEventAction()
{
}

void EeEventAction::execute(fesa::RTEvent *pEvt)
{
    fesa::MultiplexingContext *pContext = pEvt->getMultiplexingContext();
    GlobalDevice *pGlobalStore = BXFISCServiceLocator_->getGlobalDevice();
    fesaCERN::TimingContext *theTimingContext = dynamic_cast<fesaCERN::TimingContext*>(pContext);
    if (!theTimingContext) throw FesaTypeMismatchException(__FILE__, __LINE__, "Action linked to wrong event source");

    int state;
    int stateHw;
    char moduleName[9];

    const timestamp_ns acqStamp = pContext->getTimeStamp();
    const timestamp_ns cycleStamp = (acqStamp - pContext->getCycleStamp());

    pGlobalStore->acqStampEE.set(acqStamp, pContext);
    pGlobalStore->cycleStampEE.set(cycleStamp, pContext);

    std::ostringstream logTextStream;
    logTextStream << Tools::TimeString().c_str() << "executing RT action: BXFISCeeEventAction";

    for (std::vector<Device*>::iterator itr = deviceCol_.begin(); itr != deviceCol_.end(); ++itr)
    {
        try
        {
            Device *pDev = (*itr);

            bool notify = true;

            if (pDev->destination.get() != DESTINATION::NONE) {

                Timing::EventValue* pTimingEvt = theTimingContext->getEventValue();
                DomainStore* domStore = BXFISCServiceLocator_->getDomainStore(pContext->getTimingDomainName());
                Timing::Value hValue;
                pTimingEvt->getFieldValue(domStore->timing_dest.get(), &hValue);
                std::string destination = hValue.getAsName();

                if (pDev->destination.get() == DESTINATION::T8 && destination.compare("EAST_T8") != 0) {
                    notify = false;
                }
                else if (pDev->destination.get() == DESTINATION::T9 && destination.compare("EAST_T9") != 0) {
                    notify = false;
                }
                else if (pDev->destination.get() == DESTINATION::T1011 && destination.compare("EAST_N") != 0) {
                    notify = false;
                }
            }

            if ((int) (pDev->event.get(pContext)) != (int) EVENTS::EE
                    && pDev->moduleHardwareMonitorWd.get(pContext) > 15)
            {

                state = pDev->deviceDetailedStatus.get(pContext);
                stateHw = pDev->deviceHwStatus.get(pContext);
                uint32_t fifoSize = BXFISC::MAX_FIFO_SIZE;
                pDev->cycleName.set(pContext->getCycleName().c_str(), pContext);
                if (((int) stateHw & DEVICE_HW_STATUS::NO_ERROR_DRIVER_THERE)
                        == (int) DEVICE_HW_STATUS::NO_ERROR_DRIVER_THERE
                        && ((int) state & DEVICE_DETAILED_STATUS::MODULE_ON) == (int) DEVICE_DETAILED_STATUS::MODULE_ON
                        && ((int) stateHw & DEVICE_HW_STATUS::NO_ERROR_ACQUISITION_ON)
                                == (int) DEVICE_HW_STATUS::NO_ERROR_ACQUISITION_ON
                        && pDev->moduleHardwareMonitorWd.get(pContext) > 5)
                {
                    pDev->moduleHardwareAcquisitionWd.set(pDev->moduleHardwareAcquisitionWd.get(pContext) + 1,
                            pContext);
                    long counts[4];
                    long *ptrL;
                    ptrL = &counts[0];
                    Tools::ModuleNameGet(pDev->driverNumber.get(pContext), moduleName);

                    Tools::CountsGet(ptrL, pDev, pContext);
                    Tools::ModuleNameGet(pDev->driverNumber.get(pContext), moduleName);
                    pDev->counts.set(counts[0], pContext);
                    pDev->countsPm1.set(counts[1], pContext);
                    pDev->countsPm2.set(counts[2], pContext);
                    pDev->countsExtCoincidence.set(counts[3], pContext);

                    Tools::EE(pDev, pContext);

                    pDev->scanStart_last.set(pDev->scanStart_current.get(pContext), pContext);
                    pDev->scanStop_last.set(pDev->scanStop_current.get(pContext), pContext);
                    pDev->scanDelay_last.set(pDev->scanDelay_current.get(pContext), pContext);

                    if (((int) state & DEVICE_DETAILED_STATUS::COUNTS_EQUAL_ACQ_TIME_OFF)
                            != (int) DEVICE_DETAILED_STATUS::COUNTS_EQUAL_ACQ_TIME_OFF)
                    {
                        pDev->counts.set((acqStamp / TIMING_ACQSTAMP_TO_SECONDS) % 100000, pContext);
                        pDev->countsPm1.set((acqStamp / TIMING_ACQSTAMP_TO_SECONDS) % 100000, pContext);
                        pDev->countsPm2.set((acqStamp / TIMING_ACQSTAMP_TO_SECONDS) % 100000, pContext);
                        pDev->countsExtCoincidence.set((acqStamp / TIMING_ACQSTAMP_TO_SECONDS) % 100000, pContext);

                        int32_t countTest[BXFISC::MAX_FIFO_SIZE];
                        int number = pDev->countNumber.get(pContext);
                        for (unsigned int xxx = number; xxx < BXFISC::MAX_FIFO_SIZE; xxx++)
                        {
                            countTest[xxx] = (long) 0;
                        }
                        for (int xxx = 0; xxx < number; xxx++)
                        {
                            countTest[xxx] = (long) xxx;
                        }

                        pDev->countPm1Array.set(countTest, fifoSize, pContext);

                        for (int xxx = 0, y = number; xxx < number; xxx++)
                        {
                            countTest[xxx] = (long) y--;
                        }
                        pDev->countPm2Array.set(countTest, fifoSize, pContext);

                        for (int xxx = 0; xxx < number; xxx++)
                        {
                            if (xxx < (number / 2))
                            {
                                countTest[xxx] = xxx / 2;
                            }
                            else
                            {
                                countTest[xxx] = (number - xxx) / 2;
                            }
                        }
                        pDev->countArray.set(countTest, fifoSize, pContext);
                        for (int xxx = 0; xxx < number; xxx++)
                        {
                            if (xxx < (number / 2))
                            {
                                countTest[xxx] = (number - xxx) / 2;
                            }
                            else
                            {
                                countTest[xxx] = (xxx - number / 2) / 2;
                            }
                        }

                        pDev->countExtCoincidenceArray.set(countTest, fifoSize, pContext);

                    }

                    if (((int) state & DEVICE_DETAILED_STATUS::COUNTS_EQUAL_EQN_NUM_OFF)
                            != (int) DEVICE_DETAILED_STATUS::COUNTS_EQUAL_EQN_NUM_OFF)
                    {
                        float position;
                        Tools::PositionGet(&position, pDev, pContext);
                        pDev->position.set(position, pContext);

                    }

                    pDev->acqMsg.set("ACQUISITION GOOD", pContext);

                }
                else
                {
                    pDev->acqMsg.set("ACQUISITION TURN OFF", pContext);
                }
                //set only if scan started
                pDev->event.set(EVENTS::EE, pContext);

                // Arek comment-out
//                Tools::PositionSet(pDev->position_setting.get(pContext), pDev, pContext);

                if (((int) state & DEVICE_DETAILED_STATUS::LOCAL_NO) != (int) DEVICE_DETAILED_STATUS::LOCAL_NO)
                {
                    Tools::FlashLed(pDev, pContext);
                }
                // ok we are finished for this device, lets move onto the next one.

            }

            int64_t spillDuration = (pGlobalStore->acqStampEE.get(pContext) - pGlobalStore->acqStampWE.get(pContext))
                    / 1000000;
            pGlobalStore->spillTime.set(static_cast<int32_t>(spillDuration), pContext);

            auto currEvent = BXFISC::EVENTS::EE;
            triggerOnDemandEventSource(ODSetSpeed, pContext, reinterpret_cast<const char *>(&currEvent), sizeof(currEvent));

            if (notify) {
                resetManualNotification();
                registerPha(pDev);
                registerPhaAcquisition(pDev);
                registerAcquisition(pDev);
                registerAcquisitionShort(pDev);
                registerEaPiquet(pDev);
                registerEaPiquetShort(pDev);
                registerStatus(pDev);
                sendManualNotification(pContext);
            }
        }
        catch (fesa::FesaException &exception)
        {
            std::cout << __FILE__ << " " << __LINE__ << "EX- in the device loop :( " << exception.getMessage()
                    << std::endl;
        }
    }

}

} // BXFISC
