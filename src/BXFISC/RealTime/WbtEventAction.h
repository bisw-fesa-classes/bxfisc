// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#ifndef BXFISC_WbtEventAction_H_
#define BXFISC_WbtEventAction_H_



#include <BXFISC/GeneratedCode/GenRTActions.h>

#include <BXFISC/GeneratedCode/Device.h>


namespace BXFISC
{
class WbtEventAction : public WbtEventActionBase 
{
	public:

		WbtEventAction (fesa::RTActionConfig& rtActionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses);

		virtual  ~WbtEventAction();
		void execute(fesa::RTEvent* pEvt);
};
}
#endif
