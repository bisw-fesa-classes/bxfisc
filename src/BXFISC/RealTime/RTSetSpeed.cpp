// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#include <BXFISC/GeneratedCode/ServiceLocator.h>
#include <BXFISC/RealTime/RTSetSpeed.h>
#include <BXFISC/Common/SpeedHandlerFactory.h>

#include <cmw-log/Logger.h>
#include <chrono>

namespace
{

cmw::log::Logger &logger = cmw::log::LoggerFactory::getLogger("FESA.USR.BXFISC.RealTime.RTSetSpeed");

} // namespace

#define LOG_DIAG_IF(topic,message) \
{ \
    DiagnosticsDefs::DiagnosticMessage diagMsg(DiagnosticsDefs::Side::user, DiagnosticsDefs::Source::rt); \
    diagMsg.fesaClass = "BXFISC"; \
    diagMsg.name = "RTSetSpeed"; \
    diagMsg.action = DiagnosticsDefs::Action::undefined; \
    diagMsg.msg = message; \
    BXFISCServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
}

#define LOG_DIAG_DEVICE_IF(topic,message,device) \
{ \
    if (device->isLoggable()) \
    { \
        LOG_DIAG_IF(topic,message); \
    } \
}

namespace BXFISC
{

RTSetSpeed::RTSetSpeed(fesa::RTActionConfig &rtActionConfig, const fesa::AbstractServiceLocator *serviceLocator,
        const std::map<std::string, const fesa::AbstractServiceLocator*> &serviceLocatorRelatedClasses) :
                RTSetSpeedBase(rtActionConfig, serviceLocator, serviceLocatorRelatedClasses)
{
}

RTSetSpeed::~RTSetSpeed()
{
}

// This method is called whenever an event triggers this action; this is where custom code must be written.
// A real time action operates on a device collection, this is why a loop which iterates over the device collection
// of this real time action is there.
void RTSetSpeed::execute(fesa::RTEvent *pEvt)
{
    using namespace NsSpeedConverter;
    using namespace std::chrono;
    const fesa::MultiplexingContext *c = pEvt->getMultiplexingContext();
    const auto pGlobalStore = BXFISCServiceLocator_->getGlobalDevice();
    auto event = *(pEvt->getPayload()->getValue<BXFISC::EVENTS::EVENTS>());
//    const Devices &devices = getFilteredDeviceCollection(pEvt);
    const Devices& devices = deviceCol_;
    for (Devices::const_iterator it = devices.begin(); it != devices.end(); ++it)
    {
        try
        {
            Device *device = *it;
            SpeedHandlerFactory factory;
            auto speedHandler = factory.get(event, device, c);
            const auto start = device->scanStart_next.get(c);
            const auto stop = device->scanStop_next.get(c);
            auto time = pGlobalStore->spillTime.get(c);
            double distance = std::abs(stop - start);
            duration<double> durSeconds(static_cast<double>(time) / 1000.0);
            const double speed = distance / durSeconds.count();

            speedHandler->setSpeed(event, speed);
        }
        catch (const fesa::FesaException &exception)
        {
            LOG_ERROR_IF(logger, exception.getMessage());
        }
    }
}

} // BXFISC
