
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#include <BXFISC/GeneratedCode/ServiceLocator.h>
#include <BXFISC/RealTime/RTScanSetup.h>

#include <cmw-log/Logger.h>
#include <BXFISC/Common/LowLevelDac.h>
#include <BXFISC/Common/LowLevelDriver.h>
#include <BXFISC/Common/LowLevelInclude.h>
#include <BXFISC/Common/LowLevelFisc.h>
#include <BXFISC/Common/SpeedConverter.h>

#include <chrono>

namespace
{

cmw::log::Logger& logger = cmw::log::LoggerFactory::getLogger("FESA.USR.BXFISC.RealTime.RTScanSetup");

} // namespace

#define LOG_DIAG_IF(topic,message) \
{ \
    DiagnosticsDefs::DiagnosticMessage diagMsg(DiagnosticsDefs::Side::user, DiagnosticsDefs::Source::rt); \
    diagMsg.fesaClass = "BXFISC"; \
    diagMsg.name = "RTScanSetup"; \
    diagMsg.action = DiagnosticsDefs::Action::undefined; \
    diagMsg.msg = message; \
    BXFISCServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
}

#define LOG_DIAG_DEVICE_IF(topic,message,device) \
{ \
    if (device->isLoggable()) \
    { \
        LOG_DIAG_IF(topic,message); \
    } \
}

namespace BXFISC
{

RTScanSetup::RTScanSetup(fesa::RTActionConfig& rtActionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses) :
     RTScanSetupBase(rtActionConfig, serviceLocator, serviceLocatorRelatedClasses) 
{
}

RTScanSetup::~RTScanSetup()
{
}

int16_t RTScanSetup::convertPositionToDAC(const double value) {
    return static_cast<int16_t>(((value / 10) + 5) * (4096 / 10));
}

//int16_t RTScanSetup::convertSpeedToDAC(const double speed) {
//
//    const double maxVoltage = 5;
//    const double speedVoltage = [&maxVoltage](const double s) {
//        if (s < 0 || s > 65) return maxVoltage; // max speed
//        auto val = static_cast<double>((s - 0.44) / 13.62);
//        return val;
//    }(speed);
//
//    /*
//     * Explanation: 2048 is half a range on the DAC,
//     *  it is added to the ratio of a speed turned into voltage divided by the dac resolution
//     * speedVoltage
//     * */
//    const double voltageRange = maxVoltage * 2;
//    const int32_t dacRange = 4096;
//    const double resolution = voltageRange / dacRange;
//    const int16_t regValue = static_cast<int16_t>(dacRange/2 + (speedVoltage/resolution));
//
//
//    return regValue;
//}

void RTScanSetup::setStartWindow(const double high, const double low, Device * dev, const MultiplexingContext * c) {
    const auto driver = dev->driverNumber.get(c);
    LowLevelDac::DacMpcRawSet(driver, BX_DAC_MPC::DAC_MPC_START_MAX_0, convertPositionToDAC(high));
    LowLevelDac::DacMpcRawSet(driver, BX_DAC_MPC::DAC_MPC_START_MIN_1, convertPositionToDAC(low));
}

void RTScanSetup::setStopWindow(const double high, const double low, Device * dev, const MultiplexingContext * c) {
    const auto driver = dev->driverNumber.get(c);
    LowLevelDac::DacMpcRawSet(driver, BX_DAC_MPC::DAC_MPC_STOP_MAX_2, convertPositionToDAC(high));
    LowLevelDac::DacMpcRawSet(driver, BX_DAC_MPC::DAC_MPC_STOP_MIN_3, convertPositionToDAC(low));
}

void RTScanSetup::setParkWindow(const double high, const double low, Device * dev, const MultiplexingContext * c) {
    const auto driver = dev->driverNumber.get(c);
    LowLevelDac::DacMpcRawSet(driver, BX_DAC_MPC::DAC_MPC_GARAGE_MAX_4, convertPositionToDAC(high));
    LowLevelDac::DacMpcRawSet(driver, BX_DAC_MPC::DAC_MPC_GARAGE_MIN_5, convertPositionToDAC(low));
}

void RTScanSetup::setStartPosition(const double position, Device * dev, const MultiplexingContext * c) {
    const auto driver = dev->driverNumber.get(c);
    LowLevelDac::DacBaseRawSet(driver, BX_DAC_BASE::DAC_BASE_START_0, convertPositionToDAC(position));
}

void RTScanSetup::setStopPosition(const double position, Device * dev, const MultiplexingContext * c) {
    const auto driver = dev->driverNumber.get(c);
    LowLevelDac::DacBaseRawSet(driver, BX_DAC_BASE::DAC_BASE_STOP_1, convertPositionToDAC(position));
}

void RTScanSetup::setParkPosition(const double position, Device * dev, const MultiplexingContext * c) {
    const auto driver = dev->driverNumber.get(c);
    LowLevelDac::DacBaseRawSet(driver, BX_DAC_BASE::DAC_BASE_GARAGE_2, convertPositionToDAC(position));
}

void RTScanSetup::setDirection(const int32_t direction, Device * dev, const MultiplexingContext * c) {

}

void RTScanSetup::setupBaseRegister(Device * dev, const MultiplexingContext * c) {

    LowLevelDriver::BaseRegStartWindowOn(dev->driverNumber.get(c));
    LowLevelDriver::BaseRegDisableAutoReturn(dev->driverNumber.get(c));
    LowLevelDriver::BaseRegAutoReturnOnEE(dev->driverNumber.get(c));
    LowLevelDriver::BaseRegEnableExtraction(dev->driverNumber.get(c));
    LowLevelDriver::BaseRegBTDisabled(dev->driverNumber.get(c));
}

void RTScanSetup::setupDelay(Device * dev, const MultiplexingContext * c) {

    int driver = LowLevelDriver::getDriver(dev->driverNumber.get(c));

    /*
     * Clear the register (Angelo's procedure, don't ask me)
     * */
    uint16_t val;
    EaGetADACRO(driver, &val);
    EaSetADACRO(driver, 0x00);
    EaGetADACRO(driver, &val);
    /*
     * Procedure to set the delay of the filament start
     * */
    const uint16_t delayMs = static_cast<uint16_t>(dev->scanDelay_next.get(c) * 10);
    EaSetADACRO(driver, 0x19);
    EaSetADACRO(driver, delayMs & 0x00FF);
    EaSetADACRO(driver, 0x18);
    EaSetADACRO(driver, static_cast<uint16_t>(delayMs >> 8));
}



// This method is called whenever an event triggers this action; this is where custom code must be written.
// A real time action operates on a device collection, this is why a loop which iterates over the device collection
// of this real time action is there.
void RTScanSetup::execute(fesa::RTEvent* pEvt)
{
    std::cout << "RTScanSetup" << std::endl;

    using namespace std::chrono;
    const fesa::MultiplexingContext* c = pEvt->getMultiplexingContext();
    auto pGlobalStore = BXFISCServiceLocator_->getGlobalDevice();
    const Devices& devices = getFilteredDeviceCollection(pEvt);

    for (Devices::const_iterator it = devices.begin(); it != devices.end(); ++it) {

        try {

            Device * device = *it;

            if (device->scanAction.get(c) == YES_NO::YES) {

                setupBaseRegister(device, c);

                const auto start = device->scanStart_next.get(c);
                const auto stop = device->scanStop_next.get(c);
                const auto garage = device->motorParkPosition.get(c);

                setStartPosition(start, device, c);
                setStopPosition(stop, device, c);
                setParkPosition(garage, device, c);

                auto windowWidth = device->windowWidth.get(c);
                windowWidth = windowWidth != 0 ? windowWidth : 1;
                setStartWindow(start + windowWidth, start - windowWidth, device, c);
                setStopWindow(stop + windowWidth, stop - windowWidth, device, c);
                setParkWindow(garage + windowWidth, garage - windowWidth, device, c);

                setupDelay(device, c);

                const std::string & name = device->getName();
                triggerOnDemandEventSource(ODReturnToStart, c, name.c_str(), name.size() + 1);
            }

            device->scanAction.set(YES_NO::NO, c);
        }
        catch (const fesa::FesaException& exception) {
            LOG_ERROR_IF(logger, exception.getMessage());
        }
    }
}

} // BXFISC
