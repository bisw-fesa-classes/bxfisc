// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#ifndef BXFISC_WweEventAction_H_
#define BXFISC_WweEventAction_H_



#include <BXFISC/GeneratedCode/GenRTActions.h>

#include <BXFISC/GeneratedCode/Device.h>


namespace BXFISC
{
class WweEventAction : public WweEventActionBase 
{
	public:

		WweEventAction (fesa::RTActionConfig& rtActionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses);

		virtual  ~WweEventAction();
		void execute(fesa::RTEvent* pEvt);
};
}
#endif
