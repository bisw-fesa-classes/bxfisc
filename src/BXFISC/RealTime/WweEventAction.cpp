// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#include <BXFISC/RealTime/WweEventAction.h>

#include <fesa-core/RealTime/RTEvent.h>

#include <BXFISC/GeneratedCode/ServiceLocator.h>

#include <cmw-log/Logger.h>
#include <iostream>

#include <fesa-core/RealTime/RTEvent.h>
#include <fesa-core/Synchronization/NoneContext.h>
#include <fesa-core/Exception/FesaException.h>
#include <fesa-core-cern/Synchronization/TimingContext.h>

#include <BXFISC/GeneratedCode/ServiceLocator.h>
#include <BXFISC/Common/Tools.h>
#include <BXFISC/Common/LowLevel.h>

//#include <BXSCINT/Common/UTILS.h>
#include <cmw-log/Logger.h>
#include <iostream>
#include <cstdlib>
#include <boost/lexical_cast.hpp>

#include "../../../../../../../../cs-ccr-nfsop/nfs1/vol30/local/L867/fesa/fesa-core/8.3.0/include/fesa-core/Core/FesaDefs.h"
using namespace BXFISC_TOOL;

namespace
{

CMW::Log::Logger &logger = CMW::Log::LoggerFactory::getLogger("FESA.USR.BXFISC.RealTime.WweEventAction");

} // namespace

#define LOG_DIAG_IF(topic,message)                           \
	{                                                    \
		DiagnosticUtils::DiagnosticMessage diagMsg;  \
		diagMsg.side = DiagnosticUtils::user;        \
		diagMsg.source = DiagnosticUtils::rt;        \
		diagMsg.fesaClass = "BXFISC"; \
		diagMsg.name = "WweEventAction"; \
		diagMsg.action = DiagnosticUtils::undefined; \
		diagMsg.msg = message;                       \
		BXFISCServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
	}

namespace BXFISC
{

WweEventAction::WweEventAction(fesa::RTActionConfig &rtActionConfig, const fesa::AbstractServiceLocator *serviceLocator,
        const std::map<std::string, const fesa::AbstractServiceLocator*> &serviceLocatorRelatedClasses) :
                WweEventActionBase(rtActionConfig, serviceLocator, serviceLocatorRelatedClasses)
{
}

WweEventAction::~WweEventAction()
{
}

void WweEventAction::execute(fesa::RTEvent *pEvt)
{
    const fesa::MultiplexingContext *pContext = pEvt->getMultiplexingContext();
    GlobalDevice *pGlobalStore = BXFISCServiceLocator_->getGlobalDevice();

    int state;
    int stateHw;

    const timestamp_ns acqStamp = pContext->getTimeStamp();
    const timestamp_ns cycleStamp = (acqStamp - pContext->getCycleStamp());

    pGlobalStore->acqStampWWE.set(acqStamp, pContext);
    pGlobalStore->cycleStampWWE.set(cycleStamp, pContext);
    for (std::vector<Device*>::iterator itr = deviceCol_.begin(); itr != deviceCol_.end(); ++itr)
    {
        try
        {
            Device *pDev = (*itr);

            pDev->acqStamp.set(acqStamp, pContext);
            state = pDev->deviceDetailedStatus.get(pContext);
            stateHw = pDev->deviceHwStatus.get(pContext);
            if (pDev->moduleHardwareAcquisitionWd.get(pContext) < 0)
            {
                pDev->moduleHardwareAcquisitionWd.set(100, pContext);
            }

            if ((int) (pDev->event.get(pContext)) != (int) EVENTS::WWE)
            {
                pDev->event.set(EVENTS::WWE, pContext);
                if (((int) stateHw & DEVICE_HW_STATUS::NO_ERROR_DRIVER_THERE)
                        == (int) DEVICE_HW_STATUS::NO_ERROR_DRIVER_THERE
                        && ((int) state & DEVICE_DETAILED_STATUS::MODULE_ON) == (int) DEVICE_DETAILED_STATUS::MODULE_ON
                        &&

                        ((int) stateHw & DEVICE_HW_STATUS::NO_ERROR_ACQUISITION_ON)
                                == (int) DEVICE_HW_STATUS::NO_ERROR_ACQUISITION_ON
                        && ((int) state & DEVICE_DETAILED_STATUS::TEST_LED_OFF)
                                == (int) DEVICE_DETAILED_STATUS::TEST_LED_OFF
                        && ((int) state & DEVICE_DETAILED_STATUS::TEST_INTERNAL_OFF)
                                == (int) DEVICE_DETAILED_STATUS::TEST_INTERNAL_OFF)
                {
                    if (pDev->moduleHardwareAcquisitionWd.get(pContext) > 1)
                    {
                        float position;
                        Tools::PositionGet(&position, pDev, pContext);
                        pDev->position.set(position, pContext);

                        pDev->scanStart_current.set(pDev->scanStart_next.get(pContext), pContext);
                        pDev->scanStop_current.set(pDev->scanStop_next.get(pContext), pContext);
                        pDev->scanDelay_current.set(pDev->scanDelay_next.get(pContext), pContext);


                        Tools::ScanSetUp(pDev->scanStart_current.get(pContext), pDev->scanStop_current.get(pContext),
                                pDev->scanDelay_current.get(pContext),
                                pGlobalStore->spillTime.get(pContext),
                                pDev->moduleHwRegMaster_setting.get(pContext),
                                pDev->moduleHwRegCoin_setting.get(pContext), pDev, pContext);
                    }
                }
            }
        }
        catch (fesa::FesaException &exception)
        {
            std::cout << __FILE__ << " " << __LINE__ << "EX- in the device loop :( " << exception.getMessage()
                    << std::endl;
        }
    }

}

} // BXFISC
