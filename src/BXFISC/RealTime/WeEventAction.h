// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#ifndef BXFISC_WeEventAction_H_
#define BXFISC_WeEventAction_H_



#include <BXFISC/GeneratedCode/GenRTActions.h>

#include <BXFISC/GeneratedCode/Device.h>


namespace BXFISC
{
class WeEventAction : public WeEventActionBase 
{
	public:

		WeEventAction (fesa::RTActionConfig& rtActionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses);

		virtual  ~WeEventAction();
		void execute(fesa::RTEvent* pEvt);
};
}
#endif
