// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#include <BXFISC/RealTime/WeEventAction.h>

#include <fesa-core/RealTime/RTEvent.h>
#include <fesa-core/RealTime/RTEvent.h>
#include <fesa-core/Synchronization/NoneContext.h>
#include <fesa-core/Exception/FesaException.h>
#include <fesa-core-cern/Synchronization/TimingContext.h>

#include <BXFISC/GeneratedCode/ServiceLocator.h>
#include <BXFISC/Common/Tools.h>

#include <cmw-log/Logger.h>
#include <iostream>

#include <iostream>
#include <cstdlib>
#include <boost/lexical_cast.hpp>
namespace
{

CMW::Log::Logger &logger = CMW::Log::LoggerFactory::getLogger("FESA.USR.BXFISC.RealTime.WeEventAction");

} // namespace

#define LOG_DIAG_IF(topic,message)                           \
	{                                                    \
		DiagnosticUtils::DiagnosticMessage diagMsg;  \
		diagMsg.side = DiagnosticUtils::user;        \
		diagMsg.source = DiagnosticUtils::rt;        \
		diagMsg.fesaClass = "BXFISC"; \
		diagMsg.name = "WeEventAction"; \
		diagMsg.action = DiagnosticUtils::undefined; \
		diagMsg.msg = message;                       \
		BXFISCServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
	}

namespace BXFISC
{

WeEventAction::WeEventAction(fesa::RTActionConfig &rtActionConfig, const fesa::AbstractServiceLocator *serviceLocator,
        const std::map<std::string, const fesa::AbstractServiceLocator*> &serviceLocatorRelatedClasses) :
                WeEventActionBase(rtActionConfig, serviceLocator, serviceLocatorRelatedClasses)
{
}

WeEventAction::~WeEventAction()
{
}

void WeEventAction::execute(fesa::RTEvent *pEvt)
{
    fesa::MultiplexingContext *pContext = pEvt->getMultiplexingContext();
    GlobalDevice *pGlobalStore = BXFISCServiceLocator_->getGlobalDevice();

    const timestamp_ns acqStamp = pContext->getTimeStamp();
    const timestamp_ns cycleStamp = (acqStamp - pContext->getCycleStamp());
    pGlobalStore->acqStampWE.set(acqStamp, pContext);
    pGlobalStore->cycleStampWE.set(cycleStamp, pContext);

    for (std::vector<Device*>::iterator itr = deviceCol_.begin(); itr != deviceCol_.end(); ++itr)
    {
        try
        {
            Device *pDev = (*itr);
            if ((int) (pDev->event.get(pContext)) != (int) EVENTS::WE)
            {
                pDev->event.set(EVENTS::WE, pContext);
            }

        }
        catch (fesa::FesaException &exception)
        {
            std::cout << __FILE__ << " " << __LINE__ << "EX- in the device loop :( " << exception.getMessage()
                    << std::endl;
        }
    }

}

} // BXFISC
