#ifndef BXFISC_CES_H_
#define BXFISC_CES_H_

#include <fesa-core/RealTime/AbstractEventSource.h>
#include <fesa-core/RealTime/RTEvent.h>
#include <fesa-core/Synchronization/MultiplexingContext.h>
#include <BXFISC/GeneratedCode/CustomEventSourceGen.h>


namespace BXFISC
{

class CES : public CESBase 
{
	private:

	friend class EventSourceFactory;
		CES (const fesa::AbstractServiceLocator* serviceLocator,
 			const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses);

		virtual  ~CES();

		void wait(boost::shared_ptr<fesa::RTEvent>& eventToFire);

		void connect(const boost::shared_ptr<fesa::EventElement>& eventElement);

};
}
#endif
