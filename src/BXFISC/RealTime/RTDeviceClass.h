#ifndef BXFISC_RT_DEVICE_CLASS_H_
#define BXFISC_RT_DEVICE_CLASS_H_

#include <BXFISC/GeneratedCode/RTDeviceClassGen.h>

namespace BXFISC
{

class RTDeviceClass: public RTDeviceClassGen
{
	public:
		static RTDeviceClass* getInstance();
		static void releaseInstance();
		void specificInit();
		void specificShutDown() override;
	private:
		RTDeviceClass();
		virtual ~RTDeviceClass();
		static RTDeviceClass* instance_;
};

} // BXFISC

#endif // BXFISC_RT_DEVICE_CLASS_H_
