
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#include <BXFISC/GeneratedCode/ServiceLocator.h>
#include <BXFISC/RealTime/RTReturnToStart.h>

#include <cmw-log/Logger.h>
#include <BXFISC/Common/LowLevelFisc.h>
#include <BXFISC/Common/LowLevelDriver.h>


namespace
{

cmw::log::Logger& logger = cmw::log::LoggerFactory::getLogger("FESA.USR.BXFISC.RealTime.RTReturnToStart");

} // namespace

#define LOG_DIAG_IF(topic,message) \
{ \
    DiagnosticsDefs::DiagnosticMessage diagMsg(DiagnosticsDefs::Side::user, DiagnosticsDefs::Source::rt); \
    diagMsg.fesaClass = "BXFISC"; \
    diagMsg.name = "RTReturnToStart"; \
    diagMsg.action = DiagnosticsDefs::Action::undefined; \
    diagMsg.msg = message; \
    BXFISCServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
}

#define LOG_DIAG_DEVICE_IF(topic,message,device) \
{ \
    if (device->isLoggable()) \
    { \
        LOG_DIAG_IF(topic,message); \
    } \
}

namespace BXFISC
{

RTReturnToStart::RTReturnToStart(fesa::RTActionConfig& rtActionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses) :
     RTReturnToStartBase(rtActionConfig, serviceLocator, serviceLocatorRelatedClasses) 
{
}

RTReturnToStart::~RTReturnToStart()
{
}

void RTReturnToStart::sendFilamentToStart(Device * dev, const MultiplexingContext * c) {
    const auto driver = LowLevelDriver::getDriver(dev->driverNumber.get(c));
    ::EaSetMOTORPREG(driver, 0); // GO!
}


// This method is called whenever an event triggers this action; this is where custom code must be written.
// A real time action operates on a device collection, this is why a loop which iterates over the device collection
// of this real time action is there.
void RTReturnToStart::execute(fesa::RTEvent* pEvt)
{
    const fesa::MultiplexingContext* c = pEvt->getMultiplexingContext();
    const Devices& devices = getFilteredDeviceCollection(pEvt);
    for (Devices::const_iterator it = devices.begin(); it != devices.end(); ++it)
    {
        try
        {
            Device* device = *it;
            sendFilamentToStart(device, c);
        }
        catch (const fesa::FesaException& exception)
        {
            LOG_ERROR_IF(logger, exception.getMessage());
        }
    }
}

} // BXFISC
