
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#ifndef _BXFISC_RTReturnToPark_H_
#define _BXFISC_RTReturnToPark_H_

#include <BXFISC/GeneratedCode/Device.h>
#include <BXFISC/GeneratedCode/GenRTActions.h>

namespace BXFISC
{

class RTReturnToPark : public RTReturnToParkBase
{
    void sendFilamentToPark(Device * d, const fesa::MultiplexingContext * c);
public:
    RTReturnToPark (fesa::RTActionConfig& rtActionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses);
    virtual ~RTReturnToPark();
    void execute(fesa::RTEvent* pEvt);
};

} // BXFISC

#endif // _BXFISC_RTReturnToPark_H_
