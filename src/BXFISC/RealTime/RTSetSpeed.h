
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#ifndef _BXFISC_RTSetSpeed_H_
#define _BXFISC_RTSetSpeed_H_

#include <BXFISC/GeneratedCode/Device.h>
#include <BXFISC/GeneratedCode/GenRTActions.h>

namespace BXFISC
{

class RTSetSpeed : public RTSetSpeedBase
{
public:
    RTSetSpeed (fesa::RTActionConfig& rtActionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses);
    virtual ~RTSetSpeed();
    void execute(fesa::RTEvent* pEvt);
};

} // BXFISC

#endif // _BXFISC_RTSetSpeed_H_
