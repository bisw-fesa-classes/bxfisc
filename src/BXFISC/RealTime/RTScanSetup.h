
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#ifndef _BXFISC_RTScanSetup_H_
#define _BXFISC_RTScanSetup_H_

#include <BXFISC/GeneratedCode/Device.h>
#include <BXFISC/GeneratedCode/GenRTActions.h>

namespace BXFISC
{

class RTScanSetup : public RTScanSetupBase
{
    int16_t convertPositionToDAC(const double value);
//    int16_t convertSpeedToDAC(const double speed);
    void setStartWindow(const double high, const double low, Device * dev, const MultiplexingContext * c);
    void setStopWindow(const double high, const double low, Device * dev, const MultiplexingContext * c);
    void setParkWindow(const double high, const double low,  Device * dev, const MultiplexingContext * c);

    void setStartPosition(const double position, Device * dev, const MultiplexingContext * c);
    void setStopPosition(const double position, Device * dev, const MultiplexingContext * c);
    void setParkPosition(const double position, Device * dev, const MultiplexingContext * c);

//    void setSpeed(const int16_t speedDAC, Device * dev, const MultiplexingContext * c);
    void setDirection(const int32_t direction, Device * dev, const MultiplexingContext * c);

    void setupDelay(Device * dev, const MultiplexingContext * c);

    void setupBaseRegister(Device * dev, const MultiplexingContext * c);
public:
    RTScanSetup (fesa::RTActionConfig& rtActionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses);
    virtual ~RTScanSetup();
    void execute(fesa::RTEvent* pEvt);
};

} // BXFISC

#endif // _BXFISC_RTScanSetup_H_
