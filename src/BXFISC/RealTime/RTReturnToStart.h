
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#ifndef _BXFISC_RTReturnToStart_H_
#define _BXFISC_RTReturnToStart_H_

#include <BXFISC/GeneratedCode/Device.h>
#include <BXFISC/GeneratedCode/GenRTActions.h>

namespace BXFISC
{

class RTReturnToStart : public RTReturnToStartBase
{
    void sendFilamentToStart(Device * dev, const MultiplexingContext * c);
public:
    RTReturnToStart (fesa::RTActionConfig& rtActionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses);
    virtual ~RTReturnToStart();
    void execute(fesa::RTEvent* pEvt);
};

} // BXFISC

#endif // _BXFISC_RTReturnToStart_H_
