// FESA 3 framework // Generated from SampleServerAction_Cpp.xsl

#include <BXFISC/Server/GetAcquisition.h>

#include <BXFISC/GeneratedCode/ServiceLocator.h>

#include <cmw-log/Logger.h>

namespace 
{

CMW::Log::Logger& logger = CMW::Log::LoggerFactory::getLogger("FESA.USR.BXFISC.Server.GetAcquisition");

} // namespace

#define LOG_DIAG_IF(topic,message)                           \
	{                                                    \
		DiagnosticUtils::DiagnosticMessage diagMsg;  \
		diagMsg.side = DiagnosticUtils::user;        \
		diagMsg.source = DiagnosticUtils::server;    \
		diagMsg.fesaClass = "BXFISC"; \
		diagMsg.name = "GetAcquisition"; \
		diagMsg.action = DiagnosticUtils::undefined; \
		diagMsg.msg = message;                       \
		BXFISCServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
	}

namespace BXFISC
{

GetAcquisition::GetAcquisition (fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses):
		GetAcquisitionBase(actionConfig, serviceLocator, serviceLocatorRelatedClasses)
{
}

GetAcquisition::~GetAcquisition()
{
}

/*!
 * \brief This method is executed consecutively to a GET/SET action:
 * \param pEvt requestEvent from which the multiplexingContext can be extracted
 * \param pDev points to the Device or Global instance involved by this request 
 * \param data composite structure that agregates one or several types data-entries corresponding to the property definition.
 * This data object is the input (resp. output) which is passed when invoking a Get (resp. Set). 
 * \param filter optional input parameter meant to fine tune the treatment. It appears only if the property defines a filter
 */
void GetAcquisition::execute(fesa::RequestEvent* pEvt, Device* pDev, AcquisitionPropertyData& data)
{
    uint32_t size1=0;

    uint32_t fifoSize=BXFISC::MAX_FIFO_SIZE;

    const GlobalDevice * pGlobalStore = BXFISCServiceLocator_->getGlobalDevice();
    const fesa::MultiplexingContext * context = pEvt->getMultiplexingContext();

    data.acqStamp.set(pDev->acqStamp.get(context));
    data.propType.set(BDI_PROPERTY::ACQUISITION);
    data.deviceName.set(pDev->name.get());
    data.cycleName.set(pDev->cycleName.get(context));
    data.position.set(pDev->position.get(context));
    data.scanStartPosition.set(pDev->scanStart_last.get(context));
    data.scanStopPosition.set(pDev->scanStop_last.get(context));
    data.scanExecuteOn.set(pDev->scanExecute.get(context));
    data.scanMotorDelay.set(pDev->scanDelay_last.get(context));

    if (pDev->measStamp.get(size1, context))
    {
        data.measStamp.set(pDev->measStamp.get(size1, context), size1);
    }

    data.totalCharge.set(pDev->totalCharge.get(size1, context), size1);
    data.nbOfIntMeas.set(pDev->nbOfIntMeas.get(context));
    data.observables.set((enum BDI_OBSERVABLES::BDI_OBSERVABLES) pGlobalStore->observables.get());


    data.beamID.set(pDev->beamID.get(context));
    data.acqDesc.set(pDev->acqDesc.get(context));
    data.superCycleNb.set(pDev->superCycleNb.get(context));
    data.cycleTime.set(pDev->cycleTime.get(context));
    data.acqTime.set(pGlobalStore->acqTime.get(context));
    data.acqLength.set(pDev->acqLength.get(context));
    data.cycleStamp.set(pDev->cycleStamp.get(context));
    data.acqStamp.set(pDev->acqStamp.get(context));
    data.acqMsg.set(pDev->acqMsg.get(context));
    data.acqState.set((AQN_STATUS::AQN_STATUS)pDev->acqState.get(context));
    data.counts.set(pDev->counts.get(context));
    data.countNumber.set(pDev->countNumber.get(context));
    data.countsPm1.set(pDev->countsPm1.get(context));
    data.countsPm2.set(pDev->countsPm2.get(context));
    data.countsExtCoincidence.set(pDev->countsExtCoincidence.get(context));
    data.coincidenceDeviceName.set(pDev->coincidenceDeviceName.get(context));
    data.countArray.set(pDev->countArray.get(fifoSize, context),fifoSize);
    data.countPm1Array.set(pDev->countPm1Array.get(fifoSize, context),fifoSize);
    data.countPm2Array.set(pDev->countPm2Array.get(fifoSize, context),fifoSize);
    data.countExtCoincidenceArray.set(pDev->countExtCoincidenceArray.get(fifoSize, context),fifoSize);
    data.positionArray.set(pDev->motorPositionArray.get(fifoSize, context),fifoSize);
    data.deviceDetailedStatus.set((DEVICE_DETAILED_STATUS::DEVICE_DETAILED_STATUS)pDev->deviceDetailedStatus.get(context));

    data.equipmentName.set(pDev->equipmentName.get(context));
    data.event.set((EVENTS::EVENTS)pDev->event.get(context));
    data.delayPm1.set(pDev->delayPm1.get(context));
    data.delayPm2.set(pDev->delayPm2.get(context));
    data.delayPm1_default.set(pDev->delayPm1_default.get(context));
    data.delayPm2_default.set(pDev->delayPm2_default.get(context));
    data.highVoltagePm1.set(pDev->highVoltagePm1.get(context));
    data.highVoltagePm2.set(pDev->highVoltagePm2.get(context));
    data.highVoltagePm1_default.set(pDev->highVoltagePm1_default.get(context));
    data.highVoltagePm2_default.set(pDev->highVoltagePm2_default.get(context));
    data.moduleHardwareMonitorWd.set(pDev->moduleHardwareMonitorWd.get(context));
    data.moduleHardwareAcquisitionWd.set(pDev->moduleHardwareAcquisitionWd.get(context));
    const short* ptr;
    uint32_t size = MAX_NB_OF_ACCESS_TYPES  ;
    ptr=pDev->moduleWdReads.get(size1, context);
    int16_t array[MAX_NB_OF_ACCESS_TYPES]; for ( unsigned int i=0 ; i<  MAX_NB_OF_ACCESS_TYPES ; i++ ) { array[i]=ptr[i]  ; }
    array[HARDWARE_ACCESS::ACQ]++;
    pDev->moduleWdReads.set(array, size, context);
}

// Use this member function to tell the framework whether the subscribed clients need to be updated or not. See the FESA wiki for more info
bool GetAcquisition::hasDataChanged(const fesa::RequestEvent& event, fesa::AbstractDevice& abstractDevice, const cmw::data::Data& filter) const
{
	Device& device = static_cast<Device&>(abstractDevice);
	return true;
}

} // BXFISC
