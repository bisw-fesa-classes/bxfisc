// FESA 3 framework // Generated from SampleServerAction_Cpp.xsl

#include <BXFISC/Server/GetSetting.h>

#include <BXFISC/GeneratedCode/ServiceLocator.h>

#include <cmw-log/Logger.h>

#include <BXFISC/Common/Tools.h>

namespace 
{

CMW::Log::Logger& logger = CMW::Log::LoggerFactory::getLogger("FESA.USR.BXFISC.Server.GetSetting");

} // namespace

#define LOG_DIAG_IF(topic,message)                           \
	{                                                    \
		DiagnosticUtils::DiagnosticMessage diagMsg;  \
		diagMsg.side = DiagnosticUtils::user;        \
		diagMsg.source = DiagnosticUtils::server;    \
		diagMsg.fesaClass = "BXFISC"; \
		diagMsg.name = "GetSetting"; \
		diagMsg.action = DiagnosticUtils::undefined; \
		diagMsg.msg = message;                       \
		BXFISCServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
	}

namespace BXFISC
{

GetSetting::GetSetting (fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses):
		GetSettingBase(actionConfig, serviceLocator, serviceLocatorRelatedClasses)
{
}

GetSetting::~GetSetting()
{
}

/*!
 * \brief This method is executed consecutively to a GET/SET action:
 * \param pEvt requestEvent from which the multiplexingContext can be extracted
 * \param pDev points to the Device or Global instance involved by this request 
 * \param data composite structure that agregates one or several types data-entries corresponding to the property definition.
 * This data object is the input (resp. output) which is passed when invoking a Get (resp. Set). 
 * \param filter optional input parameter meant to fine tune the treatment. It appears only if the property defines a filter
 */
void GetSetting::execute(fesa::RequestEvent* pEvt, Device* pDev, SettingPropertyData& data)
{
    using namespace BXFISC_TOOL;
    MultiplexingContext* pContext  = pEvt->getMultiplexingContext();

    data.deviceDetailedStatus.set((DEVICE_DETAILED_STATUS::DEVICE_DETAILED_STATUS)pDev->deviceDetailedStatus.get(pContext));
    data.mode.set((DEVICE_MODE_SETTING::DEVICE_MODE_SETTING)Tools::DeviceMode(pDev,pContext));
    data.moduleHardwareMonitorWd.set(pDev->moduleHardwareMonitorWd.get(pContext));
    data.moduleHardwareAcquisitionWd.set(pDev->moduleHardwareAcquisitionWd.get(pContext));
    data.highVoltageRestore.set(YES_NO::NO);
    data.delayRestore.set(YES_NO::NO);
    int64_t acqStamp = pContext->getTimeStamp();
    acqStamp=(acqStamp/TIMING_ACQSTAMP_TO_NS);

    data.nbOfIntSummary.set(pDev->nbOfIntSummary.get(pContext));
    data.position.set(pDev->position.get(pContext));
    data.position_max.set(pDev->motorPosition_max.get(pContext));
    data.position_min.set(pDev->motorPosition_min.get(pContext));

    data.scanStartPosition.set(pDev->scanStart_next.get(pContext));
    data.scanStopPosition.set(pDev->scanStop_next.get(pContext));
    data.scanMotorDelay.set(pDev->scanDelay_next.get(pContext));
    const int16_t * ptr;
    uint32_t size;
    size = MAX_NB_OF_ACCESS_TYPES  ;
    ptr=pDev->moduleWdReads.get(size, pContext);
    short array[MAX_NB_OF_ACCESS_TYPES]; for ( unsigned int i=0 ; i<  MAX_NB_OF_ACCESS_TYPES ; i++ ) { array[i]=ptr[i]  ; }
    array[HARDWARE_ACCESS::SETTING]++;
    pDev->moduleWdReads.set(array,size, pContext);
}

// Use this member function to tell the framework whether the subscribed clients need to be updated or not. See the FESA wiki for more info
bool GetSetting::hasDataChanged(const fesa::RequestEvent& event, fesa::AbstractDevice& abstractDevice, const cmw::data::Data& filter) const
{
	Device& device = static_cast<Device&>(abstractDevice);
	return true;
}

} // BXFISC
