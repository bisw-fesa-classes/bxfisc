// FESA 3 framework // Generated from SampleServerAction_Hpp.xsl 
// Use this code as a starting point to develop your own equipment class 

#ifndef BXFISC_SetEaPorts_H_
#define BXFISC_SetEaPorts_H_

#include <BXFISC/GeneratedCode/GetSetDefaultServerAction.h>
#include <BXFISC/GeneratedCode/PropertyData.h>
#include <BXFISC/GeneratedCode/TypeDefinition.h>
#include <BXFISC/GeneratedCode/Device.h>

namespace BXFISC
{

class SetEaPorts : public SetEaPortsBase
{
public:
	SetEaPorts(fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses);
	virtual ~SetEaPorts();
	void execute(fesa::RequestEvent* pEvt, Device* pDev, EaPortsRegsPropertyData& data);
};

} // BXFISC

#endif // BXFISC_SetEaPorts_H_
