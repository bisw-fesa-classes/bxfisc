// FESA 3 framework // Generated from SampleServerAction_Cpp.xsl

#include <BXFISC/Server/GetEaPiquet.h>

#include <BXFISC/GeneratedCode/ServiceLocator.h>

#include <cmw-log/Logger.h>

#include <BXFISC/Common/Tools.h>

namespace 
{

CMW::Log::Logger& logger = CMW::Log::LoggerFactory::getLogger("FESA.USR.BXFISC.Server.GetEaPiquet");

} // namespace

#define LOG_DIAG_IF(topic,message)                           \
	{                                                    \
		DiagnosticUtils::DiagnosticMessage diagMsg;  \
		diagMsg.side = DiagnosticUtils::user;        \
		diagMsg.source = DiagnosticUtils::server;    \
		diagMsg.fesaClass = "BXFISC"; \
		diagMsg.name = "GetEaPiquet"; \
		diagMsg.action = DiagnosticUtils::undefined; \
		diagMsg.msg = message;                       \
		BXFISCServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
	}

namespace BXFISC
{

GetEaPiquet::GetEaPiquet (fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses):
		GetEaPiquetBase(actionConfig, serviceLocator, serviceLocatorRelatedClasses)
{
}

GetEaPiquet::~GetEaPiquet()
{
}

/*!
 * \brief This method is executed consecutively to a GET/SET action:
 * \param pEvt requestEvent from which the multiplexingContext can be extracted
 * \param pDev points to the Device or Global instance involved by this request 
 * \param data composite structure that agregates one or several types data-entries corresponding to the property definition.
 * This data object is the input (resp. output) which is passed when invoking a Get (resp. Set). 
 * \param filter optional input parameter meant to fine tune the treatment. It appears only if the property defines a filter
 */
void GetEaPiquet::execute(fesa::RequestEvent* pEvt, Device* pDev, EaPiquetPropertyData& data)
{
    using namespace BXFISC_TOOL;
    const MultiplexingContext* pContext= pEvt->getMultiplexingContext();
    const GlobalDevice * pGlobalStore = BXFISCServiceLocator_->getGlobalDevice();

    data.moduleName.set(pDev->moduleName.get(pContext));
    data.acqTime.set(pGlobalStore->acqTime.get(pContext));
    data.acqStampWE.set(pGlobalStore->acqStampWE.get(pContext));
    data.acqStampWWE.set(pGlobalStore->acqStampWWE.get(pContext));
    data.acqStampEE.set(pGlobalStore->acqStampEE.get(pContext));
    data.cycleStampWE.set(pGlobalStore->cycleStampWE.get(pContext));
    data.cycleStampWWE.set(pGlobalStore->cycleStampWWE.get(pContext));
    data.cycleStampEE.set(pGlobalStore->cycleStampEE.get(pContext));
    char upTime[NAME_SIZE];
    int64_t uptime=(pGlobalStore->acqStampMonitor.get(pContext)-pGlobalStore->acqStampStartUp.get(pContext))/TIMING_ACQSTAMP_TO_SECONDS;

    sprintf(upTime,"%03dd %02dh %02dm",(int)uptime/(24*60*60),(int)(uptime/(60*60))%24,(int)(uptime/60)%60);
    uint32_t size = NAME_SIZE  ;
    data.acqUpTime.set(upTime);
    data.action.set(DEVICE_STATE_SETTING::NONE);
    data.delayPm1Action.set(YES_NO::NO);
    data.delayPm2Action.set(YES_NO::NO);
    data.highVoltagePm1Action.set(YES_NO::NO);
    data.highVoltagePm2Action.set(YES_NO::NO);
    data.motorPositionAction.set(YES_NO::NO);
    data.scanAction.set(YES_NO::NO);
    data.returnToStart.set(YES_NO::NO);

    if (pDev->equipmentName.get(pContext))
    {
        data.equipmentName.set(pDev->equipmentName.get(pContext));
    }

    data.eaMsg.set(pDev->eaMsg.get(pContext));

    data.deviceDetailedStatus.set((DEVICE_DETAILED_STATUS::DEVICE_DETAILED_STATUS)pDev->deviceDetailedStatus.get(pContext));
    data.deviceHwStatus.set((DEVICE_HW_STATUS::DEVICE_HW_STATUS)pDev->deviceHwStatus.get(pContext));
    data.devicePortRegError.set((REGERRORS::REGERRORS)pDev->devicePortRegError.get(pContext));

    data.countNumber.set(pDev->countNumber.get(pContext));
    uint32_t fifoSize=BXFISC::MAX_FIFO_SIZE;
    data.countPm1Array.set(pDev->countPm1Array.get(fifoSize,pContext),fifoSize);
    data.countPm2Array.set(pDev->countPm2Array.get(fifoSize,pContext),fifoSize);
    data.countArray.set(pDev->countArray.get(fifoSize,pContext),fifoSize);
    data.countExtCoincidenceArray.set(pDev->countExtCoincidenceArray.get(fifoSize,pContext),fifoSize);
    data.countsPm1.set(pDev->countsPm1.get(pContext));
    data.countsPm2.set(pDev->countsPm2.get(pContext));
    data.counts.set(pDev->counts.get(pContext));
    data.countsExtCoincidence.set(pDev->countsExtCoincidence.get(pContext));
    data.counterSampling.set(pDev->counterSampling.get(pContext));
    data.coincidenceDeviceName.set(pDev->coincidenceDeviceName.get(pContext));
    data.event.set((EVENTS::EVENTS)pDev->event.get(pContext));

    auto moduleWdReads = pDev->moduleWdReads.get(size, pContext);
    data.moduleWdReads.set(moduleWdReads, size);

    data.highVoltagePm1.set(pDev->highVoltagePm1.get(pContext));
    data.highVoltagePm1_setting.set(pDev->highVoltagePm1_setting.get(pContext));
    data.highVoltagePm1_default.set(pDev->highVoltagePm1_default.get(pContext));
    data.highVoltagePm1_min.set(pDev->highVoltagePm1_min.get(pContext));
    data.highVoltagePm1_current.set(pDev->highVoltagePm1_current.get(pContext));
    data.highVoltagePm1_offset.set(pDev->highVoltagePm1_offset.get(pContext));
    data.highVoltagePm1_offsetNumber.set(pDev->highVoltagePm1_offsetNumber.get(pContext));
    data.highVoltagePm2.set(pDev->highVoltagePm2.get(pContext));
    data.highVoltagePm2_setting.set(pDev->highVoltagePm2_setting.get(pContext));
    data.highVoltagePm2_default.set(pDev->highVoltagePm2_default.get(pContext));
    data.highVoltagePm2_min.set(pDev->highVoltagePm2_min.get(pContext));
    data.highVoltagePm2_current.set(pDev->highVoltagePm2_current.get(pContext));
    data.highVoltagePm2_offset.set(pDev->highVoltagePm2_offset.get(pContext));
    data.highVoltagePm2_offsetNumber.set(pDev->highVoltagePm2_offsetNumber.get(pContext));

    double motor[6];
    motor[0]=pDev->position.get(pContext);
    motor[1]=pDev->position_setting.get(pContext);
    motor[4]=pDev->motorPosition_max.get(pContext);
    motor[5]=pDev->motorPosition_min.get(pContext);
    size=NUMBER_OF_GAINS;
    //double gain[NUMBER_OF_GAINS];
    const double* gain;
    gain=pDev->motorPosition_gain.get(size,pContext);
    motor[2]=gain[0];
    motor[3]=gain[1];
    data.motorArray.set(motor,6);
    data.motorPosition.set(pDev->position.get(pContext));
    data.motorPosition_setting.set(pDev->position_setting.get(pContext));

    data.motorPositionArray.set(pDev->motorPositionArray.get(fifoSize,pContext),fifoSize);
    data.scanStart_next.set(pDev->scanStart_next.get(pContext));
    data.scanStop_next.set(pDev->scanStop_next.get(pContext));
    data.scanDelay_next.set(pDev->scanDelay_next.get(pContext));
//  data.scanStart_current.set(pDev->scanStart_current.get(pContext));
//  data.scanStop_current.set(pDev->scanStop_current.get(pContext));
//  data.scanDelay_current.set(pDev->scanDelay_current.get(pContext));
//  data.scanStart_last.set(pDev->scanStart_last.get(pContext));
//  data.scanStop_last.set(pDev->scanStop_last.get(pContext));
//  data.scanDelay_last.set(pDev->scanDelay_last.get(pContext));
    data.moduleThresholdPm1.set(pDev->moduleThresholdPm1.get(pContext));
    data.moduleThresholdPm1_setting.set(pDev->moduleThresholdPm1_setting.get(pContext));
    data.moduleThresholdPm1_default.set(pDev->moduleThresholdPm1_default.get(pContext));
    data.moduleThresholdPm2.set(pDev->moduleThresholdPm2.get(pContext));
    data.moduleThresholdPm2_setting.set(pDev->moduleThresholdPm2_setting.get(pContext));
    data.moduleThresholdPm2_default.set(pDev->moduleThresholdPm2_default.get(pContext));
    data.msgMonitor.set(pDev->msgMonitor.get(pContext));
    data.msgCommand.set(pDev->msgCommand.get(pContext));
    data.moduleHardwareMonitorWd.set(pDev->moduleHardwareMonitorWd.get(pContext));
    data.moduleHardwareAcquisitionWd.set(pDev->moduleHardwareAcquisitionWd.get(pContext));
    data.deviceName.set(pDev->name.get());
//  REGBASE::REGBASE baseReg ;
//  Tools::BaseRegGet(&baseReg,pDev, pContext);
//  data.moduleBaseReg.set(baseReg);
    size = 6  ;
    short unsigned int ports[6];
    const short int *ptr;
    ptr=(const short int*)ports;
    ports[WHICH_PORT_REG::A]=pDev->moduleHwPortA.get(pContext);
    ports[WHICH_PORT_REG::B]=pDev->moduleHwPortB.get(pContext);
    ports[WHICH_PORT_REG::C]=pDev->moduleHwPortC.get(pContext);
    ports[WHICH_PORT_REG::BASE]=pDev->moduleHwRegBase.get(pContext);
    ports[WHICH_PORT_REG::COIN]=pDev->moduleHwRegCoin.get(pContext);
    ports[WHICH_PORT_REG::MOTOR]=pDev->moduleHwRegMotor.get(pContext);
    data.portsRegs.set(ptr,size);

    moduleWdReads = pDev->moduleWdReads.get(size, pContext);
    data.moduleWdReads.set(moduleWdReads, size);

    auto moduleWdWrites = pDev->moduleWdWrites.get(size, pContext);
    data.moduleWdWrites.set(moduleWdWrites, size);
//  data.moduleSoftwareServerStartupTime.set(pDev->pGlobalStore->moduleSoftwareServerStartupTime.get());
    //const short* ptr;
    size = MAX_NB_OF_ACCESS_TYPES  ;
    ptr=pDev->moduleWdReads.get(size, pContext);
    short array[MAX_NB_OF_ACCESS_TYPES]; for ( unsigned int i=0 ; i<  MAX_NB_OF_ACCESS_TYPES ; i++ ) { array[i]=ptr[i]  ; }
    array[HARDWARE_ACCESS::EAPIQUET]++;
    pDev->moduleWdReads.set(array,size, pContext);
}

// Use this member function to tell the framework whether the subscribed clients need to be updated or not. See the FESA wiki for more info
bool GetEaPiquet::hasDataChanged(const fesa::RequestEvent& event, fesa::AbstractDevice& abstractDevice, const cmw::data::Data& filter) const
{
	Device& device = static_cast<Device&>(abstractDevice);
	return true;
}

} // BXFISC
