// FESA 3 framework // Generated from SampleServerAction_Cpp.xsl

#include <BXFISC/Server/SetPha.h>

#include <BXFISC/GeneratedCode/ServiceLocator.h>

#include <cmw-log/Logger.h>

#include <BXFISC/Common/Tools.h>

namespace 
{

CMW::Log::Logger& logger = CMW::Log::LoggerFactory::getLogger("FESA.USR.BXFISC.Server.SetPha");

} // namespace

#define LOG_DIAG_IF(topic,message)                           \
	{                                                    \
		DiagnosticUtils::DiagnosticMessage diagMsg;  \
		diagMsg.side = DiagnosticUtils::user;        \
		diagMsg.source = DiagnosticUtils::server;    \
		diagMsg.fesaClass = "BXFISC"; \
		diagMsg.name = "SetPha"; \
		diagMsg.action = DiagnosticUtils::undefined; \
		diagMsg.msg = message;                       \
		BXFISCServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
	}

namespace BXFISC
{

SetPha::SetPha (fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses):
		SetPhaBase(actionConfig, serviceLocator, serviceLocatorRelatedClasses)
{
}

SetPha::~SetPha()
{
}

/*!
 * \brief This method is executed consecutively to a GET/SET action:
 * \param pEvt requestEvent from which the multiplexingContext can be extracted
 * \param pDev points to the Device or Global instance involved by this request 
 * \param data composite structure that agregates one or several types data-entries corresponding to the property definition.
 * This data object is the input (resp. output) which is passed when invoking a Get (resp. Set). 
 * \param filter optional input parameter meant to fine tune the treatment. It appears only if the property defines a filter
 */
void SetPha::execute(fesa::RequestEvent* pEvt, Device* pDev, PhaPropertyData& data)
{

    using namespace BXFISC_TOOL;
    const MultiplexingContext* pContext = pEvt->getMultiplexingContext();
    if( data.itemAvailable("phaIs") )
    {
        if ( data.phaIs.get()==YES_NO::YES ) { pDev->phaIs.set(YES_NO::YES, pContext); }
        if ( data.phaIs.get()==YES_NO::NO )  { pDev->phaIs.set(YES_NO::NO, pContext); }
    }
    if ( data.phaIs.get()==YES_NO::YES )
    {
        if( data.itemAvailable("phaGain") )
        {
            LOG_INFO_FORMAT_IF(logger, "Arek phaGain set");
            Tools::EmcGainSet(pDev,pContext,data.phaGain.get() );
        }
        if ( data.itemAvailable("phaReg") )
        {
//          Tools
            Tools::EmcPhaRegSet(pDev,pContext,(EMCREG::EMCREG)data.phaReg.get());
            pDev->phaReg.set(data.phaReg.get(), pContext);
        }
            if ( data.itemAvailable("phaDacAction") && data.itemAvailable("phaDacNewValue") )
    {
        if ( data.phaDacAction.get()!=EMCDACS::NONE )
        {
            Tools::EmcPhaDacSet(pDev,pContext,(EMCDACS::EMCDACS)data.phaDacAction.get(), data.phaDacNewValue.get());
        }
    }

    }

}

} // BXFISC
