
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#ifndef _BXFISC_ResetCounts_H_
#define _BXFISC_ResetCounts_H_

#include <BXFISC/GeneratedCode/GetSetDefaultServerAction.h>
#include <BXFISC/GeneratedCode/PropertyData.h>
#include <BXFISC/GeneratedCode/TypeDefinition.h>
#include <BXFISC/GeneratedCode/Device.h>

namespace BXFISC
{

class ResetCounts : public ResetCountsBase
{
public:
    ResetCounts(fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses);
    virtual ~ResetCounts();
    void execute(fesa::RequestEvent* pEvt, Device* pDev, const ResetCountsPropertyData& data, const ResetCountsFilterData& filter);
};

} // BXFISC

#endif // _BXFISC_ResetCounts_H_
