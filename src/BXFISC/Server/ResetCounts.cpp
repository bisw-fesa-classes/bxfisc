
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#include <BXFISC/Server/ResetCounts.h>
#include <BXFISC/GeneratedCode/ServiceLocator.h>

#include <cmw-log/Logger.h>

namespace 
{

cmw::log::Logger& logger = cmw::log::LoggerFactory::getLogger("FESA.USR.BXFISC.Server.ResetCounts");

} // namespace

#define LOG_DIAG_IF(topic,message) \
{ \
    DiagnosticsDefs::DiagnosticMessage diagMsg(DiagnosticsDefs::Side::user, DiagnosticsDefs::Source::rt); \
    diagMsg.fesaClass = "BXFISC"; \
    diagMsg.name = "ResetCounts"; \
    diagMsg.action = DiagnosticsDefs::Action::undefined; \
    diagMsg.msg = message; \
    BXFISCServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
}

#define LOG_DIAG_DEVICE_IF(topic,message,device) \
{ \
    if (device->isLoggable()) \
    { \
        LOG_DIAG_IF(topic,message); \
    } \
}

namespace BXFISC
{

ResetCounts::ResetCounts(fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses):
        ResetCountsBase(actionConfig, serviceLocator, serviceLocatorRelatedClasses)
{
}

ResetCounts::~ResetCounts()
{
}

/*!
 * \brief This method is executed consecutively to a GET/SET action:
 * \param pEvt requestEvent from which the multiplexingContext can be extracted
 * \param pDev points to the Device or Global instance involved by this request 
 * \param data composite structure that aggregates one or several types data-entries corresponding to the property definition.
 * This data object is the input (resp. output) which is passed when invoking a Get (resp. Set). 
 * \param filter input parameter meant to fine tune the treatment.
 */
void ResetCounts::execute(fesa::RequestEvent* pEvt, Device* pDev, const ResetCountsPropertyData& data, const ResetCountsFilterData& filter)
{
    pDev->doIpcReset.set(true, pEvt->getMultiplexingContext());
}

} // BXFISC
