// FESA 3 framework // Generated from SampleServerAction_Cpp.xsl

#include <BXFISC/Server/GetEaDac.h>

#include <BXFISC/GeneratedCode/ServiceLocator.h>

#include <cmw-log/Logger.h>
#include <BXFISC/Common/Tools.h>

namespace 
{

CMW::Log::Logger& logger = CMW::Log::LoggerFactory::getLogger("FESA.USR.BXFISC.Server.GetEaDac");

} // namespace

#define LOG_DIAG_IF(topic,message)                           \
	{                                                    \
		DiagnosticUtils::DiagnosticMessage diagMsg;  \
		diagMsg.side = DiagnosticUtils::user;        \
		diagMsg.source = DiagnosticUtils::server;    \
		diagMsg.fesaClass = "BXFISC"; \
		diagMsg.name = "GetEaDac"; \
		diagMsg.action = DiagnosticUtils::undefined; \
		diagMsg.msg = message;                       \
		BXFISCServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
	}

namespace BXFISC
{

GetEaDac::GetEaDac (fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses):
		GetEaDacBase(actionConfig, serviceLocator, serviceLocatorRelatedClasses)
{
}

GetEaDac::~GetEaDac()
{
}

/*!
 * \brief This method is executed consecutively to a GET/SET action:
 * \param pEvt requestEvent from which the multiplexingContext can be extracted
 * \param pDev points to the Device or Global instance involved by this request 
 * \param data composite structure that agregates one or several types data-entries corresponding to the property definition.
 * This data object is the input (resp. output) which is passed when invoking a Get (resp. Set). 
 * \param filter optional input parameter meant to fine tune the treatment. It appears only if the property defines a filter
 */
void GetEaDac::execute(fesa::RequestEvent* pEvt, Device* pDev, EaDacPropertyData& data)
{
    using namespace BXFISC_TOOL;
    const MultiplexingContext * pContext = pEvt->getMultiplexingContext();
    const GlobalDevice * pGlobalStore = BXFISCServiceLocator_->getGlobalDevice();
    float dacFloats[CHANNELS_DAC_NUMBER];
    uint16_t dacShorts[CHANNELS_DAC_NUMBER];

    char dacChars[HEX_SIZE];
//  char *dacChars;
//  dacChars=dacCharsMemory;
    struct timeval tAcq; struct timezone zAcq ;
    int64_t acqStamp;
    int64_t factor =(int64_t)(1.0 /pGlobalStore->measStamp_unitFactor.get());;
    gettimeofday(&tAcq,&zAcq);
    acqStamp=(int64_t)tAcq.tv_sec*1000000L+(int64_t)tAcq.tv_usec;
    acqStamp=(acqStamp*factor)/1000000000L;
    //pDev->acqStampMonitor.set(acqStamp,pContext);
    data.acqStamp.set(acqStamp);
    Tools::DacsGet(dacShorts,dacFloats,pDev,pContext);
    data.hv1.set(dacFloats[HARDWARE_DAC_CHANNELS07::HV1_0]);
    data.hv2.set(dacFloats[HARDWARE_DAC_CHANNELS07::HV2_1]);
    data.thres1.set(  dacFloats[HARDWARE_DAC_CHANNELS07::THRES1_2]);
    data.thres2.set(  dacFloats[HARDWARE_DAC_CHANNELS07::THRES2_3]);
    data.ledPed.set(  dacFloats[HARDWARE_DAC_CHANNELS07::LED_PED_4]);
    data.ledPulse.set(dacFloats[HARDWARE_DAC_CHANNELS07::LED_PULSE_EXT_5]);
    data.ref5vp.set(  dacFloats[HARDWARE_DAC_CHANNELS07::REF_5VP_6]);
    data.ref5vn.set(  dacFloats[HARDWARE_DAC_CHANNELS07::REF_5VN_7]);
    Tools::DacConvertToHexChars(dacChars,dacShorts[HARDWARE_DAC_CHANNELS07::HV1_0]);
    data.hv1Hex.set(  dacChars);
    Tools::DacConvertToHexChars(dacChars,dacShorts[HARDWARE_DAC_CHANNELS07::HV2_1]);
    data.hv2Hex.set(dacChars);
    Tools::DacConvertToHexChars(dacChars,dacShorts[HARDWARE_DAC_CHANNELS07::THRES1_2]);
    data.thres1Hex.set(dacChars);
    Tools::DacConvertToHexChars(dacChars,dacShorts[HARDWARE_DAC_CHANNELS07::THRES2_3]);
    data.thres2Hex.set(dacChars);
    Tools::DacConvertToHexChars(dacChars,dacShorts[HARDWARE_DAC_CHANNELS07::LED_PED_4]);
    data.ledPedHex.set(dacChars);
    Tools::DacConvertToHexChars(dacChars,dacShorts[HARDWARE_DAC_CHANNELS07::LED_PULSE_EXT_5]);
    data.ledPulseHex.set(dacChars);
    Tools::DacConvertToHexChars(dacChars,dacShorts[HARDWARE_DAC_CHANNELS07::REF_5VP_6]);
    data.ref5vpHex.set(dacChars);
    Tools::DacConvertToHexChars(dacChars,dacShorts[HARDWARE_DAC_CHANNELS07::REF_5VN_7]);
    data.ref5vnHex.set(dacChars);

    Tools::DacsMpcGet(dacShorts,dacFloats,pDev,pContext);
    data.start_max.set(     dacFloats[HARDWARE_DAC_MPC_CHANNELS07::START_MAX_0]);
    data.start_min.set(     dacFloats[HARDWARE_DAC_MPC_CHANNELS07::START_MIN_1]);
    data.stop_max.set(      dacFloats[HARDWARE_DAC_MPC_CHANNELS07::STOP_MAX_2]);
    data.stop_min.set(      dacFloats[HARDWARE_DAC_MPC_CHANNELS07::STOP_MIN_3]);
    data.garage_max.set(    dacFloats[HARDWARE_DAC_MPC_CHANNELS07::GARAGE_MAX_4]);
    data.garage_min.set(    dacFloats[HARDWARE_DAC_MPC_CHANNELS07::GARAGE_MIN_5]);
    data.motorSpeed.set(    dacFloats[HARDWARE_DAC_MPC_CHANNELS07::MOTOR_SPEED_6]);
    data.motorDirection.set(dacFloats[HARDWARE_DAC_MPC_CHANNELS07::MOTOR_DIRECTION_7]);

    Tools::DacConvertToHexChars(dacChars,dacShorts[HARDWARE_DAC_MPC_CHANNELS07::START_MAX_0]);
    data.start_maxHex.set(dacChars);
    Tools::DacConvertToHexChars(dacChars,dacShorts[HARDWARE_DAC_MPC_CHANNELS07::START_MIN_1]);
    data.start_minHex.set(dacChars);
    Tools::DacConvertToHexChars(dacChars,dacShorts[HARDWARE_DAC_MPC_CHANNELS07::STOP_MAX_2]);
    data.stop_maxHex.set(dacChars);
    Tools::DacConvertToHexChars(dacChars,dacShorts[HARDWARE_DAC_MPC_CHANNELS07::STOP_MIN_3]);
    data.stop_minHex.set(dacChars);
    Tools::DacConvertToHexChars(dacChars,dacShorts[HARDWARE_DAC_MPC_CHANNELS07::GARAGE_MAX_4]);
    data.garage_maxHex.set(dacChars);
    Tools::DacConvertToHexChars(dacChars,dacShorts[HARDWARE_DAC_MPC_CHANNELS07::GARAGE_MIN_5]);
    data.garage_minHex.set(dacChars);
    Tools::DacConvertToHexChars(dacChars,dacShorts[HARDWARE_DAC_MPC_CHANNELS07::MOTOR_SPEED_6]);
    data.motorSpeedHex.set(dacChars);
    Tools::DacConvertToHexChars(dacChars,dacShorts[HARDWARE_DAC_MPC_CHANNELS07::MOTOR_DIRECTION_7]);
    data.motorDirectionHex.set(dacChars);

    Tools::DacsBaseGet(dacShorts,dacFloats,pDev,pContext);

    data.start.set(dacFloats[HARDWARE_DAC_MPX_CHANNELS03::START_0]);
    data.stop.set(dacFloats[HARDWARE_DAC_MPX_CHANNELS03::STOP_1]);
    data.garage.set(dacFloats[HARDWARE_DAC_MPX_CHANNELS03::GARAGE_2]);
    data.spare.set(dacFloats[HARDWARE_DAC_MPX_CHANNELS03::SPARE_3]);
    Tools::DacConvertToHexChars(dacChars,dacShorts[HARDWARE_DAC_MPX_CHANNELS03::START_0]);
    data.startHex.set(dacChars);
    Tools::DacConvertToHexChars(dacChars,dacShorts[HARDWARE_DAC_MPX_CHANNELS03::STOP_1]);
    data.stopHex.set(dacChars);
    Tools::DacConvertToHexChars(dacChars,dacShorts[HARDWARE_DAC_MPX_CHANNELS03::GARAGE_2]);
    data.gargeHex.set(dacChars);
    Tools::DacConvertToHexChars(dacChars,dacShorts[HARDWARE_DAC_MPX_CHANNELS03::SPARE_3]);
    data.spareHex.set(dacChars);

    data.deviceDetailedStatus.set((DEVICE_DETAILED_STATUS::DEVICE_DETAILED_STATUS)pDev->deviceDetailedStatus.get(pContext));


}

// Use this member function to tell the framework whether the subscribed clients need to be updated or not. See the FESA wiki for more info
bool GetEaDac::hasDataChanged(const fesa::RequestEvent& event, fesa::AbstractDevice& abstractDevice, const cmw::data::Data& filter) const
{
	Device& device = static_cast<Device&>(abstractDevice);
	return true;
}

} // BXFISC
