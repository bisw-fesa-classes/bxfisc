// FESA 3 framework // Generated from SampleServerAction_Cpp.xsl

#include <BXFISC/Server/GetStatus.h>

#include <BXFISC/GeneratedCode/ServiceLocator.h>

#include <cmw-log/Logger.h>

#include <BXFISC/Common/Tools.h>

namespace 
{

CMW::Log::Logger& logger = CMW::Log::LoggerFactory::getLogger("FESA.USR.BXFISC.Server.GetStatus");

} // namespace

#define LOG_DIAG_IF(topic,message)                           \
	{                                                    \
		DiagnosticUtils::DiagnosticMessage diagMsg;  \
		diagMsg.side = DiagnosticUtils::user;        \
		diagMsg.source = DiagnosticUtils::server;    \
		diagMsg.fesaClass = "BXFISC"; \
		diagMsg.name = "GetStatus"; \
		diagMsg.action = DiagnosticUtils::undefined; \
		diagMsg.msg = message;                       \
		BXFISCServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
	}

namespace BXFISC
{

GetStatus::GetStatus (fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses):
		GetStatusBase(actionConfig, serviceLocator, serviceLocatorRelatedClasses)
{
}

GetStatus::~GetStatus()
{
}

/*!
 * \brief This method is executed consecutively to a GET/SET action:
 * \param pEvt requestEvent from which the multiplexingContext can be extracted
 * \param pDev points to the Device or Global instance involved by this request 
 * \param data composite structure that agregates one or several types data-entries corresponding to the property definition.
 * This data object is the input (resp. output) which is passed when invoking a Get (resp. Set). 
 * \param filter optional input parameter meant to fine tune the treatment. It appears only if the property defines a filter
 */
void GetStatus::execute(fesa::RequestEvent* pEvt, Device* pDev, StatusPropertyData& data)
{
    using namespace BXFISC_TOOL;
    const MultiplexingContext* pContext = pEvt->getMultiplexingContext();
    auto pGlobalStore = BXFISCServiceLocator_->getGlobalDevice();
    char *errorMsg[] ={ "No Error","No Error"};
    data.errorMsg.set(errorMsg,2);
    data.observables.set((enum BDI_OBSERVABLES::BDI_OBSERVABLES) pGlobalStore->observables.get());
//  data.deviceState.set((DEVICE_STATUS::DEVICE_STATUS)pDev->deviceState.get(pContext));
    data.initDevState.set((INIT_DEV_STATE::INIT_DEV_STATE)pDev->initDevState.get(pContext));
    data.initDevMsg.set(pDev->initDevMsg.get(pContext));

    data.deviceMsg.set(pDev->deviceMsg.get(pContext));

    //pDev->deviceDetailedStatus.set(0xffffffff,pContext);
    if ( (int)pDev->event.get(pContext) == (int)EVENTS::WWE ||
         (int)pDev->event.get(pContext) == (int)EVENTS::WE )
    {
        data.acqNow.set(YES_NO::YES);
    }else
    {
        data.acqNow.set(YES_NO::NO);
    }
    data.detailedStatus.set((DEVICE_DETAILED_STATUS::DEVICE_DETAILED_STATUS)pDev->deviceDetailedStatus.get(pContext));
    data.detailMode.set(Tools::DeviceMode(pDev,pContext));
    data.externalCondition.set(YES_NO::NO);
    data.mode.set(Tools::DeviceMode(pDev,pContext));

    data.control.set(Tools::DeviceControl(pDev,pContext));
    data.status.set(Tools::DeviceStatus(pDev,pContext));
    data.moduleHardwareMonitorWd.set(pDev->moduleHardwareMonitorWd.get(pContext));
    data.moduleHardwareAcquisitionWd.set(pDev->moduleHardwareAcquisitionWd.get(pContext));
    data.acqStamp.set(pDev->acqStamp.get(pContext));
    if ( (int) pDev->scanExecute.get(pContext) == 0)
    {
        data.scanExecuteON.set(YES_NO::NO);
    } else
    {
        data.scanExecuteON.set(YES_NO::YES);
    }
    const int16_t * ptr;
    uint32_t size;
    size = MAX_NB_OF_ACCESS_TYPES  ;
    ptr=pDev->moduleWdReads.get(size, pContext);
    int16_t array[MAX_NB_OF_ACCESS_TYPES]; for ( unsigned int i=0 ; i<  MAX_NB_OF_ACCESS_TYPES ; i++ ) { array[i]=ptr[i]  ; }
    array[HARDWARE_ACCESS::STATUS]++;
    pDev->moduleWdReads.set(array,size, pContext);
}

// Use this member function to tell the framework whether the subscribed clients need to be updated or not. See the FESA wiki for more info
bool GetStatus::hasDataChanged(const fesa::RequestEvent& event, fesa::AbstractDevice& abstractDevice, const cmw::data::Data& filter) const
{
	Device& device = static_cast<Device&>(abstractDevice);
	return true;
}

} // BXFISC
