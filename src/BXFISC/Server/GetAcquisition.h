// FESA 3 framework // Generated from SampleServerAction_Hpp.xsl 
// Use this code as a starting point to develop your own equipment class 

#ifndef BXFISC_GetAcquisition_H_
#define BXFISC_GetAcquisition_H_

#include <BXFISC/GeneratedCode/GetSetDefaultServerAction.h>
#include <BXFISC/GeneratedCode/PropertyData.h>
#include <BXFISC/GeneratedCode/TypeDefinition.h>
#include <BXFISC/GeneratedCode/Device.h>

namespace BXFISC
{

class GetAcquisition : public GetAcquisitionBase
{
public:
	GetAcquisition(fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses);
	virtual ~GetAcquisition();
	void execute(fesa::RequestEvent* pEvt, Device* pDev, AcquisitionPropertyData& data);
	bool hasDataChanged(const fesa::RequestEvent& event, fesa::AbstractDevice& abstractDevice, const cmw::data::Data& filter) const;
};

} // BXFISC

#endif // BXFISC_GetAcquisition_H_
