// FESA 3 framework // Generated from SampleServerAction_Hpp.xsl 
// Use this code as a starting point to develop your own equipment class 

#ifndef BXFISC_SetEaPiquetShort_H_
#define BXFISC_SetEaPiquetShort_H_

#include <BXFISC/GeneratedCode/GetSetDefaultServerAction.h>
#include <BXFISC/GeneratedCode/PropertyData.h>
#include <BXFISC/GeneratedCode/TypeDefinition.h>
#include <BXFISC/GeneratedCode/Device.h>

namespace BXFISC
{

class SetEaPiquetShort : public SetEaPiquetShortBase
{
public:
	SetEaPiquetShort(fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses);
	virtual ~SetEaPiquetShort();
	void execute(fesa::RequestEvent* pEvt, Device* pDev, EaPiquetShortPropertyData& data);
};

} // BXFISC

#endif // BXFISC_SetEaPiquetShort_H_
