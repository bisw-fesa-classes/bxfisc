#ifndef BXFISC_SERVER_DEVICE_CLASS_H_
#define BXFISC_SERVER_DEVICE_CLASS_H_

#include <BXFISC/GeneratedCode/ServerDeviceClassGen.h>

namespace BXFISC
{

class ServerDeviceClass: public ServerDeviceClassGen
{
	public:
		static ServerDeviceClass* getInstance();
		static void releaseInstance();
		void specificInit();
	private:
		ServerDeviceClass();
		virtual  ~ServerDeviceClass();
		static ServerDeviceClass* instance_;
};

} // BXFISC;

#endif // BXFISC_SERVER_DEVICE_CLASS_H_
