// FESA 3 framework // Generated from SampleServerAction_Cpp.xsl

#include <BXFISC/Server/GetEaDelay.h>

#include <BXFISC/GeneratedCode/ServiceLocator.h>

#include <cmw-log/Logger.h>
#include <BXFISC/Common/Tools.h>

namespace 
{

CMW::Log::Logger& logger = CMW::Log::LoggerFactory::getLogger("FESA.USR.BXFISC.Server.GetEaDelay");

} // namespace

#define LOG_DIAG_IF(topic,message)                           \
	{                                                    \
		DiagnosticUtils::DiagnosticMessage diagMsg;  \
		diagMsg.side = DiagnosticUtils::user;        \
		diagMsg.source = DiagnosticUtils::server;    \
		diagMsg.fesaClass = "BXFISC"; \
		diagMsg.name = "GetEaDelay"; \
		diagMsg.action = DiagnosticUtils::undefined; \
		diagMsg.msg = message;                       \
		BXFISCServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
	}

namespace BXFISC
{

GetEaDelay::GetEaDelay (fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses):
		GetEaDelayBase(actionConfig, serviceLocator, serviceLocatorRelatedClasses)
{
}

GetEaDelay::~GetEaDelay()
{
}

/*!
 * \brief This method is executed consecutively to a GET/SET action:
 * \param pEvt requestEvent from which the multiplexingContext can be extracted
 * \param pDev points to the Device or Global instance involved by this request 
 * \param data composite structure that agregates one or several types data-entries corresponding to the property definition.
 * This data object is the input (resp. output) which is passed when invoking a Get (resp. Set). 
 * \param filter optional input parameter meant to fine tune the treatment. It appears only if the property defines a filter
 */
void GetEaDelay::execute(fesa::RequestEvent* pEvt, Device* pDev, EaDelayPropertyData& data)
{
    using namespace BXFISC_TOOL;
    const MultiplexingContext* pContext = pEvt->getMultiplexingContext();
    const GlobalDevice * pGlobalStore = BXFISCServiceLocator_->getGlobalDevice();
    double delay,delay_setting;
    double delayWidth,delayWidth_setting;
    double delayRanges_max[2],delayRanges_min[2];
    double delayWidthRanges_max[2],delayWidthRanges_min[2];
    Tools::DelayInfo(pDev, pContext,
        &delay,&delay_setting,
        &delayWidth,&delayWidth_setting,
        delayRanges_max,delayRanges_min,
        delayWidthRanges_max,delayWidthRanges_min);
//    data.acqStamp.set(pGlobalStore->acqStampMonitor.get());

    data.delayPm1.set(pDev->delayPm1.get(pContext));
    data.delayPm1_setting.set(pDev->delayPm1_setting.get(pContext));
    data.delayPm1_max.set(pDev->delayPm1_max.get(pContext));
    data.delayPm1_min.set(pDev->delayPm1_min.get(pContext));
    data.delayPm1_default.set(pDev->delayPm1_default.get(pContext));
    data.delayWidthPm1.set(pDev->delayWidthPm1.get(pContext));
    data.delayWidthPm1_setting.set(pDev->delayWidthPm1_setting.get(pContext));
    data.delayWidthPm1_max.set(pDev->delayWidthPm1_max.get(pContext));
    data.delayWidthPm1_min.set(pDev->delayWidthPm1_min.get(pContext));
    data.delayWidthPm1_default.set(pDev->delayWidthPm1_default.get(pContext));

    data.delayPm2.set(pDev->delayPm2.get(pContext));
    data.delayPm2_setting.set(pDev->delayPm2_setting.get(pContext));
    data.delayPm2_max.set(pDev->delayPm2_max.get(pContext));
    data.delayPm2_min.set(pDev->delayPm2_min.get(pContext));
    data.delayPm2_default.set(pDev->delayPm2_default.get(pContext));
    data.delayWidthPm2.set(pDev->delayWidthPm2.get(pContext));
    data.delayWidthPm2_setting.set(pDev->delayWidthPm2_setting.get(pContext));
    data.delayWidthPm2_max.set(pDev->delayWidthPm2_max.get(pContext));
    data.delayWidthPm2_min.set(pDev->delayWidthPm2_min.get(pContext));
    data.delayWidthPm2_default.set(pDev->delayWidthPm2_default.get(pContext));
    data.delayAction.set(DELAY_ACTION::NONE);
    data.delayChannel.set(DELAY_CHANNEL::CH_1);
    data.delayValue.set(0.0);
}

// Use this member function to tell the framework whether the subscribed clients need to be updated or not. See the FESA wiki for more info
bool GetEaDelay::hasDataChanged(const fesa::RequestEvent& event, fesa::AbstractDevice& abstractDevice, const cmw::data::Data& filter) const
{
	Device& device = static_cast<Device&>(abstractDevice);
	return true;
}

} // BXFISC
