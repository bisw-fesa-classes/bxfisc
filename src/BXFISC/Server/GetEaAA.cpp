// FESA 3 framework // Generated from SampleServerAction_Cpp.xsl

#include <BXFISC/Server/GetEaAA.h>

#include <BXFISC/GeneratedCode/ServiceLocator.h>

#include <cmw-log/Logger.h>

#include <BXFISC/Common/Tools.h>

namespace 
{

CMW::Log::Logger& logger = CMW::Log::LoggerFactory::getLogger("FESA.USR.BXFISC.Server.GetEaAA");

} // namespace

#define LOG_DIAG_IF(topic,message)                           \
	{                                                    \
		DiagnosticUtils::DiagnosticMessage diagMsg;  \
		diagMsg.side = DiagnosticUtils::user;        \
		diagMsg.source = DiagnosticUtils::server;    \
		diagMsg.fesaClass = "BXFISC"; \
		diagMsg.name = "GetEaAA"; \
		diagMsg.action = DiagnosticUtils::undefined; \
		diagMsg.msg = message;                       \
		BXFISCServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
	}

namespace BXFISC
{

GetEaAA::GetEaAA (fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses):
		GetEaAABase(actionConfig, serviceLocator, serviceLocatorRelatedClasses)
{
}

GetEaAA::~GetEaAA()
{
}

/*!
 * \brief This method is executed consecutively to a GET/SET action:
 * \param pEvt requestEvent from which the multiplexingContext can be extracted
 * \param pDev points to the Device or Global instance involved by this request 
 * \param data composite structure that agregates one or several types data-entries corresponding to the property definition.
 * This data object is the input (resp. output) which is passed when invoking a Get (resp. Set). 
 * \param filter optional input parameter meant to fine tune the treatment. It appears only if the property defines a filter
 */
void GetEaAA::execute(fesa::RequestEvent* pEvt, Device* pDev, EaAAPropertyData& data)
{
    using namespace BXFISC_TOOL;
    const GlobalDevice * pGlobalStore = BXFISCServiceLocator_->getGlobalDevice();
    const MultiplexingContext* pContext = pEvt->getMultiplexingContext();
    struct timeval tAcq; struct timezone zAcq ;
    int64_t acqStamp;
    int64_t factor =(int64_t)(1.0 /pGlobalStore->measStamp_unitFactor.get());;
    gettimeofday(&tAcq,&zAcq);
    acqStamp=(int64_t)tAcq.tv_sec*1000000L+(int64_t)tAcq.tv_usec;
    acqStamp=(acqStamp*factor)/1000000000L;
    data.acqStamp.set(acqStamp);

    uint16_t aaShort[CHANNELS_AA_NUMBER];
    float aaFloat[CHANNELS_AA_NUMBER];


    char aaChars[HEX_SIZE];
    Tools::AAsGet(aaShort,aaFloat,pDev,pContext);

    data.hv1.set(pDev->highVoltagePm1.get(pContext));
    data.hv2.set(pDev->highVoltagePm2.get(pContext));
    data.thres1.set(pDev->moduleThresholdPm1.get(pContext));
    data.thres2.set(pDev->moduleThresholdPm2.get(pContext));
    data.hv1Cur.set(pDev->highVoltagePm1_current.get(pContext));
    data.hv2Cur.set(pDev->highVoltagePm2_current.get(pContext));
    data.motCur.set(pDev->motorCurrent.get(pContext));
    data.mpx.set(pDev->module_mpx.get(pContext));
    Tools::AAConvertToHexChars(aaChars,aaShort[HARDWARE_DAC_CHANNELS07::HV2_1]);
    data.hv1Hex.set(aaChars);
    Tools::AAConvertToHexChars(aaChars,aaShort[HARDWARE_AA_CHANNELS07::HV2_1]);
    data.hv2Hex.set(aaChars);
    Tools::AAConvertToHexChars(aaChars,aaShort[HARDWARE_AA_CHANNELS07::THRES1_2]);
    data.hv1CurHex.set(aaChars);
    Tools::AAConvertToHexChars(aaChars,aaShort[HARDWARE_AA_CHANNELS07::THRES2_3]);
    data.hv2CurHex.set(aaChars);
    Tools::AAConvertToHexChars(aaChars,aaShort[HARDWARE_AA_CHANNELS07::HV1_CUR_4]);
    data.thres1Hex.set(aaChars);
    Tools::AAConvertToHexChars(aaChars,aaShort[HARDWARE_AA_CHANNELS07::HV2_CUR_5]);
    data.thres2Hex.set(aaChars);
    Tools::AAConvertToHexChars(aaChars,aaShort[HARDWARE_AA_CHANNELS07::MOTOR_CUR_6]);
    data.motCurHex.set(aaChars);
    Tools::AAConvertToHexChars(aaChars,aaShort[HARDWARE_AA_CHANNELS07::MPX_7]);
    data.mpxHex.set(aaChars);
    Tools::AAsGetMpx(aaShort,aaFloat,pDev,pContext);

    data.P5V.set(aaFloat[0]);
    data.N5V.set(aaFloat[1]);
    data.SENS.set(aaFloat[2]);
    data.MOT.set(aaFloat[3]);
    data.SPEED.set(aaFloat[4]);
    data.DIRECT.set(aaFloat[5]);
    data.TACHI.set(aaFloat[6]);
    Tools::AAConvertToHexChars(aaChars,aaShort[0]);
    data.P5VHex.set(aaChars);
    Tools::AAConvertToHexChars(aaChars,aaShort[1]);
    data.N5VHex.set(aaChars);
    Tools::AAConvertToHexChars(aaChars,aaShort[2]);
    data.SENSHex.set(aaChars);
    Tools::AAConvertToHexChars(aaChars,aaShort[3]);
    data.MOTHex.set(aaChars);
    Tools::AAConvertToHexChars(aaChars,aaShort[4]);
    data.SPEEDHex.set(aaChars);
    Tools::AAConvertToHexChars(aaChars,aaShort[5]);
    data.DIRECTHex.set(aaChars);
    Tools::AAConvertToHexChars(aaChars,aaShort[6]);
    data.TACHHex.set(aaChars);
    data.deviceDetailedStatus.set((DEVICE_DETAILED_STATUS::DEVICE_DETAILED_STATUS)pDev->deviceDetailedStatus.get(pContext));

}

// Use this member function to tell the framework whether the subscribed clients need to be updated or not. See the FESA wiki for more info
bool GetEaAA::hasDataChanged(const fesa::RequestEvent& event, fesa::AbstractDevice& abstractDevice, const cmw::data::Data& filter) const
{
	Device& device = static_cast<Device&>(abstractDevice);
	return true;
}

} // BXFISC
