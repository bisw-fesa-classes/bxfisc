// FESA 3 framework // Generated from SampleServerAction_Cpp.xsl

#include <BXFISC/Server/SetEaDelay.h>

#include <BXFISC/GeneratedCode/ServiceLocator.h>

#include <cmw-log/Logger.h>

#include <BXFISC/Common/Tools.h>

namespace 
{

CMW::Log::Logger& logger = CMW::Log::LoggerFactory::getLogger("FESA.USR.BXFISC.Server.SetEaDelay");

} // namespace

#define LOG_DIAG_IF(topic,message)                           \
	{                                                    \
		DiagnosticUtils::DiagnosticMessage diagMsg;  \
		diagMsg.side = DiagnosticUtils::user;        \
		diagMsg.source = DiagnosticUtils::server;    \
		diagMsg.fesaClass = "BXFISC"; \
		diagMsg.name = "SetEaDelay"; \
		diagMsg.action = DiagnosticUtils::undefined; \
		diagMsg.msg = message;                       \
		BXFISCServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
	}

namespace BXFISC
{

SetEaDelay::SetEaDelay (fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses):
		SetEaDelayBase(actionConfig, serviceLocator, serviceLocatorRelatedClasses)
{
}

SetEaDelay::~SetEaDelay()
{
}

/*!
 * \brief This method is executed consecutively to a GET/SET action:
 * \param pEvt requestEvent from which the multiplexingContext can be extracted
 * \param pDev points to the Device or Global instance involved by this request 
 * \param data composite structure that agregates one or several types data-entries corresponding to the property definition.
 * This data object is the input (resp. output) which is passed when invoking a Get (resp. Set). 
 * \param filter optional input parameter meant to fine tune the treatment. It appears only if the property defines a filter
 */
void SetEaDelay::execute(fesa::RequestEvent* pEvt, Device* pDev, EaDelayPropertyData& data)
{
    using std::string;
    using namespace BXFISC_TOOL;
    MultiplexingContext* pContext = pEvt->getMultiplexingContext();
    auto GlobalDevice = BXFISCServiceLocator_->getGlobalDevice();
     if ( data.itemAvailable("delayAction" ) &&  data.itemAvailable("delayChannel" ) && data.itemAvailable("delayValue" ) )
     {
         DELAY_ACTION::DELAY_ACTION which = data.delayAction.get();
         DELAY_CHANNEL::DELAY_CHANNEL channel = data.delayChannel.get();
         double value = data.delayValue.get();
         double  defaultDelay,defaultWidth;
         if ( channel == DELAY_CHANNEL::CH_2 )
         {
             defaultDelay=pDev->delayPm2_setting.get(pContext) ;
             defaultWidth=pDev->delayWidthPm2_setting.get(pContext);
         }else
         {
             defaultDelay=pDev->delayPm1_setting.get(pContext)      ;
             defaultWidth=pDev->delayWidthPm1_setting.get(pContext) ;
         }
         switch ( which )
         {
             case DELAY_ACTION::SETTING :
                 Tools::DelaySet(pDev, pContext, channel,value,defaultWidth);
                 break;
             case DELAY_ACTION::SETTING_WITDH :
                 Tools::DelaySet(pDev, pContext, channel,defaultDelay,value);
                 break;
             case DELAY_ACTION::DEFAULT :
                 if ( channel == DELAY_CHANNEL::CH_1 )
                 {
                     pDev->delayPm1_default.set(value, pContext);
                 }
                 if ( channel == DELAY_CHANNEL::CH_2 )
                 {
                     pDev->delayPm2_default.set(value, pContext);
                 }
                 break ;
             case DELAY_ACTION::DEFAULT_WIDTH :
                 if ( channel == DELAY_CHANNEL::CH_1 )
                 {
                     pDev->delayWidthPm1_default.set(value, pContext);
                 }
                 if ( channel == DELAY_CHANNEL::CH_2 )
                 {
                     pDev->delayWidthPm2_default.set(value, pContext);
                 }
                 break ;
             case DELAY_ACTION::ALL_TO_DEFAULTS :
                 Tools::DelaySet(pDev, pContext, DELAY_CHANNEL::CH_1,pDev->delayPm1_default.get(pContext),pDev->delayWidthPm1_default.get(pContext));
                 Tools::DelaySet(pDev, pContext, DELAY_CHANNEL::CH_2,pDev->delayPm2_default.get(pContext),pDev->delayWidthPm2_default.get(pContext));
                 break;
             case DELAY_ACTION::NONE :
             default :
                 break ;
         }
//         string notificationStr = "EaPiquet:";
//         notificationStr += pDev->name.get();
//         notificationStr +=",";
//         string classNameString(pGlobalStore->className.get());
//         AbstractEquipmentRT::notify(pContext, classNameString,
//         notificationStr);
     }
}

} // BXFISC
