// FESA 3 framework // Generated from SampleServerAction_Cpp.xsl

#include <BXFISC/Server/SetEaPiquet.h>

#include <BXFISC/GeneratedCode/ServiceLocator.h>

#include <cmw-log/Logger.h>
#include <BXFISC/Common/Tools.h>


namespace 
{

CMW::Log::Logger& logger = CMW::Log::LoggerFactory::getLogger("FESA.USR.BXFISC.Server.SetEaPiquet");

} // namespace

#define LOG_DIAG_IF(topic,message)                           \
	{                                                    \
		DiagnosticUtils::DiagnosticMessage diagMsg;  \
		diagMsg.side = DiagnosticUtils::user;        \
		diagMsg.source = DiagnosticUtils::server;    \
		diagMsg.fesaClass = "BXFISC"; \
		diagMsg.name = "SetEaPiquet"; \
		diagMsg.action = DiagnosticUtils::undefined; \
		diagMsg.msg = message;                       \
		BXFISCServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
	}

namespace BXFISC
{

SetEaPiquet::SetEaPiquet (fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses):
		SetEaPiquetBase(actionConfig, serviceLocator, serviceLocatorRelatedClasses)
{
}

SetEaPiquet::~SetEaPiquet()
{
}

/*!
 * \brief This method is executed consecutively to a GET/SET action:
 * \param pEvt requestEvent from which the multiplexingContext can be extracted
 * \param pDev points to the Device or Global instance involved by this request 
 * \param data composite structure that agregates one or several types data-entries corresponding to the property definition.
 * This data object is the input (resp. output) which is passed when invoking a Get (resp. Set). 
 * \param filter optional input parameter meant to fine tune the treatment. It appears only if the property defines a filter
 */
void SetEaPiquet::execute(fesa::RequestEvent* pEvt, Device* pDev, EaPiquetPropertyData& data)
{
    using namespace BXFISC_TOOL;
    using std::string;

    const MultiplexingContext* pContext = pEvt->getMultiplexingContext();
    const GlobalDevice * pGlobalStore = BXFISCServiceLocator_->getGlobalDevice();
    int actionCount=0;
    HARDWARE_ERRORS::HARDWARE_ERRORS hardwareError=HARDWARE_ERRORS::NONE;
    LOG_ERROR_FORMAT_IF(logger, "EaPiquet started!");
    if(data.itemAvailable("scanAction")) {
        LOG_ERROR_FORMAT_IF(logger, "scanAction available!!");
    } else {
        LOG_ERROR_FORMAT_IF(logger, "scanAction missing!!");
    }
    if(data.itemAvailable("scanStart_next")) {
        LOG_ERROR_FORMAT_IF(logger, "scanStart_next available!!");
    } else {
        LOG_ERROR_FORMAT_IF(logger, "scanStart_next missing!!");
    }
    if(data.itemAvailable("scanDelay_next")) {
        LOG_ERROR_FORMAT_IF(logger, "scanDelay_next available!!");

    } else {
        LOG_ERROR_FORMAT_IF(logger, "scanDelay_next missing!!");
    }
    if(data.itemAvailable("speedGain")) {
        LOG_ERROR_FORMAT_IF(logger, "speedGain available!!");
    } else {
        LOG_ERROR_FORMAT_IF(logger, "speedGain missing!!");
    }
//    data.itemAvailable("scanAction" )

    if (data.itemAvailable("deviceName")  )
    {
        int hasAGetBeenDone =strcmp(pDev->name.get(),data.deviceName.get());
        if ( hasAGetBeenDone != 0 )
        {
            char msg[MSG_SIZE];
            hardwareError=HARDWARE_ERRORS::GET_NOT_DONE_FIRST;
            string msgStr = HARDWARE_ERRORS::HARDWARE_ERRORS_AsString.at(hardwareError);
            throw FesaException(__FILE__, __LINE__, msgStr.c_str());
        }
    }
    if ( data.itemAvailable("action" ) && (int)data.action.get()!=(int)DEVICE_STATE_SETTING::NONE  )
    {
        actionCount++;
        string msg = DEVICE_STATE_SETTING::DEVICE_STATE_SETTING_AsString.at(data.action.get());
//        DEVICE_STATE_SETTING::DEVICE_STATE_SETTINGConverter::convertToString(data.action.get(),msg);
        hardwareError=Tools::DeviceStateAction( pDev, pContext,data.action.get());
            //(DEVICE_STATE_SETTING::INIT_MODULE)

    }
    if( data.itemAvailable("moduleThresholdPm1_setting")) {
        Tools::VoltageThresholdPm1To(data.moduleThresholdPm1_setting.get(), pDev, pContext);
        actionCount++;
    }
    if( data.itemAvailable("moduleThresholdPm2_setting")) {
        Tools::VoltageThresholdPm2To(data.moduleThresholdPm2_setting.get(), pDev, pContext);
        actionCount++;
    }
    if ( data.itemAvailable("motorPositionAction" ) && data.itemAvailable("motorPosition_setting" ) )
    {
        if ( data.motorPositionAction.get()==(int)YES_NO::YES )
        {
            actionCount++;
            float position=data.motorPosition_setting.get();
            pDev->position_setting.set(position,pContext);
            Tools::PositionSet(position, true, pDev,pContext);
        }
    }
    if ( data.itemAvailable("highVoltagePm1Action" ) && data.itemAvailable("highVoltagePm1_setting") )
    {
        if ( (int)data.highVoltagePm1Action.get()==(int)YES_NO::YES  )
        {
            actionCount++;
            hardwareError =Tools::VoltageHighPm1To( data.highVoltagePm1_setting.get(), pDev,pContext) ;
        }
    }
    if ( data.itemAvailable("highVoltagePm2Action" ) && data.itemAvailable("highVoltagePm2_setting") )
    {
        if ( (int)data.highVoltagePm2Action.get()==(int)YES_NO::YES  )
        {
            actionCount++;
            hardwareError =Tools::VoltageHighPm2To( data.highVoltagePm2_setting.get(), pDev,pContext) ;
        }
    }
    if ( data.itemAvailable("delayPm1Action" ) )
    {
        if ( (int)data.delayPm1Action.get()==(int)YES_NO::YES && data.itemAvailable("delayPm1Action") )
        {
            actionCount++;
            Tools::DelaySet(pDev, pContext, 0,data.delayPm1_setting.get(),data.delayWidthPm1_setting.get());
        }
    }
    if ( data.itemAvailable("delayPm2Action" ) )
    {
        if ( (int)data.delayPm2Action.get()==(int)YES_NO::YES && data.itemAvailable("delayPm2Action") )
        {
            actionCount++;
            Tools::DelaySet(pDev, pContext, 1,data.delayPm2_setting.get(),data.delayWidthPm2_setting.get());
        }
    }
    if ( data.itemAvailable("scanAction" )  )
    {
        if ( (int)data.scanAction.get()==(int)YES_NO::YES
            && data.itemAvailable("scanStart_next" )
            && data.itemAvailable("scanStop_next" )
            && data.itemAvailable("scanDelay_next" )  )
        {

            if ((int) (pDev->event.get(pContext)) == (int) EVENTS::WWE
                                || (int) (pDev->event.get(pContext)) == (int) EVENTS::WE) {
                char msg[MSG_SIZE];
                HARDWARE_ERRORS::HARDWARE_ERRORS hardwareError = HARDWARE_ERRORS::SETTING_NOT_ALLOWED;
                sprintf(msg, "BXFISC:: %d SCAN_PLEASE_WAIT_TILL_AFTER_EE", hardwareError);
                throw FesaException(__FUNCTION__, __LINE__, msg);
            }
            else {

                pDev->scanStart_next.set(data.scanStart_next.get(), pContext);
                pDev->scanStop_next.set( data.scanStop_next.get(), pContext);
                pDev->scanDelay_next.set(data.scanDelay_next.get(), pContext);
                actionCount++;

                const string & name = pDev->getName();

                pDev->scanAction.set(YES_NO::YES, pContext);

                triggerOnDemandEventSource(ODScanSetup, pContext, name.c_str(), name.size() + 1);
            }
        }
    }

    if( data.itemAvailable("returnToStart") ) {
        if( data.returnToStart.get() == YES_NO::YES) {
            const string & name = pDev->getName();
            triggerOnDemandEventSource(ODReturnToStart, pContext, name.c_str(), name.size() + 1);
        }
    }

    if ( actionCount==0 )
    {
        hardwareError=HARDWARE_ERRORS::NO_ACTION_SET ;
    }
    const int16_t * ptr;
    uint32_t size = MAX_NB_OF_ACCESS_TYPES  ;
    ptr=pDev->moduleWdWrites.get(size, pContext);
    int16_t array[MAX_NB_OF_ACCESS_TYPES]; for ( unsigned int i=0 ; i<  MAX_NB_OF_ACCESS_TYPES ; i++ ) { array[i]=ptr[i]  ; }
    array[HARDWARE_ACCESS::EAPIQUET]++;
    pDev->moduleWdWrites.set(array,size, pContext);

    if ( (int)hardwareError!=(int)HARDWARE_ERRORS::NONE )
    {
        string msgStr = HARDWARE_ERRORS::HARDWARE_ERRORS_AsString.at(hardwareError);
        throw FesaException(__FUNCTION__, __LINE__, msgStr);
    }
}

} // BXFISC
