// FESA 3 framework // Generated from SampleServerAction_Hpp.xsl 
// Use this code as a starting point to develop your own equipment class 

#ifndef BXFISC_SetEaPiquet_H_
#define BXFISC_SetEaPiquet_H_

#include <BXFISC/GeneratedCode/GetSetDefaultServerAction.h>
#include <BXFISC/GeneratedCode/PropertyData.h>
#include <BXFISC/GeneratedCode/TypeDefinition.h>
#include <BXFISC/GeneratedCode/Device.h>

namespace BXFISC
{

class SetEaPiquet : public SetEaPiquetBase
{
public:
	SetEaPiquet(fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses);
	virtual ~SetEaPiquet();
	void execute(fesa::RequestEvent* pEvt, Device* pDev, EaPiquetPropertyData& data);
};

} // BXFISC

#endif // BXFISC_SetEaPiquet_H_
