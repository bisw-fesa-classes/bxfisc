// FESA 3 framework // Generated from SampleServerAction_Cpp.xsl

#include <BXFISC/Server/GetEaPorts.h>

#include <BXFISC/GeneratedCode/ServiceLocator.h>

#include <cmw-log/Logger.h>

#include <BXFISC/Common/Tools.h>

namespace 
{

CMW::Log::Logger& logger = CMW::Log::LoggerFactory::getLogger("FESA.USR.BXFISC.Server.GetEaPorts");

} // namespace

#define LOG_DIAG_IF(topic,message)                           \
	{                                                    \
		DiagnosticUtils::DiagnosticMessage diagMsg;  \
		diagMsg.side = DiagnosticUtils::user;        \
		diagMsg.source = DiagnosticUtils::server;    \
		diagMsg.fesaClass = "BXFISC"; \
		diagMsg.name = "GetEaPorts"; \
		diagMsg.action = DiagnosticUtils::undefined; \
		diagMsg.msg = message;                       \
		BXFISCServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
	}

namespace BXFISC
{

GetEaPorts::GetEaPorts (fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses):
		GetEaPortsBase(actionConfig, serviceLocator, serviceLocatorRelatedClasses)
{
}

GetEaPorts::~GetEaPorts()
{
}

/*!
 * \brief This method is executed consecutively to a GET/SET action:
 * \param pEvt requestEvent from which the multiplexingContext can be extracted
 * \param pDev points to the Device or Global instance involved by this request 
 * \param data composite structure that agregates one or several types data-entries corresponding to the property definition.
 * This data object is the input (resp. output) which is passed when invoking a Get (resp. Set). 
 * \param filter optional input parameter meant to fine tune the treatment. It appears only if the property defines a filter
 */
void GetEaPorts::execute(fesa::RequestEvent* pEvt, Device* pDev, EaPortsRegsPropertyData& data)
{
    using namespace BXFISC_TOOL;
    const MultiplexingContext* pContext = pEvt->getMultiplexingContext();
    const GlobalDevice * pGlobalStore = BXFISCServiceLocator_->getGlobalDevice();
    //struct timeval tAcq;
    //struct timezone zAcq ;
//  long long acqStamp;
  //  long long factor =(long long)(1.0 /pGlobalStore->measStamp_unitFactor.get());;
    uint16_t ports[3];
    data.warning.set("DANGER !!");
//    data.acqStamp.set(pGlobalStore->acqStampMonitor.get(pContext));
    data.deviceName.set(pDev->name.get());
    Tools::PortsGet(ports,pDev,pContext);
    pDev->moduleHwPortA.set((PORTA::PORTA)ports[0],pContext);
    pDev->moduleHwPortB.set((PORTB::PORTB)ports[1],pContext);
    pDev->moduleHwPortC.set((PORTC::PORTC)ports[2],pContext);
    data.moduleHwPortA.set((PORTA::PORTA)pDev->moduleHwPortA.get(pContext));
    data.moduleHwPortB.set((PORTB::PORTB)pDev->moduleHwPortB.get(pContext));
    data.moduleHwPortC.set((PORTC::PORTC)pDev->moduleHwPortC.get(pContext));
    data.moduleHwPortA_setting.set((PORTA::PORTA)pDev->moduleHwPortA_setting.get(pContext));
    data.moduleHwPortB_setting.set((PORTB::PORTB)pDev->moduleHwPortB_setting.get(pContext));
    data.moduleHwPortC_setting.set((PORTC::PORTC)pDev->moduleHwPortC_setting.get(pContext));
    data.moduleHwPortA_default.set((PORTA::PORTA)pDev->moduleHwPortA_setting.get(pContext));
    data.moduleHwPortB_default.set((PORTB::PORTB)pDev->moduleHwPortB_default.get(pContext));
    data.moduleHwPortC_default.set((PORTC::PORTC)pDev->moduleHwPortC_default.get(pContext));
    REGBASE::REGBASE baseReg ;
    // to do all the other registeres
    Tools::RegBaseGet(&baseReg,pDev, pContext);
    pDev->moduleHwRegBase.set(baseReg, pContext);
    data.moduleHwRegBase_setting.set((REGBASE::REGBASE)pDev->moduleHwRegBase_setting.get(pContext));
    data.moduleHwRegBase_default.set((REGBASE::REGBASE)pDev->moduleHwRegBase_default.get(pContext));
    data.moduleHwRegCoin_setting.set((REGCOIN::REGCOIN)pDev->moduleHwRegCoin_setting.get(pContext));
    data.moduleHwRegCoin_default.set((REGCOIN::REGCOIN)pDev->moduleHwRegCoin_default.get(pContext));
    data.moduleHwRegMaster_setting.set((REGMASTER::REGMASTER)pDev->moduleHwRegMaster_setting.get(pContext));
    data.moduleHwRegMaster_default.set((REGMASTER::REGMASTER)pDev->moduleHwRegMaster_default.get(pContext));
    data.moduleHwRegMotor_setting.set((REGMOTOR::REGMOTOR)pDev->moduleHwRegMotor_setting.get(pContext));
    data.moduleHwRegMotor_default.set((REGMOTOR::REGMOTOR)pDev->moduleHwRegMotor_default.get(pContext));
    data.moduleHwRegBase.set((REGBASE::REGBASE)pDev->moduleHwRegBase.get(pContext));
    data.moduleHwRegCoin.set((REGCOIN::REGCOIN)pDev->moduleHwRegCoin.get(pContext));
//  data.moduleHwRegMaster.set((PORTC::PORTC)pDev->moduleHwRegMaster.get(pContext));
    data.moduleHwRegMotor.set((REGMOTOR::REGMOTOR)pDev->moduleHwRegMotor.get(pContext));
    data.initModule.set(YES_NO::NO);
    data.reg_action.set(REG_ACTION::NONE);
    data.reg_value.set((REGBLANK::REGBLANK)0);
    data.port_action.set(PORT_ACTION::NONE);
    data.port_value.set((REGBLANK::REGBLANK)0);

    data.moduleHardwareMonitorWd.set(pDev->moduleHardwareMonitorWd.get(pContext));

}

// Use this member function to tell the framework whether the subscribed clients need to be updated or not. See the FESA wiki for more info
bool GetEaPorts::hasDataChanged(const fesa::RequestEvent& event, fesa::AbstractDevice& abstractDevice, const cmw::data::Data& filter) const
{
	Device& device = static_cast<Device&>(abstractDevice);
	return true;
}

} // BXFISC
