// FESA 3 framework // Generated from SampleServerAction_Cpp.xsl

#include <BXFISC/Server/SetPhaSetting.h>

#include <BXFISC/GeneratedCode/ServiceLocator.h>

#include <cmw-log/Logger.h>

#include <BXFISC/Common/Tools.h>

namespace 
{

CMW::Log::Logger& logger = CMW::Log::LoggerFactory::getLogger("FESA.USR.BXFISC.Server.SetPhaSetting");

} // namespace

#define LOG_DIAG_IF(topic,message)                           \
	{                                                    \
		DiagnosticUtils::DiagnosticMessage diagMsg;  \
		diagMsg.side = DiagnosticUtils::user;        \
		diagMsg.source = DiagnosticUtils::server;    \
		diagMsg.fesaClass = "BXFISC"; \
		diagMsg.name = "SetPhaSetting"; \
		diagMsg.action = DiagnosticUtils::undefined; \
		diagMsg.msg = message;                       \
		BXFISCServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
	}

namespace BXFISC
{

SetPhaSetting::SetPhaSetting (fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses):
		SetPhaSettingBase(actionConfig, serviceLocator, serviceLocatorRelatedClasses)
{
}

SetPhaSetting::~SetPhaSetting()
{
}

/*!
 * \brief This method is executed consecutively to a GET/SET action:
 * \param pEvt requestEvent from which the multiplexingContext can be extracted
 * \param pDev points to the Device or Global instance involved by this request 
 * \param data composite structure that agregates one or several types data-entries corresponding to the property definition.
 * This data object is the input (resp. output) which is passed when invoking a Get (resp. Set). 
 * \param filter optional input parameter meant to fine tune the treatment. It appears only if the property defines a filter
 */
void SetPhaSetting::execute(fesa::RequestEvent* pEvt, Device* pDev, PhaSettingPropertyData& data)
{
    using namespace BXFISC_TOOL;
    using std::string;
    HARDWARE_ERRORS::HARDWARE_ERRORS hardwareError=HARDWARE_ERRORS::NONE;
    MultiplexingContext* pContext = pEvt->getMultiplexingContext();
    if ( data.itemAvailable("highVoltage" )  )
    {
        auto voltage = data.highVoltage.get()/-1000.00;

         hardwareError =Tools::VoltageHighPm1To(voltage, pDev,pContext) ;

         if (hardwareError == HARDWARE_ERRORS::NONE)
         {
             pDev->highVoltagePm1_setting.set(data.highVoltage.get() / 1000, pContext);
         }
    }
    if ( data.itemAvailable("position" )  )
    {
        if ( data.position.get() > pDev->motorPosition_max.get(pContext) ||
                         data.position.get() < pDev->motorPosition_min.get(pContext) )
        {
            string msgStr = HARDWARE_ERRORS::HARDWARE_ERRORS_AsString.at(HARDWARE_ERRORS::POSITION_OUT_OF_RANGE);

            throw FesaException(__FUNCTION__, __LINE__, msgStr);
        }else
        {
            pDev->msgCommand.set(" position changing ", pContext);
            Tools::PositionSet(data.position.get(), true, pDev,pContext);
        }

    }
    if ( data.itemAvailable("gain" ) )
    {
        Tools::EmcGainSet(pDev,pContext,data.gain.get() );
    }
}

} // BXFISC
