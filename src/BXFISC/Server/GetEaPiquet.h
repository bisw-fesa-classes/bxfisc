// FESA 3 framework // Generated from SampleServerAction_Hpp.xsl 
// Use this code as a starting point to develop your own equipment class 

#ifndef BXFISC_GetEaPiquet_H_
#define BXFISC_GetEaPiquet_H_

#include <BXFISC/GeneratedCode/GetSetDefaultServerAction.h>
#include <BXFISC/GeneratedCode/PropertyData.h>
#include <BXFISC/GeneratedCode/TypeDefinition.h>
#include <BXFISC/GeneratedCode/Device.h>

namespace BXFISC
{

class GetEaPiquet : public GetEaPiquetBase
{
public:
	GetEaPiquet(fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses);
	virtual ~GetEaPiquet();
	void execute(fesa::RequestEvent* pEvt, Device* pDev, EaPiquetPropertyData& data);
	bool hasDataChanged(const fesa::RequestEvent& event, fesa::AbstractDevice& abstractDevice, const cmw::data::Data& filter) const;
};

} // BXFISC

#endif // BXFISC_GetEaPiquet_H_
