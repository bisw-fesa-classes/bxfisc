#include <BXFISC/Server/ServerDeviceClass.h>

#include <BXFISC/GeneratedCode/ServiceLocator.h>

#include <cmw-log/Logger.h>

#include <string>
#include <vector>

namespace 
{

CMW::Log::Logger& logger = CMW::Log::LoggerFactory::getLogger("FESA.USR.BXFISC.Server.ServerDeviceClass");

} // namespace

namespace BXFISC
{

ServerDeviceClass* ServerDeviceClass::instance_ = NULL;

ServerDeviceClass::ServerDeviceClass () :
	ServerDeviceClassGen()
{
}

ServerDeviceClass::~ServerDeviceClass()
{
}

ServerDeviceClass* ServerDeviceClass::getInstance()
{
	if (instance_ == NULL)
	{
		instance_ = new ServerDeviceClass();
	}
	return instance_;
}

void ServerDeviceClass::releaseInstance()
{
	if (instance_ != NULL) 
	{
		delete instance_;
		instance_ = NULL;
	}
}

void ServerDeviceClass::specificInit()
{
}

} // BXFISC
