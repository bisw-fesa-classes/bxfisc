// FESA 3 framework // Generated from SampleServerAction_Cpp.xsl

#include <BXFISC/Server/Reset.h>

#include <BXFISC/GeneratedCode/ServiceLocator.h>

#include <cmw-log/Logger.h>

namespace 
{

CMW::Log::Logger& logger = CMW::Log::LoggerFactory::getLogger("FESA.USR.BXFISC.Server.Reset");

} // namespace

#define LOG_DIAG_IF(topic,message)                           \
	{                                                    \
		DiagnosticUtils::DiagnosticMessage diagMsg;  \
		diagMsg.side = DiagnosticUtils::user;        \
		diagMsg.source = DiagnosticUtils::server;    \
		diagMsg.fesaClass = "BXFISC"; \
		diagMsg.name = "Reset"; \
		diagMsg.action = DiagnosticUtils::undefined; \
		diagMsg.msg = message;                       \
		BXFISCServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
	}

namespace BXFISC
{

Reset::Reset (fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses):
		ResetBase(actionConfig, serviceLocator, serviceLocatorRelatedClasses)
{
}

Reset::~Reset()
{
}

/*!
 * \brief This method is executed consecutively to a GET/SET action:
 * \param pEvt requestEvent from which the multiplexingContext can be extracted
 * \param pDev points to the Device or Global instance involved by this request 
 * \param data composite structure that agregates one or several types data-entries corresponding to the property definition.
 * This data object is the input (resp. output) which is passed when invoking a Get (resp. Set). 
 * \param filter optional input parameter meant to fine tune the treatment. It appears only if the property defines a filter
 */
void Reset::execute(fesa::RequestEvent* pEvt, Device* pDev, ResetPropertyData& data)
{
    MultiplexingContext* pContext = pEvt->getMultiplexingContext();
    if( data.itemAvailable("reset") )
    {
        char command[160];
        if ( (int)data.reset.get()==(int)YES_NO::YES )
        {
            {
                sprintf(command,"/usr/local/bin/wreboot -N BXFISC_M");
                printf("%s\n",command);
                system(command);
            }

        }
    }
}

} // BXFISC
