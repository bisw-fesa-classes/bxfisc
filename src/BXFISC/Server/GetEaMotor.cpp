// FESA 3 framework // Generated from SampleServerAction_Cpp.xsl

#include <BXFISC/Server/GetEaMotor.h>

#include <BXFISC/GeneratedCode/ServiceLocator.h>

#include <cmw-log/Logger.h>

#include <BXFISC/Common/Tools.h>

namespace 
{

CMW::Log::Logger& logger = CMW::Log::LoggerFactory::getLogger("FESA.USR.BXFISC.Server.GetEaMotor");

} // namespace

#define LOG_DIAG_IF(topic,message)                           \
	{                                                    \
		DiagnosticUtils::DiagnosticMessage diagMsg;  \
		diagMsg.side = DiagnosticUtils::user;        \
		diagMsg.source = DiagnosticUtils::server;    \
		diagMsg.fesaClass = "BXFISC"; \
		diagMsg.name = "GetEaMotor"; \
		diagMsg.action = DiagnosticUtils::undefined; \
		diagMsg.msg = message;                       \
		BXFISCServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
	}

namespace BXFISC
{

GetEaMotor::GetEaMotor (fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses):
		GetEaMotorBase(actionConfig, serviceLocator, serviceLocatorRelatedClasses)
{
}

GetEaMotor::~GetEaMotor()
{
}

/*!
 * \brief This method is executed consecutively to a GET/SET action:
 * \param pEvt requestEvent from which the multiplexingContext can be extracted
 * \param pDev points to the Device or Global instance involved by this request 
 * \param data composite structure that agregates one or several types data-entries corresponding to the property definition.
 * This data object is the input (resp. output) which is passed when invoking a Get (resp. Set). 
 * \param filter optional input parameter meant to fine tune the treatment. It appears only if the property defines a filter
 */
void GetEaMotor::execute(fesa::RequestEvent* pEvt, Device* pDev, EaMotorPropertyData& data)
{
    using namespace BXFISC_TOOL;

    const MultiplexingContext* pContext = pEvt->getMultiplexingContext();
    const GlobalDevice * pGlobalStore = BXFISCServiceLocator_->getGlobalDevice();
    struct timeval tAcq; struct timezone zAcq ;
    float dacFloats[CHANNELS_DAC_NUMBER];
    uint16_t dacShorts[CHANNELS_DAC_NUMBER];

    int64_t acqStamp;
    int64_t factor =(int64_t)(1.0 /pGlobalStore->measStamp_unitFactor.get());;
    gettimeofday(&tAcq,&zAcq);
    acqStamp=(int64_t)tAcq.tv_sec*1000000L+(int64_t)tAcq.tv_usec;
    acqStamp=(acqStamp*factor)/1000000000L;
    //pDev->acqStampMonitor.set(acqStamp,pContext);
    data.acqStamp.set(acqStamp);
    float position;
    Tools::DacsMpcGet(dacShorts,dacFloats,pDev,pContext);
    Tools::PositionGet(&position,pDev,pContext);
    data.position.set(position);
    data.mpcDacStart_max.set(       dacFloats[HARDWARE_DAC_MPC_CHANNELS07::START_MAX_0]);
    data.mpcDacStart_min.set(       dacFloats[HARDWARE_DAC_MPC_CHANNELS07::START_MIN_1]);
    data.mpcDacStop_max.set(        dacFloats[HARDWARE_DAC_MPC_CHANNELS07::STOP_MAX_2]);
    data.mpcDacStop_min.set(        dacFloats[HARDWARE_DAC_MPC_CHANNELS07::STOP_MIN_3]);
    data.mpcDacGarage_max.set(  dacFloats[HARDWARE_DAC_MPC_CHANNELS07::GARAGE_MAX_4]);
    data.mpcDacGarage_min.set(  dacFloats[HARDWARE_DAC_MPC_CHANNELS07::GARAGE_MIN_5]);
    data.mpcMotSpeed.set(   dacFloats[HARDWARE_DAC_MPC_CHANNELS07::MOTOR_SPEED_6]);
    data.mpcMotDir.set(dacFloats[HARDWARE_DAC_MPC_CHANNELS07::MOTOR_DIRECTION_7]);
    data.mpx_start_last.set(pDev->scanStart_last.get(pContext));
    data.mpx_start_current.set(pDev->scanStart_current.get(pContext));
    data.mpx_start_next.set(pDev->scanStart_next.get(pContext));
    data.mpx_stop_last.set(pDev->scanStop_last.get(pContext));
    data.mpx_stop_current.set(pDev->scanStop_current.get(pContext));
    data.mpx_stop_next.set(pDev->scanStop_next.get(pContext));
    data.mpx_garage_next.set(-999.99);
    data.mpx_garage_current.set(-999.99);
    data.mpx_garage_last.set(-999.99);
    data.deviceDetailedStatus.set((DEVICE_DETAILED_STATUS::DEVICE_DETAILED_STATUS)pDev->deviceDetailedStatus.get(pContext));

}

// Use this member function to tell the framework whether the subscribed clients need to be updated or not. See the FESA wiki for more info
bool GetEaMotor::hasDataChanged(const fesa::RequestEvent& event, fesa::AbstractDevice& abstractDevice, const cmw::data::Data& filter) const
{
	Device& device = static_cast<Device&>(abstractDevice);
	return true;
}

} // BXFISC
