// FESA 3 framework // Generated from SampleServerAction_Cpp.xsl

#include <BXFISC/Server/SetSetting.h>

#include <BXFISC/GeneratedCode/ServiceLocator.h>

#include <cmw-log/Logger.h>

#include <BXFISC/Common/Tools.h>

namespace
{

CMW::Log::Logger &logger = CMW::Log::LoggerFactory::getLogger("FESA.USR.BXFISC.Server.SetSetting");

} // namespace

#define LOG_DIAG_IF(topic,message)                           \
	{                                                    \
		DiagnosticUtils::DiagnosticMessage diagMsg;  \
		diagMsg.side = DiagnosticUtils::user;        \
		diagMsg.source = DiagnosticUtils::server;    \
		diagMsg.fesaClass = "BXFISC"; \
		diagMsg.name = "SetSetting"; \
		diagMsg.action = DiagnosticUtils::undefined; \
		diagMsg.msg = message;                       \
		BXFISCServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
	}

namespace BXFISC
{

SetSetting::SetSetting(fesa::ServerActionConfig &actionConfig, const fesa::AbstractServiceLocator *serviceLocator,
        const std::map<std::string, const fesa::AbstractServiceLocator*> &serviceLocatorRelatedClasses) :
                SetSettingBase(actionConfig, serviceLocator, serviceLocatorRelatedClasses)
{
}

SetSetting::~SetSetting()
{
}

/*!
 * \brief This method is executed consecutively to a GET/SET action:
 * \param pEvt requestEvent from which the multiplexingContext can be extracted
 * \param pDev points to the Device or Global instance involved by this request 
 * \param data composite structure that agregates one or several types data-entries corresponding to the property definition.
 * This data object is the input (resp. output) which is passed when invoking a Get (resp. Set). 
 * \param filter optional input parameter meant to fine tune the treatment. It appears only if the property defines a filter
 */
void SetSetting::execute(fesa::RequestEvent *pEvt, Device *pDev, SettingPropertyData &data)
{
    using namespace BXFISC_TOOL;
    using std::string;
    MultiplexingContext *pContext = pEvt->getMultiplexingContext();
    auto pGlobalStore = BXFISCServiceLocator_->getGlobalDevice();
    HARDWARE_ERRORS::HARDWARE_ERRORS hardwareError = HARDWARE_ERRORS::NONE;
    string actionStr = "Unknown";
    printf("SetSetting::execute\n");

    if (data.itemAvailable("mode") && hardwareError == HARDWARE_ERRORS::NONE) {

        int status = pDev->deviceDetailedStatus.get(pContext);
        if (((int) status & DEVICE_DETAILED_STATUS::LOCAL_NO) != (int) DEVICE_DETAILED_STATUS::LOCAL_NO) {
            hardwareError = HARDWARE_ERRORS::LOCAL_MODE;
        }
        else {

            switch ((DEVICE_MODE_SETTING::DEVICE_MODE_SETTING) data.mode.get()) {
                case DEVICE_MODE_SETTING::ON:
                    Tools::DeviceStateAction(pDev, pContext, (DEVICE_STATE_SETTING::ON));
                    break;
                case DEVICE_MODE_SETTING::OFF:
                    Tools::DeviceStateAction(pDev, pContext, (DEVICE_STATE_SETTING::OFF));
                    break;
                case DEVICE_MODE_SETTING::STANDBY:
                    Tools::DeviceStateAction(pDev, pContext, (DEVICE_STATE_SETTING::STANDBY));
                    break;
            }
        }

        if ((int) data.mode.get() != (int) DEVICE_MODE_SETTING::OFF) {
            hardwareError = Tools::SettingAllowedCesar(
                    (DEVICE_DETAILED_STATUS::DEVICE_DETAILED_STATUS) pDev->deviceDetailedStatus.get(pContext));
        }
    }
    else {
        hardwareError = Tools::SettingAllowedCesar(
                (DEVICE_DETAILED_STATUS::DEVICE_DETAILED_STATUS) pDev->deviceDetailedStatus.get(pContext));
    }

    if (data.itemAvailable("mode")) {

        if (data.itemAvailable("highVoltageRestore") && hardwareError == HARDWARE_ERRORS::NONE) {

            if ((int) data.highVoltageRestore.get() == (int) YES_NO::YES) {
                hardwareError = Tools::VoltageHighPm1To(pDev->highVoltagePm1_default.get(pContext), pDev, pContext);
                hardwareError = Tools::VoltageHighPm2To(pDev->highVoltagePm2_default.get(pContext), pDev, pContext);
            }
        }
    }

    if (data.itemAvailable("delayRestore") && hardwareError == HARDWARE_ERRORS::NONE) {

        if ((int) data.delayRestore.get() == (int) YES_NO::YES)
        {
            //To do
        }
    }

    if (data.itemAvailable("scanStartPosition") || data.itemAvailable("scanStopPosition") || data.itemAvailable("scanDelay")) {

        if (!data.itemAvailable("scanStartPosition"))
        {
            hardwareError = HARDWARE_ERRORS::MISSING_SCAN_START_FIELD;
        }
        else if (!data.itemAvailable("scanStopPosition"))
        {
            hardwareError = HARDWARE_ERRORS::MISSING_SCAN_STOP_FIELD;
        }
        else if (!data.itemAvailable("scanMotorDelay"))
        {
            hardwareError = HARDWARE_ERRORS::MISSING_SCAN_DELAY_FIELD;
        }
        else {
            uint32_t size = NUMBER_OF_GAINS;
            float gain = pDev->motorPosition_gain.get(size, pContext)[GAIN::SET];
            if ((data.scanMotorDelay.get() > 10000) || (data.scanMotorDelay.get() < 1)
                    || (data.scanStartPosition.get() > (40.00 / gain))
                    || (data.scanStartPosition.get() < (-40.00 / gain))
                    || (data.scanStopPosition.get() > (40.00 / gain))
                    || (data.scanStartPosition.get() < (-40.00 / gain))
                    || data.scanStartPosition.get() > pDev->motorPosition_max.get(pContext)
                    || data.scanStartPosition.get() < pDev->motorPosition_min.get(pContext)
                    || data.scanStopPosition.get() > pDev->motorPosition_max.get(pContext)
                    || data.scanStopPosition.get() < pDev->motorPosition_min.get(pContext))

//               (data.executeScan.get()  > pDev->scanExecute_max.get(pContext) )

            {
                hardwareError = HARDWARE_ERRORS::SCAN_VALUES_OUT_OF_RANGE;
            }
//            else if ((int) (pDev->event.get(pContext)) == (int) EVENTS::WWE
//                    || (int) (pDev->event.get(pContext)) == (int) EVENTS::WE)
//            {
//                hardwareError = HARDWARE_ERRORS::SCAN_PLEASE_WAIT_TILL_AFTER_EE;
//            }
            else if (pDev->scanExecute.get(pContext) > 0
                    && !(data.scanMotorDelay.get() == pDev->scanDelay_next.get(pContext)
                            && data.scanStartPosition.get() == pDev->scanStart_next.get(pContext)
                            && data.scanStopPosition.get() == pDev->scanStop_next.get(pContext))

                    )
            {
                hardwareError = HARDWARE_ERRORS::SCAN_ALREADY_GOING_ON;
            }
            else
            {
                // ok we have a scan so
                pDev->scanExecute.set(1, pContext);
                pDev->scanStart_next.set(data.scanStartPosition.get(), pContext);
                pDev->scanStop_next.set(data.scanStopPosition.get(), pContext);
                pDev->scanDelay_next.set(data.scanMotorDelay.get(), pContext);
                char msg[MSG_SIZE];
                sprintf(msg, " SetSetting  New scan command Start [%2.3f] Stop [%2.3f] Delay [%05d]",
                        pDev->scanStart_next.get(pContext), pDev->scanStop_next.get(pContext),
                        (int) pDev->scanDelay_next.get(pContext));
                pDev->msgCommand.set(" position changing ", pContext);

                Tools::PositionSet(pDev->scanStart_next.get(pContext), false,pDev, pContext);

            } // end setting up the scan
        } // which field is missing
    }
    else
    {
        if (data.itemAvailable("position") && hardwareError == HARDWARE_ERRORS::NONE)
        {
            if (data.position.get() > pDev->motorPosition_max.get(pContext)
                    || data.position.get() < pDev->motorPosition_min.get(pContext))
            {
                hardwareError = HARDWARE_ERRORS::POSITION_OUT_OF_RANGE;
            }
            else
            {
                pDev->msgCommand.set(" position changing ", pContext);
                Tools::PositionSet(data.position.get(), true, pDev, pContext);
            }
        }

    }

    if (hardwareError == HARDWARE_ERRORS::NONE)
    {
        const int16_t *ptr;
        uint32_t size = MAX_NB_OF_ACCESS_TYPES;
        ptr = pDev->moduleWdWrites.get(size, pContext);
        int16_t array[MAX_NB_OF_ACCESS_TYPES];
        for (unsigned int i = 0; i < MAX_NB_OF_ACCESS_TYPES; i++)
        {
            array[i] = ptr[i];
        }
        array[HARDWARE_ACCESS::SETTING]++;
        pDev->moduleWdWrites.set(array, size, pContext);
//            string notificationStr = "EaPiquet:";
//    notificationStr += pDev->name.get();
//    notificationStr +=",";
//    string classNameString(pGlobalStore->className.get());
//    AbstractEquipmentRT::notify(pContext, classNameString,
//    notificationStr);
    }
    else
    {
        char msg[MSG_SIZE];
        string msgStr = HARDWARE_ERRORS::HARDWARE_ERRORS_AsString.at(hardwareError);
        sprintf(msg, "BXFISC::Setting Error %d %s", hardwareError, msgStr.c_str());
        throw FesaException(__FUNCTION__, __LINE__, msg);
    }
}

} // BXFISC

