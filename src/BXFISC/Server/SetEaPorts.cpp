// FESA 3 framework // Generated from SampleServerAction_Cpp.xsl

#include <BXFISC/Server/SetEaPorts.h>

#include <BXFISC/GeneratedCode/ServiceLocator.h>

#include <cmw-log/Logger.h>

#include <BXFISC/Common/Tools.h>

namespace 
{

CMW::Log::Logger& logger = CMW::Log::LoggerFactory::getLogger("FESA.USR.BXFISC.Server.SetEaPorts");

} // namespace

#define LOG_DIAG_IF(topic,message)                           \
	{                                                    \
		DiagnosticUtils::DiagnosticMessage diagMsg;  \
		diagMsg.side = DiagnosticUtils::user;        \
		diagMsg.source = DiagnosticUtils::server;    \
		diagMsg.fesaClass = "BXFISC"; \
		diagMsg.name = "SetEaPorts"; \
		diagMsg.action = DiagnosticUtils::undefined; \
		diagMsg.msg = message;                       \
		BXFISCServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
	}

namespace BXFISC
{

SetEaPorts::SetEaPorts (fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses):
		SetEaPortsBase(actionConfig, serviceLocator, serviceLocatorRelatedClasses)
{
}

SetEaPorts::~SetEaPorts()
{
}

/*!
 * \brief This method is executed consecutively to a GET/SET action:
 * \param pEvt requestEvent from which the multiplexingContext can be extracted
 * \param pDev points to the Device or Global instance involved by this request 
 * \param data composite structure that agregates one or several types data-entries corresponding to the property definition.
 * This data object is the input (resp. output) which is passed when invoking a Get (resp. Set). 
 * \param filter optional input parameter meant to fine tune the treatment. It appears only if the property defines a filter
 */
void SetEaPorts::execute(fesa::RequestEvent* pEvt, Device* pDev, EaPortsRegsPropertyData& data)
{
    using std::string;
    using namespace BXFISC_TOOL;
    const MultiplexingContext* pContext = pEvt->getMultiplexingContext();
    HARDWARE_ERRORS::HARDWARE_ERRORS hardwareError=HARDWARE_ERRORS::NONE;
    if (data.itemAvailable("deviceName")  );
    {
        int hasAGetBeenDone =strcmp(pDev->name.get(),data.deviceName.get());
        if ( hasAGetBeenDone != 0 )
        {
//            char msg[MSG_SIZE];
            string msgStr = HARDWARE_ERRORS::HARDWARE_ERRORS_AsString.at(HARDWARE_ERRORS::GET_NOT_DONE_FIRST);
//            hardwareError=HARDWARE_ERRORS::GET_NOT_DONE_FIRST;
//            HARDWARE_ERRORS::HARDWARE_ERRORSConverter::convertToString(hardwareError,msgStr);
//            sprintf(msg,"BXFISC::SetConfig Error %d %s",hardwareError,msgStr.c_str());
            throw FesaException(__FUNCTION__, __LINE__, msgStr);
        }
    }
    if ( data.itemAvailable("initModule") )
    {
        if (  data.initModule.get()==YES_NO::YES)
        {
            Tools::DeviceStateAction(pDev ,pContext,DEVICE_STATE_SETTING::INIT_MODULE);
        }
    }
    if ( data.itemAvailable("reg_action") )
    {
        if ( data.itemAvailable("reg_value") )
        {
            if ( data.reg_action.get()!=REG_ACTION::NONE  )
            {
                if ( data.reg_action.get()!=REG_ACTION::BASE  ) {   Tools::RegBaseSet( (REGBASE::REGBASE)  data.reg_value.get(),pDev,pContext, false);Tools::ManualChangeOfPortsOrRegs(pDev,pContext); }
                if ( data.reg_action.get()!=REG_ACTION::COIN  ) {   Tools::RegCoinSet( (REGCOIN::REGCOIN)  data.reg_value.get(),pDev,pContext);Tools::ManualChangeOfPortsOrRegs(pDev,pContext); }
                if ( data.reg_action.get()!=REG_ACTION::MOTOR ) {   Tools::RegMotorSet((REGMOTOR::REGMOTOR)data.reg_value.get(),pDev,pContext);Tools::ManualChangeOfPortsOrRegs(pDev,pContext); }
            }
        }
    }
    if ( data.itemAvailable("port_action") )
    {
        if ( data.itemAvailable("port_value") && data.port_action.get()!=PORT_ACTION::NONE)
        {
            if ( data.port_action.get()!=PORT_ACTION::PORTA  )  {   Tools::PortsSet( static_cast<REGBLANK::REGBLANK>(data.port_value.get()),data.port_action.get(),pDev,pContext);   }
            if ( data.port_action.get()!=PORT_ACTION::PORTB  )  {   Tools::PortsSet( static_cast<REGBLANK::REGBLANK>(data.port_value.get()),data.port_action.get(),pDev,pContext);   }
            if ( data.port_action.get()!=PORT_ACTION::PORTC  )  {   Tools::PortsSet( static_cast<REGBLANK::REGBLANK>(data.port_value.get()),data.port_action.get(),pDev,pContext);   }
        }
    }
}

} // BXFISC
