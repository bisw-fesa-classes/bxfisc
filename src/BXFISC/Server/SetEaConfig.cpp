// FESA 3 framework // Generated from SampleServerAction_Cpp.xsl

#include <BXFISC/Server/SetEaConfig.h>

#include <BXFISC/GeneratedCode/ServiceLocator.h>

#include <cmw-log/Logger.h>

namespace 
{

CMW::Log::Logger& logger = CMW::Log::LoggerFactory::getLogger("FESA.USR.BXFISC.Server.SetEaConfig");

} // namespace

#define LOG_DIAG_IF(topic,message)                           \
	{                                                    \
		DiagnosticUtils::DiagnosticMessage diagMsg;  \
		diagMsg.side = DiagnosticUtils::user;        \
		diagMsg.source = DiagnosticUtils::server;    \
		diagMsg.fesaClass = "BXFISC"; \
		diagMsg.name = "SetEaConfig"; \
		diagMsg.action = DiagnosticUtils::undefined; \
		diagMsg.msg = message;                       \
		BXFISCServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
	}

namespace BXFISC
{

SetEaConfig::SetEaConfig (fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses):
		SetEaConfigBase(actionConfig, serviceLocator, serviceLocatorRelatedClasses)
{
}

SetEaConfig::~SetEaConfig()
{
}

/*!
 * \brief This method is executed consecutively to a GET/SET action:
 * \param pEvt requestEvent from which the multiplexingContext can be extracted
 * \param pDev points to the Device or Global instance involved by this request 
 * \param data composite structure that agregates one or several types data-entries corresponding to the property definition.
 * This data object is the input (resp. output) which is passed when invoking a Get (resp. Set). 
 * \param filter optional input parameter meant to fine tune the treatment. It appears only if the property defines a filter
 */
void SetEaConfig::execute(fesa::RequestEvent* pEvt, Device* pDev, EaConfigPropertyData& data)
{
    using std::string;

    MultiplexingContext* pContext = pEvt->getMultiplexingContext();
    HARDWARE_ERRORS::HARDWARE_ERRORS hardwareError=HARDWARE_ERRORS::NONE;
    printf("executing server action: BXFISCSetEaConfig\n");
    if (data.itemAvailable("deviceName")  );
    {
        int hasAGetBeenDone =strcmp(pDev->name.get(),data.deviceName.get());
        if ( hasAGetBeenDone != 0 )
        {
            string msgStr = HARDWARE_ERRORS::HARDWARE_ERRORS_AsString.at(HARDWARE_ERRORS::GET_NOT_DONE_FIRST);
            throw FesaException(__FUNCTION__, __LINE__, msgStr);
        }
    }
    if ( data.itemAvailable("equipmentName"))
    {
        pDev->equipmentName.set(data.equipmentName.get(),pContext);
    }
    if ( data.itemAvailable("coincidenceDeviceName"))
    {
        pDev->coincidenceDeviceName.set(data.coincidenceDeviceName.get(),pContext);
    }
    if ( data.itemAvailable("deviceDetailedStatus"))
    {
        pDev->deviceDetailedStatus.set(data.deviceDetailedStatus.get(),pContext);

    }
    if ( data.itemAvailable("deviceDetailedStatusConfig"))
    {

        pDev->deviceDetailedStatusConfig.set(data.deviceDetailedStatusConfig.get(),pContext);
    }
    if ( data.itemAvailable("deviceHwStatus"))
    {
        pDev->deviceHwStatus.set(data.deviceHwStatus.get(),pContext);
    }
    if ( data.itemAvailable("deviceHwStatusConfig"))
    {
        pDev->deviceHwStatusConfig.set(data.deviceHwStatusConfig.get(),pContext);
    }
    if ( data.itemAvailable("delayPm1_default"))
    {
        pDev->delayPm1_default.set(data.delayPm1_default.get(),pContext);
    }
    if ( data.itemAvailable("delayPm2_default"))
    {
        pDev->delayPm2_default.set(data.delayPm2_default.get(),pContext);
    }
    if ( data.itemAvailable("delayWidthPm1_default"))
    {
        pDev->delayWidthPm1_default.set(data.delayWidthPm1_default.get(),pContext);
    }
    if ( data.itemAvailable("delayWidthPm2_default"))
    {
        pDev->delayWidthPm2_default.set(data.delayWidthPm2_default.get(),pContext);
    }
    if ( data.itemAvailable("driverNumber"))
    {
        pDev->driverNumber.set(data.driverNumber.get(),pContext);
    }

    if ( data.itemAvailable("equipmentName"))
    {
        pDev->equipmentName.set(data.equipmentName.get(),pContext);
    }
    if ( data.itemAvailable("eaMsg"))
    {
        pDev->eaMsg.set(data.eaMsg.get(),pContext);
    }
//      if ( data.itemAvailable("emcGain_default"))
//  {
//      pDev->emcGain_default.set(data.emcGain_default.get());
//  }
//  if ( data.itemAvailable("emcPhaReg_default"))
//  {
//      pDev->emcPhaReg_default.set(data.emcPhaReg_default.get());
//  }

    if ( data.itemAvailable("highVoltagePm1_default"))
    {
        pDev->highVoltagePm1_default.set(data.highVoltagePm1_default.get(),pContext);
    }
    if ( data.itemAvailable("highVoltagePm2_default"))
    {
        pDev->highVoltagePm2_default.set(data.highVoltagePm2_default.get(),pContext);
    }
    if ( data.itemAvailable("highVoltagePm1_min"))
    {
        pDev->highVoltagePm1_min.set(data.highVoltagePm1_min.get(),pContext);
    }
    if ( data.itemAvailable("highVoltagePm2_min"))
    {
        pDev->highVoltagePm2_min.set(data.highVoltagePm2_min.get(),pContext);
    }
    if ( data.itemAvailable("highVoltagePm1_offsetNumMax"))
    {
        pDev->highVoltagePm1_offsetNumMax.set(data.highVoltagePm1_offsetNumMax.get(),pContext);
    }
    if ( data.itemAvailable("highVoltagePm2_offsetNumMax"))
    {
        pDev->highVoltagePm2_offsetNumMax.set(data.highVoltagePm2_offsetNumMax.get(),pContext);
    }
    if ( data.itemAvailable("moduleThresholdPm1_default"))
    {
        pDev->moduleThresholdPm1_default.set(data.moduleThresholdPm1_default.get(),pContext);
    }
    if ( data.itemAvailable("moduleThresholdPm2_default"))
    {
        pDev->moduleThresholdPm2_default.set(data.moduleThresholdPm2_default.get(),pContext);
    }
    if ( data.itemAvailable("moduleLedPiedestal_off"))
    {
        pDev->moduleLedPiedestal_off.set(data.moduleLedPiedestal_off.get(),pContext);
    }
    if ( data.itemAvailable("moduleLedPiedestal_internal"))
    {
        pDev->moduleLedPiedestal_internal.set(data.moduleLedPiedestal_internal.get(),pContext);
    }
    if ( data.itemAvailable("moduleLedPiedestal_external"))
    {
        pDev->moduleLedPiedestal_external.set(data.moduleLedPiedestal_external.get(),pContext);
    }
    if ( data.itemAvailable("moduleLedPulse_off"))
    {
        pDev->moduleLedPulse_off.set(data.moduleLedPulse_off.get(),pContext);
    }
    if ( data.itemAvailable("moduleLedPulse_internal"))
    {
        pDev->moduleLedPulse_internal.set(data.moduleLedPulse_internal.get(),pContext);
    }
    if ( data.itemAvailable("moduleLedPulse_external"))
    {
        pDev->moduleLedPulse_external.set(data.moduleLedPulse_external.get(),pContext);
    }
    if ( data.itemAvailable("moduleWdEETimeout"))
    {
        pDev->moduleWdEETimeout.set(data.moduleWdEETimeout.get(),pContext);
    }

    if ( data.itemAvailable("motorPosition_default"))
    {
        pDev->motorPosition_default.set(data.motorPosition_default.get(),pContext);
    }
    if ( data.itemAvailable("motorPosition_delay_default"))
    {
        pDev->motorPosition_delay_default.set(data.motorPosition_delay_default.get(),pContext);
    }
    if ( data.itemAvailable("motorPosition_max"))
    {
        pDev->motorPosition_max.set(data.motorPosition_max.get(),pContext);
    }
    if ( data.itemAvailable("motorPosition_min"))
    {
        pDev->motorPosition_min.set(data.motorPosition_min.get(),pContext);


    }
    if ( data.itemAvailable("motorPosition_gain"))
    {
        uint32_t size=NUMBER_OF_GAINS;
        pDev->motorPosition_gain.set(data.motorPosition_gain.get(size),size, pContext);
    }
    if ( data.itemAvailable("scanExecute_max"))
    {
        pDev->scanExecute_max.set(data.scanExecute_max.get(),pContext);
    }


    if ( data.itemAvailable("moduleHwRegBase_default"))
    {
        pDev->moduleHwRegBase.set(data.moduleHwRegBase_default.get(), pContext);
    }
    if ( data.itemAvailable("moduleHwRegCoin_default"))
    {
        pDev->moduleHwRegCoin_default.set(data.moduleHwRegCoin_default.get(), pContext);
    }
    if ( data.itemAvailable("moduleHwRegMaster_default"))
    {
        pDev->moduleHwRegMaster_default.set(data.moduleHwRegMaster_default.get(), pContext);
    }
    if ( data.itemAvailable("moduleHwRegMotor_default"))
    {
        pDev->moduleHwRegMotor_default.set(data.moduleHwRegMotor_default.get(), pContext);
    }
    if ( data.itemAvailable("moduleHwPortA_default"))
    {
        pDev->moduleHwPortA_default.set(data.moduleHwPortA_default.get(), pContext);
    }
    if ( data.itemAvailable("moduleHwPortB_default"))
    {
        pDev->moduleHwPortB_default.set(data.moduleHwPortB_default.get(), pContext);
    }
    if ( data.itemAvailable("moduleHwPortC_default"))
    {
        pDev->moduleHwPortC_default.set(data.moduleHwPortC_default.get(), pContext);
    }

    const int16_t * ptr;
    uint32_t size = MAX_NB_OF_ACCESS_TYPES  ;
    ptr=pDev->moduleWdWrites.get(size, pContext);
    short array[MAX_NB_OF_ACCESS_TYPES]; for ( unsigned int i=0 ; i<  MAX_NB_OF_ACCESS_TYPES ; i++ ) { array[i]=ptr[i]  ; }
    array[HARDWARE_ACCESS::CONFIG]++;
    pDev->moduleWdWrites.set(array,size, pContext);
}

} // BXFISC
