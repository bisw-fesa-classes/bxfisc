// FESA 3 framework // Generated from SampleServerAction_Hpp.xsl 
// Use this code as a starting point to develop your own equipment class 

#ifndef BXFISC_SetEaDelay_H_
#define BXFISC_SetEaDelay_H_

#include <BXFISC/GeneratedCode/GetSetDefaultServerAction.h>
#include <BXFISC/GeneratedCode/PropertyData.h>
#include <BXFISC/GeneratedCode/TypeDefinition.h>
#include <BXFISC/GeneratedCode/Device.h>

namespace BXFISC
{

class SetEaDelay : public SetEaDelayBase
{
public:
	SetEaDelay(fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses);
	virtual ~SetEaDelay();
	void execute(fesa::RequestEvent* pEvt, Device* pDev, EaDelayPropertyData& data);
};

} // BXFISC

#endif // BXFISC_SetEaDelay_H_
