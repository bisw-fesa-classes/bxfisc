// FESA 3 framework // Generated from SampleServerAction_Cpp.xsl

#include <BXFISC/Server/GetEaConfig.h>

#include <BXFISC/GeneratedCode/ServiceLocator.h>

#include <cmw-log/Logger.h>

namespace 
{

CMW::Log::Logger& logger = CMW::Log::LoggerFactory::getLogger("FESA.USR.BXFISC.Server.GetEaConfig");

} // namespace

#define LOG_DIAG_IF(topic,message)                           \
	{                                                    \
		DiagnosticUtils::DiagnosticMessage diagMsg;  \
		diagMsg.side = DiagnosticUtils::user;        \
		diagMsg.source = DiagnosticUtils::server;    \
		diagMsg.fesaClass = "BXFISC"; \
		diagMsg.name = "GetEaConfig"; \
		diagMsg.action = DiagnosticUtils::undefined; \
		diagMsg.msg = message;                       \
		BXFISCServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
	}

namespace BXFISC
{

GetEaConfig::GetEaConfig (fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses):
		GetEaConfigBase(actionConfig, serviceLocator, serviceLocatorRelatedClasses)
{
}

GetEaConfig::~GetEaConfig()
{
}

/*!
 * \brief This method is executed consecutively to a GET/SET action:
 * \param pEvt requestEvent from which the multiplexingContext can be extracted
 * \param pDev points to the Device or Global instance involved by this request 
 * \param data composite structure that agregates one or several types data-entries corresponding to the property definition.
 * This data object is the input (resp. output) which is passed when invoking a Get (resp. Set). 
 * \param filter optional input parameter meant to fine tune the treatment. It appears only if the property defines a filter
 */
void GetEaConfig::execute(fesa::RequestEvent* pEvt, Device* pDev, EaConfigPropertyData& data)
{
    const MultiplexingContext* pContext = pEvt->getMultiplexingContext();
    const GlobalDevice * pGlobalStore = BXFISCServiceLocator_->getGlobalDevice();
//    data.acqStamp.set(pGlobalStore->acqStampMonitor.get(pContext));
//  data.counterSampling.set(pDev->counterSampling.get(pContext));
    data.coincidenceDeviceName.set(pDev->coincidenceDeviceName.get(pContext));
    //data.coincidenceIsThere.set(pDev->coincidenceIsThere.get(pContext));
    data.deviceDetailedStatus.set((DEVICE_DETAILED_STATUS::DEVICE_DETAILED_STATUS)pDev->deviceDetailedStatus.get(pContext));
    data.deviceDetailedStatusConfig.set((DEVICE_DETAILED_STATUS::DEVICE_DETAILED_STATUS)pDev->deviceDetailedStatusConfig.get(pContext));
    data.deviceHwStatus.set((DEVICE_HW_STATUS::DEVICE_HW_STATUS)pDev->deviceHwStatus.get(pContext));
    data.deviceHwStatusConfig.set((DEVICE_HW_STATUS::DEVICE_HW_STATUS)pDev->deviceHwStatus.get(pContext));
    data.delayPm1_default.set(pDev->delayPm1_default.get(pContext));
    data.delayPm2_default.set(pDev->delayPm2_default.get(pContext));
    data.delayWidthPm1_default.set(pDev->delayWidthPm1_default.get(pContext));
    data.delayWidthPm2_default.set(pDev->delayWidthPm2_default.get(pContext));
    data.driverNumber.set(pDev->driverNumber.get(pContext));
    data.eaMsg.set(pDev->eaMsg.get(pContext));

//  data.emcGain_default.set(pDev->emcGain_default.get());
//  data.emcPhaReg_default.set((EMCREG::EMCREG)pDev->emcPhaReg_default.get());
    data.equipmentName.set(pDev->equipmentName.get(pContext));
    data.highVoltagePm1_default.set(pDev->highVoltagePm1_default.get(pContext));
    data.highVoltagePm2_default.set(pDev->highVoltagePm2_default.get(pContext));
    data.highVoltagePm1_min.set(pDev->highVoltagePm1_min.get(pContext));
    data.highVoltagePm2_min.set(pDev->highVoltagePm2_min.get(pContext));
    data.highVoltagePm1_offsetNumMax.set(pDev->highVoltagePm1_offsetNumMax.get(pContext));
    data.highVoltagePm2_offsetNumMax.set(pDev->highVoltagePm2_offsetNumMax.get(pContext));
    data.moduleThresholdPm1_default.set(pDev->moduleThresholdPm1_default.get(pContext));
    data.moduleThresholdPm2_default.set(pDev->moduleThresholdPm2_default.get(pContext));
    data.moduleLedPiedestal_off.set(pDev->moduleLedPiedestal_off.get(pContext));
    data.moduleLedPiedestal_internal.set(pDev->moduleLedPiedestal_internal.get(pContext));
    data.moduleLedPiedestal_external.set(pDev->moduleLedPiedestal_external.get(pContext));
    data.moduleLedPulse_off.set(pDev->moduleLedPulse_off.get(pContext));
    data.moduleLedPulse_internal.set(pDev->moduleLedPulse_internal.get(pContext));
    data.moduleLedPulse_external.set(pDev->moduleLedPulse_external.get(pContext));
    data.moduleHwRegBase_default.set((REGBASE::REGBASE)pDev->moduleHwRegBase_default.get(pContext));
    data.moduleHwRegCoin_default.set((REGCOIN::REGCOIN)pDev->moduleHwRegCoin_default.get(pContext));
    data.moduleHwRegMaster_default.set((REGMASTER::REGMASTER)pDev->moduleHwRegMaster_default.get(pContext));
    data.moduleHwRegMotor_default.set((REGMOTOR::REGMOTOR)pDev->moduleHwRegMotor_default.get(pContext));
    data.moduleHwPortA_default.set((PORTA::PORTA)pDev->moduleHwPortA_default.get(pContext));
    data.moduleHwPortB_default.set((PORTB::PORTB)pDev->moduleHwPortB_default.get(pContext));
    data.moduleHwPortC_default.set((PORTC::PORTC)pDev->moduleHwPortC_default.get(pContext));
    data.moduleWdEETimeout.set(pDev->moduleWdEETimeout.get(pContext));
    data.motorPosition_default.set(pDev->motorPosition_default.get(pContext));
    data.motorPosition_delay_default.set(pDev->motorPosition_delay_default.get(pContext));
    data.motorPosition_max.set(pDev->motorPosition_max.get(pContext));
    data.motorPosition_min.set(pDev->motorPosition_min.get(pContext));
    uint32_t size=NUMBER_OF_GAINS;
    data.motorPosition_gain.set(pDev->motorPosition_gain.get(size,pContext),size);
    data.scanExecute_max.set(pDev->scanExecute_max.get(pContext));

    data.deviceName.set(pDev->name.get());
    const int16_t* ptr;
    size = MAX_NB_OF_ACCESS_TYPES;
    ptr=pDev->moduleWdReads.get(size, pContext);
    int16_t array[MAX_NB_OF_ACCESS_TYPES]; for ( unsigned int i=0 ; i<  MAX_NB_OF_ACCESS_TYPES ; i++ ) { array[i]=ptr[i]  ; }
    array[HARDWARE_ACCESS::CONFIG]++;
    pDev->moduleWdReads.set(array,size, pContext);
}

// Use this member function to tell the framework whether the subscribed clients need to be updated or not. See the FESA wiki for more info
bool GetEaConfig::hasDataChanged(const fesa::RequestEvent& event, fesa::AbstractDevice& abstractDevice, const cmw::data::Data& filter) const
{
	Device& device = static_cast<Device&>(abstractDevice);
	return true;
}

} // BXFISC
