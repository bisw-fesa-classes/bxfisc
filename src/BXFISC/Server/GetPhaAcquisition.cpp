// FESA 3 framework // Generated from SampleServerAction_Cpp.xsl

#include <BXFISC/Server/GetPhaAcquisition.h>

#include <BXFISC/GeneratedCode/ServiceLocator.h>

#include <cmw-log/Logger.h>

namespace
{

CMW::Log::Logger &logger = CMW::Log::LoggerFactory::getLogger("FESA.USR.BXFISC.Server.GetPhaAcquisition");

} // namespace

#define LOG_DIAG_IF(topic,message)                           \
	{                                                    \
		DiagnosticUtils::DiagnosticMessage diagMsg;  \
		diagMsg.side = DiagnosticUtils::user;        \
		diagMsg.source = DiagnosticUtils::server;    \
		diagMsg.fesaClass = "BXFISC"; \
		diagMsg.name = "GetPhaAcquisition"; \
		diagMsg.action = DiagnosticUtils::undefined; \
		diagMsg.msg = message;                       \
		BXFISCServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
	}

namespace BXFISC
{

GetPhaAcquisition::GetPhaAcquisition(fesa::ServerActionConfig &actionConfig,
        const fesa::AbstractServiceLocator *serviceLocator,
        const std::map<std::string, const fesa::AbstractServiceLocator*> &serviceLocatorRelatedClasses) :
                GetPhaAcquisitionBase(actionConfig, serviceLocator, serviceLocatorRelatedClasses)
{
}

GetPhaAcquisition::~GetPhaAcquisition()
{
}

void GetPhaAcquisition::execute(fesa::RequestEvent *pEvt, Device *pDev, PhaAcquisitionPropertyData &data)
{
    const auto pContext = pEvt->getMultiplexingContext();
    const auto pGlobalStore = BXFISCServiceLocator_->getGlobalDevice();
    data.acqStamp.set(pDev->acqStamp.get(pContext));

    data.acqTime.set(pGlobalStore->acqTime.get(pContext));
    data.phaIs.set((YES_NO::YES_NO) pDev->phaIs.get(pContext));
    data.highVoltage.set(pDev->highVoltagePm1_setting.get(pContext) * 1000);
    data.position.set(pDev->position.get(pContext));
    int pha[MAX_PHA_SIZE];
    uint32_t size = MAX_PHA_SIZE;
    const int *phaPtr;
    phaPtr = pDev->phaData.get(size, pContext);
    for (unsigned int i = 1; i < MAX_PHA_SIZE - 1; i++)
    {
        pha[i] = *(phaPtr + i);
    }
    pha[0] = 0;
    pha[1023] = 0;
    data.phaData.set(pha, size);
    data.phaGain.set((EMCGAIN::EMCGAIN) pDev->phaGain.get(pContext));
}

// Use this member function to tell the framework whether the subscribed clients need to be updated or not. See the FESA wiki for more info
bool GetPhaAcquisition::hasDataChanged(const fesa::RequestEvent &event, fesa::AbstractDevice &abstractDevice,
        const cmw::data::Data &filter) const
{
    Device &device = static_cast<Device&>(abstractDevice);
    return true;
}

} // BXFISC
