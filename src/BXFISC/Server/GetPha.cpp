// FESA 3 framework // Generated from SampleServerAction_Cpp.xsl

#include <BXFISC/Server/GetPha.h>

#include <BXFISC/GeneratedCode/ServiceLocator.h>

#include <cmw-log/Logger.h>

namespace 
{

CMW::Log::Logger& logger = CMW::Log::LoggerFactory::getLogger("FESA.USR.BXFISC.Server.GetPha");

} // namespace

#define LOG_DIAG_IF(topic,message)                           \
	{                                                    \
		DiagnosticUtils::DiagnosticMessage diagMsg;  \
		diagMsg.side = DiagnosticUtils::user;        \
		diagMsg.source = DiagnosticUtils::server;    \
		diagMsg.fesaClass = "BXFISC"; \
		diagMsg.name = "GetPha"; \
		diagMsg.action = DiagnosticUtils::undefined; \
		diagMsg.msg = message;                       \
		BXFISCServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
	}

namespace BXFISC
{

GetPha::GetPha (fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses):
		GetPhaBase(actionConfig, serviceLocator, serviceLocatorRelatedClasses)
{
}

GetPha::~GetPha()
{
}

/*!
 * \brief This method is executed consecutively to a GET/SET action:
 * \param pEvt requestEvent from which the multiplexingContext can be extracted
 * \param pDev points to the Device or Global instance involved by this request 
 * \param data composite structure that agregates one or several types data-entries corresponding to the property definition.
 * This data object is the input (resp. output) which is passed when invoking a Get (resp. Set). 
 * \param filter optional input parameter meant to fine tune the treatment. It appears only if the property defines a filter
 */
void GetPha::execute(fesa::RequestEvent* pEvt, Device* pDev, PhaPropertyData& data)
{
    const MultiplexingContext * pContext = pEvt->getMultiplexingContext();
    uint32_t size=NUMBER_OF_EMC_DACS;
    int pha[MAX_PHA_SIZE];
    data.phaDataOverflow.set(0);
    data.phaDataUnderflow.set(0);
    data.phaDacAction.set(EMCDACS::NONE);
    data.phaDacNewValue.set(0.0);
    const double* phaDacPtr;
    phaDacPtr=pDev->phaDacs.get(size, pContext);
    data.phaDacOffset1.set(phaDacPtr[0]);
    data.phaDacOffset2.set(phaDacPtr[1]);
    data.phaDacThresMin.set(phaDacPtr[2]);
    data.phaDacThresMax.set(phaDacPtr[3]);

    size=MAX_PHA_SIZE;
    const int* phaPtr;
    phaPtr=pDev->phaData.get(size, pContext);
    for ( unsigned int i=0; i<MAX_PHA_SIZE-1 ; i++ )
    {
        pha[i]=*(phaPtr+i);
    }
    data.phaDataUnderflow.set(pha[0]);
    data.phaDataOverflow.set(pha[1023]);
    pha[0]=0;
    pha[1023]=0;
    data.phaData.set(pha,size);
    data.phaReg.set((EMCREG::EMCREG)pDev->phaReg.get(pContext));
    data.phaIs.set((YES_NO::YES_NO)pDev->phaIs.get(pContext));
    data.phaGain.set((EMCGAIN::EMCGAIN)pDev->phaGain.get(pContext));
}

// Use this member function to tell the framework whether the subscribed clients need to be updated or not. See the FESA wiki for more info
bool GetPha::hasDataChanged(const fesa::RequestEvent& event, fesa::AbstractDevice& abstractDevice, const cmw::data::Data& filter) const
{
	Device& device = static_cast<Device&>(abstractDevice);
	return true;
}

} // BXFISC
